package ggx_commons

import (
	"bitbucket.org/digi-sense/gg-core"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

const (
	Version = "0.3.41"

	ModeProduction = gg.ModeProduction
	ModeDebug      = gg.ModeDebug
)

func GormConfig(mode string) (config *gorm.Config) {
	if mode == ModeDebug {
		config = &gorm.Config{
			Logger: logger.Default.LogMode(logger.Info),
		}
	} else {
		config = &gorm.Config{
			Logger: logger.Default.LogMode(logger.Silent),
		}
	}
	return
}
