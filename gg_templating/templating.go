package gg_templating

import (
	"bitbucket.org/digi-sense/gg-core/gg_templating"
	"github.com/cbroglie/mustache"
)

var Templating *TemplatingHelper

func init() {
	Templating = new(TemplatingHelper)
}

//----------------------------------------------------------------------------------------------------------------------
//	TemplatingOptions
//----------------------------------------------------------------------------------------------------------------------

type TemplatingOptions struct {
}

//----------------------------------------------------------------------------------------------------------------------
//	TemplatingHelper
//----------------------------------------------------------------------------------------------------------------------

type TemplatingHelper struct {
	gg_templating.TemplatingHelper // extends gg.Templating
}

func NewTemplatingHelper() (instance *TemplatingHelper) {
	instance = new(TemplatingHelper)

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *TemplatingHelper) RenderMustache(data string, context ...interface{}) (response string, err error) {
	if nil != instance {
		response, err = mustache.Render(data, context...)
	}
	return
}

func (instance *TemplatingHelper) RenderMustacheParam(text string, param interface{}) (string, error) {
	context := map[string]interface{}{"param": param}
	response, err := mustache.Render(text, context)
	if nil != err {
		response = text
	}
	return response, err
}
