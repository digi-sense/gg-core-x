package gg_mq

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_mq/mq_commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_mq/mq_drivers/driver_amqp"
	"fmt"
)

type MQHelper struct {
}

var MQ *MQHelper

func init() {
	MQ = new(MQHelper)
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

// Build return a client implementation if supported
// config: ex {"protocol":"amqp","secret":"1234","url":"amqp://test:test@localhost:5672/"}
func (instance *MQHelper) Build(config interface{}) (mq_commons.IDriver, error) {
	if s, b := config.(string); b {
		return buildFromString(s)
	} else {
		return buildFromString(gg.JSON.Stringify(config))
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func buildFromString(config string) (mq_commons.IDriver, error) {
	var dc *mq_commons.DriverConfig
	err := gg.JSON.Read(config, &dc)
	if nil != err {
		return nil, err
	}
	switch dc.Protocol {
	case mq_commons.ProtocolAmqp:
		// amqp client
		return driver_amqp.NewDriverAmqpFromString(config)
	default:
		return nil, gg.Errors.Prefix(mq_commons.ErrorUnsupportedProtocol, fmt.Sprintf("'%v': ", dc.Protocol))
	}
}
