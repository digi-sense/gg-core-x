package driver_mqtt_aws

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	MQTT "github.com/eclipse/paho.mqtt.golang"
	"os"
	"time"
)

// ---------------------------------------------------------------------------------------------------------------------
//	Driver MQTT for AWS
//
// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
//	DriverMqttAws
// ---------------------------------------------------------------------------------------------------------------------

// AWSIoTConnection holds connection for AWS IoT
type AWSIoTConnection struct {
	options *MQTT.ClientOptions

	_client MQTT.Client
}

// NewAWSIoTConnection Creates new MQTT connection with AWS IoT
// It requires config parameter for initialisation
func NewAWSIoTConnection(config *Config) (instance *AWSIoTConnection, err error) {
	instance = new(AWSIoTConnection)
	err = instance.init(config)
	if nil != err {
		return
	}

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AWSIoTConnection) IsConnected() (response bool) {
	if nil != instance && nil != instance._client {
		response = instance._client.IsConnected()
	}
	return
}

func (instance *AWSIoTConnection) Open() (err error) {
	if nil != instance {
		_, err = instance.client(true)
		if nil != err {
			return
		}
	}
	return
}

// Close function disconnects the MQTT connection only if its already connected else returns error
func (instance *AWSIoTConnection) Close() bool {
	if nil != instance && instance.IsConnected() {
		instance._client.Disconnect(0)
		return true
	}
	return false
}

// Subscribe function subscribes on topic with level of qos (Quality of service)
// currently supported 0 & 1 (2 coming in future).
func (instance *AWSIoTConnection) Subscribe(topic string, qos byte) error {
	return instance.SubscribeWithHandler(topic, qos, nil)
}

// SubscribeWithHandler function subscribes on topic with level of qos (Quality of service)
// currently supported 0 & 1 (2 coming in future)
// & handler function to listen to incoming messages for the topic & qos level.
// It is called every time when message is received
func (instance *AWSIoTConnection) SubscribeWithHandler(topic string, qos byte, handler MQTT.MessageHandler) (err error) {
	var client MQTT.Client
	client, err = instance.client(true)
	if nil != err {
		return
	}
	if !client.IsConnected() {
		return errors.New("client not connected")
	} else {
		token := client.Subscribe(topic, qos, handler)
		token.Wait()
		err = token.Error()
	}
	return
}

// Unsubscribe function removes subscription for specified topic
func (instance *AWSIoTConnection) Unsubscribe(topic string) (err error) {
	var client MQTT.Client
	client, err = instance.client(true)
	if nil != err {
		return
	}
	if !client.IsConnected() {
		return errors.New("client not connected")
	} else {
		token := client.Unsubscribe(topic)
		token.Wait()
		err = token.Error()
	}
	return
}

// Publish function publishes data in interface on topic with level of qos (Quality of service)
// currently supported 0 & 1 (2 coming in future)
func (instance *AWSIoTConnection) Publish(topic string, data interface{}, qos byte) (err error) {
	if nil != instance {
		var client MQTT.Client
		client, err = instance.client(true)
		if nil != err {
			return
		}
		token := client.Publish(topic, qos, false, data)
		token.Wait()
		err = token.Error()
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

// init It initialises MQTT connection. Its called by NewConnection internally
func (instance *AWSIoTConnection) init(config *Config) (err error) {
	var tlsCert tls.Certificate
	tlsCert, err = tls.LoadX509KeyPair(
		config.CertPath, config.KeyPath)
	if err != nil {
		return
	}

	var caPem []byte
	caPem, err = os.ReadFile(config.CAPath)
	if err != nil {
		return
	}

	certs := x509.NewCertPool()
	certs.AppendCertsFromPEM(caPem)
	tlsConfig := &tls.Config{
		Certificates: []tls.Certificate{tlsCert},
		RootCAs:      certs,
	}

	instance.options = MQTT.NewClientOptions()
	instance.options.AddBroker("tcps://" + config.Endpoint + ":8883/mqtt")
	instance.options.SetMaxReconnectInterval(10 * time.Second)
	instance.options.SetClientID(config.ClientId)
	instance.options.SetTLSConfig(tlsConfig)
	return nil
}

func (instance *AWSIoTConnection) client(autoConnect bool) (response MQTT.Client, err error) {
	if nil != instance {
		if nil == instance._client {
			instance._client = MQTT.NewClient(instance.options)
			if autoConnect {
				token := instance._client.Connect()
				token.Wait()
				err = token.Error()
				if nil != err {
					instance._client = nil
					return
				}
			}
		}
		response = instance._client
	}
	return
}
