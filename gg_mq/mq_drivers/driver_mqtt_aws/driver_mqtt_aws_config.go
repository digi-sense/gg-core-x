package driver_mqtt_aws

import "bitbucket.org/digi-sense/gg-core"

// Config struct holds connection parameters for AWSIoTConnection initialisation
// All parameters are compulsory
// KeyPath is path to x.509 Private Key
// CertPath is path to x.509 Public Key
// CAPath is path to CA certificate
// ClientId is the clientId of thing
// Endpoint is Unique AWS IoT endpoint provided by AWS IoT
type Config struct {
	ClientId string `json:"client-id" binding:"required"`
	Endpoint string `json:"endpoint" binding:"required"`

	KeyPath  string `json:"key-path" binding:"required"`  // private key
	CertPath string `json:"cert-path" binding:"required"` // .crt file
	CAPath   string `json:"ca-path" binding:"required"`   // CA
}

func NewAWSIoTConnectionConfig(args ...interface{}) (instance *Config, err error) {
	instance = new(Config)
	err = instance.Parse(args...)

	return
}

func (instance *Config) Parse(args ...interface{}) (err error) {
	switch len(args) {
	case 1:
		// filename of map
		if filename, ok := args[0].(string); ok {
			err = gg.JSON.ReadFromFile(filename, &instance)
		} else if m, ok := args[0].(map[string]interface{}); ok {
			err = gg.JSON.Read(m, &instance)
		}
	case 5:
		instance.ClientId = gg.Convert.ToString(args[0])
		instance.Endpoint = gg.Convert.ToString(args[1])
		instance.KeyPath = gg.Convert.ToString(args[2])
		instance.CertPath = gg.Convert.ToString(args[3])
		instance.CAPath = gg.Convert.ToString(args[4])
	}
	return
}
