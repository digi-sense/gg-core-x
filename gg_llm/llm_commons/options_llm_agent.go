package llm_commons

import (
	"bitbucket.org/digi-sense/gg-core"
)

// ---------------------------------------------------------------------------------------------------------------------
//  OptionsAgent
// ---------------------------------------------------------------------------------------------------------------------

type OptionsAgent struct {
	Name              string                `json:"name"`                 // agent name
	AutoCalcMaxTokens bool                  `json:"auto-calc-max-tokens"` // if max tokens must be calculated
	LimitMaxTokens    int                   `json:"limit-max-tokens"`     // maximum number of tokens allowed in this agent
	Drivers           []*LLMDriverOptions   `json:"drivers"`              // LLM drivers
	Embeddings        *LLMEmbeddingsOptions `json:"embeddings"`           // rag configuration
}

func NewOptionsAgent() (instance *OptionsAgent) {
	instance = new(OptionsAgent)
	instance.Drivers = make([]*LLMDriverOptions, 0)

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *OptionsAgent) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *OptionsAgent) Map() map[string]interface{} {
	return gg.Convert.ToMap(gg.JSON.Stringify(instance))
}

func (instance *OptionsAgent) ContainsDriver(uid string) bool {
	if nil != instance {
		for _, driver := range instance.Drivers {
			if nil != driver && driver.Uid == uid {
				return true
			}
		}
	}
	return false
}

func (instance *OptionsAgent) AddDriver(item *LLMDriverOptions) {
	if nil != instance && nil != item {
		if !instance.ContainsDriver(item.Uid) {
			instance.Drivers = append(instance.Drivers, item)
		}
	}
}
