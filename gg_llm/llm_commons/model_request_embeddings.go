package llm_commons

import "bitbucket.org/digi-sense/gg-core"

type LLMRequestEmbeddings struct {
	Version string `json:"version"` // application version
	Driver  string `json:"driver"`  // name of driver
	Model   string `json:"model"`   // ID of the model to use. See the model endpoint compatibility table for details on which models work with the Chat API.

	Prompt string `json:"prompt"`

	modelOptions *RequestOptions
}

func NewLLMRequestEmbeddings() (instance *LLMRequestEmbeddings) {
	instance = new(LLMRequestEmbeddings)

	instance.modelOptions = NewModelOptions()
	instance.init()

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LLMRequestEmbeddings) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *LLMRequestEmbeddings) Map() (m map[string]interface{}) {
	m = gg.Convert.ToMap(gg.JSON.Stringify(instance))
	m["model-options"] = instance.GetOptions().Map()

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  model options
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LLMRequestEmbeddings) GetOptions() (response *RequestOptions) {
	if nil != instance {
		if nil == instance.modelOptions {
			instance.modelOptions = NewModelOptions()
		}
		response = instance.modelOptions
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LLMRequestEmbeddings) init() {
	if nil != instance {

	}
}
