package llm_commons

import (
	"bitbucket.org/digi-sense/gg-core"
	"github.com/cbroglie/mustache"
	"math"
	"strings"
)

//----------------------------------------------------------------------------------------------------------------------
//	u t i l s
//----------------------------------------------------------------------------------------------------------------------

func AppRoot() string {
	return gg.Paths.WorkspacePath("./")
}

func TempRoot() string {
	return gg.Paths.WorkspacePath("./.tmp")
}

func AppPath(relativePath string) string {
	root := AppRoot()
	return gg.Paths.Concat(root, relativePath)
}

func GetDirSensyPrompts() string {
	return AppPath(DirSensyPrompts)
}

// ---------------------------------------------------------------------------------------------------------------------
//	n e t w o r k
// ---------------------------------------------------------------------------------------------------------------------

func Download(url string) (data []byte, err error) {
	if gg.Paths.IsUrl(url) {
		client := gg.Net.NewHttpClient()
		resp, e := client.Get(url)

		if nil != e {
			err = e
		} else {
			data = resp.Body
		}
	} else {
		data, err = gg.IO.Download(url)
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	r e n d e r
// ---------------------------------------------------------------------------------------------------------------------

func RenderMustache(text string, context ...interface{}) (string, error) {
	response, err := mustache.Render(text, context...)
	if nil != err {
		response = text
	}
	return response, err
}

// ---------------------------------------------------------------------------------------------------------------------
//	l l m
// ---------------------------------------------------------------------------------------------------------------------

// CalculateNumCtx computes a numerical context value based on input text length and session tokens.
// Minimum response value is 2048
func CalculateNumCtx(text string, sessionTokens int) (response int) {
	if len(text) > 0 {
		words := strings.Split(text, " ")
		response = int(math.Max(float64(len(words))*10, 2048))
		if sessionTokens > 0 {
			response += sessionTokens / 20
		}
	}
	return int(math.Max(float64(response), 2048))
}

func IsRAGRequired(textSize int) (response bool) {
	response = textSize > 50000
	return
}
