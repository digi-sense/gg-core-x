package llm_commons

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t a n t s
//----------------------------------------------------------------------------------------------------------------------

const DefLang = "en" // default llm language

const DefModel = "llama3.2"

//----------------------------------------------------------------------------------------------------------------------
//	d i r e c t o r i e s
//----------------------------------------------------------------------------------------------------------------------

const DirSensyPrompts = "./sensy-prompts" // source folder for prompts (only if used AI)
const DirSensyOutputs = "./sensy-outputs" // all sensy outputs go here

//----------------------------------------------------------------------------------------------------------------------
//	r o l e s
//----------------------------------------------------------------------------------------------------------------------

const (
	RoleSystem    string = "system"
	RoleUser             = "user"
	RoleAssistant        = "assistant"
)

//----------------------------------------------------------------------------------------------------------------------
//	d r i v e r s
//----------------------------------------------------------------------------------------------------------------------

type StreamHandler func(data []byte)

const (
	DriverOllama  = "ollama"
	DriverChatGPT = "chatgpt"
	DriverClaude  = "claude"

	DriverDalle    = "dalle"
	DriverUnsplash = "unsplash"
)

type ActionType string

const (
	ActionChat     ActionType = "chat"
	ActionGenerate ActionType = "generate"
)

//----------------------------------------------------------------------------------------------------------------------
//	content types
//----------------------------------------------------------------------------------------------------------------------

const ContentOptionSyntaxTextPlain = "text"
const ContentOptionSyntaxMarkdown = "markdown"
const ContentOptionSyntaxHTML = "html"

//----------------------------------------------------------------------------------------------------------------------
//	k n o w l e d g e
//----------------------------------------------------------------------------------------------------------------------

type KnowledgeModeType string

const (
	ModeKnowledge KnowledgeModeType = "knowledge"
	ModeSession   KnowledgeModeType = "session"
	ModeNotebook                    = "notebook"
)
