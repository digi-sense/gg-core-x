package llm_commons

import "bitbucket.org/digi-sense/gg-core"

type LLMRequestContext struct {
	Version             string          `json:"version"`                          // application version
	Driver              string          `json:"driver"`                           // name of driver (usually ollama)
	Model               string          `json:"model"`                            // ID of the model to use. See the model endpoint compatibility table for details on which models work with the Chat API.
	Payload             *RequestPayload `json:"payload"`                          // payload used to format prompts
	SkillName           string          `json:"skill-name,omitempty"`             // (optional) used only in agents with a skill controller
	PromptName          string          `json:"prompt-name,omitempty"`            // (optional) used only in agents with a skill controller
	UserQuery           string          `json:"user-query,omitempty"`             // (optional) used only in RAG
	UserQueryMaxResults int             `json:"user-query.max-results,omitempty"` // (optional) used only in RAG
	SessionId           string          `json:"session-id,omitempty"`             // (optional) id of session
	VectorId            string          `json:"vector-id,omitempty"`              // (optional) ID of RAG collection if any

	modelOptions *RequestOptions
}

func NewLLMRequestContext() (instance *LLMRequestContext) {
	instance = new(LLMRequestContext)

	instance.modelOptions = NewModelOptions()
	instance.init()

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LLMRequestContext) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *LLMRequestContext) Map() (m map[string]interface{}) {
	m = gg.Convert.ToMap(gg.JSON.Stringify(instance))
	m["model-options"] = instance.GetOptions().Map()

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  model options
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LLMRequestContext) GetOptions() (response *RequestOptions) {
	if nil != instance {
		if nil == instance.modelOptions {
			instance.modelOptions = NewModelOptions()
		}
		response = instance.modelOptions
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LLMRequestContext) init() {
	if nil != instance {
		instance.Payload = NewRequestPayload()
	}
}
