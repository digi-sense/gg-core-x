package llm_commons

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_xlibs/lib_vectordb/vectordb"
)

//----------------------------------------------------------------------------------------------------------------------
//	LLMRagOptions
//----------------------------------------------------------------------------------------------------------------------

type LLMRagOptions struct {
	Databases []*LLMRagOptionsItem `json:"databases"`
}

func (instance *LLMRagOptions) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *LLMRagOptions) Map() (response map[string]interface{}) {
	if nil != instance {
		response = gg.Convert.ToMap(instance.String())
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	LLMRagOptionsItem
//----------------------------------------------------------------------------------------------------------------------

type LLMRagOptionsItem struct {
	Driver string `json:"driver"`
	*vectordb.VectorDBOptions
}

func (instance *LLMRagOptionsItem) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *LLMRagOptionsItem) Map() (response map[string]interface{}) {
	if nil != instance {
		response = gg.Convert.ToMap(instance.String())
	}
	return
}
