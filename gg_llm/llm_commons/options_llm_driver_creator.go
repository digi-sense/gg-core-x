package llm_commons

import (
	"bitbucket.org/digi-sense/gg-core"
)

type LLMDriverCreatorOptions struct {
	Uid          string   `json:"uid"`
	PlanUid      string   `json:"plan-uid"`
	Lang         string   `json:"lang"`
	TextQuery    string   `json:"text-query"`
	TitleQuery   string   `json:"title-query"`
	CreateImage  bool     `json:"create-image"`
	ImageQuery   string   `json:"image-query"`
	ImageParams  []string `json:"image-params"`
	Tags         []string `json:"tags"`
	Categories   []string `json:"categories"`
	CreateText   bool     `json:"create-text"`
	SourceSyntax string   `json:"source-syntax"` // markdown, text
	TargetSyntax string   `json:"target-syntax"` // html
}

func NewLLMDriverCreatorOptions(args ...interface{}) (instance *LLMDriverCreatorOptions) {
	instance = new(LLMDriverCreatorOptions)
	instance.Uid = gg.Rnd.RndId()
	if len(args) > 0 {
		arg := args[0]
		_ = gg.JSON.Read(gg.Convert.ToString(arg), &instance)
	}
	if len(instance.Lang) == 0 {
		instance.Lang = DefLang
	}
	if len(instance.PlanUid) == 0 {
		instance.PlanUid = instance.Uid
	}
	return
}

func (instance *LLMDriverCreatorOptions) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *LLMDriverCreatorOptions) Map() map[string]interface{} {
	return gg.Convert.ToMap(instance.String())
}

func (instance *LLMDriverCreatorOptions) HasSyntax() bool {
	if nil != instance && nil != instance.refresh() {
		return len(instance.SourceSyntax) > 0 && len(instance.TargetSyntax) > 0
	}
	return false
}

func (instance *LLMDriverCreatorOptions) refresh() *LLMDriverCreatorOptions {
	if nil != instance {
		if len(instance.SourceSyntax) > 0 || len(instance.TargetSyntax) > 0 {
			if len(instance.SourceSyntax) == 0 {
				instance.SourceSyntax = ContentOptionSyntaxMarkdown
			}
			if len(instance.TargetSyntax) == 0 {
				instance.TargetSyntax = ContentOptionSyntaxHTML
			}
		}
		if len(instance.TargetSyntax) == 0 {
			instance.TargetSyntax = ContentOptionSyntaxTextPlain
		}
	}
	return instance
}
