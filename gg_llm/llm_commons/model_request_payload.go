package llm_commons

import (
	"bitbucket.org/digi-sense/gg-core"
	"fmt"
	"strings"
	"time"
)

//----------------------------------------------------------------------------------------------------------------------
//	RequestPayload
//----------------------------------------------------------------------------------------------------------------------

type RequestPayload struct {
	Lang                 string   `json:"lang"`       // user lang
	UserQuery            string   `json:"user-query"` // request of the user
	PromptLang           string   `json:"prompt-lang"`
	FromLang             string   `json:"from-lang"`
	ToLang               string   `json:"to-lang"`
	Today                string   `json:"today"`
	Now                  string   `json:"now"`
	LangCountryPrompt    string   `json:"lang-country-prompt"`
	KBContext            any      `json:"context,omitempty"`          // (optional) Some context for prompts that need a limited knowledge for response (ex: RAG)
	KBContextRawElements []string `json:"context-elements,omitempty"` // (optional) Raw elements added to context (filenames or contents)
}

func NewRequestPayload(args ...any) (instance *RequestPayload) {
	instance = new(RequestPayload)
	instance.init(args...)
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *RequestPayload) String() string {
	return gg.JSON.Stringify(instance.ensureDefaults())
}

func (instance *RequestPayload) Map() (response map[string]interface{}) {
	if nil != instance {
		response = gg.Convert.ToMap(gg.JSON.Stringify(instance.ensureDefaults()))
		// add extensions
		response = instance.extend(response)
	}
	return
}

func (instance *RequestPayload) Parse(value any) bool {
	if nil != instance && nil != value {

		// check valid parsing values
		if m, ok := value.(map[string]interface{}); ok {
			// extend with calculated localizations
			err := gg.JSON.Read(m, &instance)
			return err == nil
		}

		// not parsed
		return false
	}
	return false
}

func (instance *RequestPayload) SetLang(value string) *RequestPayload {
	if nil != instance {
		instance.Lang = value
		if len(instance.PromptLang) == 0 {
			instance.PromptLang = DefLang
		}
		if len(instance.FromLang) == 0 {
			instance.FromLang = DefLang
		}
		if len(instance.ToLang) == 0 {
			instance.ToLang = instance.Lang
		}
	}
	return instance
}

func (instance *RequestPayload) KBContextSet(data any) (err error) {
	if nil != instance && nil != data {
		if list, ok := data.([]interface{}); ok {
			// context is a list
			instance.KBContext = list
		} else if m, ok := data.(map[string]interface{}); ok {
			// context is a map
			instance.KBContext = m
		} else {
			err = gg.Errors.Prefix(gg.PanicSystemError, "Context Type not supported. Only LIST or MAP are allowed.")
		}
	}
	return
}

func (instance *RequestPayload) KBContextSetItems(items ...any) *RequestPayload {
	if nil != instance {
		instance.KBContext = nil
		instance.KBContextAppendItems(items...)
	}
	return instance
}

// KBContextSetRawElements set a list of elements for debug info
// NOTE: this method must be invoked manually
// For performance reasons do not add full text documents, but only a shot preview
func (instance *RequestPayload) KBContextSetRawElements(items ...string) *RequestPayload {
	if nil != instance {
		instance.KBContextRawElements = make([]string, 0)
		// add raw data
		for _, item := range items {
			if gg.Paths.IsFilePath(item) {
				instance.KBContextRawElements = append(instance.KBContextRawElements, item)
			} else {
				// reduce text
				text := item
				if len(text) > 300 {
					text = text[0:300] + " ..."
				}
				instance.KBContextRawElements = append(instance.KBContextRawElements, text)
			}
		}
	}
	return instance
}

func (instance *RequestPayload) KBContextAppendItems(items ...any) *RequestPayload {
	if nil != instance {

		if nil == instance.KBContext {
			// creates a map context
			instance.KBContext = make(map[string]interface{})
		}
		if list, ok := instance.KBContext.([]interface{}); ok {
			// context is a list
			list = append(list, items...)
			instance.KBContext = list
		} else if m, ok := instance.KBContext.(map[string]interface{}); ok {
			// context is a map
			list = make([]interface{}, 0)
			for _, arg := range items {
				item := make(map[string]any)
				if s, b := arg.(string); b {
					if len(s) > 0 {
						item["content"] = s
						list = append(list, item)
					}
					continue
				}
				if mm, b := arg.(map[string]interface{}); b {
					item = mm
					if len(item) > 0 {
						list = append(list, item)
					}
					continue
				}

			}
			m["items"] = list
			instance.KBContext = m
		}
	}
	return instance
}

func (instance *RequestPayload) KBContextSize() int {
	if list, ok := instance.KBContext.([]interface{}); ok {
		// context is a list
		return len(list)
	} else if m, ok := instance.KBContext.(map[string]interface{}); ok {
		// context is a map
		return len(m)
	}
	return 0
}

func (instance *RequestPayload) KBContextAsArrayMap() (response []map[string]interface{}) {
	response = make([]map[string]interface{}, 0)
	if list, ok := instance.KBContext.([]interface{}); ok {
		// context is a list
		for _, arg := range list {
			if m, ok := arg.(map[string]interface{}); ok {
				response = append(response, m)
			}
		}
	} else if m, ok := instance.KBContext.(map[string]interface{}); ok {
		// context is a map
		items := gg.Maps.Get(m, "items")
		if itemsList, ok := items.([]interface{}); ok {
			for _, arg := range itemsList {
				if m, ok := arg.(map[string]interface{}); ok {
					response = append(response, m)
				}
			}
		}
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	u t i l s
//----------------------------------------------------------------------------------------------------------------------

func (instance *RequestPayload) Format(text string) (response string) {
	if nil != instance && len(text) > 0 {
		data := instance.Map()
		rendered, err := RenderMustache(text, data)
		if nil == err {
			response = rendered
		} else {
			response = text
		}
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *RequestPayload) init(args ...any) {
	if nil != instance {

		for _, arg := range args {
			instance.Parse(arg)
		}

		instance.ensureDefaults()
	}
}

// Ensure defaults
func (instance *RequestPayload) ensureDefaults() *RequestPayload {
	if nil != instance {
		if len(instance.Lang) == 0 {
			instance.Lang = DefLang
		}

		if len(instance.Today) == 0 {
			instance.Today = formatDate(instance.Lang, time.Now())
		}
		if len(instance.Now) == 0 {
			instance.Now = formatTime(instance.Lang, time.Now())
		}
		if len(instance.LangCountryPrompt) == 0 {
			instance.LangCountryPrompt = CountryNameOf(instance.Lang, instance.Lang)
		}
	}
	return instance
}

func (instance *RequestPayload) extend(value map[string]interface{}) (response map[string]interface{}) {
	if nil != instance && nil != value {
		response = make(map[string]interface{})
		addLocaleReference := false

		for key, val := range value {
			if !strings.HasPrefix(key, "_") && nil != val {
				// assign value to response
				response[key] = val

				// check the context
				if key == "context" {
					if m, ok := val.(map[string]interface{}); ok {
						items := gg.Maps.Get(m, "items")
						if isArray, a := gg.Compare.IsArray(items); isArray {
							if a.Len() > 0 {
								response["items_len"] = a.Len()
								for i := 0; i < a.Len(); i++ {
									ai := a.Index(i)
									if aim, aib := ai.Interface().(map[string]interface{}); aib {
										aim["idx"] = i + 1
									}
								}
							}
						}
					}
					continue
				}

				if s, ok := val.(string); ok {
					switch key {
					case "lang":
						addLocaleReference = true
						if len(s) == 0 {
							s = DefLang
							response[key] = s
						}
						code := s
						name := LangNameOut(code, DefLang)
						response["lang-name"] = name
						response["lang-code"] = code
						response["lang-name-ext"] = fmt.Sprintf("%s (%s)", name, code)
						response["lang-country"] = CountryName(code)
						instance.Lang = code
					case "prompt-lang":
						addLocaleReference = true
						if len(s) == 0 {
							s = DefLang
							response[key] = s
						}
						code := s
						name := LangNameOut(code, DefLang)
						response["prompt-lang-name"] = name
						response["prompt-lang-code"] = code
						response["prompt-lang-name-ext"] = fmt.Sprintf("%s (%s)", name, code)
						response["prompt-lang-country"] = CountryName(code)
					case "from-lang":
						addLocaleReference = true
						if len(s) == 0 {
							s = DefLang
							response[key] = s
						}
						code := s
						name := LangNameOut(code, DefLang)
						response["from-lang-name"] = name
						response["from-lang-code"] = code
						response["from-lang-name-ext"] = fmt.Sprintf("%s (%s)", name, code)
						response["from-lang-country"] = CountryName(code)
					case "to-lang":
						addLocaleReference = true
						if len(s) == 0 {
							s = DefLang
							response[key] = s
						}
						code := s
						name := LangNameOut(code, DefLang)
						response["to-lang-name"] = name
						response["to-lang-code"] = code
						response["to-lang-name-ext"] = fmt.Sprintf("%s (%s)", name, code)
						response["to-lang-country"] = CountryName(code)
					}
					continue
				} // val as string
				if m, ok := val.(map[string]interface{}); ok {
					response[key] = instance.extend(m)
					continue
				} // val as map

			}
		}

		// add time context data
		if addLocaleReference {
			pLang := instance.PromptLang // language of the prompt
			uLang := instance.Lang       // language of the user
			if len(pLang) == 0 {
				pLang = DefLang
			}
			if len(uLang) == 0 {
				uLang = DefLang
			}

			response["today"] = formatDate(pLang, time.Now())
			response["now"] = formatTime(pLang, time.Now())
			response["lang-country-prompt"] = CountryNameOf(uLang, pLang)
		}
	}
	return
}
