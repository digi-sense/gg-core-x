package llm_commons

import "bitbucket.org/digi-sense/gg-core"

type RequestOptions struct {
	// optionals useful
	N           int     `json:"n,omitempty"`           // Default 1. How many chat completion choices to generate for each input message. Note that you will be charged based on the number of generated tokens across all of the choices. Keep n as 1 to minimize costs.
	MaxTokens   int     `json:"max_tokens,omitempty"`  // The maximum number of tokens that can be generated in the chat completion. The total length of input tokens and generated tokens is limited by the model's context length
	Temperature float32 `json:"temperature,omitempty"` // Default 1. What sampling temperature to use, between 0 and 2. Higher values like 0.8 will make the output more random, while lower values like 0.2 will make it more focused and deterministic.  We generally recommend altering this or top_p but not both.
	TopP        float32 `json:"top_p,omitempty"`       // Default 1. An alternative to sampling with temperature, called nucleus sampling, where the model considers the results of the tokens with top_p probability mass. So 0.1 means only the tokens comprising the top 10% probability mass are considered. We generally recommend altering this or temperature but not both.
	User        string  `json:"user,omitempty"`        // A unique identifier representing your end-user, which can help OpenAI to monitor and detect abuse

	// optionals
	FrequencyPenalty int                    `json:"frequency_penalty,omitempty"` // Default 0. Number between -2.0 and 2.0. Positive values penalize new tokens based on their existing frequency in the text so far, decreasing the model's likelihood to repeat the same line verbatim.
	LogitBias        map[string]interface{} `json:"logit_bias,omitempty"`        // Modify the likelihood of specified tokens appearing in the completion. Accepts a JSON object that maps tokens (specified by their token ID in the tokenizer) to an associated bias value from -100 to 100. Mathematically, the bias is added to the logits generated by the model prior to sampling. The exact effect will vary per model, but values between -1 and 1 should decrease or increase likelihood of selection; values like -100 or 100 should result in a ban or exclusive selection of the relevant token.
	LogProbs         bool                   `json:"logprobs,omitempty"`          // Whether to return log probabilities of the output tokens or not. If true, returns the log probabilities of each output token returned in the content of message. This option is currently not available on the gpt-4-vision-preview model.
	TopLogProbs      int                    `json:"top_logprobs,omitempty"`      // An integer between 0 and 5 specifying the number of most likely tokens to return at each token position, each with an associated log probability. logprobs must be set to true if this parameter is used.
	PresencePenalty  float32                `json:"presence_penalty,omitempty"`  // Default 0.Number between -2.0 and 2.0. Positive values penalize new tokens based on whether they appear in the text so far, increasing the model's likelihood to talk about new topics.
	Seed             string                 `json:"seed,omitempty"`              // This feature is in Beta. If specified, our system will make a best effort to sample deterministically, such that repeated requests with the same seed and parameters should return the same result. Determinism is not guaranteed, and you should refer to the system_fingerprint response parameter to monitor changes in the backend.
	Stop             string                 `json:"stop,omitempty"`              // string / array / null. Up to 4 sequences where the API will stop generating further tokens.
	Stream           bool                   `json:"stream,omitempty"`            // If set, partial message deltas will be sent, like in ChatGPT. Tokens will be sent as data-only server-sent events as they become available, with the stream terminated by a data: [DONE] message
	Tools            []interface{}          `json:"tools,omitempty"`             // A list of tools the model may call. Currently, only functions are supported as a tool. Use this to provide a list of functions the model may generate JSON inputs for.
	ToolChoice       interface{}            `json:"tool_choice,omitempty"`       // Controls which (if any) function is called by the model. none means the model will not call a function and instead generates a message. auto means the model can pick between generating a message or calling a function. Specifying a particular function via {"type": "function", "function": {"name": "my_function"}} forces the model to call that function. none is the default when no functions are present. auto is the default if functions are present
	// ResponseFormat   string                 `json:"response_format,omitempty"`   // An object specifying the format that the model must output. Compatible with GPT-4 Turbo and gpt-3.5-turbo-1106. Setting to { "type": "json_object" } enables JSON mode, which guarantees the message the model generates is valid JSON. Important: when using JSON mode, you must also instruct the model to produce JSON yourself via a system or user message. Without this, the model may generate an unending stream of whitespace until the generation reaches the token limit, resulting in a long-running and seemingly "stuck" request. Also note that the message content may be partially cut off if finish_reason="length", which indicates the generation exceeded max_tokens or the conversation exceeded the max context length.
}

func NewModelOptions() (instance *RequestOptions) {
	instance = new(RequestOptions)
	return instance
}

func (instance *RequestOptions) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *RequestOptions) Map() map[string]interface{} {
	return gg.Convert.ToMap(gg.JSON.Stringify(instance))
}
