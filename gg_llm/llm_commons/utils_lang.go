package llm_commons

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_nlp"
	"bitbucket.org/digi-sense/gg-core-x/gg_nlp/nlp_detect"
	_ "embed"
	"golang.org/x/text/language"
	"golang.org/x/text/language/display"
	"golang.org/x/text/message"
	"time"
)

// ---------------------------------------------------------------------------------------------------------------------
//	l a n g
// ---------------------------------------------------------------------------------------------------------------------

func Lang(lang string) *nlp_detect.LanguageISO {
	return gg_nlp.NLP.LanguageDetector().GetLanguageISO(lang)
}

func LangDetect(text string) *nlp_detect.LanguageISO {
	lang := gg_nlp.NLP.GetLanguageCode(text)
	return Lang(lang)
}

// LangName return ISO language name (i.e. "Italian") from a lang code like "it"
func LangName(lang string) string {
	iso := Lang(lang)
	if nil != iso {
		code := iso.Code1
		tag := language.MustParse(code)
		namer := display.Languages(tag)
		name := namer.Name(tag)
		return gg.Strings.CapitalizeFirst(name)
	}
	return DefLang
}

func LangNameDefOut(lang string) string {
	return LangNameOut(lang, DefLang)
}

func LangNameOut(langIn, langOut string) string {
	isoIn := Lang(langIn)
	isoOut := Lang(langOut)
	if nil != isoIn && nil != isoOut {
		codeIn := isoIn.Code1
		tagIn := language.MustParse(codeIn)
		codeOut := isoOut.Code1
		tagOut := language.MustParse(codeOut)

		namer := display.Languages(tagOut)
		name := namer.Name(tagIn)
		return gg.Strings.CapitalizeFirst(name)
	}
	return DefLang
}

// LangCode return ISO code (i.e. "it") from lang code like "ita"
func LangCode(lang string) string {
	iso := Lang(lang)
	if nil != iso {
		return iso.Code1
	}
	return DefLang
}

func LangCodeDetect(text string) string {
	iso := LangDetect(text)
	if nil != iso {
		return iso.Code1
	}
	return DefLang
}

func CountryName(langTag string) string {
	return gg.Country.CountryName(langTag)
}

func CountryNameOf(countryLang, outputLang string) string {
	return gg.Country.CountryNameOf(countryLang, outputLang)
}

// ---------------------------------------------------------------------------------------------------------------------
//  S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func formatDate(langTag string, date time.Time) string {
	lang := language.Make(langTag)
	p := message.NewPrinter(lang)
	return p.Sprintf("%v", date.Format("Monday, 02 January 2006"))
}

func formatTime(langTag string, date time.Time) string {
	lang := language.Make(langTag)
	p := message.NewPrinter(lang)
	return p.Sprintf("%v", date.Format("15:04"))
}
