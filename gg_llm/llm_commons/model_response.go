package llm_commons

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_json"
	"bitbucket.org/digi-sense/gg-core/gg_stopwatch"
	"github.com/ollama/ollama/api"
)

// ---------------------------------------------------------------------------------------------------------------------
//  LLMResponse
// ---------------------------------------------------------------------------------------------------------------------

type LLMResponse struct {
	Version               string                   `json:"version"`
	Driver                string                   `json:"driver"`
	Model                 string                   `json:"model"`
	SessionId             string                   `json:"session-id"`              // id of session if any
	VectorId              string                   `json:"vector-id"`               // name of RAG collection if any
	Request               map[string]interface{}   `json:"request"`                 // entire original request (only for debug)
	RequestSystem         string                   `json:"request-system"`          // original request system prompt (only for debug)
	RequestUser           string                   `json:"request-user"`            // original request user prompt  (only for debug)
	Done                  bool                     `json:"done"`                    // used only for stream
	Elapsed               int                      `json:"elapsed"`                 // milliseconds
	UserLang              string                   `json:"user-lang"`               // language used by user
	RespRaw               map[string]interface{}   `json:"resp-raw"`                // Native JSON response
	RespContext           []int                    `json:"resp-context"`            // Context generated after response
	RespText              string                   `json:"resp-text"`               // Raw text response
	RespJsonObject        map[string]interface{}   `json:"resp-json-object"`        // JSON response
	RespJsonArray         []map[string]interface{} `json:"resp-json-array"`         // JSON response
	RespKnowledge         []map[string]interface{} `json:"resp-knowledge"`          // knowledge used in system prompt context
	RespKnowledgeElements []string                 `json:"resp-knowledge-elements"` // knowledge elements used in system prompt context. Are files or texts
	RespSession           map[string]interface{}   `json:"resp-session"`            // session info
	Metadata              map[string]interface{}   `json:"metadata"`                // Used for custom fields. This is an external payload containing fields like "project-id", "sku", etc...

	// private
	watch *gg_stopwatch.StopWatch
}

func NewLLMResponse(request *LLMRequest) (instance *LLMResponse) {
	instance = new(LLMResponse)
	instance.Version = "0.0.0"
	instance.RespJsonObject = make(map[string]interface{})
	instance.RespKnowledge = make([]map[string]interface{}, 0)
	instance.RespSession = make(map[string]interface{})
	instance.Metadata = make(map[string]interface{})
	instance.RespContext = make([]int, 0)

	instance.SetRequest(request)

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LLMResponse) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *LLMResponse) Map() (response map[string]interface{}) {
	if nil != instance {
		response = gg.Convert.ToMap(instance.String())
		// add extended fields
		response["json-data"] = instance.JSONData()
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  m a i n    a c t i v a t i o n
// ---------------------------------------------------------------------------------------------------------------------

// SetRequest associates an LLMRequest with the LLMResponse instance and updates relevant fields like Model and UserLang.
func (instance *LLMResponse) SetRequest(request *LLMRequest) *LLMResponse {
	if nil != instance && nil != request {
		instance.SessionId = request.SessionId              // persis session if id is assigned
		instance.Request = request.Map()                    // debug info about original request
		instance.RequestSystem = request.CalcPromptSystem() // the system prompt formatted
		instance.RequestUser = request.CalcPrompt()         // the user prompt formatted
		instance.Driver = request.Driver
		instance.Model = request.Model
		instance.Version = request.Version
		instance.UserLang = request.Payload.Lang

		if request.Payload.KBContextSize() > 0 {
			instance.RespKnowledge = request.Payload.KBContextAsArrayMap()
		}
		if len(request.Payload.KBContextRawElements) > 0 {
			// debug info about files or text used to create the RespKnowledge
			instance.RespKnowledgeElements = request.Payload.KBContextRawElements
		}

		if nil == instance.watch {
			instance.watch = gg.StopWatch.New().Start()
		}
	}
	return instance
}

// SetResponse updates the LLMResponse instance with the provided raw response data and marks the operation as complete.
func (instance *LLMResponse) SetResponse(rawResponse map[string]interface{}) *LLMResponse {
	if nil != instance {
		instance.RespRaw = rawResponse
		instance.Done = true
		if instance.Done {
			if nil != instance.watch {
				instance.watch.Stop()
				instance.Elapsed = instance.watch.Milliseconds()
				instance.watch = nil
			}
		}
	}
	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//  text response assignment
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LLMResponse) SetTextResponse(value string) *LLMResponse {
	if nil != instance {
		instance.RespText = value
		if gg.JSON.IsValidJsonObject(value) {
			_ = gg.JSON.Read(value, &instance.RespJsonObject)
		} else if gg.JSON.IsValidJsonArray(value) {
			_ = gg.JSON.Read(value, &instance.RespJsonArray)
		} else {
			// extract JSON
			objects, _ := gg.JSON.TextExtractor(instance.RespText).GetObjects()
			if len(objects) > 0 {
				countObjects := 0 // count only single items
				instance.RespJsonArray = make([]map[string]interface{}, 0)
				for _, object := range objects {
					if object.IsArray() {
						instance.RespJsonArray = append(instance.RespJsonArray, object.Array()...)
					} else {
						instance.RespJsonArray = append(instance.RespJsonArray, object.Map())
						countObjects++
					}
				}
				if countObjects == 1 {
					// the response is just a single JSON object, not an array
					instance.RespJsonObject = instance.RespJsonArray[0]
					instance.RespJsonArray = make([]map[string]interface{}, 0)
				}
			}
		}
	}
	return instance
}

func (instance *LLMResponse) IsJSONResponse() bool {
	if nil != instance {
		return instance.JSONCount() > 0
	}
	return false
}

func (instance *LLMResponse) JSONCount() int {
	if nil != instance {
		if len(instance.RespJsonArray) > 0 {
			return len(instance.RespJsonArray)
		}
		if len(instance.RespJsonObject) > 0 {
			return 1
		}
	}
	return 0
}

func (instance *LLMResponse) JSONForEach(iterator func(jsonItem map[string]any)) *LLMResponse {
	if nil != instance && nil != iterator {
		if len(instance.RespJsonArray) > 0 {
			for _, jsonObject := range instance.RespJsonArray {
				iterator(jsonObject)
			}
		} else if len(instance.RespJsonObject) > 0 {
			iterator(instance.RespJsonObject)
		}
	}
	return instance
}

func (instance *LLMResponse) JSONData() (response []map[string]interface{}) {
	if nil != instance {
		response = make([]map[string]interface{}, 0)
		instance.JSONForEach(func(jsonObject map[string]any) {
			response = append(response, jsonObject)
		})
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  m e t a d a t a  (custom fields)
// ---------------------------------------------------------------------------------------------------------------------

// GetMetadata retrieves the Metadata field of the LLMResponse instance, initializing it as an empty map if nil.
func (instance *LLMResponse) GetMetadata() (response map[string]interface{}) {
	if nil != instance {
		if nil == instance.Metadata {
			instance.Metadata = make(map[string]interface{})
		}
		response = instance.Metadata
	}
	return
}

// GetMetaProjectId (only for NOTEBOOK) retrieves the "project-id" value from the Metadata field of the LLMResponse instance.
// Returns an empty string if no value is found or if the instance is nil.
func (instance *LLMResponse) GetMetaProjectId() (response string) {
	if nil != instance {
		response = gg.Maps.GetString(instance.Metadata, "project-id")
	}
	return
}

// SetMetaProjectId (only for NOTEBOOK) sets the "project-id" value within the Metadata field of the LLMResponse instance and returns the instance.
func (instance *LLMResponse) SetMetaProjectId(value string) *LLMResponse {
	if nil != instance {
		instance.Metadata["project-id"] = value
	}
	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//  p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LLMResponse) parseChatGPTResponse(data map[string]interface{}) (success bool) {
	if nil != instance && nil != data {
		success = isChatGPTResponse(data)
		if !success {
			return
		}

		instance.Done = true
		instance.RespRaw = data

	}
	return
}

func (instance *LLMResponse) parseOllamaResponse(data map[string]interface{}) (success bool) {
	if nil != instance && nil != data {
		var ollamaResp api.GenerateResponse
		success, ollamaResp = isOllamaResponse(data)
		if !success {
			return
		}

		instance.Done = ollamaResp.Done
		instance.RespRaw = data
	}
	return
}

func (instance *LLMResponse) parseClaudeResponse(data map[string]interface{}) (success bool) {
	if nil != instance && nil != data {
		success = isClaudeResponse(data)
		if !success {
			return
		}

		instance.Done = true
		instance.RespRaw = data
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func isOllamaResponse(data map[string]interface{}) (success bool, ollamaResp api.GenerateResponse) {
	err := gg.JSON.Read(data, &ollamaResp)
	if nil == err && len(ollamaResp.Response) > 0 {
		success = true
	}
	return
}

func isChatGPTResponse(data map[string]interface{}) (success bool) {
	if nil != gg.Maps.Get(data, "choices") {
		success = true
	}
	return
}

func isClaudeResponse(data map[string]interface{}) (success bool) {

	return
}

func extractJSON(text string) (response map[string]interface{}, err error) {
	var objects []*gg_json.JSONWrap
	objects, err = gg.JSON.TextExtractor(text).GetObjects()
	if nil == err {
		if len(objects) > 0 {
			response, err = objects[0].AsMap()
		}
	}
	return
}
