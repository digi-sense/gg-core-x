package llm_commons

import (
	"bitbucket.org/digi-sense/gg-core"
)

// IsDocumentPrompt checks if the provided name is in the list of prompt names that require a "document" payload.
func IsDocumentPrompt(name string) bool {
	if len(name) > 0 {
		return gg.Arrays.IndexOf(name, PromptDefDocumentNames) > -1
	}
	return false
}
