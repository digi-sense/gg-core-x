package llm_commons

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/ggx_commons"
	"math"
)

// ---------------------------------------------------------------------------------------------------------------------
//  LLMRequest
// ---------------------------------------------------------------------------------------------------------------------

type LLMRequest struct {
	// required
	Version string          `json:"version"` // application version
	Driver  string          `json:"driver"`  // name of driver
	Model   string          `json:"model"`   // ID of the model to use. See the model endpoint compatibility table for details on which models work with the Chat API.
	Payload *RequestPayload `json:"payload"` // payload used to format prompts

	// optional
	SkillName        string     `json:"skill-name,omitempty"`         // (optional) used only in agents with a skill controller
	PromptName       string     `json:"prompt-name,omitempty"`        // (optional) used only in agents with a skill controller
	Action           ActionType `json:"action,omitempty"`             // (optional) chat, generate
	Format           string     `json:"format,omitempty"`             // (optional) json
	SessionId        string     `json:"session-id,omitempty"`         // (optional) ID of the session if any
	VectorId         string     `json:"vector-id,omitempty"`          // (optional) ID of RAG collection if any
	VectorMaxResults int        `json:"vector-max-results,omitempty"` // (optional) RAG max results.
	Context          []int      `json:"context,omitempty"`            // (optional) Previous context if any
	Images           []string   `json:"images,omitempty"`             // (optional) list of base64 encoded images

	// not exposed
	Stream StreamHandler `json:"-"` // Stream: only when stream is allowed

	// private fields
	messages            *RequestMessageList
	modelOptions        *RequestOptions
	calculatedMaxTokens int
	errors              []string
	toContextualize     []string
}

func NewLLMRequest(messages ...interface{}) (instance *LLMRequest) {
	instance = new(LLMRequest)
	instance.Version = ggx_commons.Version
	instance.messages = NewMessageList()
	instance.Context = make([]int, 0)
	instance.Action = ActionGenerate
	instance.errors = make([]string, 0)
	instance.calculatedMaxTokens = 0
	instance.modelOptions = NewModelOptions()
	instance.toContextualize = make([]string, 0)

	instance.init(messages...)

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LLMRequest) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *LLMRequest) Map() (m map[string]interface{}) {
	m = gg.Convert.ToMap(gg.JSON.Stringify(instance))

	// add protected fields
	m["messages"] = instance.Messages()
	m["prompt-system"] = instance.GetPromptSystem()
	m["prompt"] = instance.GetPrompt()
	m["model-options"] = instance.GetOptions().Map()

	// add options for model
	if nil != instance.modelOptions {
		m = gg.Maps.Merge(false, m, instance.modelOptions.Map())
	}
	return
}

func (instance *LLMRequest) Parse(m map[string]interface{}) *LLMRequest {
	if nil != instance {
		_ = gg.JSON.Read(m, &instance)
	}
	return instance
}

func (instance *LLMRequest) Errors() (response []string) {
	if nil != instance {
		response = append(response, instance.errors...)
	}
	return
}

func (instance *LLMRequest) Refresh() *LLMRequest {
	if nil != instance && nil != instance.modelOptions {

		// max-tokens
		if instance.modelOptions.MaxTokens > 0 {
			instance.calculatedMaxTokens = instance.modelOptions.MaxTokens
			instance.calculatedMaxTokens = int(math.Max(float64(instance.calculatedMaxTokens), 2048))
		} else {
			instance.calculatedMaxTokens = instance.CalculateMaxTokens()
		}
	}
	return instance
}

func (instance *LLMRequest) CalculateMaxTokens() int {
	if nil != instance {
		// get messages length
		tokens := float64(instance.MessagesLength()) * 0.75
		// add the existing context
		tokens += float64(len(instance.Context)) * 0.20

		return int(math.Max(tokens, 2048))
	}
	return 2048
}

// ---------------------------------------------------------------------------------------------------------------------
//  p r o m p t s
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LLMRequest) GetPromptSystem() (response string) {
	if nil != instance && nil != instance.messages {
		message := instance.messages.GetSystem()
		if nil != message {
			response = message.Content
		}
	}
	return
}

func (instance *LLMRequest) SetPromptSystem(text string) *LLMRequest {
	if nil != instance && len(text) > 0 && nil != instance.messages {
		instance.messages.SetSystem(text)
	}
	return instance
}

func (instance *LLMRequest) GetPrompt() (response string) {
	if nil != instance && nil != instance.messages {
		message := instance.messages.GetUser()
		if nil != message {
			response = message.Content
		}
	}
	return
}

// SetPrompt sets the user's prompt text in the LLMRequest messages if the instance and messages are not nil and text is non-empty.
// This may be a "prompt" wrapper that will be formatted with the request payload.
// NOTE: the request payload may contain a field "user-query" that will be merged with a prompt wrapper.
func (instance *LLMRequest) SetPrompt(text string) *LLMRequest {
	if nil != instance && len(text) > 0 && nil != instance.messages {
		instance.messages.SetUser(text)
	}
	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//  calculated values
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LLMRequest) CalcMaxTokens() int {
	if nil != instance {
		return instance.Refresh().calculatedMaxTokens
	}
	return 0
}

func (instance *LLMRequest) CalcMaxTokensMin(min int) int {
	if nil != instance {
		calculated := instance.Refresh().calculatedMaxTokens
		return int(math.Max(float64(calculated), float64(min)))
	}
	return 0
}

func (instance *LLMRequest) CalcPrompt() (response string) {
	if nil != instance {
		response = instance.GetPrompt()
		if len(response) > 0 && nil != instance.Payload {
			response = instance.Payload.Format(response)
		}
	}
	return
}

func (instance *LLMRequest) CalcPromptSystem() (response string) {
	if nil != instance {
		response = instance.GetPromptSystem()
		if len(response) > 0 && nil != instance.Payload {
			response = instance.Payload.Format(response)
		}
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  m e s s a g e s
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LLMRequest) Add(args ...interface{}) *LLMRequest {
	if nil != instance && len(args) > 0 {
		for _, arg := range args {
			_ = instance.add(arg, "")
		}
	}
	return instance
}

func (instance *LLMRequest) AddUserMessage(arg interface{}) *LLMRequest {
	if nil != instance && nil != arg {
		_ = instance.add(arg, RoleUser)
	}
	return instance
}

// Messages returns a list of request messages from the LLMRequest instance, with their content optionally formatted.
func (instance *LLMRequest) Messages() (response []*RequestMessage) {
	if nil != instance {
		response = make([]*RequestMessage, 0)
		instance.messages.ForEach(func(item *RequestMessage) any {
			if nil != item {
				clone := item.Clone()
				if nil != instance.Payload {
					clone.Content = instance.Payload.Format(clone.Content)
				}
				response = append(response, clone)
			}
			return nil
		})
	}
	return
}

func (instance *LLMRequest) MessagesLength() (response int) {
	if nil != instance && nil != instance.messages {
		response = 0
		list := instance.Messages()
		for _, item := range list {
			if nil != item {
				response += item.Length()
			}
		}
	}
	return
}

func (instance *LLMRequest) MessagesCount() (response int) {
	if nil != instance && nil != instance.messages {
		response = instance.messages.Count()
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  model options
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LLMRequest) GetOptions() (response *RequestOptions) {
	if nil != instance {
		if nil == instance.modelOptions {
			instance.modelOptions = NewModelOptions()
		}
		response = instance.modelOptions
	}
	return
}

func (instance *LLMRequest) SetOptions(options *RequestOptions) *LLMRequest {
	if nil != instance && nil != options {
		instance.modelOptions = options
	}
	return instance
}

func (instance *LLMRequest) SetOptionMaxTokens(value int) *LLMRequest {
	if nil != instance {
		options := instance.GetOptions()
		options.MaxTokens = value
	}
	return instance
}

func (instance *LLMRequest) SetOptionMaxTokensWithLimit(value, limit int) *LLMRequest {
	if nil != instance {
		options := instance.GetOptions()
		if limit > 0 {
			options.MaxTokens = int(math.Min(float64(value), float64(limit)))
		} else {
			options.MaxTokens = value
		}
	}
	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//  c o n t e x t u a l i z a t i o n
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LLMRequest) ContextualizerHasData() bool {
	if nil != instance {
		return len(instance.toContextualize) > 0
	}
	return false
}

// ContextualizerAddData appends input strings to the toContextualize slice if the instance and inputs are valid.
func (instance *LLMRequest) ContextualizerAddData(inputs ...string) *LLMRequest {
	if nil != instance && len(inputs) > 0 {
		instance.toContextualize = append(instance.toContextualize, inputs...)
	}
	return instance
}

func (instance *LLMRequest) ContextualizerGetData() []string {
	if nil != instance {
		return instance.toContextualize
	}
	return make([]string, 0)
}

// ---------------------------------------------------------------------------------------------------------------------
//  p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LLMRequest) init(messages ...interface{}) {
	if nil != instance {
		// payload defaults
		instance.Payload = NewRequestPayload()

		// messages
		if len(messages) > 0 {
			if len(messages) > 1 {
				instance.Add(messages...)
			} else {
				// single user message
				instance.AddUserMessage(messages[0])
			}
		}
	}
}

func (instance *LLMRequest) add(item interface{}, role string) (err error) {
	if nil != item {
		if s, ok := item.(string); ok {
			err = instance.addString(s, role)
			return
		}
		if m, ok := item.(map[string]interface{}); ok {
			err = instance.addMap(m)
			return
		}
		if mm, ok := item.([]map[string]interface{}); ok {
			err = instance.addMaps(mm)
			return
		}
		if m, ok := item.(*RequestMessage); ok {
			err = instance.addMessage(m)
			return
		}
		if m, ok := item.(RequestMessage); ok {
			err = instance.addMessage(&m)
			return
		}

		// fallback error
		err = gg.Errors.Prefix(gg.PanicSystemError, "Unsupported item type: ")
	}
	err = gg.Errors.Prefix(gg.PanicSystemError, "Unable to import null item: ")
	return
}

func (instance *LLMRequest) addString(s string, forcedRole string) (err error) {
	if len(s) > 0 {
		if len(forcedRole) > 0 {
			switch forcedRole {
			case RoleSystem:
				err = instance.addMessage(NewAIMessageSystem().SetContent(s))
			case RoleUser:
				err = instance.addMessage(NewAIMessageUser().SetContent(s))
			case RoleAssistant:
				err = instance.addMessage(NewAIMessageAssistant().SetContent(s))
			}
		} else {
			if array, ok := gg.JSON.StringToArray(s); ok {
				for _, item := range array {
					err = instance.add(item, "")
					if nil != err {
						break
					}
				}
				return
			}
			if m, ok := gg.JSON.StringToMap(s); ok {
				err = instance.addMap(m)
				return
			}
			// simple string
			tokens := gg.Strings.SplitTrimSpace(s, "|")
			switch len(tokens) {
			case 1:
				var message *RequestMessage
				count := instance.messages.Count()
				if count == 0 {
					message = NewAIMessageSystem().SetContent(s) // first is system
				} else {
					// add user/assistant dialog
					if count%2 == 0 {
						// even, assistant
						message = NewAIMessageAssistant().SetContent(s)
					} else {
						// odd, user
						message = NewAIMessageUser().SetContent(s)
					}
				}
				err = instance.addMessage(message)
			case 2:
				role := tokens[0]
				s := tokens[1]
				switch role {
				case RoleSystem:
					err = instance.addMessage(NewAIMessageSystem().SetContent(s))
				case RoleUser:
					err = instance.addMessage(NewAIMessageUser().SetContent(s))
				case RoleAssistant:
					err = instance.addMessage(NewAIMessageAssistant().SetContent(s))
				}
			}
		}

	}
	return
}

func (instance *LLMRequest) addMaps(mm []map[string]interface{}) (err error) {
	for _, m := range mm {
		err = instance.addMap(m)
		if nil != err {
			break
		}
	}
	return
}

func (instance *LLMRequest) addMap(m map[string]interface{}) (err error) {
	var message *RequestMessage
	err = gg.JSON.Read(m, &message)
	if nil == err {
		err = instance.addMessage(message)
	}
	return
}

func (instance *LLMRequest) addMessage(message *RequestMessage) (err error) {
	if nil != message && nil != instance.messages {
		instance.messages.Add(message)
	}
	return
}
