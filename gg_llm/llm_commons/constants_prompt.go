package llm_commons

const PromptSeparator = "\n---\n"

const PrefixPrompt = "prompt:"
const PrefixLiteral = "literal:"

const PromptDefLang = DefLang

/**
 *  SYSTEM PROMPTS
 *  Each package has one or more "default" prompts (i.e. "upload")
 */

const (
	PromptDefNameUpload   = "upload"   // special system prompt used only after a session upload
	PromptDefNameEntities = "entities" // system prompt used by tools or user actions to achieve a special task
	PromptDefNameContext  = "context"
)

// PromptDefDocumentNames contains names for prompts that require a "document" payload to be performed.
var PromptDefDocumentNames = []string{
	PromptDefNameEntities,
}
