package llm_commons

import "bitbucket.org/digi-sense/gg-core"

type LLMEmbeddingsOptions struct {
	RAG *LLMRagOptions `json:"rag"` // rag configuration
}

func (instance *LLMEmbeddingsOptions) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *LLMEmbeddingsOptions) Map() (response map[string]interface{}) {
	if nil != instance {
		response = gg.Convert.ToMap(instance.String())
	}
	return
}
