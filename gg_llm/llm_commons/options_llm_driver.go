package llm_commons

import (
	"bitbucket.org/digi-sense/gg-core"
)

// ---------------------------------------------------------------------------------------------------------------------
//  LLMDriverOptions
// ---------------------------------------------------------------------------------------------------------------------

type LLMDriverOptions struct {
	Uid            string                   `json:"uid"`         // (only for store drivers) store uid
	DriverName     string                   `json:"driver-name"` // name of the driver
	Model          string                   `json:"model"`       // name of the model
	AccessKey      string                   `json:"access-key"`  // access token
	SecretKey      string                   `json:"secret-key,omitempty"`
	OptionsCreator *LLMDriverCreatorOptions `json:"create-options,omitempty"`
	OptionsModel   *RequestOptions          `json:"model-options"` // specific AI model option
}

func NewLLMDriverOptions(args ...interface{}) (instance *LLMDriverOptions) {
	instance = new(LLMDriverOptions)
	instance.OptionsModel = new(RequestOptions)
	if len(args) > 0 {
		arg := args[0]
		_ = gg.JSON.Read(gg.Convert.ToString(arg), &instance)
		if nil == instance.OptionsCreator {
			_ = gg.JSON.Read(gg.Convert.ToString(arg), &instance.OptionsCreator)
		}
	}
	if nil == instance.OptionsCreator {
		instance.OptionsCreator = new(LLMDriverCreatorOptions)
	}

	return
}

func (instance *LLMDriverOptions) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *LLMDriverOptions) Map() map[string]interface{} {
	return gg.Convert.ToMap(gg.JSON.Stringify(instance))
}

func (instance *LLMDriverOptions) Parse(m map[string]any) *LLMDriverOptions {
	if nil != instance && nil != m {
		instance.Model = gg.Maps.GetString(m, "model")
		instance.AccessKey = gg.Maps.GetString(m, "access-key")
		instance.SecretKey = gg.Maps.GetString(m, "secret-key")
		modelOptions := gg.Maps.GetMap(m, "model-options")
		if len(modelOptions) > 0 {
			if nil == instance.OptionsModel {
				instance.OptionsModel = new(RequestOptions)
			}
			_ = gg.JSON.Read(modelOptions, &instance.OptionsModel)
		}
		createOptions := gg.Maps.GetMap(m, "create-options")
		if len(createOptions) > 0 {
			if nil == instance.OptionsCreator {
				instance.OptionsCreator = new(LLMDriverCreatorOptions)
			}
			_ = gg.JSON.Read(createOptions, &instance.OptionsCreator)
		}
	}
	return instance
}

func (instance *LLMDriverOptions) GetAccessKey() (response string) {
	if nil != instance {
		response = instance.AccessKey
	}
	return
}

func (instance *LLMDriverOptions) GetSecretKey() (response string) {
	if nil != instance {
		response = instance.SecretKey
	}
	return
}
