package llm_commons

import (
	"bitbucket.org/digi-sense/gg-core"
)

//----------------------------------------------------------------------------------------------------------------------
//	ModelMessage
//----------------------------------------------------------------------------------------------------------------------

// RequestMessage ollama standard chat message
type RequestMessage struct {
	Role    string   `json:"role"`              // the role of the message, either system, user or assistant
	Content string   `json:"content,omitempty"` // the content of the message
	Images  []string `json:"images,omitempty"`  // (optional) a list of base64-encoded images (for multimodal models such as llava)
}

func (instance *RequestMessage) SetContent(value string) *RequestMessage {
	if nil != instance {
		instance.Content = value
	}
	return instance
}

func NewAIMessageSystem() *RequestMessage {
	return &RequestMessage{
		Role:    RoleSystem,
		Content: "",
		Images:  nil,
	}
}

func NewAIMessageUser() *RequestMessage {
	return &RequestMessage{
		Role:    RoleUser,
		Content: "",
		Images:  nil,
	}
}

func NewAIMessageAssistant() *RequestMessage {
	return &RequestMessage{
		Role:    RoleAssistant,
		Content: "",
		Images:  nil,
	}
}

func (instance *RequestMessage) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *RequestMessage) Map() (response map[string]interface{}) {
	_ = gg.JSON.Read(instance.String(), &response)
	return
}

func (instance *RequestMessage) Length() int {
	if nil != instance {
		return len(instance.Content)
	}
	return 0
}

func (instance *RequestMessage) Clone() (response *RequestMessage) {
	if nil != instance {
		response = new(RequestMessage)
		response.Role = instance.Role
		response.Content = instance.Content
		response.Images = make([]string, 0)
		response.Images = append(response.Images, instance.Images...)
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	RequestMessageList
//----------------------------------------------------------------------------------------------------------------------

type RequestMessageList struct {
	list []*RequestMessage
}

func NewMessageList(args ...interface{}) (instance *RequestMessageList) {
	instance = new(RequestMessageList)
	instance.list = make([]*RequestMessage, 0)

	for _, arg := range args {
		if m, ok := arg.(*RequestMessage); ok {
			instance.list = append(instance.list, m)
			continue
		}
		if m, ok := arg.(RequestMessage); ok {
			instance.list = append(instance.list, &m)
			continue
		}
		if mm, ok := arg.(map[string]interface{}); ok {
			var m *RequestMessage
			err := gg.JSON.Read(mm, &m)
			if nil == err && nil != m && len(m.Content) > 0 {
				instance.list = append(instance.list, m)
			}
			continue
		}
		if s, ok := arg.(string); ok {
			var m *RequestMessage
			if gg.Regex.IsValidJsonObject(s) {
				err := gg.JSON.Read(s, &m)
				if nil == err && nil != m && len(m.Content) > 0 {
					instance.list = append(instance.list, m)
				}
			} else {
				err := gg.JSON.ReadFromFile(s, &m)
				if nil == err && nil != m && len(m.Content) > 0 {
					instance.list = append(instance.list, m)
				}
			}
			continue
		}
	}
	return
}

func (instance *RequestMessageList) String() string {
	return gg.JSON.Stringify(instance.list)
}

func (instance *RequestMessageList) Map() (response []map[string]interface{}) {
	_ = gg.JSON.Read(instance.String(), &response)
	return
}

func (instance *RequestMessageList) Add(item *RequestMessage) *RequestMessageList {
	if nil != instance && nil != item {
		instance.list = append(instance.list, item)
	}
	return instance
}

// Count returns the number of non-nil elements in the RequestMessageList.
func (instance *RequestMessageList) Count() (response int) {
	if nil != instance && len(instance.list) > 0 {
		response = len(instance.list)
	}
	return
}

func (instance *RequestMessageList) ForEach(f func(item *RequestMessage) any) {
	if nil != instance && nil != instance.list {
		for _, item := range instance.list {
			if nil != item {
				if nil != f(item) {
					break
				}
			}
		}
	}
}

// Length calculates the total length of the content across all non-nil RequestMessage objects in the list.
func (instance *RequestMessageList) Length() (response int) {
	if nil != instance && len(instance.list) > 0 {
		for _, m := range instance.list {
			if nil != m {
				response += m.Length()
			}
		}
	}
	return
}

func (instance *RequestMessageList) GetSystem() (response *RequestMessage) {
	if nil != instance && nil != instance.list {
		for _, m := range instance.list {
			if nil != m {
				if m.Role == RoleSystem {
					response = m
					break
				}
			}
		}
	}
	return
}

func (instance *RequestMessageList) SetSystem(text string) *RequestMessageList {
	if nil != instance && nil != instance.list {
		item := instance.GetSystem()
		if nil != item {
			item.Content = text
		} else {
			item = &RequestMessage{
				Role:    RoleSystem,
				Content: text,
			}
			list := make([]*RequestMessage, 0)
			list = append(list, item)
			list = append(list, instance.list...)
			instance.list = list
		}
	}
	return instance
}

func (instance *RequestMessageList) GetUser() (response *RequestMessage) {
	if nil != instance && nil != instance.list {
		for i := len(instance.list) - 1; i >= 0; i-- {
			if nil != instance.list[i] {
				if instance.list[i].Role == RoleUser {
					response = instance.list[i]
					break
				}
			}
		}
	}
	return
}

func (instance *RequestMessageList) SetUser(text string, images ...string) *RequestMessageList {
	if nil != instance && nil != instance.list {
		item := &RequestMessage{
			Role:    RoleUser,
			Content: text,
			Images:  images,
		}
		instance.list = append(instance.list, item)
	}
	return instance
}
