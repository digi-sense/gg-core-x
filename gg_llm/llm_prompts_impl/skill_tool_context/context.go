package skill_tool_context

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_prompts/skills"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_prompts_impl/skill_tool_context/pkg_tool_context"
	_ "embed"
)

const UID = "context"

// ---------------------------------------------------------------------------------------------------------------------
//	embed
// ---------------------------------------------------------------------------------------------------------------------

//go:embed index.json
var index string

//go:embed system.txt
var context string

// ---------------------------------------------------------------------------------------------------------------------
//	Skill
// ---------------------------------------------------------------------------------------------------------------------

// Skill is an agent specialized in writing articles
// Skill is an agent specialized in translations
var skill *skills.Skill

func Skill() *skills.Skill {
	return skill.Clone().SetUid(UID)
}

// ---------------------------------------------------------------------------------------------------------------------
//	init
// ---------------------------------------------------------------------------------------------------------------------

func init() {
	options, _ := skills.NewSkillOptions(index)

	//
	skill, _ = skills.NewSkill(options, pkg_tool_context.Package.Clone())
	skill.SetSystem(context)
}
