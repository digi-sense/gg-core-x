package pkg_newspaper

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_prompts/prompts"
	_ "embed"
)

// ---------------------------------------------------------------------------------------------------------------------
//	P R O M P T S    P A C K A G E
// ---------------------------------------------------------------------------------------------------------------------

const UID = "newspaper"

// ---------------------------------------------------------------------------------------------------------------------
//	prompt names
// ---------------------------------------------------------------------------------------------------------------------

const PromptChronicle = "chronicle"
const PromptSport = "sport"

// ---------------------------------------------------------------------------------------------------------------------
//	embed
// ---------------------------------------------------------------------------------------------------------------------

//go:embed chronicle/en.txt
var ChronicleEN string

//go:embed sport/en.txt
var SportEN string

// ---------------------------------------------------------------------------------------------------------------------
//	init
// ---------------------------------------------------------------------------------------------------------------------

var Package *prompts.PromptPackage

func init() {
	Package = prompts.NewPromptPackage(UID)
	_ = Package.Store().Add(llm_commons.PromptDefLang, PromptChronicle, ChronicleEN)
	_ = Package.Store().Add(llm_commons.PromptDefLang, PromptSport, SportEN)
}
