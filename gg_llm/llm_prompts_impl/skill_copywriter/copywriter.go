package skill_copywriter

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_prompts/skills"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_prompts_impl/skill_copywriter/pkg_newspaper"
	_ "embed"
)

const UID = "copywriter"

// ---------------------------------------------------------------------------------------------------------------------
//	embed
// ---------------------------------------------------------------------------------------------------------------------

//go:embed index.json
var index string

//go:embed system.txt
var context string

// ---------------------------------------------------------------------------------------------------------------------
//	Skill
// ---------------------------------------------------------------------------------------------------------------------

// Skill is an agent specialized in writing articles
var skill *skills.Skill

func Skill() *skills.Skill {
	return skill.Clone().SetUid(UID)
}

// ---------------------------------------------------------------------------------------------------------------------
//	init
// ---------------------------------------------------------------------------------------------------------------------

func init() {
	options, _ := skills.NewSkillOptions(index)

	//
	skill, _ = skills.NewSkill(options, pkg_newspaper.Package.Clone())
	skill.SetSystem(context)
}
