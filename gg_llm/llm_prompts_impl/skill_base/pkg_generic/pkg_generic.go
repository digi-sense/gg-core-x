package pkg_generic

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_prompts/prompts"
	_ "embed"
)

// ---------------------------------------------------------------------------------------------------------------------
//	P R O M P T S    P A C K A G E
// ---------------------------------------------------------------------------------------------------------------------

const UID = "generic"

// ---------------------------------------------------------------------------------------------------------------------
//	prompt names
// ---------------------------------------------------------------------------------------------------------------------

const PromptBasic = "basic"
const PromptContext = llm_commons.PromptDefNameContext

// ---------------------------------------------------------------------------------------------------------------------
//	embed
// ---------------------------------------------------------------------------------------------------------------------

//go:embed basic/en.txt
var BasicEN string

//go:embed context/en.txt
var ContextEN string

// ---------------------------------------------------------------------------------------------------------------------
//	init
// ---------------------------------------------------------------------------------------------------------------------

var Package *prompts.PromptPackage

func init() {
	Package = prompts.NewPromptPackage(UID)
	_ = Package.Store().Add(llm_commons.PromptDefLang, PromptBasic, BasicEN)
	_ = Package.Store().Add(llm_commons.PromptDefLang, PromptContext, ContextEN) // generate a vector context to use instead of file content
}
