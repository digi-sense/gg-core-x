package pkg_translator

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_prompts/prompts"
	_ "embed"
)

// ---------------------------------------------------------------------------------------------------------------------
//	translator
// ---------------------------------------------------------------------------------------------------------------------

const UID = "translator"

// ---------------------------------------------------------------------------------------------------------------------
//	prompt names
// ---------------------------------------------------------------------------------------------------------------------

const PromptTranslate = "translate"
const PromptTranslateFromTo = "translate_from_to"

// ---------------------------------------------------------------------------------------------------------------------
//	embed
// ---------------------------------------------------------------------------------------------------------------------

//go:embed translate/en.txt
var TranslateEN string

//go:embed translate_from_to/en.txt
var TranslateFromToEN string

//go:embed translate_from_to/options.json
var TranslateOptions string

// ---------------------------------------------------------------------------------------------------------------------
//	init
// ---------------------------------------------------------------------------------------------------------------------

var Package *prompts.PromptPackage

func init() {
	// create a package
	Package = prompts.NewPromptPackage(UID)
	_ = Package.Store().Add(llm_commons.PromptDefLang, PromptTranslate, TranslateEN)
	_ = Package.Store().AddWithOptions(llm_commons.PromptDefLang, PromptTranslateFromTo, TranslateFromToEN,
		gg.Convert.ToMap(TranslateOptions))
}
