package skill_interpreter

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_prompts/skills"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_prompts_impl/skill_interpreter/pkg_translator"
	_ "embed"
)

// ---------------------------------------------------------------------------------------------------------------------
//	embed
// ---------------------------------------------------------------------------------------------------------------------

//go:embed index.json
var index string

//go:embed system.txt
var context string

// ---------------------------------------------------------------------------------------------------------------------
//	Skill
// ---------------------------------------------------------------------------------------------------------------------

const UID = "interpreter"

// Skill is an agent specialized in translations
var skill *skills.Skill

func Skill() *skills.Skill {
	return skill.Clone().SetUid(UID)
}

// ---------------------------------------------------------------------------------------------------------------------
//	init
// ---------------------------------------------------------------------------------------------------------------------

func init() {
	options, _ := skills.NewSkillOptions(index)

	//
	skill, _ = skills.NewSkill(options, pkg_translator.Package.Clone())
	skill.SetSystem(context)
}
