package skill_prompt_engineer

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_prompts/skills"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_prompts_impl/skill_prompt_engineer/pkg_prompt_engineer"
	_ "embed"
)

const UID = "prompt_engineer"

// ---------------------------------------------------------------------------------------------------------------------
//	embed
// ---------------------------------------------------------------------------------------------------------------------

//go:embed index.json
var index string

//go:embed system.txt
var context string

// ---------------------------------------------------------------------------------------------------------------------
//	Skill
// ---------------------------------------------------------------------------------------------------------------------

// Skill is an agent specialized in writing articles
var skill *skills.Skill

func Skill() *skills.Skill {
	return skill.Clone().SetUid(UID)
}

// ---------------------------------------------------------------------------------------------------------------------
//	init
// ---------------------------------------------------------------------------------------------------------------------

func init() {
	options, _ := skills.NewSkillOptions(index)

	//
	skill, _ = skills.NewSkill(options, pkg_prompt_engineer.Package.Clone())
	skill.SetSystem(context)
}
