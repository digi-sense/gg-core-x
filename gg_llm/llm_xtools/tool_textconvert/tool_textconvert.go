package tool_textconvert

//----------------------------------------------------------------------------------------------------------------------
//	i n i t
//----------------------------------------------------------------------------------------------------------------------

var ToolTextConvert *ToolTextConvertHelper

func init() {
	ToolTextConvert = &ToolTextConvertHelper{}
}

//----------------------------------------------------------------------------------------------------------------------
//	ToolTextConvertHelper
//----------------------------------------------------------------------------------------------------------------------

// ToolTextConvertHelper provides utility functions to assist in converting and processing files in various formats to text format.
// Converted text must be passed to a chunker if RAG is required.
type ToolTextConvertHelper struct {
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------
