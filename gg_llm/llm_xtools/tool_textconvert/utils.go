package tool_textconvert

import (
	"bitbucket.org/digi-sense/gg-core"
	"fmt"
	"path/filepath"
	"strings"
)

// ConvertFileToText converts the content of a given file to text if the file type is supported.
// Supported file types are determined by their extensions, e.g., ".txt", ".pdf".
// It returns the file content as a string or an error if the conversion fails.
func ConvertFileToText(filename string) (response string, err error) {
	ext := strings.ToLower(filepath.Ext(filename))
	switch ext {
	case ".txt":
		response, err = gg.IO.ReadTextFromFile(filename)
	default:
		err = gg.Errors.Prefix(gg.PanicSystemError, fmt.Sprintf("File conversion for '%s' file not supported: ", ext))
	}
	return
}
