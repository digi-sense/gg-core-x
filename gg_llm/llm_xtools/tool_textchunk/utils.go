package tool_textchunk

import "strings"

func ChunkText(text string, maxRows int) (response []map[string]interface{}) {
	response = make([]map[string]interface{}, 0)
	if len(text) > 0 {
		rows := SplitIntoChunks(text, maxRows)
		for _, row := range rows {
			response = append(response, map[string]interface{}{"content": row})
		}
	}
	return
}

func ChunkTextAsAny(text string, maxRows int) (response []any) {
	response = make([]any, 0)
	if len(text) > 0 {
		rows := SplitIntoChunks(text, maxRows)
		for _, row := range rows {
			response = append(response, map[string]interface{}{"content": row})
		}
	}
	return
}

func SplitIntoChunks(text string, maxRows int) (response []string) {
	response = make([]string, 0)
	rows := strings.Split(text, "\n")
	count := 0
	sb := strings.Builder{}
	for _, row := range rows {
		if len(row) > 0 {
			count++
			if sb.Len() > 0 {
				sb.WriteString("\n")
			}
			sb.WriteString(row)
			if count >= maxRows {
				response = append(response, sb.String())
				count = 0              // reset
				sb = strings.Builder{} // reset
			}
		}
	}
	return
}
