package test

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_xlibs/lib_vectordb/vectordb"
	"log"
	"testing"
)

func TestDBBuilder(t *testing.T) {
	gg.Paths.SetWorkspacePath(".")
	text, err := gg.IO.ReadTextFromFile("./text_02.txt")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	filename, err := vectordb.WrapTextToFile(text, "\n\n", map[string]string{"category": "test"})
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	log.Println(filename)
}

func TestWrapContent(t *testing.T) {
	gg.Paths.SetWorkspacePath(".")
	text, err := gg.IO.ReadTextFromFile("./text_02.txt")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	items, err := vectordb.WrapContent(text, "\n\n", map[string]string{"category": "test"})
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	log.Println("ITEMS: ", len(items))
	for _, item := range items {
		log.Println(item)
	}
}
