package vectordb

import "bitbucket.org/digi-sense/gg-core"

//----------------------------------------------------------------------------------------------------------------------
//	VectorDBOptions
//----------------------------------------------------------------------------------------------------------------------

type VectorDBOptions struct {
	Name              string `json:"name"` // vector-id
	Path              string `json:"path"`
	Compress          bool   `json:"compress"`
	LLMEmbeddingModel string `json:"llm-embedding-model"`
	LLMDriver         string `json:"llm-driver"`
	LLMBaseUrl        string `json:"llm-base-url"`
}

func (instance *VectorDBOptions) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *VectorDBOptions) Map() (response map[string]interface{}) {
	if nil != instance {
		response = gg.Convert.ToMap(instance.String())
	}
	return
}
