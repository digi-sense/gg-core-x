package vectordb

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_xlibs/lib_vectordb/chromemgo"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_xlibs/lib_vectordb/vectordb_commons"
	"fmt"
	"sort"
	"strings"
)

const (
	DriverChromemGo = "chromem-go"
)

//----------------------------------------------------------------------------------------------------------------------
//	IVectorDB
//----------------------------------------------------------------------------------------------------------------------

// New initializes and returns an implementation of IVectorDB based on the specified driver and its arguments.
func New(driver string, args ...interface{}) (response vectordb_commons.IVectorDB, err error) {
	switch driver {
	case DriverChromemGo:
		response, err = chromemgo.NewChromemDB(args...)
	default:
		err = gg.Errors.Prefix(gg.PanicSystemError, fmt.Sprintf("Invalid driver: '%s'", driver))
	}
	return
}

func WrapContent(doc interface{}, options ...interface{}) (response []*vectordb_commons.DBResult, err error) {
	response = make([]*vectordb_commons.DBResult, 0)
	// document
	if cd, ok := doc.(vectordb_commons.DBResult); ok {
		response = append(response, &cd)
		return
	}
	if cd, ok := doc.(*vectordb_commons.DBResult); ok {
		response = append(response, cd)
		return
	}
	// string
	if s, ok := doc.(string); ok {
		if gg.Paths.IsFilePath(s) || gg.JSON.IsValidJsonObject(s) {
			// file or json
			var m map[string]interface{}
			err = gg.JSON.Read(s, &m)
			if nil == err {
				response, err = WrapContent(m)
			} else {
				// no filename and no JSON string
				response = append(response, &vectordb_commons.DBResult{Content: s})
			}
		} else {
			// text only
			splitter := "\n\n"
			metadata := make(map[string]string)
			for _, option := range options {
				if value, ok := option.(string); ok {
					splitter = value
					continue
				}
				if value, ok := option.(map[string]interface{}); ok {
					for k, v := range value {
						metadata[k] = gg.Convert.ToString(v)
					}
					continue
				}
				if value, ok := option.(map[string]string); ok {
					metadata = value
				}
			}
			response = WrapText(s, splitter, metadata)
		}
		return
	}
	// map string
	if m, ok := doc.(map[string]string); ok {
		response, err = WrapContent(gg.Convert.ToMap(gg.JSON.Stringify(m)))
		return
	}
	// map
	if m, ok := doc.(map[string]interface{}); ok {
		metadata := make(map[string]string)
		content := ""
		id := ""
		for k, v := range m {
			if k == "content" || k == "text" {
				content = gg.Convert.ToString(v)
			} else if k == "id" {
				id = gg.Convert.ToString(v)
			} else if k == "metadata" {
				meta := gg.Convert.ToMap(v)
				for mk, mv := range meta {
					metadata[mk] = gg.Convert.ToString(mv)
				}
			} else if k == "embedding" {
				// nothing to do
			} else {
				metadata[k] = gg.Convert.ToString(v)
			}
		}
		response = append(response, &vectordb_commons.DBResult{
			ID:        id,
			Metadata:  metadata,
			Embedding: nil,
			Content:   content,
		})
		return
	}

	err = gg.Errors.Prefix(gg.PanicSystemError, "Wrong document type: ")
	return
}

func WrapText(text string, splitter string, metadata map[string]string) (response []*vectordb_commons.DBResult) {
	response = make([]*vectordb_commons.DBResult, 0)
	rows := strings.Split(text, splitter)
	for _, row := range rows {
		if len(row) > 0 {
			item := &vectordb_commons.DBResult{
				Content:  row,
				Metadata: metadata,
			}
			response = append(response, item)
		}
	}
	return
}

func WrapTextToFile(text string, splitter string, metadata map[string]string) (filename string, err error) {
	filename = gg.Paths.TempPath(fmt.Sprintf("%s.jsonl", gg.Rnd.RndId()))
	err = gg.Paths.Mkdir(filename)
	if nil != err {
		return
	}

	items := WrapText(text, splitter, metadata)
	sb := strings.Builder{}
	for _, item := range items {
		sb.WriteString(item.String() + "\n")
	}
	_, err = gg.IO.WriteTextToFile(sb.String(), filename)
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	utils
//----------------------------------------------------------------------------------------------------------------------

func SortAsc(data []map[string]interface{}, field string) {
	sort.Slice(data, func(i, j int) bool {
		m1 := data[i]
		m2 := data[j]
		if v1, ok := m1[field]; ok {
			if v2, ok := m2[field]; ok {
				return gg.Compare.IsLower(v1, v2)
			}
		}
		return true
	})
}

func SortDesc(data []map[string]interface{}, field string) {
	sort.Slice(data, func(i, j int) bool {
		m1 := data[i]
		m2 := data[j]
		if v1, ok := m1[field]; ok {
			if v2, ok := m2[field]; ok {
				return gg.Compare.IsGreater(v1, v2)
			}
		}
		return true
	})
}
