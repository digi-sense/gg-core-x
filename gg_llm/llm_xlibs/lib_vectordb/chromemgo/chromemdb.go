package chromemgo

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_xlibs/lib_vectordb/vectordb_commons"
	"bitbucket.org/digi-sense/gg-core/gg_"
	"context"
	"github.com/philippgille/chromem-go"
)

const prefixContent = "search_document: "
const prefixQuery = "search_query: "

//----------------------------------------------------------------------------------------------------------------------
//	ChromemDB
//----------------------------------------------------------------------------------------------------------------------

type ChromemDB struct {
	options *ChromemDBOptions
	logger  gg_.ILogger
	schema  *ChromemDBSchema

	__db *chromem.DB
}

func NewChromemDB(args ...interface{}) (instance *ChromemDB, err error) {
	instance = new(ChromemDB)
	err = instance.init(args...)
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *ChromemDB) Options() *ChromemDBOptions {
	return instance.options
}

//----------------------------------------------------------------------------------------------------------------------
//	i v e c t o r d b
//----------------------------------------------------------------------------------------------------------------------

func (instance *ChromemDB) String() string {
	return gg.JSON.Stringify(instance.options)
}

func (instance *ChromemDB) Open() (err error) {
	if nil != instance {
		_, err = instance.db()
	}
	return
}

func (instance *ChromemDB) Close() {
	if nil != instance {
		if nil != instance.__db {
			instance.__db = nil
		}
	}
}

func (instance *ChromemDB) Collections() []string {
	if nil != instance {
		return instance.schema.Collections()
	}
	return []string{}
}

func (instance *ChromemDB) Load(filename string, writeMode string) (err error) {
	if nil != instance {
		collName := gg.Paths.FileName(filename, false)
		if len(collName) > 0 {
			var coll vectordb_commons.IVectorDBCollection
			if writeMode == vectordb_commons.WriteModeReset {
				_ = instance.DeleteCollection(collName)
			}
			coll, err = instance.GetOrCreateCollection(collName)
			if nil == err {
				switch writeMode {
				case vectordb_commons.WriteModeOnlyGreater:
					// write, only if the file has different number of items
					if CountDocuments(filename) != coll.Count() {
						// remove collection
						_ = instance.DeleteCollection(collName)
						coll, err = instance.GetOrCreateCollection(collName)
						if nil == err {
							if ok, _ := gg.Paths.Exists(filename); ok {
								err = coll.InsertDocuments(context.Background(), filename)
							}
						}
					}
				case vectordb_commons.WriteModeInsert:
					// append to the current collection
					if ok, _ := gg.Paths.Exists(filename); ok {
						err = coll.InsertDocuments(context.Background(), filename)
					}
				case vectordb_commons.WriteModeUpdate:
					// append to the current collection
					if ok, _ := gg.Paths.Exists(filename); ok {
						err = coll.UpdateDocuments(context.Background(), filename)
					}
				default:
					// append to the current collection
					if ok, _ := gg.Paths.Exists(filename); ok {
						err = coll.UpdateDocuments(context.Background(), filename)
					}
				}
			}
		}
	}
	return
}

func (instance *ChromemDB) AddToCollection(collName string, writeMode string, docs ...interface{}) (err error) {
	if nil != instance {
		if len(collName) > 0 {

			instance.schema.CollectionSetHash(collName, docs...)

			var coll vectordb_commons.IVectorDBCollection
			if writeMode == vectordb_commons.WriteModeReset {
				_ = instance.DeleteCollection(collName)
			}
			coll, err = instance.GetOrCreateCollection(collName)
			if nil == err {
				switch writeMode {
				case vectordb_commons.WriteModeOnlyGreater:
					// write, only if the file has different number of items
					if len(docs) != coll.Count() {
						// remove collection
						_ = instance.DeleteCollection(collName)
						coll, err = instance.GetOrCreateCollection(collName)
						if nil == err {
							err = coll.InsertDocuments(context.Background(), docs...)
						}
					}
				case vectordb_commons.WriteModeInsert:
					// append to the current collection
					err = coll.InsertDocuments(context.Background(), docs...)
				case vectordb_commons.WriteModeUpdate:
					// append to the current collection
					err = coll.UpdateDocuments(context.Background(), docs...)
				default:
					// append to the current collection
					err = coll.UpdateDocuments(context.Background(), docs...)
				}
			}
		}
	}
	return
}

func (instance *ChromemDB) DeleteCollection(name string) (err error) {
	if nil != instance {
		var db *chromem.DB
		db, err = instance.db()
		if nil != err {
			return
		}

		err = db.DeleteCollection(name)
		// remove collection from schema
		instance.schema.CollectionRemove(name)
	}
	return
}

func (instance *ChromemDB) GetCollectionHash(collName string) string {
	if nil != instance {
		return instance.schema.CollectionGetHash(collName)
	}
	return ""
}

func (instance *ChromemDB) GetCollection(name string, params ...interface{}) (response vectordb_commons.IVectorDBCollection, err error) {
	if nil != instance && len(name) > 0 {

		var db *chromem.DB
		db, err = instance.db()
		if nil != err {
			return
		}

		embeddingFunc, _ := parseParams(params...)
		embeddingFunc = instance.validateEmbeddingFunc(embeddingFunc)
		c := db.GetCollection(name, embeddingFunc)
		if nil != c {
			response = &ChromemDBCollection{
				options:    instance.options,
				name:       c.Name,
				db:         db,
				collection: c,
			}
		}

		// add collection to schema
		instance.schema.CollectionAdd(name)
	}
	return
}

func (instance *ChromemDB) GetOrCreateCollection(name string, params ...interface{}) (response vectordb_commons.IVectorDBCollection, err error) {
	if nil != instance && len(name) > 0 {

		var db *chromem.DB
		db, err = instance.db()
		if nil != err {
			return
		}

		embeddingFunc, metadata := parseParams(params...)
		embeddingFunc = instance.validateEmbeddingFunc(embeddingFunc)
		c, e := db.GetOrCreateCollection(name, metadata, embeddingFunc)
		if nil != e {
			err = e
			return
		}

		if nil != c {
			response = &ChromemDBCollection{
				options:    instance.options,
				name:       c.Name,
				db:         db,
				collection: c,
			}
		}

		// add collection to schema
		instance.schema.CollectionAdd(name)
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *ChromemDB) init(args ...interface{}) (err error) {
	if nil != instance {

		for _, arg := range args {
			if l, ok := arg.(gg_.ILogger); ok {
				instance.logger = l
				continue
			}
			if o, ok := arg.(*ChromemDBOptions); ok {
				instance.options = o
				continue
			}
			if s, ok := arg.(string); ok {
				err = gg.JSON.Read(s, &instance.options)
				if nil != err {
					return
				}
				continue
			}
			if m, ok := arg.(map[string]interface{}); ok {
				err = gg.JSON.Read(m, &instance.options)
				if nil != err {
					return
				}
				continue
			}
			if iSerializable, ok := arg.(gg_.ISerializable); ok {
				err = gg.JSON.Read(iSerializable.Map(), &instance.options)
				if nil != err {
					return
				}
				continue
			}
		}

		if nil == instance.logger {
			filename := gg.Paths.WorkspacePath("./logging/chromemdb.log")
			instance.logger = gg.Log.NewNoRotate("info", filename)
			instance.logger.Info("ChromemDB.init: Created into local logger.")
		}

		if nil == instance.options {
			instance.options = &ChromemDBOptions{
				Name: gg.Rnd.Uuid(),
				Path: "./db",
			}
		}

	}
	return
}

func (instance *ChromemDB) checkOptions() (err error) {
	if nil != instance {
		if nil == instance.options {
			err = gg.Errors.Prefix(gg_.PanicSystemError, "Missing options: ")
			return
		}
		if len(instance.options.Path) > 0 {
			instance.options.Path = gg.Paths.WorkspacePath(instance.options.Path) // absolutize
		}
	}
	return
}

func (instance *ChromemDB) db() (response *chromem.DB, err error) {
	if nil != instance {
		if nil == instance.__db {
			err = instance.checkOptions()
			if nil != err {
				return
			}

			if len(instance.options.Path) > 0 {
				instance.__db, err = chromem.NewPersistentDB(instance.options.Path, instance.options.Compress)
				if nil != err {
					return
				}
			} else {
				instance.__db = chromem.NewDB()
			}

			// init schema
			instance.schema, err = NewChromemDBSchema(instance.options.Name, instance.options.Path)
			if nil != err {
				return
			}
		}
		response = instance.__db
	}
	return
}

func (instance *ChromemDB) validateEmbeddingFunc(embeddingFunc chromem.EmbeddingFunc) (response chromem.EmbeddingFunc) {
	if nil == embeddingFunc {
		if len(instance.options.LLMEmbeddingModel) > 0 {
			switch instance.options.LLMDriver {
			case "ollama":
				response = chromem.NewEmbeddingFuncOllama(instance.options.LLMEmbeddingModel, instance.options.LLMBaseUrl)
			default:

			}
		}
	} else {
		response = embeddingFunc
	}
	return
}
