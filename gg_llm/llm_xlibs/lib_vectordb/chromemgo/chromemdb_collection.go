package chromemgo

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_xlibs/lib_vectordb/vectordb_commons"
	"context"
	"errors"
	"github.com/philippgille/chromem-go"
	"runtime"
	"strings"
)

//----------------------------------------------------------------------------------------------------------------------
//	ChromemDBCollection
//----------------------------------------------------------------------------------------------------------------------

type ChromemDBCollection struct {
	name       string
	options    *ChromemDBOptions
	db         *chromem.DB
	collection *chromem.Collection
}

func (instance *ChromemDBCollection) String() string {
	return gg.JSON.Stringify(instance.Map())
}

func (instance *ChromemDBCollection) Map() map[string]interface{} {
	return map[string]interface{}{
		"name":    instance.name,
		"count":   instance.Count(),
		"db-path": instance.options.Path,
	}
}

func (instance *ChromemDBCollection) Count() (response int) {
	if nil != instance && nil != instance.collection {
		response = instance.collection.Count()
	}
	return
}

func (instance *ChromemDBCollection) GetById(ctx context.Context, id string) (response *vectordb_commons.DBResult, err error) {
	if nil != instance && nil != instance.collection {
		var item chromem.Document
		item, err = instance.collection.GetByID(ctx, id)
		if nil == err {
			response = &vectordb_commons.DBResult{
				ID:         item.ID,
				Metadata:   item.Metadata,
				Embedding:  item.Embedding,
				Content:    item.Content,
				Similarity: 1,
			}
		}
	}
	return
}

func (instance *ChromemDBCollection) DeleteById(ctx context.Context, id string) (err error) {
	if nil != instance && nil != instance.collection {
		err = instance.collection.Delete(ctx, nil, nil, id)
	}
	return
}

func (instance *ChromemDBCollection) Delete(ctx context.Context, params ...interface{}) (err error) {
	if nil != instance && nil != instance.collection {
		where, whereDocument := parseDeleteParams(params...)
		err = instance.collection.Delete(ctx, where, whereDocument)
	}
	return
}

func (instance *ChromemDBCollection) InsertDocument(ctx context.Context, doc interface{}) (err error) {
	if nil != instance && nil != instance.collection && nil != doc {
		document, e := ParseDocument(doc)
		if nil != document {
			if nil == e && instance.existsDocument(document) {
				e = errors.New("document already exists")
			}
			if nil != e {
				err = e
				return
			}
			err = instance.collection.AddDocument(ctx, *document)
		}
	}
	return
}

func (instance *ChromemDBCollection) InsertDocuments(ctx context.Context, docs ...interface{}) (err error) {
	if nil != instance && nil != instance.collection && len(docs) > 0 {
		documents, e := instance.mapDocuments(true, docs...)
		if nil != e {
			err = e
			return
		}
		if len(documents) > 0 {
			err = instance.collection.AddDocuments(ctx, documents, runtime.NumCPU())
		}
	}
	return
}

func (instance *ChromemDBCollection) UpdateDocument(ctx context.Context, doc interface{}) (err error) {
	if nil != instance && nil != instance.collection && nil != doc {
		document, e := ParseDocument(doc)
		if nil != document {
			_ = instance.deleteDocument(document) // try to delete existing
			if nil != e {
				err = e
				return
			}
			err = instance.collection.AddDocument(ctx, *document)
		}
	}
	return
}

func (instance *ChromemDBCollection) UpdateDocuments(ctx context.Context, docs ...interface{}) (err error) {
	if nil != instance && nil != instance.collection && len(docs) > 0 {
		documents, e := instance.mapDocuments(false, docs...)
		if nil != e {
			err = e
			return
		}
		if len(documents) > 0 {
			for _, document := range documents {
				_ = instance.deleteDocument(&document)
			}
			err = instance.collection.AddDocuments(ctx, documents, runtime.NumCPU())
		}
	}
	return
}

func (instance *ChromemDBCollection) Query(ctx context.Context, params ...interface{}) (response []*vectordb_commons.DBResult, err error) {
	if nil != instance && nil != instance.collection {
		queryText, maxResults, where, whereDocument := parseQueryParams(params...)

		if len(queryText) > 0 {
			if !strings.HasPrefix(queryText, strings.TrimSpace(prefixQuery)) {
				queryText = prefixQuery + strings.TrimSpace(queryText)
			}
			var data []chromem.Result
			data, err = instance.collection.Query(ctx, queryText, maxResults, where, whereDocument)
			if nil != err {
				return
			}
			for _, item := range data {
				response = append(response, &vectordb_commons.DBResult{
					ID:         item.ID,
					Metadata:   item.Metadata,
					Embedding:  item.Embedding,
					Content:    item.Content,
					Similarity: item.Similarity,
				})
			}
		} else {
			err = gg.Errors.Prefix(gg.PanicSystemError, "Missing Query: ")
		}
	}
	return
}

func (instance *ChromemDBCollection) mapDocuments(onlyNew bool, docs ...interface{}) (response []chromem.Document, err error) {
	response, err = MapDocuments(func(index int, document *chromem.Document) (mResp *chromem.Document, mErr error) {
		if nil != document {
			if len(document.ID) == 0 {
				document.ID = gg.Rnd.RndId() //gg.Convert.ToString(index)
			}
			if len(document.Content) > 0 {
				if !strings.HasPrefix(document.Content, strings.TrimSpace(prefixContent)) {
					document.Content = prefixContent + strings.TrimSpace(document.Content)
				}
			}
			mErr = ValidateDocument(document)
			if nil == mErr {
				if onlyNew {
					if !instance.existsDocument(document) {
						mResp = document
					}
				} else {
					mResp = document
				}
			}
		}
		return
	}, docs...)
	return
}

func (instance *ChromemDBCollection) existsDocument(document *chromem.Document) bool {
	if nil != document {
		item, _ := instance.GetById(context.Background(), document.ID)
		if nil == item {
			if uid, ok := document.Metadata["uid"]; ok {
				items, _ := instance.Query(context.Background(), map[string]interface{}{"uid": uid})
				return len(items) > 0
			} else {
				return false // cannot check UID
			}
		}
	}
	return true
}

func (instance *ChromemDBCollection) deleteDocument(document *chromem.Document) *vectordb_commons.DBResult {
	if nil != document {
		item, _ := instance.GetById(context.Background(), document.ID)
		if nil != item {
			_ = instance.DeleteById(context.Background(), document.ID)
			return item
		}

		if uid, ok := document.Metadata["uid"]; ok {
			items, _ := instance.Query(context.Background(), map[string]interface{}{"uid": uid})
			if len(items) == 1 {
				_ = instance.DeleteById(context.Background(), items[0].ID)
			}
		}
	}
	return nil
}
