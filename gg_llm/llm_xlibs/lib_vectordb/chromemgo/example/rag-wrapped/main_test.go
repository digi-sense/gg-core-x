package test

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_xlibs/lib_vectordb/chromemgo"
	"context"
	"fmt"
	"log"
	"strings"
	"testing"
	"time"
)

const (
	question = "When did the Bigelow Company exist?"
)

func TestChromeDB(t *testing.T) {
	gg.Paths.SetWorkspacePath("./")
	ctx := context.Background()

	db, err := chromemgo.NewChromemDB("options.json")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(db)

	collection, err := db.GetOrCreateCollection("Wikipedia")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	if collection.Count() == 0 {
		fmt.Println("ADDING DOCUMENTS....")
		// load database
		err = collection.InsertDocuments(ctx, "./db/dbpedia_sample.jsonl")
		if nil != err {
			t.Error(err)
			t.FailNow()
		}
	}
	fmt.Println("COLLECTION:", collection)

	start := time.Now()
	log.Println("Querying chromem-go...")
	where := map[string]interface{}{"category": "Company"}
	// "nomic-embed-text" specific prefix (not required with OpenAI's or other models)
	docRes, err := collection.Query(ctx, question, 2, where)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	log.Println("Search (incl query embedding) took", time.Since(start))

	// Print the retrieved documents and their similarity to the question.
	for i, res := range docRes {
		// Cut off the prefix we added before adding the document (see comment above).
		// This is specific to the "nomic-embed-text" model.
		content := strings.TrimPrefix(res.Content, "search_document: ")
		log.Printf("Document %d (similarity: %f): \"%s\"\n", i+1, res.Similarity, content)
	}

	// Now we can ask the LLM again, augmenting the question with the knowledge we retrieved.
	// In this example we just use both retrieved documents as context.
	contexts := []string{docRes[0].Content, docRes[1].Content}
	log.Println("Asking LLM with augmented question...")
	reply := askLLM(ctx, contexts, question)
	log.Printf("Reply after augmenting the question with knowledge: \n\"" + reply + "\"\n")
}
