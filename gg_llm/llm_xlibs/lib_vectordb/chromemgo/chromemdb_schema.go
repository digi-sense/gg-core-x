package chromemgo

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_xlibs/lib_vectordb/vectordb_commons"
	"fmt"
	"sync"
)

// ---------------------------------------------------------------------------------------------------------------------
//  ChromemDBSchema
// ---------------------------------------------------------------------------------------------------------------------

type ChromemDBSchema struct {
	Name           string            `json:"name"`
	Path           string            `json:"path"`
	CollectionData map[string]string `json:"collections"` // name , hash

	filename string
	mux      *sync.Mutex
}

func NewChromemDBSchema(name, path string) (instance *ChromemDBSchema, err error) {
	instance = new(ChromemDBSchema)
	instance.Name = name
	instance.Path = path
	err = instance.init()
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *ChromemDBSchema) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *ChromemDBSchema) Map() (response map[string]interface{}) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		response = gg.Convert.ToMap(instance.String())
	}
	return
}

func (instance *ChromemDBSchema) Collections() (response []string) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		for k := range instance.CollectionData {
			response = append(response, k)
		}
	}
	return
}

func (instance *ChromemDBSchema) CollectionAdd(collection string) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		if _, ok := instance.CollectionData[collection]; !ok {
			instance.CollectionData[collection] = ""

			_ = instance.save()
		}
	}
}

func (instance *ChromemDBSchema) CollectionRemove(collection string) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		if _, ok := instance.CollectionData[collection]; ok {
			delete(instance.CollectionData, collection)

			_ = instance.save()
		}
	}
}

func (instance *ChromemDBSchema) CollectionSetHash(collection string, data ...any) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		instance.CollectionData[collection] = vectordb_commons.Hash(data...)
		_ = instance.save()
	}
}

func (instance *ChromemDBSchema) CollectionGetHash(collection string) string {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		if hash, ok := instance.CollectionData[collection]; ok {
			return hash
		}
	}
	return ""
}

// ---------------------------------------------------------------------------------------------------------------------
//  p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *ChromemDBSchema) init() (err error) {
	if nil != instance {
		instance.mux = new(sync.Mutex)
		instance.CollectionData = make(map[string]string)
		instance.filename = gg.Paths.Concat(instance.Path, fmt.Sprintf("%s.json", instance.Name))
		err = gg.Paths.Mkdir(instance.filename)
		if nil != err {
			return
		}

		// try load existing
		if b, _ := gg.Paths.Exists(instance.filename); b {
			err = gg.JSON.ReadFromFile(instance.filename, &instance)
		} else {
			err = instance.save()
		}
	}
	return
}

func (instance *ChromemDBSchema) save() (err error) {
	if nil != instance {
		_, err = gg.IO.WriteTextToFile(gg.JSON.Stringify(instance), instance.filename)
	}
	return
}
