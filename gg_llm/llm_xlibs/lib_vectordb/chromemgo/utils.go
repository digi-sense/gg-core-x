package chromemgo

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_"
	"fmt"
	"github.com/philippgille/chromem-go"
)

func CountDocuments(docs ...interface{}) (response int) {
	response, _ = countDocuments(docs...)
	return
}

func MapDocuments(callback func(index int, document *chromem.Document) (*chromem.Document, error), docs ...interface{}) (response []chromem.Document, err error) {
	response = make([]chromem.Document, 0)
	if nil != callback {
		parseDocs, e := parseDocuments(docs...)
		if nil != e {
			err = e
			return
		}
		for i, doc := range parseDocs {
			if nil != doc {
				doc, err = callback(i+1, doc)
				if nil != err {
					return
				}
				if nil != doc {
					response = append(response, *doc)
				}
			}
		}
	}
	return
}

func ValidateDocument(doc *chromem.Document) (err error) {
	if nil == doc {
		err = gg.Errors.Prefix(gg.PanicSystemError, "NULL DOCUMENT: ")
		return
	}
	if len(doc.ID) > 0 && len(doc.Content) > 0 {
		return
	} else {
		err = gg.Errors.Prefix(gg.PanicSystemError, fmt.Sprintf("This document '%s' is missing ID or CONTENT: ", doc.ID))
	}
	return
}

func parseParams(params ...interface{}) (embeddingFunc chromem.EmbeddingFunc, metadata map[string]string) {
	for _, param := range params {
		if e, ok := param.(chromem.EmbeddingFunc); ok {
			embeddingFunc = e
			continue
		}
		if m, ok := param.(map[string]string); ok {
			metadata = m
			continue
		}
	}
	return
}

func parseDeleteParams(params ...interface{}) (where map[string]string, whereDocument map[string]string) {
	for _, param := range params {
		if v, ok := param.(map[string]string); ok {
			if nil == where {
				where = v
			} else {
				whereDocument = v
			}
			continue
		}
		if v, ok := param.(map[string]interface{}); ok {
			if nil == where {
				where = make(map[string]string)
				for key, value := range v {
					where[key] = gg.Convert.ToString(value)
				}
			} else {
				whereDocument = make(map[string]string)
				for key, value := range v {
					whereDocument[key] = gg.Convert.ToString(value)
				}
			}
			continue
		}
	}
	return
}

func parseQueryParams(params ...interface{}) (queryText string, maxResults int, where map[string]string, whereDocument map[string]string) {
	for _, param := range params {
		if v, ok := param.(string); ok {
			queryText = v
			continue
		}
		if v, ok := param.(int); ok {
			maxResults = v
			continue
		}
		if v, ok := param.(map[string]string); ok {
			if nil == where {
				where = v
			} else {
				whereDocument = v
			}
			continue
		}
		if v, ok := param.(map[string]interface{}); ok {
			if nil == where {
				where = make(map[string]string)
				for key, value := range v {
					where[key] = gg.Convert.ToString(value)
				}
			} else {
				whereDocument = make(map[string]string)
				for key, value := range v {
					whereDocument[key] = gg.Convert.ToString(value)
				}
			}
			continue
		}
	}
	return
}

func ParseDocument(doc interface{}) (response *chromem.Document, err error) {
	// document
	if cd, ok := doc.(chromem.Document); ok {
		response = &cd
		return
	}
	if cd, ok := doc.(*chromem.Document); ok {
		response = cd
		return
	}
	// string
	if s, ok := doc.(string); ok {
		// file or json
		var m map[string]interface{}
		err = gg.JSON.Read(s, &m)
		if nil == err {
			response, err = ParseDocument(m)
		} else {
			// no filename and no JSON string
			response = &chromem.Document{
				ID:        "",
				Metadata:  nil,
				Embedding: nil,
				Content:   s,
			}
		}
		return
	}
	// map
	if m, ok := doc.(map[string]interface{}); ok {
		metadata := make(map[string]string)
		content := ""
		id := ""
		for k, v := range m {
			if k == "content" || k == "text" {
				content = gg.Convert.ToString(v)
			} else if k == "id" {
				id = gg.Convert.ToString(v)
			} else if k == "metadata" {
				meta := gg.Convert.ToMap(v)
				for mk, mv := range meta {
					metadata[mk] = gg.Convert.ToString(mv)
				}
			} else if k == "embedding" {
				// nothing to do
			} else {
				metadata[k] = gg.Convert.ToString(v)
			}
		}
		response = &chromem.Document{
			ID:        toNumId(id),
			Metadata:  metadata,
			Embedding: nil,
			Content:   content,
		}
		return
	}
	// map string
	if m, ok := doc.(map[string]string); ok {
		metadata := make(map[string]string)
		content := ""
		id := ""
		for k, v := range m {
			if k == "content" || k == "text" {
				content = v
			} else if k == "id" {
				id = v
			} else {
				metadata[k] = v
			}
		}
		response = &chromem.Document{
			ID:        toNumId(id),
			Metadata:  metadata,
			Embedding: nil,
			Content:   content,
		}
		return
	}

	err = gg.Errors.Prefix(gg.PanicSystemError, "Wrong document type: ")
	return
}

func parseDocuments(docs ...interface{}) (response []*chromem.Document, err error) {
	response = make([]*chromem.Document, 0)
	if len(docs) == 1 && gg_.IsStringType(docs[0]) {
		i := docs[0]
		if s, ok := i.(string); ok {
			err = gg.JSON.Decode(s, func(index int, m map[string]interface{}) {
				if _, b := m["id"]; !b {
					m["id"] = gg.Convert.ToString(index)
				}
				item, e := ParseDocument(m)
				if e != nil {
					return
				}
				response = append(response, item)
			})
			return
		}
	} else {
		for _, doc := range docs {
			item, e := ParseDocument(doc)
			if e != nil {
				err = e
				return
			}
			response = append(response, item)
		}
	}
	return
}

func countDocuments(docs ...interface{}) (response int, err error) {
	response = 0
	if len(docs) == 1 {
		i := docs[0]
		if s, ok := i.(string); ok {
			err = gg.JSON.Decode(s, func(index int, m map[string]interface{}) {
				response++
			})
			return
		}
	} else {
		response = len(docs)
	}
	return
}

func toNumId(v string) string {
	if len(v) > 0 {
		n := gg.Convert.ToIntDef(v, -1)
		if n > 0 {
			return v
		}
	}
	return ""
}
