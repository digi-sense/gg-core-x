package chromemgo

import "bitbucket.org/digi-sense/gg-core"

//----------------------------------------------------------------------------------------------------------------------
//	ChromemDBOptions
//----------------------------------------------------------------------------------------------------------------------

type ChromemDBOptions struct {
	Name              string `json:"name"`
	Path              string `json:"path"`
	Compress          bool   `json:"compress"`
	LLMEmbeddingModel string `json:"llm-embedding-model"`
	LLMDriver         string `json:"llm-driver"`
	LLMBaseUrl        string `json:"llm-base-url"`
}

func (instance *ChromemDBOptions) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *ChromemDBOptions) Map() (response map[string]interface{}) {
	if nil != instance {
		response = gg.Convert.ToMap(instance.String())
	}
	return
}
