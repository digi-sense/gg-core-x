package vectordb_commons

import "bitbucket.org/digi-sense/gg-core"

// DBResult represents a single result from a query.
type DBResult struct {
	ID        string            `json:"id,omitempty"`
	Metadata  map[string]string `json:"metadata,omitempty"`
	Embedding []float32         `json:"embedding,omitempty"`
	Content   string            `json:"content"`

	// The cosine similarity between the query and the document.
	// The higher the value, the more similar the document is to the query.
	// The value is in the range [-1, 1].
	Similarity float32 `json:"similarity,omitempty"`
}

func (instance *DBResult) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *DBResult) Map() map[string]interface{} {
	return gg.Convert.ToMap(instance.String())
}
