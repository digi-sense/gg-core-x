package vectordb_commons

const (
	WriteModeInsert      = "insert" // always append if not exists
	WriteModeUpdate      = "update" // insert or update
	WriteModeReset       = "reset"  // always overwrite
	WriteModeOnlyGreater = "only_greater"
)
