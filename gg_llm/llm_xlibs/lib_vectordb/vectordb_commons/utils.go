package vectordb_commons

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bytes"
	"fmt"
)

func Hash(docs ...interface{}) (response string) {
	if len(docs) > 0 {
		var buff bytes.Buffer
		for _, item := range docs {
			if nil != item {
				data := []byte(fmt.Sprintf("%v", item))
				if len(data) > 0 {
					buff.Write(data)
				}
			}
		}
		response, _ = gg_.IOReadHashFromBytes(buff.Bytes())
	}
	return
}
