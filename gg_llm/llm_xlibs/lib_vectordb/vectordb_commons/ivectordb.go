package vectordb_commons

import (
	"context"
)

type IVectorDB interface {
	Open() (err error)
	Close()
	String() string // return serialized options
	Load(filename string, writeMode string) (err error)
	AddToCollection(collName string, writeMode string, docs ...interface{}) (err error)
	DeleteCollection(name string) (err error)
	GetCollection(name string, params ...interface{}) (response IVectorDBCollection, err error)
	GetOrCreateCollection(name string, params ...interface{}) (response IVectorDBCollection, err error)
	Collections() []string
	GetCollectionHash(collName string) string
}

type IVectorDBCollection interface {
	Count() (response int)

	GetById(ctx context.Context, id string) (response *DBResult, err error)
	InsertDocument(ctx context.Context, doc interface{}) error
	InsertDocuments(ctx context.Context, docs ...interface{}) error
	UpdateDocument(ctx context.Context, doc interface{}) (err error)
	UpdateDocuments(ctx context.Context, docs ...interface{}) (err error)
	Delete(ctx context.Context, params ...interface{}) (err error)
	DeleteById(ctx context.Context, id string) (err error)

	Query(ctx context.Context, params ...interface{}) (response []*DBResult, err error)
}
