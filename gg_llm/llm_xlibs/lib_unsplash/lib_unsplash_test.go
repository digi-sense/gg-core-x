package lib_unsplash_test

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_xlibs/lib_unsplash"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_xlibs/lib_unsplash/unsplash_commons"
	"fmt"
	"testing"
)

// TestUnsplash is a function that tests the functionality of the Unsplash API.
// It is a unit test for the package containing the Unsplash API implementation.
func TestUnsplash(t *testing.T) {
	unsplash, err := lib_unsplash.NewLibUnsplash(settings())
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("UNSPLASH:", unsplash)

	response, err := unsplash.SearchOne("startup ideas")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("SEARCH RESPONSE: ", gg.JSON.Stringify(response))

	response, err = unsplash.RandomByColor("startup ideas", "red")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("SEARCH BY COLOR RESPONSE: ", gg.JSON.Stringify(response))

	response, err = unsplash.Random("startup ideas")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("SEARCH RANDOM: ", gg.JSON.Stringify(response))
}

func TestUnsplashRandom(t *testing.T) {
	unsplash, err := lib_unsplash.NewLibUnsplash(settings())
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("UNSPLASH:", unsplash)
	keywords := []string{"startup", "ideas", "genius", "build"}

	response, err := unsplash.RandomFromKeywords(keywords)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("SEARCH RESPONSE: ", gg.JSON.Stringify(response))
}

func TestSettings(t *testing.T) {
	s := settings()
	fmt.Println(s)
}

// ---------------------------------------------------------------------------------------------------------------------
//  S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func settings() *unsplash_commons.UnsplashSettings {
	response := &unsplash_commons.UnsplashSettings{
		Payload: nil,
	}
	filename := "./lib_unsplash_test_settings.json"
	s, err := gg.IO.ReadTextFromFile(filename)
	if nil != err {
		_, _ = gg.IO.WriteTextToFile(response.String(), filename)
	} else {
		_ = gg.JSON.Read(s, &response)
	}
	return response
}
