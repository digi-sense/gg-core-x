package lib_unsplash

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_xlibs/lib_unsplash/unsplash_commons"
	"bitbucket.org/digi-sense/gg-core/gg_net/gg_net_http"
	"errors"
	"fmt"
	"strings"
)

type LibUnsplash struct {
	settings *unsplash_commons.UnsplashSettings
}

func NewLibUnsplash(args ...interface{}) (instance *LibUnsplash, err error) {
	instance = new(LibUnsplash)
	err = instance.init(args...)

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LibUnsplash) String() string {
	return gg.JSON.Stringify(instance.Map())
}

func (instance *LibUnsplash) Map() (response map[string]interface{}) {
	response = gg.Convert.ToMap(instance.settings)
	return
}

func (instance *LibUnsplash) ProducerSettings() (response *llm_commons.LLMDriverOptions) {
	if nil != instance && nil != instance.settings {
		if nil == instance.settings.LLMDriverOptions {
			instance.settings.LLMDriverOptions = new(llm_commons.LLMDriverOptions)
		}
		return instance.settings.LLMDriverOptions
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	search
// ---------------------------------------------------------------------------------------------------------------------

// Search https://unsplash.com/documentation#search-photos
func (instance *LibUnsplash) Search(query string) (response []map[string]interface{}, err error) {
	params := getSearchParams()
	params["query"] = query
	response, err = instance.SearchWith(params)
	return
}

func (instance *LibUnsplash) SearchOne(query string) (response map[string]interface{}, err error) {
	params := getSearchParams()
	params["query"] = query
	params["per_page"] = 1

	var list []map[string]interface{}
	list, err = instance.SearchWith(params)
	if nil == err && len(list) > 0 {
		response = list[0]
	}
	return
}

func (instance *LibUnsplash) SearchBlackAndWhite(query string) (response []map[string]interface{}, err error) {
	response, err = instance.SearchByColor(query, "black_and_white")
	return
}

// SearchByColor Filter results by color.
// Valid values are:
// black_and_white, black, white, yellow, orange, red, purple, magenta, green, teal, and blue.
func (instance *LibUnsplash) SearchByColor(query, color string) (response []map[string]interface{}, err error) {
	params := getSearchParams()
	params["query"] = query
	params["color"] = color
	response, err = instance.SearchWith(params)
	return
}

func (instance *LibUnsplash) SearchWith(params map[string]interface{}) (response []map[string]interface{}, err error) {
	url := instance.url(unsplash_commons.ApiImageSearch)
	url += "?" + gg.Convert.ToUrlQuery(params)

	var m map[string]interface{}
	m, err = instance.get(url)

	if nil == err && nil != m {
		if results, ok := m["results"]; ok {
			list := gg.Convert.ToArray(results)
			response = make([]map[string]interface{}, 0)
			for _, item := range list {
				response = append(response, gg.Convert.ToMap(item))
			}
		}
	}
	return
}

// Random https://unsplash.com/documentation#get-a-random-photo
func (instance *LibUnsplash) Random(query string) (response map[string]interface{}, err error) {
	params := getRandomParams()
	params["query"] = query
	url := instance.url(unsplash_commons.ApiPhotoRandom)
	url += "?" + gg.Convert.ToUrlQuery(params)
	response, err = instance.get(url)
	return
}

func (instance *LibUnsplash) RandomByColor(query, color string) (response map[string]interface{}, err error) {
	params := getSearchParams()
	params["query"] = query
	params["color"] = color
	params["per_page"] = 10

	var list []map[string]interface{}
	list, err = instance.SearchWith(params)
	if nil == err && len(list) > 0 {
		i := int(gg.Rnd.Between(0, int64(len(list)-1)))
		response = list[i]
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	random multiple keywords
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LibUnsplash) RandomFromKeywords(keywords []string) (response map[string]interface{}, err error) {
	query := keywords[int(gg.Rnd.Between(0, int64(len(keywords))))]
	response, err = instance.Random(query)
	if nil == response {
		response, err = instance.EnsureRandomFromKeywords(keywords, "")
	}
	return
}

func (instance *LibUnsplash) RandomByColorFromKeywords(keywords []string, color string) (response map[string]interface{}, err error) {
	query := keywords[int(gg.Rnd.Between(0, int64(len(keywords))))]
	response, err = instance.RandomByColor(query, color)
	if nil == response {
		response, err = instance.EnsureRandomFromKeywords(keywords, color)
	}
	return
}

func (instance *LibUnsplash) EnsureRandomFromKeywords(keywords []string, color string) (response map[string]interface{}, err error) {
	keywords = splitAll(keywords)
	for _, keyword := range keywords {
		if len(color) > 0 {
			response, err = instance.RandomByColor(keyword, color)
			if nil != response {
				return
			}
		} else {
			response, err = instance.Random(keyword)
			if nil != response {
				return
			}
		}
	}

	// try with last option, a random image
	response, err = instance.Random("")

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LibUnsplash) init(args ...interface{}) (err error) {
	if nil != instance {
		// auto-detect arguments
		for _, arg := range args {
			if nil == arg {
				continue
			}

			err = instance.tryAddSettings(arg)
			if nil != err {
				return
			}
		}

		if len(instance.settings.AccessKey) == 0 {
			err = gg.Errors.Prefix(gg.PanicSystemError, "LibUnsplash.init() Missing access key: ")
			return
		}

		if nil != err {
			err = gg.Errors.Prefix(err, "LibUnsplash.init() -> Error loading initialization arguments: ")
			return
		}
	}

	return
}

func (instance *LibUnsplash) tryAddSettings(arg interface{}) (err error) {
	if nil != instance && nil != arg {
		// settings
		if settings, ok := arg.(*unsplash_commons.UnsplashSettings); ok {
			instance.settings = settings
			return
		}

		if m, ok := arg.(map[string]interface{}); ok {
			err = gg.JSON.Read(m, &instance.settings)
			if nil == err && nil != instance.settings {
				instance.settings.AccessKey = gg.Maps.GetString(m, "access-key")
			}
			return
		}
	}

	return
}

func (instance *LibUnsplash) client() (client *gg_net_http.HttpClient) {
	client = gg.Net.NewHttpClient()
	client.AddHeader("Content-Type", "application/json")
	client.AddHeader("Authorization", "Client-ID "+instance.settings.AccessKey)
	return
}

func (instance *LibUnsplash) url(api string) string {
	return gg.Paths.Concat(unsplash_commons.BaseApiUrl, api)
}

func (instance *LibUnsplash) get(url string) (response map[string]interface{}, err error) {
	client := instance.client()
	resp, e := client.Get(url)
	if nil != e {
		err = e
	} else {
		response, err = checkError(resp.BodyAsMap(), e)
	}
	return
}

func (instance *LibUnsplash) post(url string, body interface{}) (response map[string]interface{}, err error) {
	client := instance.client()
	resp, e := client.Post(url, body)
	if nil != e {
		err = e
	} else {
		response, err = checkError(resp.BodyAsMap(), e)
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

// https://unsplash.com/documentation#error-messages
func checkError(m map[string]interface{}, e error) (response map[string]interface{}, err error) {
	if nil != e {
		err = e
	} else if len(m) > 0 {
		if ee, ok := m["errors"]; ok {
			errs := gg.Convert.ToArrayOfString(ee)
			if len(errs) > 0 {
				err = errors.New(fmt.Sprintf("Errors occurred [%s]:", strings.Join(errs, ", ")))
			} else {
				response = m
			}
		} else {
			response = m
		}
	}
	return
}

func splitAll(list []string) (response []string) {
	response = make([]string, 0)
	for _, s := range list {
		tokens := gg.Strings.Split(s, " ,;.\n")
		response = append(response, tokens...)
	}
	return
}

func getSearchParams() (params map[string]interface{}) {
	params = map[string]interface{}{
		"query":          "",
		"page":           -1,          // Page number to retrieve. (Optional; default: 1)
		"per_page":       5,           // Number of items per page. (Optional; default: 10)
		"order_by":       "relevant",  // How to sort the photos. (Optional; default: relevant). Valid values are latest and relevant.
		"collections":    "",          // Collection ID(‘s) to narrow search. Optional. If multiple, comma-separated.
		"content_filter": "low",       // Limit results by content safety. (Optional; default: low). Valid values are low and high.
		"color":          "",          // Filter results by color. Optional. Valid values are: black_and_white, black, white, yellow, orange, red, purple, magenta, green, teal, and blue.
		"orientation":    "landscape", // Filter by photo orientation. Optional. (Valid values: landscape, portrait, squarish)
	}
	return
}

func getRandomParams() (params map[string]interface{}) {
	params = map[string]interface{}{
		"query":    "",
		"username": "", // Limit selection to a single user.
		// "count":          1,           // The number of photos to return. (Default: 1; max: 30)
		"collections":    "",          // Collection ID(‘s) to narrow search. Optional. If multiple, comma-separated.
		"topics":         "",          // Public topic ID(‘s) to filter selection. If multiple, comma-separated
		"orientation":    "landscape", // Filter by photo orientation. Optional. (Valid values: landscape, portrait, squarish)
		"content_filter": "low",       // Limit results by content safety. (Optional; default: low). Valid values are low and high.
	}
	return
}
