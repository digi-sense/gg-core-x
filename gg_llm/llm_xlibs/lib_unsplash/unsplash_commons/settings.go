package unsplash_commons

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_commons"
)

type UnsplashSettings struct {
	*llm_commons.LLMDriverOptions // access key, secret, etc...

	Payload map[string]interface{} `json:"payload"`
}

func NewUnsplashSettings() (instance *UnsplashSettings) {
	instance = new(UnsplashSettings)
	instance.LLMDriverOptions = llm_commons.NewLLMDriverOptions()
	return
}

func (instance *UnsplashSettings) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *UnsplashSettings) Load(m map[string]interface{}) (err error) {
	if nil != instance {
		err = gg.JSON.Read(gg.JSON.Stringify(m), instance)
		if nil != err {
			return
		}

		if nil == instance.LLMDriverOptions {
			instance.LLMDriverOptions = llm_commons.NewLLMDriverOptions(m)
		}
		if nil == instance.LLMDriverOptions.OptionsCreator {
			_ = gg.JSON.Read(gg.JSON.Stringify(m), &instance.LLMDriverOptions.OptionsCreator)
		}
	}
	return
}
