package unsplash_commons

const (
	BaseApiUrl = "https://api.unsplash.com"

	ApiImageSearch = "/search/photos"
	ApiPhotoRandom = "/photos/random"
)

// 256x256, 512x512, or 1024x1024
const (
	Size256  = "256x256"
	Size512  = "512x512"
	Size1024 = "1024x1024"
)

const UnsplashModel = "unsplash"
