package unsplash_commons

import "bitbucket.org/digi-sense/gg-core"

// ---------------------------------------------------------------------------------------------------------------------
//  ModelUnsplash
// ---------------------------------------------------------------------------------------------------------------------

type ModelUnsplash struct {
	Id          string              `json:"id"`
	Slug        string              `json:"slug"`
	Name        string              `json:"name"`
	Description string              `json:"description"`
	AssetType   string              `json:"asset_types"`
	Color       string              `json:"color"`
	CreatedAt   string              `json:"created_at"`
	UpdatedAt   string              `json:"updated_at"`
	PromotedAt  string              `json:"promoted_at"`
	Likes       int                 `json:"likes"`
	Width       int                 `json:"width"`
	Height      int                 `json:"height"`
	Links       *ModelUnsplashLinks `json:"links"`
	Urls        *ModelUnsplashUrls  `json:"urls"`
	User        *ModelUnsplashUser  `json:"user"`
}

func NewModelUnsplash() *ModelUnsplash {
	instance := new(ModelUnsplash)
	instance.Links = new(ModelUnsplashLinks)
	instance.Urls = new(ModelUnsplashUrls)
	instance.User = new(ModelUnsplashUser)
	instance.User.Links = new(ModelUnsplashUserLinks)
	instance.User.ProfileImage = new(ModelUnsplashUserProfileImage)

	return instance
}

func (instance *ModelUnsplash) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *ModelUnsplash) Map() map[string]interface{} {
	return gg.Convert.ToMap(gg.JSON.Stringify(instance))
}

func (instance *ModelUnsplash) Parse(data any) *ModelUnsplash {
	if nil != instance {
		_ = gg.JSON.Read(data, &instance)
	}
	return instance
}

func (instance *ModelUnsplash) FullName() (response string) {
	if nil != instance && nil != instance.User {
		response = instance.User.FullName()
	}
	return
}

func (instance *ModelUnsplash) ProfileUrl() (response string) {
	if nil != instance && nil != instance.User {
		response = instance.User.PortfolioUrl
	}
	return
}

func (instance *ModelUnsplash) ImageUrl() (response string) {
	if nil != instance && nil != instance.Links {
		response = instance.Links.Html
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  ModelUnsplashLinks
// ---------------------------------------------------------------------------------------------------------------------

type ModelUnsplashLinks struct {
	Download         string `json:"download"`
	DownloadLocation string `json:"download_location"`
	Self             string `json:"self"`
	Html             string `json:"html"`
}

func (instance *ModelUnsplashLinks) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *ModelUnsplashLinks) Map() map[string]interface{} {
	return gg.Convert.ToMap(gg.JSON.Stringify(instance))
}

// ---------------------------------------------------------------------------------------------------------------------
//  ModelUnsplashUrls
// ---------------------------------------------------------------------------------------------------------------------

type ModelUnsplashUrls struct {
	Full    string `json:"full"`
	Raw     string `json:"raw"`
	Regular string `json:"regular"`
	Thumb   string `json:"thumb"`
	Small   string `json:"small"`
	SmallS3 string `json:"small_s3"`
}

func (instance *ModelUnsplashUrls) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *ModelUnsplashUrls) Map() map[string]interface{} {
	return gg.Convert.ToMap(gg.JSON.Stringify(instance))
}

// ---------------------------------------------------------------------------------------------------------------------
//  ModelUnsplashUser
// ---------------------------------------------------------------------------------------------------------------------

type ModelUnsplashUser struct {
	Id                         string                         `json:"id"`
	FirstName                  string                         `json:"first_name"`
	LastName                   string                         `json:"last_name"`
	Username                   string                         `json:"username"`
	UpdatedAt                  string                         `json:"updated_at"`
	Bio                        string                         `json:"bio"`
	InstagramUsername          string                         `json:"instagram_username"`
	Location                   string                         `json:"location"`
	PortfolioUrl               string                         `json:"portfolio_url"`
	TotalCollections           int                            `json:"total_collection"`
	TotalIllustrations         int                            `json:"total_illustrations"`
	TotalLikes                 int                            `json:"total_likes"`
	TotalPhotos                int                            `json:"total_photos"`
	TotalPromotedPhotos        int                            `json:"total_promoted_photos"`
	TotalPromotedIllustrations int                            `json:"total_promoted_illustrations"`
	Links                      *ModelUnsplashUserLinks        `json:"links"`
	ProfileImage               *ModelUnsplashUserProfileImage `json:"profile_image"`
	Social                     *ModelUnsplashUserSocial       `json:"social"`
}

func (instance *ModelUnsplashUser) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *ModelUnsplashUser) Map() map[string]interface{} {
	return gg.Convert.ToMap(gg.JSON.Stringify(instance))
}

func (instance *ModelUnsplashUser) FullName() (response string) {
	if nil != instance {
		response = instance.FirstName
		if len(instance.LastName) > 0 {
			response += " " + instance.LastName
		}
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  ModelUnsplashUserLinks
// ---------------------------------------------------------------------------------------------------------------------

type ModelUnsplashUserLinks struct {
	Followers string `json:"followers"`
	Following string `json:"following"`
	Html      string `json:"html"`
	Likes     string `json:"likes"`
	Photos    string `json:"photos"`
	Portfolio string `json:"portfolio"`
	Self      string `json:"self"`
}

func (instance *ModelUnsplashUserLinks) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *ModelUnsplashUserLinks) Map() map[string]interface{} {
	return gg.Convert.ToMap(gg.JSON.Stringify(instance))
}

// ---------------------------------------------------------------------------------------------------------------------
//  ModelUnsplashUserProfileImage
// ---------------------------------------------------------------------------------------------------------------------

type ModelUnsplashUserProfileImage struct {
	Large  string `json:"large"`
	Medium string `json:"medium"`
	Small  string `json:"small"`
}

func (instance *ModelUnsplashUserProfileImage) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *ModelUnsplashUserProfileImage) Map() map[string]interface{} {
	return gg.Convert.ToMap(gg.JSON.Stringify(instance))
}

// ---------------------------------------------------------------------------------------------------------------------
//  ModelUnsplashUserProfileImage
// ---------------------------------------------------------------------------------------------------------------------

type ModelUnsplashUserSocial struct {
	InstagramUsername string `json:"instagram_username"`
	PaypalEmail       string `json:"paypal_email"`
	PortfolioUrl      string `json:"portfolio_url"`
	TwitterUsername   string `json:"twitter_username"`
}

func (instance *ModelUnsplashUserSocial) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *ModelUnsplashUserSocial) Map() map[string]interface{} {
	return gg.Convert.ToMap(gg.JSON.Stringify(instance))
}
