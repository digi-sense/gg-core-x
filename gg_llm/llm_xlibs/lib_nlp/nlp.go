package lib_nlp

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_nlp"
	"bitbucket.org/digi-sense/gg-core/gg_tagcloud"
	"math"
	"strings"
)

var NLP *NLPHelper

func init() {
	NLP = NewNLPHelper()
}

type NLPHelper struct {
}

func NewNLPHelper() *NLPHelper {
	return &NLPHelper{}
}

// ---------------------------------------------------------------------------------------------------------------------
//  p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *NLPHelper) LinkFactor(text string) (response float64) {
	if nil != instance && len(text) > 0 {
		links := len(instance.Links(text))
		words := instance.WordCount(text)
		if links > 0 && words > 0 {
			factor := float64(links) / float64(words)
			response = math.Floor(factor*1000) / 10
		}
	}
	return
}

func (instance *NLPHelper) LangCode(text string) (response string) {
	if nil != instance && len(text) > 0 {
		response = llm_commons.LangCode(gg_nlp.NLP.GetLanguageCode(text))
	}
	return
}

func (instance *NLPHelper) Links(text string) (response []string) {
	response = make([]string, 0)
	if nil != instance && len(text) > 0 {
		var list []string
		list = gg.Regex.Links(text)
		for _, link := range list {
			if strings.HasPrefix(link, "http") {
				response = append(response, link)
			}
		}
	}
	return
}

func (instance *NLPHelper) Emails(text string) (response []string) {
	response = make([]string, 0)
	if nil != instance && len(text) > 0 {
		var list []string
		list = gg.Regex.Emails(text)
		for _, link := range list {
			response = append(response, link)
		}
	}
	return
}

func (instance *NLPHelper) LinksUnsubscribe(links []string) (response []string) {
	response = make([]string, 0)
	if len(links) > 0 {
		unsubscribe := []string{"unsubscribe", "gdpr", "remove", "privacy"}
		for _, link := range links {
			for _, unsub := range unsubscribe {
				if strings.Contains(link, unsub) {
					response = append(response, link)
				}
			}
		}
	}
	return
}

func (instance *NLPHelper) Keywords(text string, max int) (response []string) {
	response = make([]string, 0)
	if nil != instance && len(text) > 0 {
		keys := gg_nlp.NLP.GetKeywordsSorted(text, 4)
		for i, key := range keys {
			if i > max {
				break
			}
			key = strings.ToLower(key)
			if !gg.Arrays.Contains(key, response) {
				response = append(response, key)
			}
		}
	}
	return
}

func (instance *NLPHelper) TagCloud(text string) (response *gg_tagcloud.TagCloudList) {
	if nil != instance && len(text) > 0 {
		response = gg.TagCloud.New()
		tags := gg_nlp.NLP.GetKeywordsSorted(text, 4)
		for _, tag := range tags {
			response.Add(tag)
		}
	}
	return
}

func (instance *NLPHelper) TagCloudAsMap(text string, max int) (response map[string]interface{}) {
	response = make(map[string]interface{})
	if nil != instance && len(text) > 0 {
		tagCloud := instance.TagCloud(text)
		if len(tagCloud.Items()) > 0 {
			tagCloud.SortAsc()
			if max > 0 && len(tagCloud.Items()) > max {
				items := tagCloud.Items()[:max-1]
				for _, tag := range items {
					response[tag.Name] = tag.Score
				}
			} else {
				response = tagCloud.ItemsAsMap()
			}
		}
	}
	return
}

func (instance *NLPHelper) TagCloudAsMapAndNames(text string, max int) (tagMap map[string]interface{}, tagArray []string) {
	tagMap = make(map[string]interface{})
	if nil != instance && len(text) > 0 {
		tagCloud := instance.TagCloud(text)
		if len(tagCloud.Items()) > 0 {
			tagCloud.SortAsc()
			if max > 0 && len(tagCloud.Items()) > max {
				items := tagCloud.Items()[:max-1]
				for _, tag := range items {
					tagMap[strings.ToLower(tag.Name)] = tag.Score
				}
			} else {
				tagMap = tagCloud.ItemsAsMap()
			}
		}
		for k, _ := range tagMap {
			tagArray = append(tagArray, strings.ToLower(k))
		}
	}
	return
}

func (instance *NLPHelper) WordCount(text string) int {
	if len(text) > 0 {
		words := gg.Strings.SplitTrim(text, " .\n", " \n\r()")
		rows := make([]string, 0)
		for _, word := range words {
			if len(word) > 0 {
				rows = append(rows, word)
			}
		}
		return len(rows)
	}
	return 0
}

func (instance *NLPHelper) Summary(text string) (response string) {
	if nil != instance && len(text) > 0 {
		var sb strings.Builder
		if len(text) > 0 {
			rows := gg_nlp.NLP.SummarizeLexRankMedium(text)
			for i, row := range rows {
				clean := strings.TrimSpace(row)
				if len(clean) > 0 {
					if i > 0 {
						sb.WriteString("\n")
					}
					sb.WriteString(clean)
				}
			}
		}
		return sb.String()
	}
	return
}
