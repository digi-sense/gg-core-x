package llm_ctrl_embeddings

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_ctrl_utils"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_xlibs/lib_vectordb/chromemgo"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_xlibs/lib_vectordb/vectordb"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_xlibs/lib_vectordb/vectordb_commons"
	"bitbucket.org/digi-sense/gg-core/gg_"
	"context"
	"fmt"
	"math"
	"strings"
)

// ---------------------------------------------------------------------------------------------------------------------
//	LLMEmbeddingsRAG
// ---------------------------------------------------------------------------------------------------------------------

// LLMEmbeddingsRAG represents a structure with access to vectorDB.
// This object is the gate for the RAG for LLMs
type LLMEmbeddingsRAG struct {
	ctrlDirs *llm_ctrl_utils.CtrlDirs
	options  *llm_commons.LLMRagOptions

	isOpen  bool
	vectors map[string]vectordb_commons.IVectorDB // databases

	_logger gg_.ILogger
}

func NewLLMEmbeddingsRAG(args ...interface{}) (instance *LLMEmbeddingsRAG, err error) {
	instance = new(LLMEmbeddingsRAG)
	err = instance.init(args...)

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LLMEmbeddingsRAG) Options(dbName string) (response *llm_commons.LLMRagOptionsItem) {
	if nil != instance {
		response = instance.getOptions(dbName)
	}
	return
}

func (instance *LLMEmbeddingsRAG) VectorRoot(dbName string) (response string) {
	if nil != instance {
		response = instance.dbRoot(dbName)
	}
	return
}

func (instance *LLMEmbeddingsRAG) Open() (err error) {
	if nil != instance && !instance.isOpen {
		instance.isOpen = true
		err = instance.open()
	}
	return
}

func (instance *LLMEmbeddingsRAG) Close() {
	if nil != instance && instance.isOpen {
		for _, db := range instance.vectors {
			db.Close()
		}
		instance.vectors = nil
		instance.options = nil
		instance.isOpen = false
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	d a t a b a s e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LLMEmbeddingsRAG) DB(dbName string) (response vectordb_commons.IVectorDB, err error) {
	if nil != instance {
		_ = instance.Open()
		response, err = instance.db(dbName)
	}
	return
}

func (instance *LLMEmbeddingsRAG) DBExists(dbName string) (response bool) {
	if nil != instance {
		_ = instance.Open()
		db, e := instance.db(dbName)
		return nil == e && nil != db
	}
	return
}

func (instance *LLMEmbeddingsRAG) CollectionExists(dbName, collName string) (response bool) {
	if nil != instance && instance.DBExists(dbName) {
		db, _ := instance.db(dbName)
		if nil != db {
			coll, err := db.GetCollection(collName)
			return coll != nil && nil == err
		}
	}
	return
}

func (instance *LLMEmbeddingsRAG) CollectionHash(dbName, collName string) (response string) {
	if nil != instance && instance.DBExists(dbName) {
		db, _ := instance.db(dbName)
		if nil != db {
			response = db.GetCollectionHash(collName)
		}
	}
	return
}

func (instance *LLMEmbeddingsRAG) RemoveCollection(dbName, collName string) (err error) {
	if nil != instance && instance.DBExists(dbName) {
		db, _ := instance.db(dbName)
		if nil != db {
			err = db.DeleteCollection(collName)
		}
	}
	return
}

func (instance *LLMEmbeddingsRAG) Put(dbName, name string, writeMode string, docs ...interface{}) (err error) {
	if nil != instance {

		if len(docs) > 0 {
			// we have data. best is an array of maps
			err = instance.AddToCollection(dbName, name, writeMode, docs...)
		} else {
			// we have a file
			err = instance.Load(dbName, name, writeMode)
		}

		if nil != err {
			instance.logError(fmt.Sprintf("LLMEmbeddingsRAG.Put('%s', '%s') -> Error on db: '%s'", dbName, name, err))
		}
	}
	return
}

func (instance *LLMEmbeddingsRAG) Load(dbName string, filename string, writeMode string) (err error) {
	if nil != instance {

		// get database
		var db vectordb_commons.IVectorDB
		db, err = instance.db(dbName)
		if nil != err {
			instance.logError(fmt.Sprintf("LLMEmbeddingsRAG.Load('%s') -> Error getting db: '%s'", filename, err))
			return
		}

		// load data
		err = db.Load(filename, writeMode) // update only if different from source
		if nil != err {
			instance.logError(fmt.Sprintf("LLMEmbeddingsRAG.Load('%s') -> Error loading data: '%s'", filename, err))
			return
		}
	}
	return
}

func (instance *LLMEmbeddingsRAG) AddToCollection(dbName string, collName string, writeMode string, docs ...interface{}) (err error) {
	if nil != instance {
		// get database
		var db vectordb_commons.IVectorDB
		db, err = instance.db(dbName)
		if nil != err {
			instance.logError(fmt.Sprintf("LLMEmbeddingsRAG.AddToCollection('%s', '%s') -> Error on db: '%s'", dbName, collName, err))
			return
		}

		// load data
		err = db.AddToCollection(collName, writeMode, docs...) // update only if different from source
		if nil != err {
			instance.logError(fmt.Sprintf("LLMEmbeddingsRAG.AddToCollection('%s', '%s') -> Error on db: '%s'", dbName, collName, err))
			return
		}

	}
	return
}

func (instance *LLMEmbeddingsRAG) RemoveFromCollection(dbName string, collName string, params ...interface{}) (err error) {
	if nil != instance && len(collName) > 0 {
		// get database
		var db vectordb_commons.IVectorDB
		db, err = instance.db(dbName)
		if nil != err {
			return
		}

		var coll vectordb_commons.IVectorDBCollection
		coll, err = db.GetCollection(collName)
		if nil != err {
			return
		}

		if nil != coll {
			err = coll.Delete(context.Background(), params...)
		}
	}
	return
}

func (instance *LLMEmbeddingsRAG) Upsert(dbName string, collName string, docs ...interface{}) (err error) {
	if nil != instance {
		// get database
		var db vectordb_commons.IVectorDB
		db, err = instance.db(dbName)
		if nil != err {
			return
		}
		// collection
		var coll vectordb_commons.IVectorDBCollection
		coll, err = db.GetCollection(collName)
		if nil != err {
			return
		}

		// loop on items
		for _, doc := range docs {
			docVec, docErr := chromemgo.ParseDocument(doc)
			if nil != docErr {
				err = docErr
				return
			}
			// remove
			if len(docVec.ID) > 0 {
				_ = coll.DeleteById(context.Background(), docVec.ID)
			} else {
				dbId := gg.Maps.GetString(gg.Convert.ToMap(docVec.Metadata), "db-id")
				if len(dbId) > 0 {
					filter := map[string]interface{}{"db-id": dbId}
					_ = coll.Delete(context.Background(), filter)
				}
			}
			// insert
			err = coll.InsertDocument(context.Background(), docVec)
			if nil != err {
				return
			}
		}
	}
	return
}

// Query executes a search query on a specified collection within the VectorDB, applying given filters and returning results.
// "wheres" is an array of filters. More filters and more loops to get different data
func (instance *LLMEmbeddingsRAG) Query(dbName, collection, query string, wheres []map[string]interface{}, maxResults int) (response []map[string]interface{}, err error) {
	if nil != instance && len(query) > 0 {
		err = instance.Open()
		if nil != err {
			instance.logError(fmt.Sprintf("LLMEmbeddingsRAG.Query('%s') -> Error opening: '%s'", collection, err))
			return
		}

		response = make([]map[string]interface{}, 0)

		var db vectordb_commons.IVectorDB
		db, err = instance.db(dbName)
		if nil != err {
			instance.logError(fmt.Sprintf("LLMEmbeddingsRAG.Query('%s') -> Error getting db: '%s'", collection, err))
			return
		}

		var coll vectordb_commons.IVectorDBCollection
		coll, err = db.GetCollection(collection)
		if nil != err || nil == coll {
			if nil == coll {
				instance.logError(fmt.Sprintf("LLMEmbeddingsRAG.Query('%s') -> Collection not found!", collection))
			} else {
				instance.logError(fmt.Sprintf("LLMEmbeddingsRAG.Query('%s') -> Error getting collection: '%s'", collection, err))
			}
			return
		}

		if coll.Count() == 0 {
			return
		}

		ctx := context.Background()
		maxResults = int(math.Min(float64(coll.Count()), float64(maxResults)))

		// multiple loops with different filters to collect a unique context
		data := make([]*vectordb_commons.DBResult, 0)
		if len(wheres) > 0 {
			for _, where := range wheres {

				removeEmptyValues(where)

				var res []*vectordb_commons.DBResult
				if len(where) > 0 {
					res, err = queryCollection(coll, ctx, query, where, maxResults)
				} else {
					res, err = queryCollection(coll, ctx, query, nil, maxResults)
				}

				if nil != err {
					return
				}
				// collect all rows into data
				for _, row := range res {
					data = append(data, row)
				}
			}
		} else {
			var res []*vectordb_commons.DBResult
			res, err = queryCollection(coll, ctx, query, nil, maxResults)

			if nil != err {
				instance.logError(fmt.Sprintf("LLMEmbeddingsRAG.Query('%s') -> Error queryng collection: '%s'", collection, err))
				return
			}
			// collect all rows into data
			for _, row := range res {
				data = append(data, row)
			}
		}

		for _, item := range data {
			response = append(response, item.Map())
		}
		vectordb.SortDesc(response, "similarity")
	}
	return
}

// Sanitize processes and filters the input data to remove unwanted fields and sanitize the content field.
// Only specific fields ("content", "similarity", "metadata", "id") are retained in the response.
// The "content" field is sanitized by removing specific text patterns ("search_document: ").
func (instance *LLMEmbeddingsRAG) Sanitize(data []map[string]interface{}) (response []map[string]interface{}) {
	if nil != instance {
		response = make([]map[string]interface{}, 0)
		// get clean items
		for _, m := range data {
			item := make(map[string]interface{})
			for k, v := range m {
				if nil != v {
					switch k {
					case "content":
						item[k] = strings.Replace(v.(string), "search_document: ", "", 1)
					case "similarity", "metadata", "id":
						item[k] = v
					}
				}
			}
			response = append(response, item)
		}
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LLMEmbeddingsRAG) init(args ...interface{}) (err error) {
	if nil != instance {
		instance.vectors = make(map[string]vectordb_commons.IVectorDB)
		instance.options = new(llm_commons.LLMRagOptions)

		for _, arg := range args {

			if ctrlDirs, ok := arg.(*llm_ctrl_utils.CtrlDirs); ok {
				instance.ctrlDirs = ctrlDirs
				continue
			}

			if l, ok := arg.(gg_.ILogger); ok {
				instance._logger = l
				continue
			}

			if o, ok := arg.(*llm_commons.LLMRagOptions); ok {
				instance.options = o
				continue
			}
			if o, ok := arg.(llm_commons.LLMRagOptions); ok {
				instance.options = &o
				continue
			}
			if m, ok := arg.(map[string]interface{}); ok {
				err = gg.JSON.Read(m, &instance.options)
				if nil != err {
					return
				}
				continue
			}
		}

		if nil == instance.ctrlDirs {
			err = gg.Errors.Prefix(gg.PanicSystemError, "LLMEmbeddingsRAG.ini() -> Dirs controller is required: ")
			return
		}

		if nil == instance.options || len(instance.options.Databases) == 0 {
			err = gg.Errors.Prefix(gg.PanicSystemError, "LLMEmbeddingsRAG.ini() -> Databases options are required: ")
			return
		}

	}
	return
}

func (instance *LLMEmbeddingsRAG) logger() (response gg_.ILogger) {
	if nil != instance {
		if nil == instance._logger {
			filename := gg.Paths.Concat(instance.ctrlDirs.Vectors, "logging.log")
			instance._logger = gg.Log.NewNoRotate("info", filename)
			instance._logger.Info("Initialized Logger for RAG")
		}
		response = instance._logger
	}
	return
}

func (instance *LLMEmbeddingsRAG) logInfo(args ...interface{}) {
	if nil != instance {
		logger := instance.logger()
		if nil != logger {
			logger.Info(args...)
		}
	}
}

func (instance *LLMEmbeddingsRAG) logError(args ...interface{}) {
	if nil != instance {
		logger := instance.logger()
		if nil != logger {
			logger.Error(args...)
		}
	}
}

func (instance *LLMEmbeddingsRAG) open() (err error) {
	if nil != instance {
		instance.vectors = make(map[string]vectordb_commons.IVectorDB)
		for _, dbOptions := range instance.options.Databases {
			dbOptions.Path = gg.Paths.Absolutize(dbOptions.Path, instance.ctrlDirs.Vectors)
			_ = gg.Paths.Mkdir(dbOptions.Path + gg_.OS_PATH_SEPARATOR)

			// create db instance
			var db vectordb_commons.IVectorDB
			db, err = vectordb.New(
				vectordb.DriverChromemGo,
				dbOptions.Map(),
				instance.logger(),
			)
			if nil != err {
				return
			}
			err = db.Open()
			if nil != err {
				return
			} else {
				instance.logInfo(fmt.Sprintf("VECTOR DB '%s' IS OPEN: '%s'", dbOptions.Name, db.String()))
			}
			// dd instance to db map
			instance.vectors[dbOptions.Name] = db
		}
	}
	return
}

func (instance *LLMEmbeddingsRAG) db(dbName string) (response vectordb_commons.IVectorDB, err error) {
	if nil != instance {
		if v, ok := instance.vectors[dbName]; ok {
			response = v
		} else {
			err = gg.Errors.Prefix(gg.PanicSystemError, fmt.Sprintf("VECTOR DB IS NOT FOUND: '%s'", dbName))
		}
	}
	return
}

func (instance *LLMEmbeddingsRAG) getOptions(dbName string) (response *llm_commons.LLMRagOptionsItem) {
	if nil != instance {
		for _, dbOptions := range instance.options.Databases {
			if nil != dbOptions {
				if dbOptions.Name == dbName {
					response = dbOptions
					return
				}
			}
		}
	}
	return
}

func (instance *LLMEmbeddingsRAG) dbRoot(dbName string) (response string) {
	if nil != instance {
		dbOptions := instance.getOptions(dbName)
		if nil != dbOptions {
			dbOptions.Path = gg.Paths.Absolutize(dbOptions.Path, instance.ctrlDirs.Vectors)
			_ = gg.Paths.Mkdir(dbOptions.Path + gg_.OS_PATH_SEPARATOR)
			response = dbOptions.Path
		}
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func removeEmptyValues(m map[string]interface{}) {
	for key, value := range m {
		if len(gg.Convert.ToString(value)) == 0 {
			delete(m, key)
		}
	}
}

func queryCollection(coll vectordb_commons.IVectorDBCollection, ctx context.Context, query string, where map[string]interface{}, maxResults int) (res []*vectordb_commons.DBResult, err error) {
	if maxResults > 0 {
		res, err = coll.Query(ctx, query, maxResults, where)
	} else {
		res, err = coll.Query(ctx, query, where)
	}
	return
}
