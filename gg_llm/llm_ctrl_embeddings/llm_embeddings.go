package llm_ctrl_embeddings

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_ctrl_drivers/llm_driver_ollama"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_ctrl_utils"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_prompts/skills"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_prompts_impl/skill_tool_context"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_prompts_impl/skill_tool_context/pkg_tool_context"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_xlibs/lib_vectordb/vectordb_commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_xtools/tool_textchunk"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_xtools/tool_textconvert"
	"fmt"
)

//----------------------------------------------------------------------------------------------------------------------
//	const
//----------------------------------------------------------------------------------------------------------------------

// https://ollama.com/blog/embedding-models
const (
	modelMXBAI = "mxbai-embed-large" // 334M
	modelNOMIC = "nomic-embed-text"  // 137M
	modelMINI  = "all-minilm"        // 27M

	defVectorId = "vector-common"
)

//----------------------------------------------------------------------------------------------------------------------
//	LLMEmbeddings & RAG controller
//----------------------------------------------------------------------------------------------------------------------

type LLMEmbeddings struct {
	options    *llm_commons.LLMEmbeddingsOptions
	ctrlDirs   *llm_ctrl_utils.CtrlDirs
	ctrlSkills *skills.Skills // skill and prompts controller
	ctrlRAG    *LLMEmbeddingsRAG

	driver  *llm_driver_ollama.OllamaAPI
	vectors map[string]vectordb_commons.IVectorDB // databases
}

func NewLLMEmbeddings(args ...any) (instance *LLMEmbeddings, err error) {
	instance = new(LLMEmbeddings)
	err = instance.init(args...)
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *LLMEmbeddings) Open() (err error) {
	if nil != instance {
		if nil != instance.ctrlRAG {
			err = instance.ctrlRAG.Open()
			if nil != err {
				return
			}

		}
	}
	return
}

func (instance *LLMEmbeddings) Close() {
	if nil != instance {
		if nil != instance.ctrlRAG {
			instance.ctrlRAG.Close()
		}
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n t e x t u a l i z e r
//----------------------------------------------------------------------------------------------------------------------

// Contextualize processes contextual knowledge to generate an LLM response or returns an error if input is invalid.
func (instance *LLMEmbeddings) Contextualize(sessionId, vectorId, userQuery string, userQueryMaxResults int, lang, model string, contextKnowledge ...string) (response *llm_commons.LLMResponse, err error) {
	if nil != instance {
		if len(lang) == 0 {
			lang = llm_commons.DefLang
		}
		if userQueryMaxResults == 0 {
			userQueryMaxResults = 10
		}
		if len(contextKnowledge) > 0 {
			request := llm_commons.NewLLMRequestContext()
			request.Driver = llm_commons.DriverOllama
			request.Model = model
			request.SkillName = skill_tool_context.UID
			request.PromptName = pkg_tool_context.PromptContext
			request.UserQuery = userQuery
			request.UserQueryMaxResults = userQueryMaxResults
			request.Payload.SetLang(lang)
			request.SessionId = sessionId
			// vector ID can be forced with a value, or assigned if this controlled decide to do RAG
			// If vectorId is assigned, the RAG is forced
			request.VectorId = vectorId

			response, err = instance.Context(request, contextKnowledge...)
		} else {
			err = gg.Errors.Prefix(gg.PanicSystemError, "No knowledge to contextualize: ")
		}
	}
	return
}

// Context processes a request with context knowledge and returns an LLM response or an error.
func (instance *LLMEmbeddings) Context(requestContext *llm_commons.LLMRequestContext, contextKnowledge ...string) (response *llm_commons.LLMResponse, err error) {
	if nil != instance && nil != requestContext {

		// set raw data
		requestContext.Payload.KBContextSetRawElements(contextKnowledge...)

		// CONVERT TO GET TEXT
		// MAY BE IS TIME CONSUMING IF USING OCR OR OTHER MODELS
		textConv, errConv := instance.convert(contextKnowledge...)

		// calculate size of text to process
		textSize := 0
		for i := 0; i < len(textConv); i++ {
			textSize += len(textConv[i])
		}

		// decide if do RAG
		if len(requestContext.VectorId) > 0 || llm_commons.IsRAGRequired(textSize) {
			// DO RAG
			// db-name = vector-id
			// collection-name=session-id
			var dbName, dbCollection string
			if len(requestContext.VectorId) == 0 {
				if len(requestContext.SessionId) > 0 {
					requestContext.VectorId = requestContext.SessionId
					dbName = requestContext.VectorId
					dbCollection = requestContext.SessionId
				} else {
					requestContext.VectorId = defVectorId
					dbName = requestContext.VectorId
					dbCollection = defVectorId
				}
			}
			// check if there is any text to write
			if len(textConv) > 0 {
				// CREATE/UPDATE DATABASE AND COLLECTION
				// prepare data to create chunks on vector database
				for _, text := range textConv {
					if len(text) > 0 {
						// prepare data to create chunks on vector database
						chunks := tool_textchunk.ChunkTextAsAny(text, 10)
						// try to avoid writing again the database for lag of performance
						if instance.ctrlRAG.CollectionHash(dbName, dbCollection) != vectordb_commons.Hash(chunks...) {
							// add chunks to database
							err = instance.ctrlRAG.AddToCollection(dbName, dbCollection, vectordb_commons.WriteModeUpdate, chunks...)
							if nil != err {
								return
							}
						}
					}
				}
			}
			// query the RAG to retrieve context items for response
			var KBItems []map[string]interface{}
			KBItems, err = instance.ctrlRAG.Query(dbName, dbCollection,
				requestContext.UserQuery, nil, requestContext.UserQueryMaxResults)
			if nil != err {
				return
			}
			for _, KBItem := range KBItems {
				requestContext.Payload.KBContextAppendItems(KBItem)
			}
		} else {
			// NO RAG, JUST CONTEXT DATA
			// loop on content
			for i := 0; i < len(textConv); i++ {
				content := textConv[i] // text or filename
				e := errConv[i]        // error or nil
				if nil == e {
					KBItem := map[string]interface{}{
						"content": content,
					}
					requestContext.Payload.KBContextAppendItems(KBItem)
				} else {
					err = gg.Errors.Prefix(e, fmt.Sprintf("File '%s' conversion produced an error: ", content))
					return
				}
			}
		}

		if len(requestContext.SkillName) == 0 {
			requestContext.SkillName = skill_tool_context.UID
		}
		if len(requestContext.PromptName) == 0 {
			requestContext.PromptName = pkg_tool_context.PromptContext
		}
		if requestContext.Payload.KBContextSize() == 0 {
			err = gg.Errors.Prefix(gg.PanicSystemError, "Missing Context data: ")
		}

		// prepare the request
		request := llm_commons.NewLLMRequest()
		request.Driver = requestContext.Driver
		request.Model = requestContext.Model
		request.Version = requestContext.Version
		request.SetOptions(requestContext.GetOptions())
		request.Payload = requestContext.Payload
		request.SkillName = requestContext.SkillName
		request.PromptName = requestContext.PromptName
		request.SessionId = requestContext.SessionId
		request.VectorId = requestContext.VectorId

		response, err = instance.Submit(request)
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	e m b e d d i n g s
//----------------------------------------------------------------------------------------------------------------------

// Embeddings
// https://ollama.com/blog/embedding-models
func (instance *LLMEmbeddings) Embeddings(request *llm_commons.LLMRequestEmbeddings) (response []float64, err error) {
	if nil != instance {
		response, err = instance.driver.Embeddings(request)
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	s u b m i t
//----------------------------------------------------------------------------------------------------------------------

func (instance *LLMEmbeddings) Submit(request *llm_commons.LLMRequest) (response *llm_commons.LLMResponse, err error) {
	if nil != instance {
		// check skill and prompt
		err = llm_ctrl_utils.EnsurePrompts(instance.ctrlSkills, request)
		if nil != err {
			return
		}
		response, err = instance.driver.Submit(request)
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *LLMEmbeddings) init(args ...any) (err error) {
	if nil != instance {

		for _, arg := range args {
			if d, ok := arg.(*llm_driver_ollama.OllamaAPI); ok {
				instance.driver = d
				continue
			}
			if k, ok := arg.(*skills.Skills); ok {
				instance.ctrlSkills = k
				continue
			}
			if d, ok := arg.(*llm_ctrl_utils.CtrlDirs); ok {
				instance.ctrlDirs = d
				continue
			}
			if o, ok := arg.(llm_commons.LLMEmbeddingsOptions); ok {
				instance.options = &o
				continue
			}
			if o, ok := arg.(*llm_commons.LLMEmbeddingsOptions); ok {
				instance.options = o
				continue
			}
		}

		if nil == instance.options {
			instance.options = new(llm_commons.LLMEmbeddingsOptions)
		}
		if nil == instance.driver {
			instance.driver, err = llm_driver_ollama.NewOllamaAPI()
		}
		if nil == instance.ctrlDirs {
			err = gg.Errors.Prefix(gg.PanicSystemError, "Missing directories controller: ")
			return
		}

		// CTRL Skills
		instance.ctrlSkills, err = skills.NewSkills(instance.ctrlDirs.Prompts, instance.ctrlDirs.Vectors)
		if nil != err {
			return
		}
		// add skills
		instance.ctrlSkills.Add(skill_tool_context.Skill())
		// deploy skills
		err = instance.ctrlSkills.Deploy()
		if nil != err {
			return
		}

		// CTRL RAG
		instance.ctrlRAG, err = NewLLMEmbeddingsRAG(instance.options.RAG, instance.ctrlDirs)
	}
	return
}

func (instance *LLMEmbeddings) convert(items ...string) (response []string, errs []error) {
	if nil != instance && nil != items {
		for _, item := range items {
			if gg.Paths.IsFilePath(item) {
				filename := gg.Paths.Absolute(item)
				text, err := tool_textconvert.ConvertFileToText(filename)
				if nil != err {
					errs = append(errs, err)
					response = append(response, filename)
				} else {
					response = append(response, text)
					errs = append(errs, nil)
				}
			} else {
				response = append(response, item)
				errs = append(errs, nil)
			}
		}
	}
	return
}
