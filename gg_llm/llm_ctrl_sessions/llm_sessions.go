package llm_ctrl_sessions

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_"
	"fmt"
	"os"
	"strings"
	"sync"
)

type LLMSessions struct {
	root         string // application root
	rootSessions string // sessions root

	logger gg_.ILogger
	mux    *sync.Mutex

	sessions map[string]*LLMSessionItem // id, session
}

func NewLLMSessions(root, rootSessions string, args ...interface{}) (instance *LLMSessions) {
	instance = new(LLMSessions)
	instance.init(root, rootSessions, args...)
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LLMSessions) Save(id string) {
	if nil != instance && len(id) > 0 {
		id = ID(id)
		session := instance.GetSession(id)
		if nil != session {
			session.Save()
		}
	}
	return
}

func (instance *LLMSessions) HasSession(id string) (response bool) {
	if nil != instance {
		return nil != instance.GetSession(id)
	}
	return
}

func (instance *LLMSessions) GetSession(id string) (response *LLMSessionItem) {
	if nil != instance && len(id) > 0 {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		if id == "." {
			id = gg.Rnd.RndId()
		}

		id = ID(id)
		if _, ok := instance.sessions[id]; !ok {
			path := gg.Paths.Concat(instance.rootSessions, id)
			_ = gg.Paths.Mkdir(path + gg_.OS_PATH_SEPARATOR)
			instance.sessions[id] = NewLLMSessionItem(path)
		}
		response = instance.sessions[id]
	}
	return
}

// GetSessionData retrieves and returns session data for a given session ID.
// Returns a map containing session data.
// If the session ID is invalid or the session does not exist, an empty map is returned.
// Session Data may contain chat history and files to be processed from the LLM
func (instance *LLMSessions) GetSessionData(id string) (response *LLMSessionData) {
	if nil != instance && len(id) > 0 {
		id = ID(id)
		session := instance.GetSession(id)
		if nil != session {
			response = session.GetData()
		}
	}
	return
}

func (instance *LLMSessions) GetSessionDataLite(id string) (response map[string]interface{}) {
	if nil != instance && len(id) > 0 {
		id = ID(id)
		session := instance.GetSession(id)
		if nil != session {
			response = session.GetDataLite()
		}
	}
	return
}

func (instance *LLMSessions) Clear(id string) (response *LLMSessionData) {
	if nil != instance && len(id) > 0 {
		id = ID(id)
		item := instance.GetSession(id)
		if nil != item {
			item.Clear()
			response = item.GetData()
		}
	}
	return
}

func (instance *LLMSessions) AddSystemText(id string, text string) {
	if nil != instance && len(id) > 0 {
		id = ID(id)
		session := instance.GetSession(id)
		if nil != session {
			session.AddSystemText(text)
		}
	}
}

func (instance *LLMSessions) AddUserText(id string, text string) {
	if nil != instance && len(id) > 0 {
		id = ID(id)
		session := instance.GetSession(id)
		if nil != session {
			session.AddUserText(text)
		}
	}
}

func (instance *LLMSessions) AddAssistantText(id string, text string) {
	if nil != instance && len(id) > 0 {
		id = ID(id)
		session := instance.GetSession(id)
		if nil != session {
			session.AddAssistantText(text)
		}
	}
}

func (instance *LLMSessions) AddContext(id, model string, data interface{}) {
	if nil != instance && len(id) > 0 && nil != data {
		id = ID(id)
		session := instance.GetSession(id)
		if nil != session {
			session.AddContext(model, data)
		}
	}
}

func (instance *LLMSessions) GetContextData(id string) (response interface{}) {
	if nil != instance && len(id) > 0 {
		id = ID(id)
		session := instance.GetSession(id)
		if nil != session {
			response = session.GetContextData()
		}
	}
	return
}

func (instance *LLMSessions) GetContextLen(id string) (response int) {
	if nil != instance && len(id) > 0 {
		id = ID(id)
		session := instance.GetSession(id)
		if nil != session {
			response = session.GetContextLen()
		}
	}
	return
}

func (instance *LLMSessions) AddModel(id string, value string) {
	if nil != instance && len(id) > 0 {
		id = ID(id)
		session := instance.GetSession(id)
		if nil != session {
			session.AddModel(value)
		}
	}
	return
}

func (instance *LLMSessions) GetLastUsedModel(id string) string {
	if nil != instance && len(id) > 0 {
		id = ID(id)
		session := instance.GetSession(id)
		if nil != session {
			return session.GetLastUsedModel()
		}
	}
	return ""
}

func (instance *LLMSessions) AddFile(id string, filename string) (response *LLMSessionItemFile, err error) {
	if nil != instance && len(id) > 0 {
		id = ID(id)
		session := instance.GetSession(id)
		if nil != session {
			response, err = session.AddFile(filename)
		}
	}
	return
}

func (instance *LLMSessions) AddFileContent(id string, name string, bytes []byte) (response *LLMSessionItemFile, err error) {
	if nil != instance && len(id) > 0 {
		id = ID(id)
		session := instance.GetSession(id)
		if nil != session {
			response, err = session.AddFileContent(name, bytes)
		}
	}
	return
}

func (instance *LLMSessions) GetLastTextFileContent(id string) (response string) {
	if nil != instance && len(id) > 0 {
		id = ID(id)
		session := instance.GetSession(id)
		if nil != session {
			response = session.GetLastTextFileContent()
		}
	}
	return
}

func (instance *LLMSessions) GetLastTextFileContentChunks(id string) (response []map[string]interface{}) {
	if nil != instance && len(id) > 0 {
		id = ID(id)
		session := instance.GetSession(id)
		if nil != session {
			response = session.GetLastTextFileContentChunks()
		}
	}
	return
}

func (instance *LLMSessions) PinContext(id string, pinned bool) {
	if nil != instance && len(id) > 0 {
		id = ID(id)
		session := instance.GetSession(id)
		if nil != session {
			session.PinContext(pinned)
		}
	}
}

func (instance *LLMSessions) IsPinned(id string) bool {
	if nil != instance && len(id) > 0 {
		id = ID(id)
		session := instance.GetSession(id)
		if nil != session {
			return session.IsPinned()
		}
	}
	return false
}

// ---------------------------------------------------------------------------------------------------------------------
//  p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LLMSessions) init(root, rootSessions string, args ...interface{}) {
	if nil != instance {
		instance.mux = new(sync.Mutex)
		instance.sessions = make(map[string]*LLMSessionItem)
		instance.root = root
		instance.rootSessions = rootSessions
		if len(instance.rootSessions) == 0 {
			instance.rootSessions = gg.Paths.Absolutize("./sessions", instance.root)
		}

		// autodetect
		for _, arg := range args {
			// logger
			if l, ok := arg.(gg_.ILogger); ok {
				instance.logger = l
				continue
			}

		}

		// logger
		if nil == instance.logger {
			// creates fallback logger
			filename := gg.Paths.Absolutize("./logging/logger_ai_llm_sessions.log", instance.root)
			instance.logger = gg.Log.NewNoRotate("info", filename)
		}

		err := instance.load()
		if nil != err {
			instance.logger.Error("LLMSessions.load()-> Error loading sessions: ", err)
		}
	}

	return
}

// load existing sessions from root
func (instance *LLMSessions) load() (err error) {
	if nil != instance {
		if ok, _ := gg.Paths.Exists(instance.rootSessions); ok {
			// get list of directories
			var files []os.DirEntry
			files, err = os.ReadDir(instance.rootSessions)
			if nil != err {
				return
			}
			for _, dir := range files {
				if dir.IsDir() {
					name := dir.Name()
					if len(name) > 0 {
						_ = instance.GetSession(name)
					}
				}
			}
		}
	}
	return
}

func ID(id string) string {
	if strings.HasPrefix(id, "enc-") {
		return id
	}
	md5 := gg.Coding.MD5(id)
	return fmt.Sprintf("enc-%s", md5)
}
