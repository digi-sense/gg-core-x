package llm_ctrl_sessions

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_xtools/tool_textconvert"
	"mime"
	"path/filepath"
	"strings"
)

// ---------------------------------------------------------------------------------------------------------------------
//  LLMSessionItemFile
// ---------------------------------------------------------------------------------------------------------------------

type LLMSessionItemFile struct {
	Path     string `json:"path"`
	MimeType string `json:"mime-type"`
	IsImage  bool   `json:"is_image"`
	IsPDF    bool   `json:"is_pdf"`
	IsTXT    bool   `json:"is_txt"`
	Content  string `json:"content"`
}

func NewCtrlAISessionItemFile(filename string) (instance *LLMSessionItemFile) {
	instance = new(LLMSessionItemFile)
	instance.Path = filename
	instance.MimeType = mime.TypeByExtension(filepath.Ext(filename))
	if instance.MimeType == "" {
		instance.MimeType = "application/octet-stream"
	}
	instance.IsImage = strings.HasPrefix(instance.MimeType, "image/")
	instance.IsPDF = strings.HasSuffix(instance.MimeType, "/pdf")
	instance.IsTXT = strings.HasPrefix(instance.MimeType, "text/plain")

	return
}

func (instance *LLMSessionItemFile) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *LLMSessionItemFile) Map() map[string]interface{} {
	return gg.Convert.ToMap(gg.JSON.Stringify(instance))
}

func (instance *LLMSessionItemFile) Clone() *LLMSessionItemFile {
	return &LLMSessionItemFile{
		Path:     instance.Path,
		MimeType: instance.MimeType,
		IsImage:  instance.IsImage,
		IsPDF:    instance.IsPDF,
		IsTXT:    instance.IsTXT,
		Content:  instance.Content,
	}
}

func (instance *LLMSessionItemFile) GetContent() (response string) {
	if nil != instance {
		if len(instance.Content) == 0 {
			if instance.IsImage {
				var byteData []byte
				byteData, err := gg.IO.ReadBytesFromFile(instance.Path)
				if nil == err {
					instance.Content = gg.Coding.EncodeBase64(byteData)
				}
			} else {
				instance.Content, _ = tool_textconvert.ConvertFileToText(instance.Path)
			}
		}
		response = instance.Content
	}
	return
}

func (instance *LLMSessionItemFile) RemoveContent() {
	if nil != instance {
		instance.Content = ""
	}
}

func (instance *LLMSessionItemFile) GetPath() (response string) {
	if nil != instance {
		response = instance.Path
	}
	return
}

func (instance *LLMSessionItemFile) GetMimeType() (response string) {
	if nil != instance {
		response = instance.MimeType
	}
	return
}

func (instance *LLMSessionItemFile) GetIsImage() (response bool) {
	if nil != instance {
		response = instance.IsImage
	}
	return
}

func (instance *LLMSessionItemFile) GetIsPDF() (response bool) {
	if nil != instance {
		response = instance.IsPDF
	}
	return
}

func (instance *LLMSessionItemFile) GetIsTXT() (response bool) {
	if nil != instance {
		response = instance.IsTXT
	}
	return
}
