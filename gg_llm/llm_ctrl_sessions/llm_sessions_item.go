package llm_ctrl_sessions

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_xtools/tool_textchunk"
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_collections"
	"errors"
	"fmt"
	"mime"
	"path/filepath"
)

const (
	roleSystem    = "system"
	roleUser      = "user"
	roleAssistant = "assistant"
)

// ---------------------------------------------------------------------------------------------------------------------
//  LLMSessionItemMessage
// ---------------------------------------------------------------------------------------------------------------------

type LLMSessionItemMessage struct {
	Role    string `json:"role"`
	Content string `json:"content"`
}

type LLMSessionItemContext struct {
	Model   string      `json:"model"`
	Context interface{} `json:"context"` // array of num. Always incremented
}

// ---------------------------------------------------------------------------------------------------------------------
//  LLMSessionData
// ---------------------------------------------------------------------------------------------------------------------

type LLMSessionData struct {
	Id       string                                    `json:"id"`
	Pinned   bool                                      `json:"pinned"`
	Models   []string                                  `json:"models"`
	Messages []*LLMSessionItemMessage                  `json:"messages"`
	Context  *LLMSessionItemContext                    `json:"context"`
	FileList *gg_collections.List[*LLMSessionItemFile] `json:"file-list"`
}

func (instance *LLMSessionData) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *LLMSessionData) Map() map[string]interface{} {
	return gg.Convert.ToMap(gg.JSON.Stringify(instance))
}

// ---------------------------------------------------------------------------------------------------------------------
//  LLMSessionItem
// ---------------------------------------------------------------------------------------------------------------------

type LLMSessionItem struct {
	root      string
	rootFiles string
	id        string
	enabled   bool

	models          *gg_collections.List[string]              // array of latest five used models
	messages        []*LLMSessionItemMessage                  // array of messages
	context         *LLMSessionItemContext                    // array of num. Always incremented
	contextPinned   *LLMSessionItemContext                    // fist context. Write only once
	contextIsPinned bool                                      // the initial context is always returned
	_files          *gg_collections.List[*LLMSessionItemFile] // list of file item
}

func NewLLMSessionItem(root string) (instance *LLMSessionItem) {
	instance = new(LLMSessionItem)
	instance.root = root
	instance.rootFiles = filepath.Join(instance.root, "files")
	instance.id = filepath.Base(root)
	instance.enabled = len(instance.id) > 0
	instance.messages = make([]*LLMSessionItemMessage, 0)
	instance.models = gg_collections.NewList[string](5)

	_ = gg.Paths.Mkdir(instance.root + gg_.OS_PATH_SEPARATOR)
	_ = gg.Paths.Mkdir(instance.rootFiles + gg_.OS_PATH_SEPARATOR)

	// load existing session item data
	instance.loadMessages()
	instance.loadContext()
	instance.loadFiles()

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LLMSessionItem) Save() {
	if nil != instance {
		instance.saveFiles()
		instance.saveMessages()
		instance.saveContext()
	}
}

func (instance *LLMSessionItem) GetId() (response string) {
	if nil != instance {
		response = instance.id
	}
	return
}

func (instance *LLMSessionItem) GetData() (response *LLMSessionData) {
	if nil != instance && instance.enabled {
		response = &LLMSessionData{
			Id:       instance.GetId(),
			Pinned:   instance.contextIsPinned,
			Models:   instance.GetModels(),
			Messages: instance.GetMessages(),
			Context:  instance.GetContext(),
			FileList: instance.GetFiles(),
		}
	}
	return
}

func (instance *LLMSessionItem) GetDataLite() (response map[string]interface{}) {
	if nil != instance && instance.enabled {
		response = make(map[string]interface{})
		response["id"] = instance.GetId()
		// add session data if any
		response["pinned"] = instance.contextIsPinned
		response["models"] = instance.GetModels()
		response["messages"] = instance.GetMessagesLen()
		response["context"] = instance.GetContextLen()
		response["file-list"] = instance.GetFiles()
	}
	return
}

func (instance *LLMSessionItem) Clear() *LLMSessionItem {
	if nil != instance && instance.enabled {
		instance.messages = make([]*LLMSessionItemMessage, 0)
		instance.context = nil
		instance.contextPinned = nil
		instance.contextIsPinned = false
		instance._files = nil
		instance.models = gg_collections.NewList[string](5)
		// remove
		_ = gg.IO.RemoveAll(instance.root)
		// create dirs
		_ = gg.Paths.Mkdir(instance.root + gg_.OS_PATH_SEPARATOR)
		_ = gg.Paths.Mkdir(instance.rootFiles + gg_.OS_PATH_SEPARATOR)
	}
	return instance
}

func (instance *LLMSessionItem) GetModels() (response []string) {
	if nil != instance && instance.enabled {
		response = make([]string, 0)
		if nil == instance.models {
			instance.models = gg_collections.NewList[string](5)
		}
		instance.models.For(func(item string, _ int, _ []string) error {
			response = append(response, item)
			return nil
		})
	}
	return
}

func (instance *LLMSessionItem) GetFiles() (response *gg_collections.List[*LLMSessionItemFile]) {
	if nil != instance && instance.enabled {
		instance.saveFiles()
		instance.loadFiles()
		response = instance.files()
	}
	return
}

func (instance *LLMSessionItem) GetImageFiles() (response []*LLMSessionItemFile) {
	if nil != instance && instance.enabled {
		response = make([]*LLMSessionItemFile, 0)
		files := instance.files()
		files.For(func(item *LLMSessionItemFile, _ int, _ []*LLMSessionItemFile) error {
			if item.GetIsImage() {
				_ = item.GetContent() // load content
				response = append(response, item)
			}
			return nil
		})
	}
	return
}

func (instance *LLMSessionItem) GetTextFiles() (response []*LLMSessionItemFile) {
	if nil != instance && instance.enabled {
		response = make([]*LLMSessionItemFile, 0)
		files := instance.files()
		files.For(func(item *LLMSessionItemFile, _ int, _ []*LLMSessionItemFile) error {
			if !item.GetIsImage() {
				_ = item.GetContent() // load content
				response = append(response, item)
			}
			return nil
		})
	}
	return
}

func (instance *LLMSessionItem) GetContext() (response *LLMSessionItemContext) {
	if nil != instance && instance.enabled {
		instance.saveContext()
		instance.loadContext()
		if instance.contextIsPinned {
			response = instance.contextPinned
		} else {
			response = instance.context
		}

	}
	return
}

func (instance *LLMSessionItem) GetContextData() (response interface{}) {
	if nil != instance && instance.enabled {
		context := instance.GetContext()
		if nil != context {
			response = context.Context
		}
	}
	return
}

func (instance *LLMSessionItem) GetContextLen() (response int) {
	if nil != instance && instance.enabled {
		context := instance.GetContext()
		if nil != context {
			if ok, a := gg.Compare.IsArray(context); ok {
				return a.Len()
			} else {
				return len(gg.Convert.ToString(context))
			}
		}
	}
	return
}

func (instance *LLMSessionItem) GetMessages() (response []*LLMSessionItemMessage) {
	if nil != instance && instance.enabled {
		instance.saveMessages()
		instance.loadMessages()
		response = instance.messages
	}
	return
}

func (instance *LLMSessionItem) GetMessagesLen() (response int) {
	if nil != instance && instance.enabled {
		messages := instance.GetMessages()
		return len(messages)
	}
	return
}

func (instance *LLMSessionItem) AddSystemText(text string) {
	if nil != instance && instance.enabled && len(text) > 0 {
		messages := instance.getMessages(roleSystem)
		if len(messages) == 0 {
			instance.messages = append(instance.messages, &LLMSessionItemMessage{
				Role:    roleSystem,
				Content: text,
			})
		} else {
			messages[0].Content = text
		}
	}
}

func (instance *LLMSessionItem) AddUserText(text string) {
	if nil != instance && instance.enabled && len(text) > 0 {
		instance.messages = append(instance.messages, &LLMSessionItemMessage{
			Role:    roleUser,
			Content: text,
		})
	}
}

func (instance *LLMSessionItem) AddAssistantText(text string) {
	if nil != instance && instance.enabled && len(text) > 0 {
		instance.messages = append(instance.messages, &LLMSessionItemMessage{
			Role:    roleAssistant,
			Content: text,
		})
	}
}

func (instance *LLMSessionItem) AddContext(model string, data interface{}) {
	if nil != instance && instance.enabled {
		if nil != data {
			if ok, a := gg_.IsArrayValue(data); ok && a.Len() > 0 {
				if len(model) == 0 && instance.models.Len() > 0 {
					model = instance.models.Last()
				}
				context := &LLMSessionItemContext{
					Model:   model,
					Context: data,
				}
				instance.context = context
				if instance.contextIsPinned && nil == instance.contextPinned {
					instance.contextPinned = context
				}
			}
		}
	}
}

func (instance *LLMSessionItem) AddModel(text string) {
	if nil != instance && instance.enabled && len(text) > 0 && nil != instance.models {
		instance.models.Push(text)
	}
}

func (instance *LLMSessionItem) GetLastUsedModel() (response string) {
	if nil != instance && instance.enabled && nil != instance.models {
		response = instance.models.Get(0)
	}
	return
}

func (instance *LLMSessionItem) AddFile(filename string) (response *LLMSessionItemFile, err error) {
	if nil != instance && instance.enabled {
		// read file data and save into session
		var bytes []byte
		bytes, err = gg.IO.ReadBytesFromFile(filename)
		if nil != err {
			return
		}
		if len(bytes) == 0 {
			err = gg.Errors.Prefix(gg.PanicSystemError, fmt.Sprintf("File '%s' is empty: ", filepath.Base(filename)))
			return
		}

		response, err = instance.AddFileContent(filename, bytes)
	}
	return
}

func (instance *LLMSessionItem) AddFileContent(name string, bytes []byte) (response *LLMSessionItemFile, err error) {
	if nil != instance && instance.enabled {
		name = filepath.Base(name)
		ext := filepath.Ext(name)
		mimeType := mime.TypeByExtension(ext)
		if len(mimeType) == 0 {
			err = gg.Errors.Prefix(gg.PanicSystemError, fmt.Sprintf("File '%s' has an unsupported media type.", name))
			return
		}

		// ok, the file is supported and can be saved.
		path := filepath.Join(instance.rootFiles, name)
		response = NewCtrlAISessionItemFile(path)
		if !instance.fileExists(path) {
			_ = gg.Paths.Mkdir(path) // ensure path exists
			response.Path, err = instance.saveFile(bytes, path)
			if nil == err {
				instance.files().Push(response)
			}
		}
	}
	return
}

func (instance *LLMSessionItem) GetLastTextFileContent() (response string) {
	if nil != instance && instance.enabled && nil != instance.models {
		files := instance.GetTextFiles()
		if len(files) > 0 {
			response = files[len(files)-1].GetContent()
		}
	}
	return
}

func (instance *LLMSessionItem) GetLastTextFileContentChunks() (response []map[string]interface{}) {
	if nil != instance && instance.enabled && nil != instance.models {
		response = make([]map[string]interface{}, 0)
		text := instance.GetLastTextFileContent()
		if len(text) > 0 {
			// create chunks
			// a chunk is a map with a "content" field
			response = tool_textchunk.ChunkText(text, 10)
		}
	}
	return
}

func (instance *LLMSessionItem) PinContext(value bool) {
	if nil != instance && instance.enabled {
		instance.contextIsPinned = value
	}
}

func (instance *LLMSessionItem) IsPinned() (response bool) {
	if nil != instance && instance.enabled {
		response = instance.contextIsPinned
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LLMSessionItem) files() (response *gg_collections.List[*LLMSessionItemFile]) {
	if nil != instance && instance.enabled {
		if instance._files == nil {
			// LIFO
			instance._files = gg_collections.NewList[*LLMSessionItemFile](0).SetFIFO(false)
			list, _ := gg.Paths.ListFiles(instance.rootFiles, "*.*")
			if len(list) > 0 {
				for _, file := range list {
					item := NewCtrlAISessionItemFile(file)
					if nil != item {
						instance._files.Push(item)
					}
				}
			}
		}
		response = instance._files
	}
	return
}

func (instance *LLMSessionItem) fileExists(path string) (response bool) {
	if nil != instance {
		response = false
		files := instance.files()
		files.For(func(item *LLMSessionItemFile, index int, arr []*LLMSessionItemFile) error {
			if nil != item && item.GetPath() == path {
				response = true
				return errors.New("exit")
			}
			return nil
		})
	}
	return
}

func (instance *LLMSessionItem) getMessages(role string) (response []*LLMSessionItemMessage) {
	if nil != instance && instance.enabled {
		response = make([]*LLMSessionItemMessage, 0)
		for _, message := range instance.messages {
			if nil != message && message.Role == role {
				response = append(response, message)
			}
		}
	}
	return
}

func (instance *LLMSessionItem) saveFile(bytes []byte, filename string) (response string, err error) {
	if nil != instance && instance.enabled {
		_, err = gg.IO.WriteBytesToFile(bytes, filename)
		if nil == err {
			response = filename
		}
	}
	return
}

func (instance *LLMSessionItem) saveContext() {
	if nil != instance && instance.enabled {
		if nil != instance.context {
			filename := gg.Paths.Concat(instance.root, "context.json")
			s := gg.JSON.Stringify(instance.context)
			_, _ = gg.IO.WriteTextToFile(s, filename)

			if instance.contextIsPinned {
				filename = gg.Paths.Concat(instance.root, "context_pinned.json")
				s = gg.JSON.Stringify(instance.contextPinned)
				_, _ = gg.IO.WriteTextToFile(s, filename)
			}
		}
	}
}

func (instance *LLMSessionItem) loadContext() {
	if nil != instance && instance.enabled {
		filename := gg.Paths.Concat(instance.root, "context.json")
		_ = gg.JSON.ReadFromFile(filename, &instance.context)

		filename = gg.Paths.Concat(instance.root, "context_pinned.json")
		_ = gg.JSON.ReadFromFile(filename, &instance.contextPinned)
		if nil != instance.contextPinned {
			instance.contextIsPinned = true
		}
	}
}

func (instance *LLMSessionItem) saveMessages() {
	if nil != instance && instance.enabled {
		if nil != instance.messages {
			filename := gg.Paths.Concat(instance.root, "messages.json")
			s := gg.JSON.Stringify(instance.messages)
			_, _ = gg.IO.WriteTextToFile(s, filename)
		}
	}
}

func (instance *LLMSessionItem) loadMessages() {
	if nil != instance && instance.enabled {
		filename := gg.Paths.Concat(instance.root, "messages.json")
		_ = gg.JSON.ReadFromFile(filename, &instance.messages)
	}
}

func (instance *LLMSessionItem) saveFiles() {
	if nil != instance && instance.enabled {
		if nil != instance._files {
			filename := gg.Paths.Concat(instance.rootFiles, "index.json")
			_ = instance._files.MapToFile(filename, func(item *LLMSessionItemFile, _ int, _ []*LLMSessionItemFile) (selected *LLMSessionItemFile, err error) {
				if nil != item && filename != item.GetPath() {
					selected = item.Clone()
					selected.RemoveContent() // clear text
				} else {
					selected = nil
				}
				return
			})
		}
	}
}

func (instance *LLMSessionItem) loadFiles() {
	if nil != instance && instance.enabled {
		filename := gg.Paths.Concat(instance.rootFiles, "index.json")
		_ = instance.files().LoadFromFile(filename)
	}
}
