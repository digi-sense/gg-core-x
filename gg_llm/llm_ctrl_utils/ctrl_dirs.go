package llm_ctrl_utils

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_"
	"strings"
)

//----------------------------------------------------------------------------------------------------------------------
//	CtrlDirs
//----------------------------------------------------------------------------------------------------------------------

type CtrlDirs struct {
	Root      string `json:"root"`
	Workspace string `json:"workspace"`
	Prompts   string `json:"prompts"`
	Vectors   string `json:"vectors"`
	Sessions  string `json:"sessions"`

	name string
	slug string
}

func NewCtrlDirs(name, root string) (instance *CtrlDirs) {
	instance = new(CtrlDirs)
	instance.name = name
	instance.slug = strings.ToLower(gg.Strings.Slugify(instance.name, " :_", "-:_", ".:_"))
	instance.Root = gg.Paths.Absolute(root)
	instance.Workspace = gg.Paths.Absolutize(instance.slug, instance.Root)
	instance.Prompts = gg.Paths.Absolutize("./prompts", instance.Workspace)
	instance.Vectors = gg.Paths.Absolutize("./vectors", instance.Workspace)
	instance.Sessions = gg.Paths.Absolutize("./sessions", instance.Workspace)

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *CtrlDirs) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *CtrlDirs) Map() (m map[string]interface{}) {
	return gg.Convert.ToMap(gg.JSON.Stringify(instance))
}

func (instance *CtrlDirs) Mkdir() *CtrlDirs {
	if nil != instance {
		_ = gg.Paths.Mkdir(instance.Prompts + gg_.OS_PATH_SEPARATOR)
		_ = gg.Paths.Mkdir(instance.Vectors + gg_.OS_PATH_SEPARATOR)
		_ = gg.Paths.Mkdir(instance.Sessions + gg_.OS_PATH_SEPARATOR)
	}
	return instance
}
