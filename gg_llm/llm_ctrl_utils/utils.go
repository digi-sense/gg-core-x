package llm_ctrl_utils

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_prompts/skills"
)

func EnsurePrompts(ctrlSkills *skills.Skills, request *llm_commons.LLMRequest) (err error) {
	if nil != ctrlSkills && nil != request {
		if len(request.SkillName) > 0 && len(request.PromptName) > 0 {
			skill, prompt := ctrlSkills.GetAll(request.SkillName, request.PromptName, request.Payload.Lang)
			if nil != skill && nil != prompt {
				system := skill.System()
				if len(system) > 0 {
					request.SetPromptSystem(system) // will be rendered with context data later
				}
				template := prompt.Content
				if len(template) > 0 {
					request.SetPrompt(template)
				}
			}
		}

		// the request must have messages for prompt
		if request.MessagesCount() == 0 {
			err = gg.Errors.Prefix(gg.PanicSystemError, "Unable to submit empty request: ")
			return
		}
	}
	return
}
