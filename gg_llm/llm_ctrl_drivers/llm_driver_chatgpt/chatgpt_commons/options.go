package chatgpt_commons

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_commons"
)

// ---------------------------------------------------------------------------------------------------------------------
//  ChatGptOptions
// ---------------------------------------------------------------------------------------------------------------------

type ChatGptOptions struct {
	DriverOptions *llm_commons.LLMDriverOptions `json:"-"`
}

func NewChatGptOptions() (instance *ChatGptOptions) {
	instance = new(ChatGptOptions)
	instance.DriverOptions = llm_commons.NewLLMDriverOptions()
	return
}

func (instance *ChatGptOptions) String() (response string) {
	if nil != instance {
		response = gg.JSON.Stringify(instance)
	}
	return
}

func (instance *ChatGptOptions) Map() (response map[string]interface{}) {
	if nil != instance {
		response = gg.Convert.ToMap(instance.String())
	}
	return
}
