package chatgpt_commons

import (
	"bitbucket.org/digi-sense/gg-core"
	"errors"
)

/*
  "error": {
    "code": null,
    "message": "Invalid model gpt-3.5-turbo. The model argument should be left blank.",
    "param": null,
    "type": "invalid_request_error"
  }
}*/

type openAIErrorObject struct {
	Code    string      `json:"code"`
	Message string      `json:"message"`
	Type    string      `json:"type"`
	Param   interface{} `json:"param"`
}

type OpenAIError struct {
	Error interface{} `json:"error,omitempty"` // only if response has error
}

func (instance *OpenAIError) ErrorMessage() string {
	if nil != instance && nil != instance.Error {
		if s, ok := instance.Error.(string); ok {
			return s
		}
		if m, ok := instance.Error.(map[string]interface{}); ok {
			var err *openAIErrorObject
			_ = gg.JSON.Read(m, &err)
			if nil != err {
				return err.Message
			}
		}
	}
	return ""
}

func (instance *OpenAIError) ErrorObject() error {
	if nil != instance && nil != instance.Error {
		message := instance.ErrorMessage()
		if len(message) > 0 {
			return errors.New(message)
		}
	}
	return nil
}
