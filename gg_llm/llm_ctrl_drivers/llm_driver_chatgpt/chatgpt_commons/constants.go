package chatgpt_commons

const ApiUrl = "https://api.openai.com/v1/chat/completions"

const (
	ChatModelGPT4           = "gpt-4"
	ChatModelGPT4TurboPrev  = "gpt-4-turbo-preview"
	ChatModelGPT4VisionPrev = "gpt-4-vision-preview"
	ChatModelGPT432k        = "gpt-4-32k"
	ChatModelGPT35Turbo     = "gpt-3.5-turbo"
	ChatModelGPT35Turbo16k  = "gpt-3.5-turbo-16k"
)
