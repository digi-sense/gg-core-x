package llm_driver_chatgpt

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_ctrl_drivers/llm_driver_chatgpt/chatgpt_commons"
	"bitbucket.org/digi-sense/gg-core/gg_net/gg_net_http"
)

// ChatGPTAPI represents a structure for configuring and interacting with the ChatGPT service.
// WEB: https://platform.openai.com/docs/overview?lang=curl
type ChatGPTAPI struct {
	options *chatgpt_commons.ChatGptOptions
	enabled bool
	host    string
}

func NewChatGPTAPI(args ...interface{}) (instance *ChatGPTAPI, err error) {
	instance = new(ChatGPTAPI)
	instance.host = chatgpt_commons.ApiUrl // API
	instance.enabled = false
	err = instance.init(args...)

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *ChatGPTAPI) SetAccessKey(value string) *ChatGPTAPI {
	if nil != instance && nil != instance.options {
		instance.options.DriverOptions.AccessKey = value
	}
	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	i l l m d r i v e r
//----------------------------------------------------------------------------------------------------------------------

func (instance *ChatGPTAPI) Uid() (response string) {
	if nil != instance && nil != instance.options {
		response = instance.options.DriverOptions.Uid
	}
	return
}

func (instance *ChatGPTAPI) GetOptions() (response *llm_commons.LLMDriverOptions) {
	if nil != instance {
		response = instance.options.DriverOptions
	}
	return
}

func (instance *ChatGPTAPI) Submit(request *llm_commons.LLMRequest) (response *llm_commons.LLMResponse, err error) {
	if nil != instance && nil != request {
		request.Driver = llm_commons.DriverChatGPT
		request.SetOptions(instance.options.DriverOptions.OptionsModel)
		// update max tokens
		if request.GetOptions().MaxTokens > 0 {
			request.GetOptions().MaxTokens = request.CalcMaxTokensMin(request.GetOptions().MaxTokens)
		}
		if len(request.Model) == 0 {
			request.Model = instance.options.DriverOptions.Model
		}
		if len(request.Model) == 0 {
			request.Model = chatgpt_commons.ChatModelGPT35Turbo
		}

		// submit request
		response, err = instance.submit(request)
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *ChatGPTAPI) init(args ...interface{}) (err error) {
	if nil != instance {

		for _, arg := range args {
			if o, ok := arg.(*chatgpt_commons.ChatGptOptions); ok {
				instance.options = o
				continue
			}
			if o, ok := arg.(chatgpt_commons.ChatGptOptions); ok {
				instance.options = &o
				continue
			}
			if o, ok := arg.(*llm_commons.LLMDriverOptions); ok {
				if nil == instance.options {
					instance.options = chatgpt_commons.NewChatGptOptions()
					instance.options.DriverOptions = o
				}
				if nil == instance.options.DriverOptions {
					instance.options.DriverOptions = o
				}
				continue
			}
			if o, ok := arg.(llm_commons.LLMDriverOptions); ok {
				if nil == instance.options {
					instance.options = chatgpt_commons.NewChatGptOptions()
					instance.options.DriverOptions = &o
				}
				if nil == instance.options.DriverOptions {
					instance.options.DriverOptions = &o
				}
				continue
			}
			if m, ok := arg.(map[string]interface{}); ok {
				err = gg.JSON.Read(m, &instance.options)
				if nil == err {
					if nil == instance.options.DriverOptions {
						instance.options.DriverOptions = llm_commons.NewLLMDriverOptions()
						_ = gg.JSON.Read(m, &instance.options.DriverOptions)
					}
				}
				continue
			}
			if s, ok := arg.(string); ok {
				if gg.Regex.IsValidJsonObject(s) {
					err = gg.JSON.Read(s, &instance.options)
				} else {
					err = gg.JSON.ReadFromFile(s, &instance.options)
				}
				continue
			}
		} // for

		// validate options
		if nil == instance.options {
			instance.options = chatgpt_commons.NewChatGptOptions()
		}
		if nil == instance.options.DriverOptions {
			instance.options.DriverOptions = llm_commons.NewLLMDriverOptions()
		}

		// test runtime is running, or run
		// err = instance.testOllamaRuntime()
		if nil != err {
			return
		}

		instance.enabled = nil != instance.options &&
			nil != instance.options.DriverOptions &&
			len(instance.options.DriverOptions.AccessKey) > 0
	}
	return
}

func (instance *ChatGPTAPI) client() (client *gg_net_http.HttpClient) {
	client = gg.Net.NewHttpClient()
	client.AddHeader("Content-Type", "application/json")
	client.AddHeader("Authorization", "Bearer "+instance.options.DriverOptions.AccessKey)
	// client.AddHeader("OpenAI-Organization", "xxxxx")
	// client.AddHeader("OpenAI-Project", "xxxxx)
	return
}

func (instance *ChatGPTAPI) get(url string) (response map[string]interface{}, err error) {
	client := instance.client()
	resp, e := client.Get(url)
	if nil != e {
		err = e
	} else {
		response = resp.BodyAsMap()
	}
	return
}

func (instance *ChatGPTAPI) post(url string, body interface{}) (response map[string]interface{}, err error) {
	client := instance.client()
	resp, e := client.Post(url, body)
	if nil != e {
		err = e
	} else {
		response = resp.BodyAsMap()
	}
	return
}

func (instance *ChatGPTAPI) submit(request *llm_commons.LLMRequest) (response *llm_commons.LLMResponse, err error) {
	if nil != instance && nil != request {
		response = llm_commons.NewLLMResponse(request)

		// init request
		req := make(map[string]interface{})
		req["model"] = request.Model
		messages := make([]map[string]interface{}, 0)
		list := request.Messages()
		for _, message := range list {
			messages = append(messages, message.Map())
		}
		req["messages"] = messages
		req = gg.Maps.Merge(false, req, request.GetOptions().Map())

		var resp map[string]interface{}
		resp, err = instance.post(instance.host, req)
		if nil != err {
			return
		}

		var chatGptResponse *chatgpt_commons.ChatResponseCompletion
		chatGptResponse, err = chatgpt_commons.ParseChatResponseCompletion(resp)
		if nil != err {
			return
		}

		// parse resp
		response.Model = chatGptResponse.Model
		response.SetResponse(chatGptResponse.Map())
		if nil != chatGptResponse.OpenAIError {
			err = chatGptResponse.OpenAIError.ErrorObject()
			return
		}
		response.SetTextResponse(chatGptResponse.GetMessageContent())

	}
	return
}
