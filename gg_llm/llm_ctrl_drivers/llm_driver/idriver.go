package llm_driver

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_commons"
)

type ILLMDriver interface {
	Uid() string
	GetOptions() (response *llm_commons.LLMDriverOptions)
	Submit(request *llm_commons.LLMRequest) (response *llm_commons.LLMResponse, err error)
}
