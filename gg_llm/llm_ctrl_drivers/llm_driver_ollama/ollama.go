package llm_driver_ollama

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_ctrl_drivers/llm_driver_ollama/ollama_commons"
	"bytes"
	"context"
	"github.com/ollama/ollama/api"
	"log"
	"net/http"
	"net/url"
)

// OLLAMA "FINE-TUNING"
// https://github.com/ollama/ollama/blob/main/docs/faq.md#how-can-i-set-the-quantization-type-for-the-kv-cache
//
// * How can I enable Flash Attention?
//   Flash Attention is a feature of most modern models that can significantly reduce memory usage as the context size grows. To enable Flash Attention, set the OLLAMA_FLASH_ATTENTION environment variable to 1 when starting the Ollama server.
// * How can I set the quantization type for the K/V cache?
//   The K/V context cache can be quantized to significantly reduce memory usage when Flash Attention is enabled.
//
//   To use quantized K/V cache with Ollama you can set the following environment variable:
//
//   OLLAMA_KV_CACHE_TYPE - The quantization type for the K/V cache. Default is f16.

// OllamaAPI is a struct that manages interactions with the Ollama API, handling configuration and API client setup.
type OllamaAPI struct {
	options *ollama_commons.OllamaOptions
	enabled bool
	host    *url.URL

	_cli *api.Client
}

func NewOllamaAPI(args ...interface{}) (instance *OllamaAPI, err error) {
	instance = new(OllamaAPI)
	err = instance.init(args...)

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *OllamaAPI) Map() (response map[string]interface{}) {
	if nil != instance {
		response = instance.Options().Map()
	}
	return
}

func (instance *OllamaAPI) String() (response string) {
	if nil != instance {
		response = instance.Options().String()
	}
	return
}

func (instance *OllamaAPI) Options() (response *ollama_commons.OllamaOptions) {
	if nil != instance {
		if nil == instance.options {
			instance.options = ollama_commons.NewOllamaOptions()
			instance.options.DriverOptions = llm_commons.NewLLMDriverOptions()
		}
		response = instance.options
	}
	return
}

func (instance *OllamaAPI) DriverOptions() (response *llm_commons.LLMDriverOptions) {
	if nil != instance {
		response = instance.Options().DriverOptions
	}
	return
}

func (instance *OllamaAPI) GetModel() (response string) {
	if nil != instance {
		options := instance.DriverOptions()
		if nil != options {
			response = options.Model
		}
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	s e r v i c e
//----------------------------------------------------------------------------------------------------------------------

func (instance *OllamaAPI) Version() string {
	if nil != instance && instance.enabled {
		response, _ := Version()
		return response
	}
	return ""
}

// Serve start ollama server. WARN: USE ONLY IF THE SERVICE DOES NOT START AUTOMATICALLY!!!
func (instance *OllamaAPI) Serve() {
	if nil != instance {
		go func() {
			// cmd := fmt.Sprintf("OLLAMA_FLASH_ATTENTION=1 OLLAMA_KV_CACHE_TYPE=f16 %s", command)
			// cmd := fmt.Sprintf("OLLAMA_NUM_PARALLEL=2 %s", ollama_commons.OllamaExec)
			cmd := ollama_commons.OllamaExec
			_, err := gg.Exec.RunBackground(cmd, "serve")
			if nil != err {
				log.Println("ERROR LAUNCHING OLLAMA: ", err)
			}
		}()
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	m o d e l s
//----------------------------------------------------------------------------------------------------------------------

func (instance *OllamaAPI) List() (response *ollama_commons.OllamaCLIResponse) {
	if nil != instance && instance.enabled {
		response = ollama_commons.NewOllamaCLIResponse()
		client, err := instance.cli()
		if nil != err {
			response.SetError(err)
			return
		}
		if nil != client {
			var list *api.ListResponse
			list, err = client.List(context.Background())
			if nil != err {
				response.SetError(err)
			} else {
				response.SetResponse(list.Models)
			}
		}
	}
	return
}

func (instance *OllamaAPI) ListNames() (response *ollama_commons.OllamaCLIResponse) {
	if nil != instance && instance.enabled {
		response = instance.List()
		if response.HasError() {
			return
		}

		names := make([]string, 0)
		if entities, ok := response.GetResponse().([]api.ListModelResponse); ok {
			for _, entity := range entities {
				names = append(names, entity.Name)
			}
		}
		response.SetResponse(names)
	}
	return
}

// Pull Download or Update a model
func (instance *OllamaAPI) Pull(model string, enableStream bool) (response *ollama_commons.OllamaCLIResponse) {
	if nil != instance && instance.enabled {
		response = ollama_commons.NewOllamaCLIResponse()
		cli, err := instance.cli()
		if nil != err {
			response.SetError(err)
			return
		}
		request := new(api.PullRequest)
		request.Model = model
		request.Stream = &enableStream
		err = cli.Pull(context.Background(), request, func(response api.ProgressResponse) error {
			return nil
		})
		if nil != err {
			response.SetError(err)
		} else {
			response.SetResponse("finished")
		}
	}
	return
}

// Delete removes a specific model identified by the given `model` name from the Ollama API server and returns the result.
func (instance *OllamaAPI) Delete(model string) (response *ollama_commons.OllamaCLIResponse) {
	if nil != instance && instance.enabled {
		response = ollama_commons.NewOllamaCLIResponse()
		cli, err := instance.cli()
		if nil != err {
			response.SetError(err)
			return
		}
		request := new(api.DeleteRequest)
		request.Model = model
		err = cli.Delete(context.Background(), request)
		if nil != err {
			response.SetError(err)
		} else {
			response.SetResponse("finished")
		}
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	i l l m d r i v e r
//----------------------------------------------------------------------------------------------------------------------

func (instance *OllamaAPI) Uid() (response string) {
	if nil != instance && nil != instance.options {
		response = instance.options.DriverOptions.Uid
	}
	return
}

func (instance *OllamaAPI) GetOptions() (response *llm_commons.LLMDriverOptions) {
	if nil != instance {
		response = instance.options.DriverOptions
	}
	return
}

func (instance *OllamaAPI) Submit(request *llm_commons.LLMRequest) (response *llm_commons.LLMResponse, err error) {
	if nil != instance && nil != request {
		request.Driver = llm_commons.DriverOllama
		request.SetOptions(instance.options.DriverOptions.OptionsModel)
		// update max tokens with calculated
		if request.GetOptions().MaxTokens > 0 {
			count := request.CalcMaxTokensMin(request.GetOptions().MaxTokens)
			request.GetOptions().MaxTokens = count
		}
		if len(request.Model) == 0 {
			request.Model = ollama_commons.DefaultModel
		}

		// submit request
		response, err = instance.submit(request)
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	embeddings
//----------------------------------------------------------------------------------------------------------------------

// Embeddings
// https://ollama.com/blog/embedding-models
func (instance *OllamaAPI) Embeddings(request *llm_commons.LLMRequestEmbeddings) (response []float64, err error) {
	if nil != instance && instance.enabled {
		background := context.Background()
		var cli *api.Client
		cli, err = instance.cli()
		if nil != err {
			return
		}

		reqEmbeddings := new(api.EmbeddingRequest)
		reqEmbeddings.Model = request.Model // nomic-embed-text, mxbai-embed-large, all-minilm
		reqEmbeddings.Options = request.GetOptions().Map()
		reqEmbeddings.Prompt = request.Prompt

		var respEmbeddings *api.EmbeddingResponse
		respEmbeddings, err = cli.Embeddings(background, reqEmbeddings)
		if nil != err {
			return
		}

		response = respEmbeddings.Embedding
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	q u e r y
//----------------------------------------------------------------------------------------------------------------------

func (instance *OllamaAPI) Chat(model string, oMessages llm_commons.RequestMessageList,
	options map[string]interface{}, keepAlive *api.Duration, format string,
	stream llm_commons.StreamHandler) (response *ollama_commons.OllamaCLIResponse) {
	if nil != instance && instance.enabled {
		isStreamEnabled := nil != stream
		response = ollama_commons.NewOllamaCLIResponse()
		cli, err := instance.cli()
		if nil != err {
			response.SetError(err)
		} else {
			request := new(api.ChatRequest)
			request.Model = model
			request.Stream = &isStreamEnabled
			request.Options = options
			if nil != keepAlive {
				request.KeepAlive = keepAlive
			}
			if oMessages.Count() > 0 {
				oMessages.ForEach(func(item *llm_commons.RequestMessage) any {
					message := api.Message{
						Role:      item.Role,
						Content:   item.Content,
						Images:    make([]api.ImageData, 0),
						ToolCalls: nil,
					}
					for _, image := range item.Images {
						message.Images = append(message.Images, []byte(image))
					}
					request.Messages = append(request.Messages, message)
					return nil
				})
			}
			if len(format) > 0 {
				request.Format = JsonFormat(format)
			}

			buf := new(bytes.Buffer)
			err = cli.Chat(context.Background(), request, func(resp api.ChatResponse) error {
				if isStreamEnabled {
					stream([]byte(resp.Message.Content))
				}
				buf.Write([]byte(resp.Message.Content))
				if resp.Done {
					if isStreamEnabled {
						resp.Message.Content = buf.String()
					}
					response.SetResponse(gg.Convert.ToMap(gg.JSON.Stringify(resp)))
				}
				return nil
			})
			if nil != err {
				response.SetError(err)
			}
		}
	}
	return
}

func (instance *OllamaAPI) Generate(model string, prompt *llm_commons.RequestMessage, raw bool,
	ctx interface{}, template, system string,
	options map[string]interface{}, keepAlive *api.Duration, format string,
	stream llm_commons.StreamHandler) (response *ollama_commons.OllamaCLIResponse) {
	if nil != instance && instance.enabled && nil != prompt {
		isStreamEnabled := nil != stream
		response = ollama_commons.NewOllamaCLIResponse()
		cli, err := instance.cli()
		if nil != err {
			response.SetError(err)
		} else {
			request := new(api.GenerateRequest)
			request.Model = model
			request.Prompt = prompt.Content
			request.Stream = &isStreamEnabled
			request.Options = options
			if raw {
				request.Raw = raw
			}
			if nil != keepAlive {
				request.KeepAlive = keepAlive
			}
			if nil != prompt.Images {
				request.Images = make([]api.ImageData, 0)
				for _, image := range prompt.Images {
					request.Images = append(request.Images, []byte(image))
				}
			}
			if nil != ctx {
				request.Context = gg.Convert.ToArrayOfInt(ctx)
			}
			if len(template) > 0 {
				request.Template = template
			}
			if len(system) > 0 {
				request.System = system
			}
			if len(format) > 0 {
				request.Format = JsonFormat(format)
			}

			buf := new(bytes.Buffer)
			background := context.Background()
			err = cli.Generate(background, request, func(resp api.GenerateResponse) error {
				if isStreamEnabled {
					stream([]byte(resp.Response))
				}
				buf.Write([]byte(resp.Response))
				if resp.Done {
					if isStreamEnabled {
						resp.Response = buf.String()
					}
					response.SetResponse(gg.Convert.ToMap(gg.JSON.Stringify(resp)))
				}
				return nil
			})
			if nil != err {
				response.SetError(err)
			}
		}
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *OllamaAPI) init(args ...interface{}) (err error) {
	if nil != instance {

		for _, arg := range args {
			if o, ok := arg.(*ollama_commons.OllamaOptions); ok {
				instance.options = o
				continue
			}
			if o, ok := arg.(ollama_commons.OllamaOptions); ok {
				instance.options = &o
				continue
			}
			if o, ok := arg.(*llm_commons.LLMDriverOptions); ok {
				if nil == instance.options {
					instance.options = ollama_commons.NewOllamaOptions()
					instance.options.DriverOptions = o
				}
				if nil == instance.options.DriverOptions {
					instance.options.DriverOptions = o
				}
				continue
			}
			if o, ok := arg.(llm_commons.LLMDriverOptions); ok {
				if nil == instance.options {
					instance.options = ollama_commons.NewOllamaOptions()
					instance.options.DriverOptions = &o
				}
				if nil == instance.options.DriverOptions {
					instance.options.DriverOptions = &o
				}
				continue
			}
			if m, ok := arg.(map[string]interface{}); ok {
				err = gg.JSON.Read(m, &instance.options)
				if nil == err {
					if nil == instance.options.DriverOptions {
						instance.options.DriverOptions = llm_commons.NewLLMDriverOptions()
						_ = gg.JSON.Read(m, &instance.options.DriverOptions)
					}
				}
				continue
			}
			if s, ok := arg.(string); ok {
				if gg.Regex.IsValidJsonObject(s) {
					err = gg.JSON.Read(s, &instance.options)
				} else {
					err = gg.JSON.ReadFromFile(s, &instance.options)
				}
				continue
			}
		} // for

		// validate options
		if nil == instance.options {
			instance.options = ollama_commons.NewOllamaOptions()
		}
		if nil == instance.options.DriverOptions {
			instance.options.DriverOptions = llm_commons.NewLLMDriverOptions()
		}
		if nil == instance.options.Api {
			instance.options.Api = &ollama_commons.OllamaApiOptions{
				Host: ollama_commons.OllamaHost,
			}
		}
		if len(instance.options.Api.Host) == 0 {
			instance.options.Api.Host = ollama_commons.OllamaHost
		}

		// test runtime is running, or run
		err = instance.testOllamaRuntime()
		if nil != err {
			return
		}

		// enable the cli tool. OLLAMA is installed and is responding
		instance.enabled = true
	}
	return
}

func (instance *OllamaAPI) submit(request *llm_commons.LLMRequest) (response *llm_commons.LLMResponse, err error) {
	if nil != instance && nil != request {
		response = llm_commons.NewLLMResponse(request)

		background := context.Background()
		buf := new(bytes.Buffer)
		isStreamEnabled := nil != request.Stream

		var cli *api.Client
		cli, err = instance.cli()
		if nil != err {
			return
		}

		switch request.Action {
		case llm_commons.ActionChat:
			//-- CHAT  [ tools ] --//

			ollamaReq := new(api.ChatRequest)
			ollamaReq.Model = request.Model
			ollamaReq.Stream = &isStreamEnabled
			ollamaReq.Format = JsonFormat(request.Format)
			ollamaReq.KeepAlive = nil
			ollamaReq.Options = request.GetOptions().Map()

			for _, item := range request.Messages() {
				if nil != item {
					message := api.Message{
						Role:    item.Role,
						Content: item.Content,
					}
					if len(item.Images) > 0 {
						for _, image := range item.Images {
							message.Images = append(message.Images, []byte(image))
						}
					}
					ollamaReq.Messages = append(ollamaReq.Messages, message)
				}
			}
			// ollamaReq.Tools

			// response
			var ollamaResp api.ChatResponse
			err = cli.Chat(background, ollamaReq, func(resp api.ChatResponse) error {
				if isStreamEnabled {
					request.Stream([]byte(resp.Message.Content))
				}
				buf.Write([]byte(resp.Message.Content))
				if resp.Done {
					resp.Message.Content = buf.String()
					response.SetResponse(gg.Convert.ToMap(gg.JSON.Stringify(resp)))
					ollamaResp = resp
				}
				return nil
			})
			if nil == err {
				response.SetTextResponse(ollamaResp.Message.Content)
			}
		case llm_commons.ActionGenerate:
			//-- GENERATE [ no tools ] --//

			// request
			ollamaReq := new(api.GenerateRequest)
			ollamaReq.Model = request.Model
			ollamaReq.Stream = &isStreamEnabled
			ollamaReq.Format = JsonFormat(request.Format)
			ollamaReq.KeepAlive = nil
			ollamaReq.Options = request.GetOptions().Map()

			ollamaReq.Prompt = request.CalcPrompt()
			ollamaReq.System = request.CalcPromptSystem()
			if len(request.Context) > 0 {
				ollamaReq.Context = request.Context
			}
			if len(request.Images) > 0 {
				for _, image := range request.Images {
					ollamaReq.Images = append(ollamaReq.Images, []byte(image))
				}
			}

			// response
			var ollamaResp api.GenerateResponse
			err = cli.Generate(background, ollamaReq, func(resp api.GenerateResponse) error {
				if isStreamEnabled {
					request.Stream([]byte(resp.Response))
				}
				buf.Write([]byte(resp.Response))
				if resp.Done {
					resp.Response = buf.String()
					response.SetResponse(gg.Convert.ToMap(gg.JSON.Stringify(resp)))
					ollamaResp = resp
				}
				return nil
			})
			if nil == err {
				response.RespContext = ollamaResp.Context
				response.SetTextResponse(ollamaResp.Response)
			}
		default:

		}

	}
	return
}

func (instance *OllamaAPI) cli() (response *api.Client, err error) {
	if nil != instance {
		if nil == instance._cli {
			instance.host, err = url.Parse(instance.options.Api.Host)
			if nil != err {
				return
			}
			instance._cli = api.NewClient(instance.host, http.DefaultClient)
		}
		response = instance._cli
	}
	return
}

func (instance *OllamaAPI) testOllamaRuntime() (err error) {
	if nil != instance {
		var cli *api.Client
		cli, err = instance.cli()
		if nil != err {
			return
		}
		_, err = cli.Version(context.Background())
	}
	return
}
