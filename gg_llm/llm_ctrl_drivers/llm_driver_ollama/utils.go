package llm_driver_ollama

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_ctrl_drivers/llm_driver_ollama/ollama_commons"
	"strings"
)

const command = ollama_commons.OllamaExec

func JsonFormat(value string) (response []byte) {
	if len(value) > 0 {
		if gg.JSON.IsValidJsonObject(value) {
			response = []byte(value)
		} else {
			m := map[string]interface{}{
				"type": "object",
			}
			response = []byte(gg.JSON.Stringify(m))
		}
	}
	return
}

func Version() (response string, err error) {
	var data []byte
	data, err = gg.Exec.Run(command, "--version")
	if nil == err {
		text := string(data)
		if strings.Index(text, "could not connect") != -1 {
			err = gg.Errors.Prefix(gg.PanicSystemError, "Could not connect to ollama server instance: ")
			return
		}
		response = strings.Replace(text, "ollama version is", "", 1)
	}
	return
}

func List() (response string, err error) {
	var data []byte
	data, err = gg.Exec.Run(command, "list")
	if nil == err {
		response = string(data)
	}
	return
}

func Pull(name string) (response string, err error) {
	var data []byte
	data, err = gg.Exec.Run(command, "pull", name)
	if nil == err {
		response = string(data)
	}
	return
}

func Remove(name string) (response string, err error) {
	var data []byte
	data, err = gg.Exec.Run(command, "rm", name)
	if nil == err {
		response = string(data)
	}
	return
}

func Run(name string) (response string, err error) {
	var data []byte
	data, err = gg.Exec.Run(command, "run", name)
	if nil == err {
		response = string(data)
	}
	return
}

func Preload(name string) (response string, err error) {
	var data []byte
	data, err = gg.Exec.Run(command, "run", name, "")
	if nil == err {
		response = string(data)
	}
	return
}

func CopyModel(source, target string) (response string, err error) {
	var data []byte
	data, err = gg.Exec.Run(command, "cp", source, target)
	if nil == err {
		response = string(data)
	}
	return
}
