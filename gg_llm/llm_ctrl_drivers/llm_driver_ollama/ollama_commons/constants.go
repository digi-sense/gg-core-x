package ollama_commons

const (
	OllamaExec = "ollama"
	OllamaRoot = "{{dir_user}}/.ollama/"

	OllamaHost = "http://127.0.0.1:11434/"

	DefaultModel = "llama3.2"
)
