package ollama_commons

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_commons"
)

// ---------------------------------------------------------------------------------------------------------------------
//  OllamaOptions
// ---------------------------------------------------------------------------------------------------------------------

type OllamaOptions struct {
	DriverOptions *llm_commons.LLMDriverOptions `json:"-"`
	Api           *OllamaApiOptions             `json:"api"`
}

func NewOllamaOptions() (instance *OllamaOptions) {
	instance = new(OllamaOptions)
	instance.DriverOptions = llm_commons.NewLLMDriverOptions()
	instance.Api = &OllamaApiOptions{
		Host: OllamaHost,
	}
	return
}

func (instance *OllamaOptions) String() (response string) {
	if nil != instance {
		response = gg.JSON.Stringify(instance)
	}
	return
}

func (instance *OllamaOptions) Map() (response map[string]interface{}) {
	if nil != instance {
		response = gg.Convert.ToMap(instance.String())
	}
	return
}

type OllamaApiOptions struct {
	Host string `json:"host"` // url
}
