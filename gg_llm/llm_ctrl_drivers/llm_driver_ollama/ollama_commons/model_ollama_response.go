package ollama_commons

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_stopwatch"
)

type OllamaCLIResponse struct {
	elapsed  int
	response interface{}
	err      error

	watch *gg_stopwatch.StopWatch
}

func NewOllamaCLIResponse() (instance *OllamaCLIResponse) {
	instance = new(OllamaCLIResponse)
	instance.watch = gg.StopWatch.New().Start()
	return
}

func NewOllamaCLIResponseError(err error) (instance *OllamaCLIResponse) {
	instance = new(OllamaCLIResponse)
	instance.err = err
	instance.watch = gg.StopWatch.New().Start()
	return
}

func (instance *OllamaCLIResponse) String() string {
	if nil != instance {
		return gg.JSON.Stringify(instance.Map())
	}
	return ""
}

func (instance *OllamaCLIResponse) Map() map[string]interface{} {

	return map[string]interface{}{
		"elapsed":  instance.elapsed,
		"response": instance.response,
		"err":      instance.err,
	}
}

func (instance *OllamaCLIResponse) Usage() map[string]interface{} {
	m := instance.GetResponseAsMap()
	completionCount := gg.Maps.GetInt(m, "eval_count")
	promptCount := gg.Maps.GetInt(m, "prompt_eval_count")
	return map[string]interface{}{
		"completion_tokens": completionCount,               // Number of tokens in the generated completion.
		"prompt_tokens":     promptCount,                   // Number of tokens in the prompt.
		"total_tokens":      completionCount + promptCount, // Total number of tokens
	}
}

func (instance *OllamaCLIResponse) SetError(err error) *OllamaCLIResponse {
	if nil != instance {
		instance.err = err
		if nil != instance.watch {
			instance.watch.Stop()
			instance.elapsed = instance.watch.Milliseconds()
			instance.watch = nil
		}
	}
	return instance
}

func (instance *OllamaCLIResponse) GetError() (err error) {
	if nil != instance {
		err = instance.err
	}
	return
}

func (instance *OllamaCLIResponse) HasError() bool {
	if nil != instance {
		return instance.err != nil
	}
	return false
}

func (instance *OllamaCLIResponse) SetResponse(value interface{}) *OllamaCLIResponse {
	if nil != instance {
		instance.response = value
		if nil != instance.watch {
			instance.watch.Stop()
			instance.elapsed = instance.watch.Milliseconds()
			instance.watch = nil
		}
	}
	return instance
}

func (instance *OllamaCLIResponse) GetResponse() (response interface{}) {
	if nil != instance {
		response = instance.response
	}
	return
}

func (instance *OllamaCLIResponse) GetResponseAsMap() (response map[string]interface{}) {
	if nil != instance && nil != instance.response {
		if m, ok := instance.response.(map[string]interface{}); ok {
			return m
		}
		if s, ok := instance.response.(string); ok {
			return gg.Convert.ToMap(s)
		}
	}
	return
}

func (instance *OllamaCLIResponse) GetPromptResponse() (response string) {
	if nil != instance && nil != instance.response {
		if s, ok := instance.response.(string); ok {
			response = s
			return
		}
		if m, ok := instance.response.(map[string]interface{}); ok {
			response = gg.Maps.GetString(m, "message.content")
			if len(response) == 0 {
				response = gg.Maps.GetString(m, "response")
			}
			return
		}
		response = gg.JSON.Stringify(instance.response)
	}
	return
}

func (instance *OllamaCLIResponse) GetPromptResponseFormatted(format string) (response interface{}) {
	if nil != instance {
		r := instance.GetPromptResponse()
		if len(r) > 0 {
			response = r
			if len(format) > 0 {
				switch format {
				case "json":
					if gg.Regex.IsValidJsonObject(r) {
						var m map[string]interface{}
						err := gg.JSON.Read(r, &m)
						if nil == err {
							response = m
						}
					}
					if gg.Regex.IsValidJsonArray(r) {
						var a []map[string]interface{}
						err := gg.JSON.Read(r, &a)
						if nil == err {
							response = a
						}
					}
				}
			}
		}
	}
	return
}

func (instance *OllamaCLIResponse) GetUid() string {
	s := gg.Convert.ToString(instance.response)
	return gg.Coding.MD5(s)
}

func (instance *OllamaCLIResponse) GetContext() []int {
	response := make([]int, 0)
	if nil != instance && nil != instance.response {
		context := gg.Maps.Get(instance.GetResponseAsMap(), "context")
		if nil != context {
			response = gg.Convert.ToArrayOfInt(context)
		}
	}
	return response
}
