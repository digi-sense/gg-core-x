package ollama_commons

import (
	"bitbucket.org/digi-sense/gg-core"
)

func ImageData(files ...string) (response []string, err error) {
	response = make([]string, 0)
	if len(files) > 0 {
		for _, file := range files {
			path := gg.Paths.Absolute(file)
			var data []byte
			data, err = gg.IO.ReadBytesFromFile(path)
			if err != nil {
				return
			}
			response = append(response, gg.Coding.EncodeBase64(data))
		}
	}
	return
}
