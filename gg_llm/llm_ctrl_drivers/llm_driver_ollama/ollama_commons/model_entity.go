package ollama_commons

import (
	"bitbucket.org/digi-sense/gg-core"
	"fmt"
	"strings"
)

// NAME         	ID          	SIZE  	MODIFIED
// llama3:latest	365c0bd3c000	4.7 GB	Less than a second ago

// ModelEntity represents a model with its name, version, unique identifier, size, and last modification date.
type ModelEntity struct {
	Name     string `json:"name"`
	Version  string `json:"version"`
	Id       string `json:"id"`
	Size     string `json:"size"`
	Modified string `json:"last_update"`
}

func NewModelEntity(args ...interface{}) (instance *ModelEntity, err error) {
	instance = new(ModelEntity)
	err = instance.init(args...)

	return
}

func (instance *ModelEntity) FullName() (response string) {
	if nil != instance {
		response = instance.Name
		if len(instance.Version) > 0 && instance.Version != "latest" {
			response = fmt.Sprintf("%s:%s", instance.Name, instance.Version)
		}
	}
	return
}

func (instance *ModelEntity) init(args ...interface{}) (err error) {
	if nil != instance {
		// autodetect
		for _, arg := range args {
			if s, ok := arg.(string); ok {
				if gg.Regex.IsValidJsonObject(s) {
					err = gg.JSON.Read(s, &instance)
				} else if gg.Paths.IsFilePath(s) {
					err = gg.JSON.ReadFromFile(s, &instance)
				} else {
					// parse : llama3:latest	365c0bd3c000	4.7 GB	Less than a second ago
					tokens := strings.Split(strings.TrimSpace(s), "\t")
					nv := gg.Arrays.GetAt(tokens, 0, nil) // name and version
					id := gg.Arrays.GetAt(tokens, 1, nil)
					sz := gg.Arrays.GetAt(tokens, 2, nil)
					mo := gg.Arrays.GetAt(tokens, 3, nil)
					if nil != nv && nil != id && nil != sz && nil != mo {
						instance.Id = gg.Convert.ToString(id)
						instance.Size = gg.Convert.ToString(sz)
						instance.Modified = gg.Convert.ToString(mo)
						// name:version
						tokens = strings.Split(gg.Convert.ToString(nv), ":")
						instance.Name = gg.Convert.ToString(gg.Arrays.GetAt(tokens, 0, ""))
						instance.Version = gg.Convert.ToString(gg.Arrays.GetAt(tokens, 1, ""))
					}
				}
				continue
			}
			if m, ok := arg.(map[string]interface{}); ok {
				nv := gg.Maps.GetString(m, "name")
				tokens := strings.Split(nv, ":")
				size := gg.Convert.ToGigaBytes(gg.Convert.ToInt64(gg.Maps.GetString(m, "size")))
				instance.Name = tokens[0]
				instance.Version = gg.Convert.ToString(gg.Arrays.GetAt(tokens, 1, ""))
				instance.Id = gg.Maps.GetString(m, "digest")
				instance.Size = fmt.Sprintf("%.1f GB", size)
				instance.Modified = gg.Maps.GetString(m, "modified_at")
			}
		}

	}
	return
}
