package llm_ctrl_drivers

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_ctrl_drivers/llm_driver"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_ctrl_drivers/llm_driver_chatgpt"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_ctrl_drivers/llm_driver_ollama"
	"fmt"
)

//----------------------------------------------------------------------------------------------------------------------
//	d r i v e r s
//----------------------------------------------------------------------------------------------------------------------

const DriverOllama = llm_commons.DriverOllama
const DriverChatGPT = llm_commons.DriverChatGPT
const DriverClaude = llm_commons.DriverClaude

//----------------------------------------------------------------------------------------------------------------------
//	i n i t
//----------------------------------------------------------------------------------------------------------------------

var Factory *LLMDriverHelper

func init() {
	Factory = new(LLMDriverHelper)
}

//----------------------------------------------------------------------------------------------------------------------
//	LLMDriverHelper
//----------------------------------------------------------------------------------------------------------------------

type LLMDriverHelper struct {
	store *LLMDriverStore
}

func (instance *LLMDriverHelper) New(driver string, args ...interface{}) (response llm_driver.ILLMDriver, err error) {
	if nil != instance {
		switch driver {
		case DriverChatGPT:
			// CHAT GPT
			response, err = llm_driver_chatgpt.NewChatGPTAPI(args...)
		case DriverOllama:
			// OLLAMA
			response, err = llm_driver_ollama.NewOllamaAPI(args...)
		default:
			err = gg.Errors.Prefix(gg.PanicSystemError, fmt.Sprintf("Driver '%s' not supported: ", driver))
		}
	}
	return
}

func (instance *LLMDriverHelper) Store() (response *LLMDriverStore) {
	if nil != instance {
		if nil == instance.store {
			instance.store = NewDriverStore()
		}
		response = instance.store
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	LLMDriverStore
//----------------------------------------------------------------------------------------------------------------------

// LLMDriverStore manages and stores instances of LLM drivers mapped by their unique identifiers.
type LLMDriverStore struct {
	driverOptions   map[string]*llm_commons.LLMDriverOptions
	driverInstances map[string]llm_driver.ILLMDriver // name, driver. Contain multiple driver instances
}

func NewDriverStore() (instance *LLMDriverStore) {
	instance = new(LLMDriverStore)
	instance.driverInstances = make(map[string]llm_driver.ILLMDriver)
	instance.driverOptions = make(map[string]*llm_commons.LLMDriverOptions)

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *LLMDriverStore) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *LLMDriverStore) Map() (m map[string]interface{}) {
	return gg.Convert.ToMap(gg.JSON.Stringify(instance))
}

//----------------------------------------------------------------------------------------------------------------------
//	s t o r e    l o a d    a n d    s a v e
//----------------------------------------------------------------------------------------------------------------------

func (instance *LLMDriverStore) LoadFromFile(filename string) (response []llm_driver.ILLMDriver, err error) {
	if nil != instance {
		if len(filename) == 0 {
			err = gg.Errors.Prefix(gg.PanicSystemError, "filename is empty")
			return
		}
		var a []map[string]interface{}
		err = gg.JSON.ReadFromFile(filename, &a)
		if nil != err {
			return
		}

		for _, item := range a {
			var options *llm_commons.LLMDriverOptions
			err = gg.JSON.Read(item, &options)
			if nil != err {
				return
			}

			instance.driverOptions[options.Uid] = options

			var driver llm_driver.ILLMDriver
			driver, err = instance.GetOrCreate(options)
			if nil != err {
				return
			}
			response = append(response, driver)
		}
	}
	return
}

func (instance *LLMDriverStore) SaveToFile(filename string) (err error) {
	if nil != instance {
		if len(filename) == 0 {
			err = gg.Errors.Prefix(gg.PanicSystemError, "filename is empty")
			return
		}
		data := make([]*llm_commons.LLMDriverOptions, 0)
		for uid, driver := range instance.driverInstances {
			if nil != driver {
				options := driver.GetOptions()
				if len(options.Uid) == 0 {
					options.Uid = uid
				}
				data = append(data, options)
			}
		}

		filename = gg.Paths.Absolute(filename)
		_, err = gg.IO.WriteTextToFile(gg.JSON.Stringify(data), filename)
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	s t o r e    p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *LLMDriverStore) Count() (response int) {
	if nil != instance {
		response = len(instance.driverInstances)
	}
	return
}

// Names returns a slice of strings containing the names of all registered driver instances in the store.
func (instance *LLMDriverStore) Names() (response []string) {
	if nil != instance && len(instance.driverInstances) > 0 {
		response = make([]string, 0)
		for name, _ := range instance.driverInstances {
			response = append(response, name)
		}
	}
	return
}

// Drivers returns a slice of all registered driver instances.
func (instance *LLMDriverStore) Drivers() (response []llm_driver.ILLMDriver) {
	if nil != instance && len(instance.driverInstances) > 0 {
		response = make([]llm_driver.ILLMDriver, 0)
		for _, driver := range instance.driverInstances {
			response = append(response, driver)
		}
	}
	return
}

func (instance *LLMDriverStore) Contains(uid string) (found bool) {
	if nil != instance && len(instance.driverInstances) > 0 {
		_, found = instance.driverInstances[uid]
		return
	}
	return
}

// New initializes and registers a new driver instance with a unique identifier and specific driver type.
// It returns the created driver instance or an error if the registration fails.
func (instance *LLMDriverStore) New(options *llm_commons.LLMDriverOptions) (response llm_driver.ILLMDriver, err error) {
	if nil != instance && nil != options {
		uid := options.Uid
		if len(uid) == 0 {
			uid = fmt.Sprintf("driver-%v", gg.Rnd.RndDigits(6))
		}
		if _, ok := instance.driverInstances[uid]; ok {
			err = gg.Errors.Prefix(gg.PanicSystemError, fmt.Sprintf("Driver '%s' is already registered: ", uid))
			return
		}
		driver := options.DriverName
		if len(driver) == 0 {
			err = gg.Errors.Prefix(gg.PanicSystemError, fmt.Sprintf("Unable to register '%s' because 'driver-name' is not specified: ", uid))
			return
		}
		response, err = Factory.New(driver, options)
		if nil != err {
			return
		}
		instance.driverOptions[uid] = options
		instance.driverInstances[uid] = response
	}
	return
}

// GetOrCreate retrieves an existing driver instance by UID or creates a new one with the specified type and arguments.
// If the driver does not already exist, it attempts to create and register it, returning the driver or an error.
func (instance *LLMDriverStore) GetOrCreate(options *llm_commons.LLMDriverOptions) (response llm_driver.ILLMDriver, err error) {
	if nil != instance && nil != options {
		uid := options.Uid
		if len(uid) == 0 {
			uid = fmt.Sprintf("driver-%v", gg.Rnd.RndDigits(6))
		}
		if _, ok := instance.driverInstances[uid]; !ok {

		}
		if _, ok := instance.driverInstances[uid]; !ok {
			instance.driverInstances[uid], err = instance.New(options)
			if nil != err {
				return
			}
		}
		response = instance.driverInstances[uid]
	}
	return
}

// Get retrieves a registered driver instance by its unique identifier (UID) or returns an error if not found.
func (instance *LLMDriverStore) Get(uid string) (response llm_driver.ILLMDriver, err error) {
	if nil != instance {
		if _, ok := instance.driverInstances[uid]; !ok {
			err = gg.Errors.Prefix(gg.PanicSystemError, fmt.Sprintf("Driver '%s' is not registered: ", uid))
			return
		}
		response = instance.driverInstances[uid]
	}
	return
}

func (instance *LLMDriverStore) GetByDriverAndModel(driver, model string) (response llm_driver.ILLMDriver, err error) {
	if nil != instance {
		options := instance.GetOptionsByDriverAndModel(driver, model)
		if nil != options {
			response, err = instance.Get(options.Uid)
		}
	}
	return
}

func (instance *LLMDriverStore) GetOptionsByDriverAndModel(driver, model string) (response *llm_commons.LLMDriverOptions) {
	if nil != instance && nil != instance.driverInstances {
		if doptions, ok := instance.driverOptions[driver]; ok {
			response = doptions
			return
		}

		for _, v := range instance.driverOptions {
			if nil != v {
				if (v.Uid == driver || v.DriverName == driver) && (v.Model == model || model == "") {
					response = v
					return
				}
			}
		}
	}
	return
}

// EnsureModel validates and updates the provided LLMRequest object to ensure a valid driver and model are set.
// Returns an error if the driver is not registered or a model is missing for the request.
func (instance *LLMDriverStore) EnsureModel(request *llm_commons.LLMRequest) (err error) {
	if nil != instance && nil != instance.driverInstances {
		// get the driver and submit request
		driverNickName := request.Driver
		options := instance.GetOptionsByDriverAndModel(request.Driver, request.Model)
		// update the request with option data
		if nil != options {
			request.Driver = options.DriverName
			if len(request.Model) == 0 {
				request.Model = options.Model
			}
		} else {
			err = gg.Errors.Prefix(gg.PanicSystemError, fmt.Sprintf("Driver '%s' is not registered: ", driverNickName))
			return
		}

		if len(request.Model) == 0 {
			err = gg.Errors.Prefix(gg.PanicSystemError, fmt.Sprintf("Driver '%s' has no default model for this request: ", driverNickName))
		}
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	r e q u e s t
//----------------------------------------------------------------------------------------------------------------------

func (instance *LLMDriverStore) Submit(request *llm_commons.LLMRequest) (response *llm_commons.LLMResponse, err error) {
	if nil != instance && nil != request {
		if len(request.Driver) == 0 {
			err = gg.Errors.Prefix(gg.PanicSystemError, "driver name is empty")
			return
		}

		err = instance.EnsureModel(request)
		if nil != err {
			return
		}

		// get the driver with the nickname
		var driver llm_driver.ILLMDriver
		driver, err = instance.GetByDriverAndModel(request.Driver, request.Model)
		if nil != err {
			return
		}

		// submit request to the retrieved driver
		response, err = driver.Submit(request)
	}
	return
}
