package llm_zagent

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_ctrl_drivers"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_ctrl_drivers/llm_driver"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_ctrl_embeddings"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_ctrl_sessions"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_ctrl_utils"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_prompts/prompts"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_prompts/skills"
	"bitbucket.org/digi-sense/gg-core/gg_"
	"fmt"
	"time"
)

//----------------------------------------------------------------------------------------------------------------------
//	AIAgent
//----------------------------------------------------------------------------------------------------------------------

type AIAgent struct {
	name    string
	dirRoot string
	logger  gg_.ILogger

	options     *llm_commons.OptionsAgent
	filename    string // load or save file
	skillsToAdd []*skills.Skill

	ctrlDirs       *llm_ctrl_utils.CtrlDirs
	ctrlDrivers    *llm_ctrl_drivers.LLMDriverStore   // Agent can use many drivers
	ctrlSkills     *skills.Skills                     // skill and prompts controller
	ctrlSessions   *llm_ctrl_sessions.LLMSessions     // session controller
	ctrlEmbeddings *llm_ctrl_embeddings.LLMEmbeddings // generate context from documents (used before session load new context)
}

func NewAIAgent(root string, args ...any) (instance *AIAgent, err error) {
	instance = new(AIAgent)
	instance.name = fmt.Sprintf("agent-%v", gg.Rnd.RndDigits(6))
	instance.dirRoot = gg.Paths.Absolute(root)

	err = instance.init(args...)

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *AIAgent) String() string {
	if nil != instance {
		return gg.JSON.Stringify(instance.Map())
	}
	return ""
}

func (instance *AIAgent) Map() (response map[string]any) {
	if nil != instance {
		response = make(map[string]any)
		response["name"] = instance.name
		response["dir-root"] = instance.ctrlDirs.Root
		response["dir-prompts"] = instance.ctrlDirs.Prompts
		response["dir-vectors"] = instance.ctrlDirs.Vectors
		if nil != instance.options {
			response["options"] = instance.options.Map()
		}
		if nil != instance.ctrlDrivers {
			response["drivers"] = instance.ctrlDrivers.Map()
		}
		if nil != instance.ctrlSkills {
			response["skills"] = instance.ctrlSkills.Map()
		}
	}
	return
}

func (instance *AIAgent) GetName() string {
	if nil != instance {
		return instance.name
	}
	return ""
}

func (instance *AIAgent) SetName(value string) *AIAgent {
	if nil != instance && len(value) > 0 {
		instance.name = value
	}
	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	l o a d / s a v e
//----------------------------------------------------------------------------------------------------------------------

func (instance *AIAgent) SetFilename(filename string) *AIAgent {
	if nil != instance {
		instance.filename = gg.Paths.Absolutize(filename, instance.dirRoot)
	}
	return instance
}

func (instance *AIAgent) LoadFromFile(filename string) (err error) {
	if nil != instance {
		instance.SetFilename(filename)
		err = gg.JSON.ReadFromFile(filename, &instance.options)
		if nil != err {
			return
		}

		if nil != instance.options {
			// update the name
			if len(instance.options.Name) > 0 {
				instance.SetName(instance.options.Name)
			}
		}

		// align options
		err = instance.refreshOptions()
	}
	return
}

func (instance *AIAgent) SaveToFile(filename string) (err error) {
	if nil != instance && nil != instance.options {
		instance.SetFilename(filename)
		_, err = gg.IO.WriteTextToFile(gg.JSON.Stringify(instance.options), filename)
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	d r i v e r s
//----------------------------------------------------------------------------------------------------------------------

// GetDrivers retrieves the collection of AI drivers associated with the agent.
// Returns a slice of ILLMDriver instances.
func (instance *AIAgent) GetDrivers() (response []llm_driver.ILLMDriver) {
	if nil != instance {
		response = instance.ctrlDrivers.Drivers()
	}
	return
}

// AddDriver adds an AI driver to the agent based on the provided argument,
// which can be a map, string, or driver options.
// It updates the agent's configuration and saves it to a file if a filename is specified.
func (instance *AIAgent) AddDriver(arg any) (err error) {
	if nil != instance && nil != instance.ctrlDrivers {
		var options *llm_commons.LLMDriverOptions
		if m, ok := arg.(map[string]interface{}); ok {
			_ = gg.JSON.Read(m, &options)
		} else if s, sok := arg.(string); sok {
			_ = gg.JSON.Read(s, &options)
		} else if o, ook := arg.(llm_commons.LLMDriverOptions); ook {
			options = &o
		} else if o, ook := arg.(*llm_commons.LLMDriverOptions); ook {
			options = o
		} else {
			err = gg.Errors.Prefix(gg.PanicSystemError, "Unsupported argument type: ")
			return
		}

		if nil != options {
			uid := options.Uid
			if !instance.ctrlDrivers.Contains(uid) {
				var driver llm_driver.ILLMDriver
				driver, err = instance.ctrlDrivers.GetOrCreate(options)
				if nil == err {
					instance.options.AddDriver(driver.GetOptions())
				}

				// UPDATE FILE
				if len(instance.filename) > 0 {
					err = instance.SaveToFile(instance.filename)
				}
			}
		}
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	s k i l l s
//----------------------------------------------------------------------------------------------------------------------

func (instance *AIAgent) AddSkill(skill *skills.Skill) {
	if nil != instance {
		if nil != instance.ctrlSkills {
			// after open
			instance.ctrlSkills.Add(skill)
			_ = instance.ctrlSkills.Deploy()
		} else {
			// before open
			instance.skillsToAdd = append(instance.skillsToAdd, skill)
		}
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	o p e n  /  c l o s e
//----------------------------------------------------------------------------------------------------------------------

func (instance *AIAgent) Open() (err error) {
	if nil != instance {
		err = instance.open()
	}
	return
}

func (instance *AIAgent) Close() {
	if nil != instance {
		instance.close()
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	s e s s i o n s
//----------------------------------------------------------------------------------------------------------------------

func (instance *AIAgent) SessionClear(sessionId string) (response *llm_ctrl_sessions.LLMSessionData) {
	if nil != instance && nil != instance.ctrlSessions {
		response = instance.ctrlSessions.Clear(sessionId)
	}
	return
}

func (instance *AIAgent) SessionGetData(sessionId string) (response *llm_ctrl_sessions.LLMSessionData) {
	if nil != instance && nil != instance.ctrlSessions {
		session := instance.ctrlSessions.GetSession(sessionId)
		if nil != session {
			response = session.GetData()
		}
	}
	return
}

// SessionPin set a pin to session.
// Returns the session data
func (instance *AIAgent) SessionPin(sessionId string, value bool) (response *llm_ctrl_sessions.LLMSessionData) {
	if nil != instance && nil != instance.ctrlSessions {
		session := instance.ctrlSessions.GetSession(sessionId)
		if nil != session {
			session.PinContext(value)
			response = session.GetData()
		}
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	r e q u e s t s
//----------------------------------------------------------------------------------------------------------------------

func (instance *AIAgent) Submit(request *llm_commons.LLMRequest) (response *llm_commons.LLMResponse, err error) {
	if nil != instance && nil != request {

		// ensure that the request has a valid model
		err = instance.ctrlDrivers.EnsureModel(request)
		if nil != err {
			return
		}

		// check skill and prompt
		err = llm_ctrl_utils.EnsurePrompts(instance.ctrlSkills, request)
		if nil != err {
			return
		}

		// add session info if required
		// and get context knowledge if used in prompt
		contextResponse := instance.initRequestSession(request)

		// check agent options
		if instance.options.AutoCalcMaxTokens {
			maxTokens := request.CalculateMaxTokens()
			request.SetOptionMaxTokensWithLimit(maxTokens, instance.options.LimitMaxTokens)
		}

		// get a response
		response, err = instance.ctrlDrivers.Submit(request)
		if nil != err {
			return
		}

		// add context response to response
		if nil != contextResponse {
			if len(contextResponse.RespKnowledge) > 0 {
				if len(response.RespKnowledge) == 0 {
					response.RespKnowledge = contextResponse.RespKnowledge
				} else {
					response.RespKnowledge = append(response.RespKnowledge, contextResponse.RespKnowledge...)
				}
			}
			if len(contextResponse.RespKnowledgeElements) > 0 {
				if len(response.RespKnowledgeElements) == 0 {
					response.RespKnowledgeElements = contextResponse.RespKnowledgeElements
				} else {
					response.RespKnowledgeElements = append(response.RespKnowledgeElements, contextResponse.RespKnowledgeElements...)
				}
			}
		}

		// add session info
		instance.initResponseSession(response)
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *AIAgent) init(args ...any) (err error) {
	if nil != instance {

		// parse args
		for _, arg := range args {
			if nil != arg {
				if l, ok := arg.(gg_.ILogger); ok {
					instance.logger = l
					continue
				}
				if m, ok := arg.(map[string]interface{}); ok {
					err = instance.AddDriver(m)
					if nil != err {
						return
					}
					continue
				}
				if s, ok := arg.(map[string]interface{}); ok {
					err = instance.AddDriver(s)
					if nil != err {
						return
					}
					continue
				}
				if o, ok := arg.(llm_commons.LLMDriverOptions); ok {
					err = instance.AddDriver(o)
					if nil != err {
						return
					}
					continue
				}
				if o, ok := arg.(*llm_commons.LLMDriverOptions); ok {
					err = instance.AddDriver(o)
					if nil != err {
						return
					}
					continue
				}
				if o, ok := arg.(*llm_commons.OptionsAgent); ok {
					instance.options = o
					continue
				}
				if o, ok := arg.(llm_commons.OptionsAgent); ok {
					instance.options = &o
					continue
				}
			}
		}

		if nil == instance.options {
			instance.options = llm_commons.NewOptionsAgent()
		}

	}
	return
}

func (instance *AIAgent) refreshOptions() (err error) {
	if nil != instance && nil != instance.options {

		// add drivers
		if len(instance.options.Drivers) > 0 {
			for _, driver := range instance.options.Drivers {
				err = instance.AddDriver(driver)
				if nil != err {
					return
				}
			}
		}

	}
	return
}

func (instance *AIAgent) open() (err error) {
	if nil != instance {
		// set the name
		if len(instance.options.Name) > 0 && len(instance.name) == 0 {
			instance.name = instance.options.Name
		}

		// directories
		instance.ctrlDirs = llm_ctrl_utils.NewCtrlDirs(instance.name, instance.dirRoot)
		instance.ctrlDirs.Mkdir()

		// init the logger if missing
		if nil == instance.logger {
			filename := gg_.PathConcat(instance.ctrlDirs.Workspace, "./logging/agent.log")
			instance.logger = gg.Log.NewNoRotate("info", filename)
		}

		// initialize controllers
		// CTRL sessions
		instance.ctrlSessions = llm_ctrl_sessions.NewLLMSessions(instance.dirRoot, instance.ctrlDirs.Sessions)
		// CTRL drivers
		instance.ctrlDrivers = llm_ctrl_drivers.NewDriverStore()
		if nil != instance.options {
			drivers := instance.options.Drivers
			for _, driver := range drivers {
				_, _ = instance.ctrlDrivers.GetOrCreate(driver)
			}
		}
		// CTRL Skills
		instance.ctrlSkills, err = skills.NewSkills(instance.ctrlDirs.Prompts, instance.ctrlDirs.Vectors)
		if nil != err {
			return
		}
		// add skills into list
		if len(instance.skillsToAdd) > 0 {
			for _, skill := range instance.skillsToAdd {
				instance.ctrlSkills.Add(skill)
			}
			instance.skillsToAdd = nil
		}
		err = instance.ctrlSkills.Deploy()
		if nil != err {
			return
		}
		// CTRL context
		instance.ctrlEmbeddings, err = llm_ctrl_embeddings.NewLLMEmbeddings(instance.ctrlDirs, instance.options.Embeddings)
		if nil != err {
			return
		}
		err = instance.ctrlEmbeddings.Open()
		if nil != err {
			return
		}

		// align options
		err = instance.refreshOptions()
		if nil != err {
			return
		}

		instance.logger.Info(fmt.Sprintf("Agent '%s' open success. dirRoot: %s",
			instance.name, instance.dirRoot))
	}
	return
}

func (instance *AIAgent) close() {
	if nil != instance {
		time.Sleep(300 * time.Millisecond)
	}
}

func (instance *AIAgent) getRequestSkill(request *llm_commons.LLMRequest) (skill *skills.Skill, prompt *prompts.Prompt) {
	if nil != instance && nil != instance.ctrlSkills && nil != request {
		if len(request.SkillName) > 0 && len(request.PromptName) > 0 {
			skill, prompt = instance.ctrlSkills.GetAll(request.SkillName, request.PromptName, request.Payload.Lang)
		}
	}
	return
}

func (instance *AIAgent) initRequestSession(request *llm_commons.LLMRequest) (contextResponse *llm_commons.LLMResponse) {
	if nil != instance && nil != instance.ctrlSessions && nil != request {

		userQuery := request.CalcPrompt()
		model := request.Model
		lang := request.Payload.Lang

		// check if there are data to add to context
		// or RAG is requested from vector-id field
		if request.ContextualizerHasData() || len(request.VectorId) > 0 {
			// contextualize
			// Contextualization decide if do RAG or not.
			// If RAG is done a vector-id field is returned from response
			knowledge := request.ContextualizerGetData()
			ctxContext, err := instance.ctrlEmbeddings.Contextualize(request.SessionId,
				request.VectorId, userQuery, request.VectorMaxResults, lang, model, knowledge...)
			if nil == err {
				// SET RESPONSE
				contextResponse = ctxContext
			}
		}

		if len(request.SessionId) > 0 && nil != contextResponse {
			// put context data to session
			contextData := contextResponse.RespContext
			instance.ctrlSessions.AddContext(request.SessionId, model, contextData)
			// get data from session
			sessionData := instance.ctrlSessions.GetSessionData(request.SessionId)
			if nil != sessionData {
				if nil != sessionData.Context && nil != sessionData.Context.Context {
					request.Context = gg.Convert.ToArrayOfInt(sessionData.Context.Context)
				}
			}
		}
	}
	return
}

func (instance *AIAgent) initResponseSession(response *llm_commons.LLMResponse) {
	if nil != instance && nil != instance.ctrlSessions && nil != response {
		instance.ctrlSessions.AddModel(response.SessionId, response.Model)
		instance.ctrlSessions.AddContext(response.SessionId, response.Model, response.RespContext)
		instance.ctrlSessions.AddSystemText(response.SessionId, response.RequestSystem)
		instance.ctrlSessions.AddUserText(response.SessionId, response.RequestUser)
		instance.ctrlSessions.AddAssistantText(response.SessionId, response.RespText)

		// add elements as files or content to session for debug info.
		// NOTE: the real context data are saved as vectors in context.
		if len(response.RespKnowledgeElements) > 0 {
			for _, element := range response.RespKnowledgeElements {
				if gg.Paths.IsFilePath(element) {
					_, _ = instance.ctrlSessions.AddFile(response.SessionId, element)
				} else {
					_, _ = instance.ctrlSessions.AddFileContent(response.SessionId, "", []byte(element))
				}
			}
		}

		instance.ctrlSessions.Save(response.SessionId)
	}
}
