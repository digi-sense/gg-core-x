# LLM Agents

This package contains an implementation of a basic AI Agent.

An agent is able to instantiate an LLM driver, use tools, libs and elaborate complex tasks.

Agents use:
- drivers: to connect to different LLMs. An agent can use multiple drivers
- tools: usually exposed to LLM requests