package llm_ctrl_compiler

import "bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_ctrl_drivers/llm_driver_ollama"

//----------------------------------------------------------------------------------------------------------------------
//	LLMCompiler
//----------------------------------------------------------------------------------------------------------------------

// LLMCompiler is a struct responsible for compiling and managing LLM operations through an OllamaAPI driver.
type LLMCompiler struct {
	driver *llm_driver_ollama.OllamaAPI
}

func NewLLMCompiler(args ...any) (instance *LLMCompiler, err error) {
	instance = new(LLMCompiler)
	err = instance.init(args...)
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *LLMCompiler) init(args ...any) (err error) {
	if nil != instance {

	}
	return
}
