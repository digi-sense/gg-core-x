# Prompt Controller

This package contains a controller for "prompts".

PROMPTS are collections of text requests for LLMs.

The data structure of prompt organization is:

- SKILL 1
  - PACKAGE 1.1
    - PROMPT 1.1.1
    - PROMPT 1.1.2
    - PROMPT 1.1.3  
- SKILL 2
    - PACKAGE 2.1
        - PROMPT 2.1.1
        - PROMPT 2.1.2
        - PROMPT 2.1.3  
    - PACKAGE 2.2
      - PROMPT 2.2.1
      - PROMPT 2.2.2
      - PROMPT 2.2.3  

A SKILL contains PACKAGES of PROMPTS.

Real World example:

- SKILL COPYWRITER 
  - PACKAGE NEWSPAPER
    - PROMPT SPORT
    - PROMPT CHRONICLE
  - PACKAGE BLOG
    - PROMPT FRIENDLY
    - PROMPT PROFESSIONAL

Packages are just collections of prompts.

Skills contains Packages and also have a system prompt and json metadata containing configuration options.

