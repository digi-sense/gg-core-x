package prompts

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_commons"
	"fmt"
	"github.com/cbroglie/mustache"
	"path/filepath"
	"strings"
)

// ---------------------------------------------------------------------------------------------------------------------
//  S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func getPrompt(store map[string]map[string]*Prompt, lang, name string) (response *Prompt) {
	promptsLock.RLock()
	defer promptsLock.RUnlock()

	if m, ok := store[name]; ok {
		if s, ok := m[lang]; ok {
			response = s
		} else if s, ok = m[llm_commons.PromptDefLang]; ok {
			response = s
		}
	}
	return
}

func getPromptContent(store map[string]map[string]*Prompt, lang, name string) (response string) {
	p := getPrompt(store, lang, name)
	if nil != p {
		response = p.GetContent()
	}
	return
}

func existsPrompt(store map[string]map[string]*Prompt, lang, name string) bool {
	if nil != store && len(name) > 0 {
		if len(lang) == 0 {
			lang = llm_commons.PromptDefLang
		}
		if langs, ok := store[name]; ok {
			if _, ok := langs[lang]; ok {
				return true
			}
		}
	}
	return false
}

func addPrompt(store map[string]map[string]*Prompt, lang, name, text string, options map[string]interface{}) (err error) {
	if len(lang) == 0 {
		lang = llm_commons.PromptDefLang
	}
	if len(name) > 0 && len(text) > 0 {
		if _, ok := store[name]; !ok {
			store[name] = make(map[string]*Prompt)
		}
		err = addPromptItem(store, &Prompt{
			store:   store,
			Lang:    lang,
			Name:    name,
			Content: text,
			Payload: make(map[string]interface{}),
			Options: options,
		})
	}
	return
}

func deployPrompt(store map[string]map[string]*Prompt, lang, name, text, root string, options map[string]interface{}, overwrite bool) (err error) {
	if len(text) == 0 {
		err = gg.Errors.Prefix(gg.PanicSystemError, "Cannot deploy empty prompt!: ")
		return
	}
	if len(name) == 0 {
		err = gg.Errors.Prefix(gg.PanicSystemError, "Cannot deploy prompt with empty name!: ")
		return
	}
	if len(root) == 0 {
		err = gg.Errors.Prefix(gg.PanicSystemError, "Cannot deploy prompt in empty root path: ")
		return
	}
	if strings.Index(root, "{{") > -1 {
		err = gg.Errors.Prefix(gg.PanicSystemError,
			fmt.Sprintf("Cannot deploy prompt in unresolved root path '%s': ", root))
		return
	}
	if len(lang) == 0 {
		lang = llm_commons.PromptDefLang
	}

	dir := root
	base := filepath.Base(root)
	if base != name {
		dir = gg.Paths.Concat(dir, name)
	}

	err = writePrompt(store, lang, name, text, options, overwrite, dir)

	return
}

func writePrompt(store map[string]map[string]*Prompt, lang, name, text string, options map[string]interface{}, overwrite bool, dir string) (err error) {
	// write prompt
	filename := gg.Paths.Concat(dir, fmt.Sprintf("%s.txt", lang))
	exists, _ := gg.Paths.Exists(filename)
	if exists && !overwrite {
		if !existsPrompt(store, lang, name) {
			err = addPrompt(store, lang, name, text, options)
		}
		return
	} else {
		err = gg.Paths.Mkdir(filename)
		if err != nil {
			return
		}

		_, err = gg.IO.WriteTextToFile(text, filename)
		if err != nil {
			return
		}
		// addPrompt
		err = addPrompt(store, lang, name, text, options)
	}

	// write options
	if len(options) > 0 {
		filename = gg.Paths.Concat(dir, "options.json")
		exists, _ = gg.Paths.Exists(filename)
		if !exists || overwrite {
			_, err = gg.IO.WriteTextToFile(gg.JSON.Stringify(options), filename)
		}
	}
	return
}

func addPromptItem(store map[string]map[string]*Prompt, prompt *Prompt) (err error) {
	promptsLock.Lock()
	defer promptsLock.Unlock()

	if nil != prompt && len(prompt.Name) > 0 {
		name := prompt.Name
		lang := prompt.Lang
		if _, ok := store[name]; !ok {
			store[name] = make(map[string]*Prompt)
		}
		store[name][lang] = prompt
	} else {
		err = gg.Errors.Prefix(gg.PanicSystemError, "Invalid Prompt: ")
	}
	return
}

func loadPromptFromDir(store map[string]map[string]*Prompt, path string) (response []string, err error) {
	var files []string
	files, err = gg.Paths.ListFiles(path, "*.txt")
	for _, filename := range files {
		var promptName string
		lang := gg.Paths.FileName(filename, false)
		langName := llm_commons.LangName(lang)
		if len(langName) > 0 {
			promptName, err = loadPromptFromFile(store, lang, filename)
		} else {
			promptName, err = loadPromptFromFile(store, llm_commons.PromptDefLang, filename)
		}
		if nil != err {
			break
		}
		// add prompt name to response
		if len(promptName) > 0 {
			response = append(response, promptName)
		}
	}
	return
}

func loadPromptFromFile(store map[string]map[string]*Prompt, lang, filename string) (promptName string, err error) {
	var text string
	text, err = gg.IO.ReadTextFromFile(filename)
	if nil == err && len(text) > 0 {
		dir := gg.Paths.Dir(filename)
		name := gg.Paths.FileName(dir, false)
		prompt := &Prompt{
			Name:    name,
			Lang:    lang,
			Content: text,
			Payload: nil,
			Options: loadPromptOptions(filename),
			store:   store,
		}
		err = addPromptItem(store, prompt)
		promptName = getPromptName(filename)
	}
	return
}

func loadPromptOptions(promptFile string) (response map[string]interface{}) {
	dir := gg.Paths.Dir(promptFile)
	filename := gg.Paths.Concat(dir, "options.json")
	s, _ := gg.IO.ReadTextFromFile(filename)
	if len(s) > 0 {
		response = gg.Convert.ToMap(s)
	}
	return
}

func getPromptName(filename string) string {
	return filepath.Base(filepath.Dir(filename))
}

func renderMustache(text string, context ...interface{}) (string, error) {
	response, err := mustache.Render(text, context...)
	if nil != err {
		response = text
	}
	return response, err
}
