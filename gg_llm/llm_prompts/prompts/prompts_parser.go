package prompts

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_commons"
	"strings"
)

type PromptValueParser struct {
	Prefix       string
	PromptName   string
	PromptParams map[string]interface{}
	Value        string
}

func NewPromptValueParser(text string) (instance *PromptValueParser) {
	instance = new(PromptValueParser)
	instance.PromptParams = make(map[string]interface{})
	instance.Value = text
	// parse text
	if strings.Index(text, llm_commons.PrefixPrompt) == 0 {
		// START WITH prompt:
		instance.Prefix = llm_commons.PrefixPrompt
		text = strings.ReplaceAll(text, llm_commons.PrefixPrompt, "")
		if len(text) > 0 {
			tokens := strings.Split(text, ":")
			instance.PromptName = tokens[0]
			// parse value map
			// param1=a&param2=b
			if len(tokens) == 2 {
				pairs := strings.Split(tokens[1], "&")
				count := 0
				for _, pair := range pairs {
					// param1=a
					kv := strings.Split(pair, "=")
					if len(kv) == 2 {
						k := strings.TrimSpace(kv[0])
						v := strings.TrimSpace(kv[1])
						instance.PromptParams[k] = v
						if count == 0 {
							instance.Value = v
						}
						count++
					}
				}
			}
		}
	} else if strings.Index(text, llm_commons.PrefixLiteral) == 0 {
		instance.Prefix = llm_commons.PrefixLiteral
		text = strings.ReplaceAll(text, llm_commons.PrefixLiteral, "")
		if len(text) > 0 {
			instance.Value = text
		}
	}
	return
}
