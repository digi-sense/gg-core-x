package prompts

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_commons"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"sync"
)

// ---------------------------------------------------------------------------------------------------------------------
//  prompts
// ---------------------------------------------------------------------------------------------------------------------

var promptsLock sync.RWMutex

// ---------------------------------------------------------------------------------------------------------------------
//  PromptPackages
// ---------------------------------------------------------------------------------------------------------------------

// PromptPackages is a collection that maps package names to their PromptPackage instances.
type PromptPackages struct {
	root      string // force relative paths to this root
	overwrite bool
	mux       *sync.Mutex
	packages  map[string]*PromptPackage
}

func NewPromptPackages() (instance *PromptPackages) {
	instance = new(PromptPackages)
	instance.packages = make(map[string]*PromptPackage)
	instance.mux = new(sync.Mutex)
	return
}

func (instance *PromptPackages) Map() map[string]interface{} {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()
		response := make(map[string]interface{})
		for name, pkg := range instance.packages {
			response[name] = pkg.Map()
		}
		return response
	}
	return nil
}

func (instance *PromptPackages) String() string {
	if nil != instance {
		return gg.JSON.Stringify(instance.Map())
	}
	return ""
}

func (instance *PromptPackages) SetOverwrite(v bool) *PromptPackages {
	if nil != instance {
		instance.overwrite = v
	}
	return instance
}

func (instance *PromptPackages) GetOverwrite() bool {
	if nil != instance {
		return instance.overwrite
	}
	return false
}

func (instance *PromptPackages) SetRoot(v string) *PromptPackages {
	if nil != instance {
		instance.Root(v)
	}
	return instance
}

func (instance *PromptPackages) Root(v ...string) string {
	if nil != instance {
		if len(v) > 0 {
			instance.root = v[0]
			instance.mux.Lock()
			defer instance.mux.Unlock()
			for _, pkg := range instance.packages {
				pkg.Root(instance.root)
			}
		}
		return instance.root
	}
	return ""
}

func (instance *PromptPackages) Name() (response string) {
	if nil != instance && len(instance.root) > 0 {
		response = filepath.Base(instance.root)
	}
	return
}

//-- packages --//

func (instance *PromptPackages) Exists(name string) (response bool) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		if _, ok := instance.packages[name]; ok {
			response = true
		}
	}
	return
}

func (instance *PromptPackages) Names() (response []string) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		response = make([]string, 0)
		for name := range instance.packages {
			response = append(response, name)
		}
	}
	return
}

func (instance *PromptPackages) Count() int {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		return len(instance.packages)
	}
	return 0
}

func (instance *PromptPackages) New(name string) (response *PromptPackage) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		response = instance.new(name)
	}
	return
}

func (instance *PromptPackages) Get(name string) (response *PromptPackage) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		if _, ok := instance.packages[name]; ok {
			response = instance.packages[name]
			return
		}

	}
	return
}

func (instance *PromptPackages) Add(item *PromptPackage) (response *PromptPackages) {
	if nil != instance && nil != item && len(item.name) > 0 {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		if len(item.root) == 0 {
			item.Root(instance.root)
		}
		name := item.name
		overwrite := instance.overwrite
		if _, ok := instance.packages[name]; !ok || overwrite {
			instance.packages[item.name] = item
		}
	}
	return instance
}

func (instance *PromptPackages) LoadDirs(packagesDir []string) (response []string, err error) {
	if nil != instance {
		for _, dir := range packagesDir {
			response, err = instance.LoadDir(dir)
			if nil != err {
				return
			}
		}
	}
	return
}

func (instance *PromptPackages) Load() (response []string, err error) {
	return instance.LoadDir(instance.root)
}

func (instance *PromptPackages) LoadDir(packagesDir string) (response []string, err error) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		// absolutize
		packagesDir = instance.absolutize(packagesDir)
		// read al subdirectories
		var dirs []os.DirEntry
		dirs, err = os.ReadDir(packagesDir)
		// loop on each dir/package-name
		for _, dir := range dirs {
			name := dir.Name()
			if !strings.HasPrefix(name, ".") {
				pkg := instance.new(name)
				pkg.Root(packagesDir)
				_, err = pkg.Load(gg.Paths.Concat(packagesDir, dir.Name()))
				if nil == err {
					response = append(response, pkg.name)
				} else {
					return
				}
			}
		}
	}
	return
}

func (instance *PromptPackages) ForEach(callback func(packageName, name, lang string, prompt *Prompt) (err error)) error {
	if nil != instance && nil != instance.packages && nil != callback {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		for pn, pkg := range instance.packages {
			if nil != pkg {
				err := pkg.ForEach(func(name, lang string, prompt *Prompt) error {
					return callback(pn, name, lang, prompt)
				})
				if nil != err {
					return err
				}
			}
		}
	}
	return nil
}

func (instance *PromptPackages) ForEachPackage(callback func(pkg *PromptPackage) (err error)) error {
	if nil != instance && nil != instance.packages && nil != callback {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		for _, pkg := range instance.packages {
			if nil != pkg {
				err := callback(pkg)
				if nil != err {
					return err
				}
			}
		}
	}
	return nil
}

//-- prompt --//

func (instance *PromptPackages) Prompt(packageName, name, lang string) (response *Prompt) {
	if nil != instance {
		if len(packageName) > 0 {
			p := instance.Get(packageName)
			if nil != p {
				response = p.Store().Get(lang, name)
				return
			}
		} else {
			for _, pkg := range instance.packages {
				response = pkg.Store().Get(lang, name)
				if nil != response {
					return
				}
			}
		}
	}
	return
}

func (instance *PromptPackages) PromptExists(promptName string) bool {
	if nil != instance {
		return instance.PromptExistsLang(promptName, llm_commons.PromptDefLang)
	}
	return false
}

func (instance *PromptPackages) PromptExistsLang(promptName string, lang string) bool {
	if nil != instance && nil != instance.packages {
		for _, pkg := range instance.packages {
			if pkg.Store().ExistsLang(promptName, lang) {
				return true
			}
		}
	}
	return false
}

func (instance *PromptPackages) PromptNames() (response []string) {
	if nil != instance && nil != instance.packages {
		for _, pkg := range instance.packages {
			response = append(response, pkg.Store().Names()...)
		}
	}
	return
}

func (instance *PromptPackages) PromptNamesFromPackage(packageName string) (response []string) {
	if nil != instance && nil != instance.packages {
		for _, pkg := range instance.packages {
			if pkg.name == packageName {
				response = append(response, pkg.Store().Names()...)
			}
		}
	}
	return
}

func (instance *PromptPackages) PromptGetDefault(promptName string) (response *Prompt) {
	if nil != instance && nil != instance.packages {
		response = instance.PromptGet(promptName, llm_commons.PromptDefLang)
	}
	return
}

func (instance *PromptPackages) PromptGet(promptName, promptLang string) (response *Prompt) {
	if nil != instance && nil != instance.packages {
		_ = instance.ForEach(func(packageName, name, lang string, prompt *Prompt) error {
			if promptName == name && promptLang == lang {
				response = prompt
				return errors.New("break")
			}
			return nil
		})
		if nil == response && promptLang != llm_commons.PromptDefLang {
			response = instance.PromptGet(promptName, llm_commons.PromptDefLang)
		}
	}
	return
}

func (instance *PromptPackages) PromptLoadFile(packageName, lang, filename string) (promptName string, err error) {
	if nil != instance && len(lang) > 0 && len(filename) > 0 {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		promptName, err = instance.new(packageName).Store().LoadFile(lang, filename)
	}
	return
}

func (instance *PromptPackages) PromptAdd(packageName, lang, name, text string) (err error) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()
		err = instance.new(packageName).Store().Add(lang, name, text)
	}
	return
}

func (instance *PromptPackages) PromptDeploy(packageName, lang, name, text, root string, options map[string]interface{}, overwrite bool) (err error) {
	if nil != instance && len(lang) > 0 && len(name) > 0 && len(text) > 0 && len(root) > 0 {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		// absolutize
		root = instance.absolutize(root)
		// add package name
		root = gg.Paths.Concat(root, packageName)
		// deploy
		err = instance.new(packageName).Store().Deploy(lang, name, text, root, options, overwrite)
	}
	return
}

//-- io --//

func (instance *PromptPackages) Deploy() (err error) {
	if nil != instance {
		err = instance.DeployTo(instance.root, instance.overwrite)
	}
	return
}

// DeployTo deploys all packages in PromptPackages to the specified root directory.
// root: The target directory for deployment.
// overwrite: Whether to overwrite existing files.
// Returns an error if any package deployment fails.
// Target Dir is: ./root/pkg_name/prompt_name
func (instance *PromptPackages) DeployTo(root string, overwrite bool) (err error) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		root = instance.rootValidate(root)
		for _, pkg := range instance.packages {
			err = pkg.Deploy(root, overwrite)
			if nil != err {
				return
			}
		}
	}
	return
}

func (instance *PromptPackages) rootValidate(root string) string {
	if len(root) == 0 {
		root = instance.root
	}
	return instance.absolutize(root)
}

func (instance *PromptPackages) new(name string) (response *PromptPackage) {
	if nil != instance {
		if _, ok := instance.packages[name]; !ok {
			response = NewPromptPackage(name)
			response.Root(instance.root)
			instance.packages[name] = response
		} else {
			response = instance.packages[name]
		}
	}
	return
}

func (instance *PromptPackages) absolutize(path string) string {
	root := instance.root
	if len(root) == 0 {
		root = llm_commons.GetDirSensyPrompts()
	}
	return gg.Paths.Absolutize(path, root)
}

// ---------------------------------------------------------------------------------------------------------------------
//  PromptPackage
// ---------------------------------------------------------------------------------------------------------------------

// PromptPackage represents a container for prompt configuration and logic in a conversational AI application.
// A package is a group of prompts
type PromptPackage struct {
	name string
	root string

	_store *PromptStore // `json:"-"`
}

func NewPromptPackage(name string) (instance *PromptPackage) {
	instance = new(PromptPackage)
	instance.name = name
	return
}

func (instance *PromptPackage) Map() map[string]interface{} {
	if nil != instance {
		return map[string]interface{}{
			"name":    instance.name,
			"root":    instance.Root(),
			"prompts": instance.Store().Map(),
		}
	}
	return nil
}

func (instance *PromptPackage) String() string {
	if nil != instance {
		return instance.Store().String()
	}
	return ""
}

func (instance *PromptPackage) Name() string {
	if nil != instance {
		return instance.name
	}
	return ""
}

func (instance *PromptPackage) Clone() *PromptPackage {
	if nil != instance {
		return &PromptPackage{
			name:   instance.name,
			root:   instance.root,
			_store: instance.Store().Clone(),
		}
	}
	return nil
}

func (instance *PromptPackage) Store() (response *PromptStore) {
	if nil != instance {
		if nil == instance._store {
			instance._store = NewPromptStore()
			instance._store.root = instance.Root()
		}
		response = instance._store
	}
	return
}

func (instance *PromptPackage) Root(v ...string) string {
	if nil != instance {
		if len(v) > 0 {
			instance.root = v[0]
			if nil != instance._store {
				instance._store.root = instance.root
			}
		}
		return instance.root
	}
	return ""
}

func (instance *PromptPackage) Names() []string {
	if nil != instance {
		return instance.Store().Names()
	}
	return nil
}

func (instance *PromptPackage) Count() int {
	if nil != instance {
		return len(instance.Store().Names())
	}
	return 0
}

func (instance *PromptPackage) Load(packageDir string) (response []string, err error) {
	if nil != instance {
		if len(instance.Root()) == 0 {
			instance.Root(packageDir)
		}
		// get package name from dir to load
		name := gg.Paths.FileName(packageDir, true)
		if name != instance.name {
			err = fmt.Errorf("package name mismatch: %s != %s", name, instance.name)
		} else {
			if strings.Index(name, ".") == -1 {
				response, err = instance.Store().LoadDir(packageDir)
			}
		}
	}
	return
}

func (instance *PromptPackage) ForEach(callback func(name, lang string, prompt *Prompt) (err error)) error {
	if nil != instance && nil != callback {
		return instance.Store().ForEach(callback)
	}
	return nil
}

// Deploy writes the prompt files from the PromptPackage to the specified directory.
// dir: The target directory to deploy prompt files.
// overwrite: A flag to determine whether to overwrite existing files.
// Returns an error if the deployment fails.
func (instance *PromptPackage) Deploy(root string, overwrite bool) (err error) {
	if nil != instance {
		dir := root
		// add package name to a dir path if not already
		base := filepath.Base(root)
		if base != instance.name {
			dir = gg.Paths.Concat(root, instance.name)
		}
		// absolutize
		dir = gg.Paths.Absolutize(dir, llm_commons.GetDirSensyPrompts())
		// start loop to deploy
		err = instance.Store().ForEach(func(name, lang string, prompt *Prompt) error {
			return instance.Store().Deploy(lang, name, prompt.GetContent(), dir, prompt.Options, overwrite)
		})
	}
	return
}
