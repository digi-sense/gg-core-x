package prompts

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_commons"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"fmt"
	"strings"
)

// ---------------------------------------------------------------------------------------------------------------------
//  PromptStore
// ---------------------------------------------------------------------------------------------------------------------

type PromptStore struct {
	root        string
	initialized bool

	store       map[string]map[string]*Prompt // name, lang, prompt
	promptNames []string
}

func NewPromptStore() (instance *PromptStore) {
	instance = new(PromptStore)
	instance.store = make(map[string]map[string]*Prompt) // name, lang, prompt
	instance.promptNames = make([]string, 0)             // auto-collected names of prompts
	return
}

func (instance *PromptStore) String() string {
	if nil != instance && nil != instance.store {
		return gg.JSON.Stringify(instance.store)
	}
	return ""
}

func (instance *PromptStore) Map() (response map[string]interface{}) {
	if nil != instance && nil != instance.store {
		response = make(map[string]interface{})
		for name, m := range instance.store {
			response[name] = make(map[string]interface{})
			for lang, prompt := range m {
				response[name].(map[string]interface{})[lang] = prompt.Map()
			}
		}
	}
	return
}

func (instance *PromptStore) Clone() (response *PromptStore) {
	if nil != instance {
		response = NewPromptStore()
		for name, m := range instance.store {
			if _, ok := response.store[name]; !ok {
				response.store[name] = make(map[string]*Prompt)
			}
			for lang, prompt := range m {
				if _, ok := response.store[name][lang]; !ok {
					response.store[name][lang] = &Prompt{
						Name:    prompt.Name,
						Lang:    prompt.Lang,
						Content: prompt.Content,
						Payload: prompt.Payload,
						Options: prompt.Options,
						store:   response.store,
					}
				}
			}
		}
		response.initialized = true
	}
	return
}

func (instance *PromptStore) Root(v ...string) string {
	if nil != instance {
		if len(v) > 0 {
			value := v[0]
			if value != instance.root {
				instance.root = value

			}
		}
		return instance.root
	}
	return ""
}

func (instance *PromptStore) Names() (response []string) {
	if nil != instance {
		response = make([]string, len(instance.promptNames))
		copy(response, instance.promptNames)
	}
	return
}

func (instance *PromptStore) Get(lang, name string) (response *Prompt) {
	if nil != instance {
		response = instance.init().getPrompt(lang, name)
	}
	return
}

func (instance *PromptStore) Exists(name string) (response bool) {
	if nil != instance {
		response = nil != instance.init().getPrompt(llm_commons.PromptDefLang, name)
	}
	return
}

func (instance *PromptStore) ExistsLang(name, lang string) (response bool) {
	if nil != instance {
		response = nil != instance.init().getPrompt(lang, name)
	}
	return
}

func (instance *PromptStore) LoadDirs(paths []string) (response []string, err error) {
	if nil != instance {
		for _, path := range paths {
			var names []string
			names, err = instance.LoadDir(path)
			if nil != err {
				return
			}
			response = append(response, names...)
			instance.promptNames = gg.Arrays.AppendUnique(instance.promptNames, response).([]string)
		}
	}
	return
}

func (instance *PromptStore) LoadDir(path string) (response []string, err error) {
	if nil != instance {
		response, err = instance.init().loadPromptFromDir(path)
		instance.promptNames = gg.Arrays.AppendUnique(instance.promptNames, response).([]string)
	}
	return
}

func (instance *PromptStore) LoadFile(lang, filename string) (promptName string, err error) {
	if nil != instance {
		promptName, err = instance.init().loadPromptFromFile(lang, filename)
		instance.promptNames = gg.Arrays.AppendUnique(instance.promptNames, promptName).([]string)
	}
	return
}

// Add prompt to store but do not write any file. Use "Deploy" to create a file on file system.
func (instance *PromptStore) Add(lang, name, text string) (err error) {
	if nil != instance {
		err = instance.init().addPrompt(lang, name, text, nil)
		instance.promptNames = gg.Arrays.AppendUnique(instance.promptNames, name).([]string)
	}
	return
}

func (instance *PromptStore) AddWithOptions(lang, name, text string, options map[string]interface{}) (err error) {
	if nil != instance {
		err = instance.init().addPrompt(lang, name, text, options)
		instance.promptNames = gg.Arrays.AppendUnique(instance.promptNames, name).([]string)
		if nil == err {
			prompt := instance.getPrompt(lang, name)
			if nil != prompt {
				prompt.Options = options
			}
		}
	}
	return
}

func (instance *PromptStore) Deploy(lang, name, text string, optRoot string, options map[string]interface{}, overwrite bool) (err error) {
	if nil != instance {
		err = instance.init().deployPrompt(lang, name, text, optRoot, options, overwrite)
		instance.promptNames = gg.Arrays.AppendUnique(instance.promptNames, name).([]string)
	}
	return
}

func (instance *PromptStore) AddItem(prompt *Prompt) (err error) {
	if nil != instance {
		err = instance.init().addPromptItem(prompt)
		instance.promptNames = gg.Arrays.AppendUnique(instance.promptNames, prompt.Name).([]string)
	}
	return
}

func (instance *PromptStore) ForEach(callback func(name, lang string, prompt *Prompt) (err error)) error {
	if nil != instance {
		gg.Recover("PromptStore.ForEach")
		for name, m := range instance.store {
			instance.promptNames = gg.Arrays.AppendUnique(instance.promptNames, name).([]string)
			for lang, prompt := range m {
				if nil != callback {
					err := callback(name, lang, prompt)
					if nil != err {
						return err
					}
				} else {
					fmt.Printf("name: %s, lang: %s, content: %s\n", name, lang, prompt.Content)
				}
			}
		}
	}
	return nil
}

func (instance *PromptStore) init() *PromptStore {
	if nil != instance && !instance.initialized {
		instance.initialized = true

		if nil == instance.store {
			instance.store = make(map[string]map[string]*Prompt)
		}
	}
	return instance
}

func (instance *PromptStore) load() *PromptStore {
	if nil != instance {
		instance.init()
		if len(instance.root) == 0 {
			// check if workspace has been defined
			if gg.Paths.GetWorkspace("*").IsChanged() {
				instance.root = llm_commons.GetDirSensyPrompts()
				_ = gg.Paths.Mkdir(instance.root + gg_utils.OS_PATH_SEPARATOR)
			}

			// load prompts from root
			if ok, _ := gg.Paths.Exists(instance.root); ok {
				_, _ = loadPromptFromDir(instance.store, instance.root)
			}
		}
	}
	return instance
}

func (instance *PromptStore) loadPromptFromDir(path string) (response []string, err error) {
	response, err = loadPromptFromDir(instance.store, path)
	return
}

func (instance *PromptStore) loadPromptFromFile(lang, filename string) (promptName string, err error) {
	promptName, err = loadPromptFromFile(instance.store, lang, filename)
	return
}

func (instance *PromptStore) addPrompt(lang, name, text string, options map[string]interface{}) (err error) {
	err = addPrompt(instance.store, lang, name, text, options)
	return
}

func (instance *PromptStore) deployPrompt(lang, name, text, root string, options map[string]interface{}, overwrite bool) (err error) {
	if len(root) == 0 {
		root = instance.root
	}
	root = gg.Paths.Absolutize(root, gg.Paths.GetWorkspacePath())
	err = deployPrompt(instance.store, lang, name, text, root, options, overwrite)
	return
}

func (instance *PromptStore) addPromptItem(prompt *Prompt) (err error) {
	err = addPromptItem(instance.store, prompt)
	return
}

func (instance *PromptStore) getPrompt(lang, name string) (response *Prompt) {
	return getPrompt(instance.store, lang, name)
}

// ---------------------------------------------------------------------------------------------------------------------
//  Prompt
// ---------------------------------------------------------------------------------------------------------------------

type Prompt struct {
	Name    string                 `json:"name"`
	Lang    string                 `json:"lang"`
	Content string                 `json:"content"`
	Payload map[string]interface{} `json:"payload,omitempty"` // payload for rendering
	Options map[string]interface{} `json:"options,omitempty"` // options for LLMs when prompt is passed as parameter

	store map[string]map[string]*Prompt
}

func (instance *Prompt) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *Prompt) Map() map[string]interface{} {
	if nil != instance {
		return map[string]interface{}{
			"name":    instance.Name,
			"lang":    instance.Lang,
			"content": instance.Content,
			"payload": instance.Payload,
		}
	}
	return nil
}

func (instance *Prompt) PayloadMergeWith(m map[string]interface{}, overwrite bool) *Prompt {
	if nil != instance {
		if nil == instance.Payload {
			instance.Payload = make(map[string]interface{})
		}
		instance.Payload = gg.Maps.Merge(overwrite, instance.Payload, m)
	}
	return instance
}

func (instance *Prompt) Render(data ...interface{}) (response string, err error) {
	if nil != instance {
		content := instance.GetContent()
		if len(data) == 1 {
			payload := data[0]
			if m, ok := payload.(map[string]interface{}); ok {
				response, err = renderMustache(content, instance.solveInner(m))
			} else {
				// single value wrapped into map
				m = map[string]interface{}{"value": payload}
				response, err = renderMustache(content, instance.solveInner(m))
			}
		} else {
			response, err = renderMustache(content, instance.payload(data...))
		}
	}
	return
}

func (instance *Prompt) GetLang() string {
	if nil != instance {
		if len(instance.Lang) > 0 {
			return instance.Lang
		}
	}
	return llm_commons.PromptDefLang
}

func (instance *Prompt) GetContent() string {
	if nil != instance {
		if len(instance.Content) == 0 {
			instance.Content = getPromptContent(instance.store, instance.GetLang(), instance.Name)
		}
		return instance.Content
	}
	return ""
}

func (instance *Prompt) LoadContent(uri string) *Prompt {
	if nil != instance && len(uri) > 0 {
		data, err := llm_commons.Download(uri)
		if nil == err {
			instance.Content = string(data)
		}
	}
	return instance
}

// SplitContentRows splits the content of a Prompt instance into context and prompt rows separated by the PromptSeparator "---".
// CONTEXT is the first row if the prompt has more than 1 row.
// PROMPT is a sequence of rows after the first separated from ---
func (instance *Prompt) SplitContentRows() (context []string, prompt []string) {
	if nil != instance {
		context = make([]string, 0)
		prompt = make([]string, 0)
		content := instance.GetContent()
		if len(content) > 0 {
			tokens := strings.Split(content, llm_commons.PromptSeparator)
			count := len(tokens)
			if count > 0 {
				switch count {
				case 1:
					// just 1
					prompt = strings.Split(tokens[0], "\n")
				case 2:
					context = strings.Split(tokens[0], "\n")
					prompt = strings.Split(tokens[1], "\n")
				default:
					// 3 or more
					context = strings.Split(tokens[0], "\n")
					prompt = strings.Split(gg.Strings.ConcatSep("\n", tokens[1:]), "\n")
				}
			}
		}
	}
	return
}

func (instance *Prompt) SplitContent() (context string, prompt string) {
	if nil != instance {
		contextRows, promptRows := instance.SplitContentRows()
		context = strings.Join(contextRows, "\n")
		prompt = strings.Join(promptRows, "\n")
	}
	return
}

func (instance *Prompt) payload(data ...interface{}) map[string]interface{} {
	if nil != instance {
		if nil == instance.Payload {
			instance.Payload = make(map[string]interface{})
		}
		response := gg.Maps.Clone(instance.Payload)
		for _, item := range data {
			if m, ok := item.(map[string]interface{}); ok {
				response = gg.Maps.Merge(true, response, m)
			}
		}
		return instance.solveInner(response)
	}
	return make(map[string]interface{})
}

func (instance *Prompt) solveInner(m map[string]interface{}) (response map[string]interface{}) {
	if nil != instance && nil != m {
		response = gg.Maps.Clone(m)
		// replace content
		for k, v := range m {
			if s, ok := v.(string); ok {
				// prompt:docu_message_bottom:message=hello, this is an inner object
				if strings.Index(s, llm_commons.PrefixPrompt) == 0 {
					name := strings.TrimSpace(strings.ReplaceAll(s, llm_commons.PrefixPrompt, ""))
					tokens := strings.Split(name, ":") // name_of_prompt:param=value
					name = tokens[0]
					if len(tokens) == 2 {
						// add param and value
						kv := strings.Split(tokens[1], "=") // param=value
						if len(kv) == 2 {
							m[kv[0]] = kv[1]
						} else {
							m[name] = kv[0]
						}
					}
					p := getPrompt(instance.store, instance.Lang, name)
					if nil != p && len(p.Content) > 0 {
						response[k], _ = renderMustache(p.GetContent(), m)
					}
				}
			}
		}
	}
	return
}
