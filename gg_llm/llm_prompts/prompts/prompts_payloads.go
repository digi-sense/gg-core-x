package prompts

import "bitbucket.org/digi-sense/gg-core"

// ---------------------------------------------------------------------------------------------------------------------
//  PromptDocuArticlePayload
// ---------------------------------------------------------------------------------------------------------------------

type PromptDocuArticlePayload struct {
	Lang          string `json:"lang"`
	Topic         string `json:"topic"`
	MessageTop    string `json:"message_top"`
	MessageBottom string `json:"message_bottom"`
}

func (instance *PromptDocuArticlePayload) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *PromptDocuArticlePayload) Map() map[string]interface{} {
	return gg.Convert.ToMap(instance.String())
}

// ---------------------------------------------------------------------------------------------------------------------
//  PromptDocuMsgTopPayload
// ---------------------------------------------------------------------------------------------------------------------

type PromptDocuMsgTopPayload struct {
	Value string `json:"docu_msg_top_value"`
}

func (instance *PromptDocuMsgTopPayload) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *PromptDocuMsgTopPayload) Map() map[string]interface{} {
	return gg.Convert.ToMap(instance.String())
}

// ---------------------------------------------------------------------------------------------------------------------
//  PromptDocuMsgBottomPayload
// ---------------------------------------------------------------------------------------------------------------------

type PromptDocuMsgBottomPayload struct {
	Value string `json:"docu_msg_bottom_value"`
}

func (instance *PromptDocuMsgBottomPayload) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *PromptDocuMsgBottomPayload) Map() map[string]interface{} {
	return gg.Convert.ToMap(instance.String())
}

// ---------------------------------------------------------------------------------------------------------------------
//  PromptMiscMaxWordsPayload
// ---------------------------------------------------------------------------------------------------------------------

type PromptMiscMaxWordsPayload struct {
	Value string `json:"value"`
}

func (instance *PromptMiscMaxWordsPayload) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *PromptMiscMaxWordsPayload) Map() map[string]interface{} {
	return gg.Convert.ToMap(instance.String())
}

// ---------------------------------------------------------------------------------------------------------------------
//  PromptMiscMaxTokensPayload
// ---------------------------------------------------------------------------------------------------------------------

type PromptMiscMaxTokensPayload struct {
	Value string `json:"value"`
}

func (instance *PromptMiscMaxTokensPayload) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *PromptMiscMaxTokensPayload) Map() map[string]interface{} {
	return gg.Convert.ToMap(instance.String())
}
