package skills

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_prompts/prompts"
	"bitbucket.org/digi-sense/gg-core/gg_"
	"fmt"
	"path/filepath"
)

// ---------------------------------------------------------------------------------------------------------------------
//	const
// ---------------------------------------------------------------------------------------------------------------------

const (
	fileIndex  = "index.json"
	fileSystem = "system.txt"
)

// ---------------------------------------------------------------------------------------------------------------------
//	Skill
// ---------------------------------------------------------------------------------------------------------------------

type Skill struct {
	root       string // path of skill dir
	rootVector string // root for vector database

	uid            string                 // uid of the skill
	name           string                 // Name
	description    string                 // Description
	system         string                 // system directives
	systemRendered string                 // system directives after render
	model          string                 // name of the AI model
	knowledge      map[string]string      // map of knowledge (collection, content/filename)
	modelOptions   map[string]interface{} // options for the AI model
	ragOptions     map[string]interface{} // options for the AI RAG model

	opened  bool
	prompts *prompts.PromptPackages
	options *SkillOptions
}

func NewSkill(args ...interface{}) (instance *Skill, err error) {
	instance = new(Skill)
	err = instance.init(args...)
	return
}

func LoadSkill(dir string) (instance *Skill, err error) {
	filename := gg.Paths.WorkspacePath(gg.Paths.Concat(dir, fileIndex))
	instance, err = NewSkill(filename)
	if nil == err {
		instance.root = dir
		instance.uid = filepath.Base(dir)
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Skill) String() string {
	if nil != instance {
		return gg.JSON.Stringify(instance.Map())
	}
	return ""
}

func (instance *Skill) Map() (response map[string]interface{}) {
	if nil != instance {
		response = map[string]interface{}{
			"uid":          instance.Uid(),
			"name":         instance.Name(),
			"description":  instance.Description(),
			"system":       instance.System(),
			"model":        instance.Model(),
			"modelOptions": instance.ModelOptions(),
			"ragOptions":   instance.RagOptions(),
			"knowledge":    instance.knowledge,
			"root":         instance.Root(),
			"parameters":   instance.options.Parameters,
			"packages":     instance.prompts.Map(),
			"actions":      instance.options.Actions,
		}
	}
	return
}

func (instance *Skill) Root() (response string) {
	if nil != instance {
		response = instance.root
	}
	return
}

// SetRoot sets the root directory for the Skill instance.
// If the provided directory name differs from the instance's name,
// it concatenates the name with the provided path.
// Returns the Skill instance.
func (instance *Skill) SetRoot(v string) *Skill {
	if nil != instance {
		base := filepath.Base(v)
		if len(base) > 0 && len(instance.uid) > 0 && base != instance.uid {
			v = gg.Paths.Concat(v, instance.uid)
		}
		instance.root = v
		if nil != instance.prompts {
			instance.prompts.SetRoot(instance.promptsRoot())
		}
	}
	return instance
}

func (instance *Skill) SetRootVector(v string) *Skill {
	if nil != instance && len(v) > 0 {
		instance.rootVector = v
	}
	return instance
}

func (instance *Skill) Uid() (response string) {
	if nil != instance {
		response = instance.uid
	}
	return
}

func (instance *Skill) SetUid(v string) *Skill {
	if nil != instance && !instance.opened {
		instance.uid = v
		instance.options.Uid = v
		if len(instance.root) > 0 {
			instance.SetRoot(instance.root)
		}
	}
	return instance
}

func (instance *Skill) Name() (response string) {
	if nil != instance {
		response = instance.name
	}
	return
}

func (instance *Skill) SetName(v string) *Skill {
	if nil != instance {
		instance.name = v
		instance.options.Name = v
	}
	return instance
}

func (instance *Skill) Description() (response string) {
	if nil != instance {
		response = instance.description
	}
	return
}

func (instance *Skill) SetDescription(v string) *Skill {
	if nil != instance {
		instance.description = v
		instance.options.Description = v
	}
	return instance
}

func (instance *Skill) System() (response string) {
	if nil != instance {
		response = instance.system
	}
	return
}

func (instance *Skill) SetSystem(v string) *Skill {
	if nil != instance {
		instance.system = v
		instance.options.System = v
	}
	return instance
}

func (instance *Skill) GetSystemRendered() (response string) {
	if nil != instance {
		response = instance.system
		if nil != instance.modelOptions {
			text := instance.ModelOptionsValueAsString("system")
			if len(text) > 0 {
				response = text
			}
		}
	}
	return
}

func (instance *Skill) AddKnowledge(filename, content string) *Skill {
	if nil != instance {
		if nil == instance.knowledge {
			instance.knowledge = make(map[string]string)
		}
		if nil != instance.knowledge {
			instance.knowledge[filename] = content
		}
	}
	return instance
}

func (instance *Skill) Model() (response string) {
	if nil != instance {
		response = instance.model
	}
	return
}

func (instance *Skill) SetModel(v string) *Skill {
	if nil != instance {
		instance.model = v
		instance.options.Model = v
	}
	return instance
}

func (instance *Skill) SetOverwrite(v bool) *Skill {
	if nil != instance {
		instance.options.Overwrite = v
		if nil != instance.prompts {
			instance.prompts.SetOverwrite(v)
		}
	}
	return instance
}

func (instance *Skill) ModelOptions() (response map[string]interface{}) {
	if nil != instance {
		if nil == instance.modelOptions {
			instance.modelOptions = make(map[string]interface{})
		}
		response = instance.modelOptions
	}
	return
}

func (instance *Skill) RagOptions() (response map[string]interface{}) {
	if nil != instance {
		if len(instance.ragOptions) == 0 {
			// 		"compress": false,
			//    	"llm-base-url": "",
			//    	"llm-driver": "ollama",
			//    	"llm-embedding-model": "nomic-embed-text",
			//    	"name": "sample-vector-db",
			//    	"path": "./vector-db"
			instance.ragOptions = make(map[string]interface{})
			instance.ragOptions["compress"] = false
			instance.ragOptions["llm-driver"] = "ollama"
			instance.ragOptions["llm-embedding-model"] = "nomic-embed-text"
			instance.ragOptions["name"] = instance.uid
			instance.ragOptions["path"] = "./vector-db"
		}
		response = instance.ragOptions
	}
	return
}

func (instance *Skill) SetRAGContext(data []map[string]interface{}, userPayload map[string]interface{}, rawOptions map[string]interface{}, field string) {
	if nil != instance {
		if nil == instance.modelOptions {
			instance.modelOptions = make(map[string]interface{})
		}
		if nil == rawOptions {
			rawOptions = make(map[string]interface{})
		}
		var options map[string]interface{}
		if len(field) > 0 {
			options = gg.Maps.GetMap(rawOptions, field)
		} else {
			options = rawOptions
		}

		payload := make(map[string]interface{})
		// clone existing payload if any
		payload = gg.Maps.Clone(userPayload)
		if len(data) > 0 {
			// creates the context data
			ctx := map[string]interface{}{}
			if m, ok := payload["context"]; !ok {
				payload["context"] = ctx
			} else {
				// already have a context
				ctx = m.(map[string]interface{})
			}
			items := gg.Maps.Get(ctx, "items")
			if list, ok := items.([]map[string]interface{}); ok {
				list = append(list, data...)
				ctx["items"] = list
				ctx["items_len"] = len(list)
				// add index to items
				for i, item := range list {
					item["idx"] = i + 1
				}
			} else {
				ctx["items"] = data
				ctx["items_len"] = len(data)
				// add index to items
				for i, item := range data {
					item["idx"] = i + 1
				}
			}
		}

		// render the system prompt with context data
		renderedSystem, err := llm_commons.RenderMustache(instance.system, payload)
		if nil == err {
			instance.systemRendered = renderedSystem
		} else {
			instance.systemRendered = instance.system
		}

		if len(options) > 0 {
			instance.modelOptions = gg.Maps.Merge(true, instance.modelOptions, options)

			// adjust system prompt
			optionsSystem := gg.Maps.GetString(options, "system")
			if len(optionsSystem) > 0 {
				instance.modelOptions["system"] = instance.systemRendered + "\n" + optionsSystem
			} else {
				instance.modelOptions["system"] = instance.systemRendered
			}
		} else {
			// NO CUSTOM OPTIONS
			instance.modelOptions["system"] = instance.systemRendered
		}
	}
}

func (instance *Skill) ModelOptionsValue(field string) (response interface{}) {
	if nil != instance {
		options := instance.ModelOptions()
		response = gg.Maps.Get(options, field)
	}
	return
}

func (instance *Skill) ModelOptionsValueAsString(field string) (response string) {
	if nil != instance {
		response = gg.Convert.ToString(instance.ModelOptionsValue(field))
	}
	return
}

func (instance *Skill) Options() (response *SkillOptions) {
	if nil != instance {
		response = instance.options
	}
	return
}

func (instance *Skill) PromptReset() *Skill {
	if nil != instance && !instance.opened {
		instance.prompts = prompts.NewPromptPackages()
		if instance.opened {
			instance.prompts.SetRoot(instance.promptsRoot())
		}
	}
	return instance
}

func (instance *Skill) Prompts() (response *prompts.PromptPackages) {
	if nil != instance {
		response = instance.prompts
		if len(instance.prompts.Root()) == 0 {
			instance.prompts.SetRoot(instance.promptsRoot())
		}

	}
	return
}

func (instance *Skill) Knowledge() (response map[string]string) {
	if nil != instance {
		if nil == instance.knowledge {
			instance.knowledge = make(map[string]string)
		}
		response = instance.knowledge
	}
	return
}

func (instance *Skill) KnowledgeKeys() (response []string) {
	if nil != instance {
		for k, _ := range instance.knowledge {
			response = append(response, k)
		}
	}
	return
}

// KnowledgeNames extracts and returns a list of knowledge resource names derived from knowledge keys in the Skill instance.
// The list is an array of "collection" names
func (instance *Skill) KnowledgeNames() (response []string) {
	if nil != instance {
		keys := instance.KnowledgeKeys()
		for _, name := range keys {
			response = append(response, gg.Paths.FileName(name, false))
		}
	}
	return
}

func (instance *Skill) IsOpened() (response bool) {
	if nil != instance {
		response = instance.opened
	}
	return
}

func (instance *Skill) Open() (err error) {
	if nil != instance && !instance.opened {
		err = instance.open()
	}
	return
}

// Clone creates a copy of the current Skill instance with a unique UID and cloned properties.
func (instance *Skill) Clone() (response *Skill) {
	if nil != instance {
		response = new(Skill)
		response.uid = gg.Rnd.Uuid()
		response.root = filepath.Dir(instance.root) // set parent dir after clone
		response.rootVector = instance.rootVector
		response.name = fmt.Sprintf("%s", instance.name)
		response.description = fmt.Sprintf("* %s", instance.description)
		response.system = instance.system
		// response.knowledge = instance.knowledge
		response.knowledge = make(map[string]string)
		for key, value := range instance.knowledge {
			response.knowledge[key] = value
		}
		response.model = instance.model
		response.modelOptions = gg.Convert.ToMap(gg.JSON.Stringify(instance.modelOptions))
		response.ragOptions = gg.Convert.ToMap(gg.JSON.Stringify(instance.ragOptions))
		response.opened = false
		response.prompts = prompts.NewPromptPackages()
		_ = instance.prompts.ForEachPackage(func(pkg *prompts.PromptPackage) (err error) {
			if nil != pkg {
				response.prompts.Add(pkg.Clone())
			}
			return
		})
		response.options = instance.options.Clone()
		response.options.Uid = response.uid
		response.options.Name = response.name
		response.options.Description = response.description
	}
	return
}

func (instance *Skill) Deploy() (err error) {
	if nil != instance {
		overwrite := false
		if nil != instance.options {
			overwrite = instance.options.Overwrite
		}

		root := instance.Root()
		if len(root) > 0 {
			err = gg.Paths.Mkdir(root + gg_.OS_PATH_SEPARATOR)
			if nil != err {
				return
			}
		}

		// save index
		indexFile := gg.Paths.Concat(root, fileIndex)
		if ok, _ := gg.Paths.Exists(indexFile); !ok || overwrite {
			_, err = gg.IO.WriteTextToFile(instance.options.String(), indexFile)
			if nil != err {
				return
			}
		}

		// write system prompt
		contextFile := gg.Paths.Concat(root, fileSystem)
		if ok, _ := gg.Paths.Exists(contextFile); !ok || overwrite {
			_, err = gg.IO.WriteTextToFile(instance.system, contextFile)
			if nil != err {
				return
			}
		}

		// write KNOWLEDGE for rag
		if len(instance.knowledge) > 0 {
			kRoot := root
			dbRoot := gg.Maps.GetString(instance.options.RagOptions, "path")
			if len(dbRoot) > 0 {
				kRoot = gg.Paths.Absolutize(dbRoot, root)
			}
			_ = gg.Paths.Mkdir(kRoot + gg_.OS_PATH_SEPARATOR)
			for k, v := range instance.knowledge {
				isPath := gg.Paths.IsFilePath(v)
				var filename string
				if len(v) > 0 {
					if isPath {
						filename = gg.Paths.Absolutize(v, kRoot)
					} else {
						filename = gg.Paths.Concat(kRoot, k)
						_, err = gg.IO.WriteTextToFile(v, filename)
						if nil != err {
							return
						}
					}
				} else {
					// only file name for custom knowledge
					filename = gg.Paths.Concat(kRoot, k)
				}
				instance.knowledge[k] = filename
			}
		}

		// prompts
		if nil != instance.prompts {
			instance.prompts.SetOverwrite(overwrite)
			err = instance.prompts.Deploy()
			if nil != err {
				return
			}
		}
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Skill) init(args ...interface{}) (err error) {
	if nil != instance {
		packages := make([]*prompts.PromptPackage, 0)

		for _, arg := range args {
			if o, ok := arg.(*SkillOptions); ok {
				instance.options = o
				continue
			}
			if s, ok := arg.(string); ok {
				instance.options, err = NewSkillOptions(s)
				if nil != err {
					return
				}
				continue
			}
			if m, ok := arg.(map[string]interface{}); ok {
				instance.options, err = NewSkillOptions(m)
				if nil != err {
					return
				}
				continue
			}
			if p, ok := arg.(*prompts.PromptPackages); ok {
				instance.prompts = p
				continue
			}
			if p, ok := arg.(*prompts.PromptPackage); ok {
				packages = append(packages, p)
				continue
			}
		}

		if nil == instance.options {
			instance.options, _ = NewSkillOptions()
		} else {
			instance.refresh()
		}

		if nil == instance.prompts {
			instance.prompts = prompts.NewPromptPackages()
		}
		for _, pkg := range packages {
			instance.prompts.Add(pkg)
		}

		instance.knowledge = make(map[string]string) // collection, filename

	}
	return
}

func (instance *Skill) refresh() {
	if nil != instance && nil != instance.options {
		if len(instance.options.Uid) == 0 {
			if len(instance.uid) > 0 {
				instance.options.Uid = instance.uid
			} else {
				instance.options.Uid = gg.Rnd.Uuid()
			}
		} else {
			instance.uid = instance.options.Uid
			if len(instance.uid) == 0 {
				instance.uid = gg.Rnd.Uuid()
			}
		}

		if len(instance.options.Name) == 0 {
			if len(instance.name) > 0 {
				instance.options.Name = instance.name
			} else {
				instance.options.Name = instance.options.Uid
			}
		} else {
			instance.name = instance.options.Name
		}

		if len(instance.options.Description) == 0 {
			if len(instance.description) > 0 {
				instance.options.Description = instance.description
			} else {
				instance.options.Description = instance.options.Name
			}
		} else {
			instance.description = instance.options.Description
		}

		if len(instance.options.System) == 0 {
			if len(instance.system) > 0 {
				instance.options.System = instance.system
			} else {
				instance.options.System = ""
			}
		} else {
			instance.system = instance.options.System
		}

		if len(instance.options.Model) == 0 {
			if len(instance.model) > 0 {
				instance.options.Model = instance.model
			} else {
				instance.options.Model = ""
			}
		} else {
			instance.model = instance.options.Model
		}

		if len(instance.options.ModelOptions) > 0 {
			if nil == instance.modelOptions {
				instance.modelOptions = make(map[string]interface{})
			}
			instance.modelOptions = gg.Maps.Merge(false, instance.modelOptions, instance.options.ModelOptions)
		}

		if len(instance.options.RagOptions) > 0 {
			if nil == instance.ragOptions {
				instance.ragOptions = make(map[string]interface{})
			}
			instance.ragOptions = gg.Maps.Merge(false, instance.ragOptions, instance.options.RagOptions)
		}

	}
}

func (instance *Skill) promptsRoot() (response string) {
	if nil != instance {
		response = gg.Paths.Concat(instance.root, "prompts")
	}
	return
}

func (instance *Skill) open() (err error) {
	if nil != instance {
		if nil != instance.options {

			// complete current instance
			instance.refresh()

			if len(instance.root) == 0 {
				instance.root = gg.Paths.WorkspacePath("./skills/" + instance.uid)
			}
			_ = gg.Paths.Mkdir(instance.root + gg_.OS_PATH_SEPARATOR)

			// system
			if len(instance.options.System) > 0 {
				filename := gg.Paths.Concat(instance.root, instance.options.System)
				if ok, _ := gg.Paths.Exists(filename); ok {
					text, e := gg.IO.ReadTextFromFile(filename)
					if nil != e {
						err = e
						return
					}
					instance.system = text
				} else {
					if len(instance.system) == 0 {
						instance.system = instance.options.System
					}
				}
			}

			// model
			instance.model = instance.options.Model
			instance.modelOptions = gg.Convert.ToMap(gg.JSON.Stringify(instance.options.ModelOptions))
			instance.ragOptions = gg.Convert.ToMap(gg.JSON.Stringify(instance.options.RagOptions))

			// prompts
			if nil != instance.prompts && len(instance.prompts.Root()) == 0 {
				instance.prompts.SetRoot(instance.promptsRoot())
				instance.prompts.SetOverwrite(instance.options.Overwrite)
				_ = gg.Paths.Mkdir(instance.prompts.Root() + gg_.OS_PATH_SEPARATOR)
				_, err = instance.prompts.Load()
				if nil != err {
					return
				}
			}

		} else {
			err = gg.Errors.Prefix(gg.PanicSystemError, "Skill.open -> options not set: ")
			return
		}

		instance.opened = true
	}
	return
}
