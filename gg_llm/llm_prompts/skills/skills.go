package skills

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_prompts/prompts"
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_fs"
	"errors"
	"path/filepath"
	"strings"
	"sync"
)

// ---------------------------------------------------------------------------------------------------------------------
//	Skills
// ---------------------------------------------------------------------------------------------------------------------

type Skills struct {
	root       string // parent dir of all skills
	rootVector string
	mux        *sync.Mutex

	override bool
	skills   map[string]*Skill
}

func NewSkills(root, rootVector string) (instance *Skills, err error) {
	instance = new(Skills)
	instance.root = gg.Paths.Absolute(root)
	instance.rootVector = rootVector
	instance.mux = new(sync.Mutex)

	err = instance.init()
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Skills) String() string {
	if nil != instance {
		return gg.JSON.Stringify(instance.Map())
	}
	return ""
}

func (instance *Skills) Map() (response map[string]interface{}) {
	if nil != instance {
		response = make(map[string]interface{})
		instance.mux.Lock()
		defer instance.mux.Unlock()

		for _, skill := range instance.skills {
			response[skill.Uid()] = skill.Map()
		}
	}
	return
}

func (instance *Skills) List() (response []map[string]interface{}) {
	if nil != instance {
		response = make([]map[string]interface{}, 0)
		instance.mux.Lock()
		defer instance.mux.Unlock()

		for _, skill := range instance.skills {
			response = append(response, skill.Map())
		}
	}
	return
}

func (instance *Skills) Root() string {
	return instance.root
}

func (instance *Skills) SetRoot(v string) *Skills {
	if nil != instance {
		instance.root = v
		for _, skill := range instance.skills {
			skill.SetRoot(gg.Paths.Concat(instance.root, skill.Uid()))
		}
	}
	return instance
}

func (instance *Skills) SetRootVector(value string) *Skills {
	if nil != instance && len(value) > 0 {
		instance.rootVector = value
		for _, skill := range instance.skills {
			path := gg.Paths.Concat(instance.rootVector, skill.Uid())
			skill.SetRootVector(path)
		}
	}
	return instance
}

func (instance *Skills) Overwrite() bool {
	return instance.override
}

func (instance *Skills) SetOverwrite(v bool) *Skills {
	if nil != instance {
		instance.override = v
		for _, skill := range instance.skills {
			skill.SetOverwrite(v)
		}
	}
	return instance
}

func (instance *Skills) Names() (response []string) {
	if nil != instance {
		_ = instance.ForEach(func(skill *Skill) error {
			if nil != skill {
				response = append(response, skill.Uid())
			}
			return nil
		})
	}
	return
}

func (instance *Skills) Count() int {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		return len(instance.skills)
	}
	return 0
}

func (instance *Skills) ForEach(f func(skill *Skill) error) (err error) {
	if nil != instance && nil != f {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		for _, skill := range instance.skills {
			if nil != skill {
				_ = skill.Open()
				err = f(skill)
				if nil != err {
					return
				}
			}
		}
	}
	return
}

//-- SKILL --//

func (instance *Skills) Has(uid string) bool {
	if nil != instance && nil != instance.skills {
		return nil != instance.Get(uid)
	}
	return false
}

func (instance *Skills) Contains(uid string) bool {
	if nil != instance && nil != instance.skills && len(uid) > 0 {
		return nil != instance.Get(uid)
	}
	return false
}

func (instance *Skills) Get(uid string) (skill *Skill) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		if s, ok := instance.skills[uid]; ok {
			skill = s
		}
	}
	return
}

// GetAll retrieves a skill and a corresponding prompt based on the provided skill UID, prompt name, and prompt language.
// It locks the instance for thread safety and ensures the skill and prompt exist before returning them.
func (instance *Skills) GetAll(skillUid, promptName, promptLang string) (skill *Skill, prompt *prompts.Prompt) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		if s, ok := instance.skills[skillUid]; ok {
			skill = s
			if nil != skill {
				prompt = skill.Prompts().Prompt("", promptName, promptLang)
			}
		}
	}
	return
}

func (instance *Skills) Clone(uid string) (response *Skill) {
	if nil != instance {
		skill := instance.Get(uid)
		if nil != skill {
			response = skill.Clone()
		}
	}
	return
}

func (instance *Skills) Add(v interface{}) (response *Skills) {
	if nil != instance && nil != v {
		response = instance
		instance.mux.Lock()
		defer instance.mux.Unlock()

		if skill, ok := v.(*Skill); ok {
			instance.add(skill)
			return
		}

		if pkg, ok := v.(*prompts.PromptPackage); ok {
			_, _ = instance.NewSkillFromPackage(pkg)
			return
		}

		if prmpts, ok := v.(*prompts.PromptPackages); ok {
			_, _ = instance.NewSkillWithPrompts(prmpts)
			return
		}

	}
	return
}

func (instance *Skills) NewSkill(uid string) (skill *Skill, err error) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		skill, err = instance.newSkill(uid)
	}
	return
}

func (instance *Skills) NewSkillWithPrompts(prompts *prompts.PromptPackages) (skill *Skill, err error) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		uid := prompts.Name()
		skill, err = NewSkill(prompts)
		if nil != err {
			return
		}
		skill.
			SetUid(uid).
			SetRoot(gg.Paths.Concat(instance.root, uid)).
			SetOverwrite(instance.override)
		instance.add(skill)
	}
	return
}

func (instance *Skills) NewSkillFromPackage(pkg *prompts.PromptPackage) (skill *Skill, err error) {
	if nil != instance && nil != pkg {
		skill, err = instance.newSkill(pkg.Name())
		if nil != err {
			return
		}
		// pkg.Root(skill.Root())
		skill.SetName(pkg.Name())
		skill.Prompts().Add(pkg)
	}
	return
}

func (instance *Skills) NewSkillFromDir(dir string) (skill *Skill, err error) {
	if nil != instance {
		name := filepath.Base(dir)
		pkg := prompts.NewPromptPackage(name)
		_, err = pkg.Load(dir)
		if nil != err {
			return
		}
		skill, err = instance.NewSkillFromPackage(pkg)
	}
	return
}

//-- PACKAGES --//

func (instance *Skills) PackageNames() (response []string) {
	if nil != instance {
		_ = instance.ForEach(func(skill *Skill) error {
			if nil != skill {
				response = append(response, skill.Prompts().Names()...)
			}
			return nil
		})
	}
	return
}

//-- PROMPTS --//

func (instance *Skills) PromptCount() (response int) {
	if nil != instance {
		response = len(instance.PromptNames())
	}
	return
}

func (instance *Skills) PromptNames() (response []string) {
	if nil != instance {
		_ = instance.ForEach(func(skill *Skill) error {
			if nil != skill {
				response = append(response, skill.Prompts().PromptNames()...)
			}
			return nil
		})
	}
	return
}

func (instance *Skills) PromptNamesMap() (response []map[string]interface{}) {
	if nil != instance {
		promptNames := make([]string, 0)
		response = make([]map[string]interface{}, 0)
		_ = instance.ForEach(func(skill *Skill) error {
			if nil != skill {
				_ = skill.Prompts().ForEach(func(pPackage, pName, pLang string, prompt *prompts.Prompt) (err error) {
					if gg.Arrays.IndexOf(pName, promptNames) == -1 {
						promptNames = append(promptNames, pName)
						response = append(response, map[string]interface{}{
							"skill":             skill.Uid(),
							"skill-name":        skill.Name(),
							"skill-description": skill.Description(),
							"package":           pPackage,
							"prompt":            pName,
							"prompt-name":       prompt.Name,
						})
					}
					return nil
				})
			}
			return nil
		})
	}
	return
}

func (instance *Skills) Knowledge(detailed bool) (response []map[string]interface{}) {
	if nil != instance {
		response = make([]map[string]interface{}, 0)
		_ = instance.ForEach(func(skill *Skill) (err error) {
			if nil != skill {
				k := skill.Knowledge()
				if nil != k {
					response = append(response, gg.Convert.ToMap(gg.JSON.Stringify(k)))
				}
			}
			return
		})
	}
	return
}

func (instance *Skills) PromptNamesFromPackage(packageName string) (response []string) {
	if nil != instance {
		_ = instance.ForEach(func(skill *Skill) error {
			if nil != skill {
				response = append(response, skill.Prompts().PromptNamesFromPackage(packageName)...)
			}
			return nil
		})
	}
	return
}

func (instance *Skills) PromptGet(skillName, packageName, promptName, lang string) (response *prompts.Prompt) {
	if nil != instance {
		if len(lang) == 0 {
			lang = llm_commons.PromptDefLang
		}
		skillName = strings.ToLower(skillName)
		_ = instance.ForEach(func(skill *Skill) error {
			name := strings.ToLower(skill.Uid())
			if len(skillName) == 0 || name == skillName {
				return skill.Prompts().ForEach(func(pPackage, pName, pLang string, prompt *prompts.Prompt) (err error) {
					if len(packageName) == 0 || packageName == pPackage {
						if len(promptName) == 0 || promptName == pName {
							if len(lang) == 0 || lang == pLang {
								response = prompt
								return errors.New("response")
							}
						}
					}
					return nil
				})
			}
			return nil
		})
	}
	return
}

func (instance *Skills) PromptGetDefault(promptName string) (response *prompts.Prompt) {
	if nil != instance {
		response = instance.PromptGet("", "", promptName, "")
	}
	return
}

// PromptHasUpload checks if a given skill has an associated upload prompt available. Returns true if it exists.
func (instance *Skills) PromptHasUpload(skillName string) (response bool) {
	if nil != instance {
		response = nil != instance.PromptGet(skillName, "",
			llm_commons.PromptDefNameUpload, llm_commons.PromptDefLang)
	}
	return
}

//-- DEPLOY --//

// Deploy initializes and deploys all skills within the Skills struct.
// It creates the root directory if it does not exist and calls the Deploy method for each skill.
// Returns an error if directory creation or skill deployment fails.
func (instance *Skills) Deploy() (err error) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		if len(instance.root) == 0 {
			err = gg.Errors.Prefix(gg.PanicSystemError, "No root dir for skills")
			return
		}
		err = gg.Paths.Mkdir(instance.root)
		if nil != err {
			return
		}

		for _, skill := range instance.skills {
			err = skill.Deploy()
			if nil != err {
				return
			}
		}
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Skills) init() (err error) {
	if nil != instance {
		instance.skills = make(map[string]*Skill)
		err = gg.Paths.Mkdir(instance.root + gg_.OS_PATH_SEPARATOR)
		if nil != err {
			return
		}

		// load existing skills if any
		err = instance.load()
		if nil != err {
			return
		}

	}
	return
}

func (instance *Skills) load() (err error) {
	if nil != instance {
		if ok, e := gg.Paths.Exists(instance.root); !ok {
			err = e
			return
		}

		var files []*gg_fs.FileWrapper
		files, err = gg.Fs.ReadDir(instance.root)
		if nil != err {
			return
		}

		for _, f := range files {
			if f.IsDir {
				index := gg.Paths.Concat(f.Path, "index.json")
				if ok, _ := gg.Paths.Exists(index); ok {
					skill, e := LoadSkill(f.Path)
					if nil != e {
						err = gg.Errors.Prefix(e, "Failed to load skill: "+f.Name+" ("+f.Path+")")
						return
					}
					skill.rootVector = instance.rootVector
					err = skill.Open()
					if nil != err {
						return
					}
					instance.skills[skill.Uid()] = skill
				}
			}
		}
	}
	return
}

func (instance *Skills) add(skill *Skill) *Skills {
	if nil != instance && nil != skill {
		skill.SetRoot(gg.Paths.Concat(instance.root, skill.Uid()))
		instance.skills[skill.Uid()] = skill
	}
	return instance
}

func (instance *Skills) newSkill(uid string) (skill *Skill, err error) {
	if nil != instance {
		skill, err = NewSkill()
		if nil != err {
			return
		}
		skill.
			SetUid(uid).
			SetRoot(gg.Paths.Concat(instance.root, uid)).
			SetOverwrite(instance.override)
		instance.add(skill)
	}
	return
}
