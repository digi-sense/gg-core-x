package skills

import "bitbucket.org/digi-sense/gg-core"

// ---------------------------------------------------------------------------------------------------------------------
//	SkillActionOptions
// ---------------------------------------------------------------------------------------------------------------------

type SkillActionOptions struct {
	Name        string `json:"name"`        // name of this action
	Description string `json:"description"` // description of this action
	Command     string `json:"command"`     // command to perform
}

func (instance *SkillActionOptions) String() (response string) {
	if nil != instance {
		response = gg.JSON.Stringify(instance)
	}
	return ""
}

func (instance *SkillActionOptions) Map() (response map[string]interface{}) {
	if nil != instance {
		response = gg.Convert.ToMap(instance.String())
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	SkillOptions
// ---------------------------------------------------------------------------------------------------------------------

type SkillOptions struct {
	Uid          string                 `json:"uid"`           // unique identifier
	Name         string                 `json:"name"`          // name of this skill
	Description  string                 `json:"description"`   // description of this skill
	Overwrite    bool                   `json:"overwrite"`     // overwrite prompts on deploy
	Model        string                 `json:"model"`         // required model
	System       string                 `json:"system"`        // filename or text for context refining
	ModelOptions map[string]interface{} `json:"model_options"` // optional parameters for model
	RagOptions   map[string]interface{} `json:"rag_options"`   // optional parameters for rag
	Parameters   map[string]interface{} `json:"parameters"`    // skill parameters
	Actions      []*SkillActionOptions  `json:"actions"`       // defines the list of actions that the skill can perform.
}

func NewSkillOptions(args ...interface{}) (instance *SkillOptions, err error) {
	instance = new(SkillOptions)
	err = instance.init(args...)
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *SkillOptions) String() (response string) {
	if nil != instance {
		response = gg.JSON.Stringify(instance)
	}
	return
}

func (instance *SkillOptions) Map() (response map[string]interface{}) {
	if nil != instance {
		response = gg.Convert.ToMap(instance.String())
	}
	return
}

func (instance *SkillOptions) Clone() (response *SkillOptions) {
	if nil != instance {
		response = new(SkillOptions)
		s := instance.String()
		_ = gg.JSON.Read(s, &response)
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *SkillOptions) init(args ...interface{}) (err error) {
	if nil != instance {
		for _, arg := range args {
			if s, ok := arg.(string); ok {
				err = gg.JSON.Read(s, &instance)
				break
			}
			if m, ok := arg.(map[string]interface{}); ok {
				err = gg.JSON.Read(m, &instance)
				break
			}
		}
	}
	return
}
