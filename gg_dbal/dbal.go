package gg_dbal

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_dbal/gg_dbal_commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_dbal/gg_dbal_drivers"
	"bitbucket.org/digi-sense/gg-core-x/gg_dbal/gg_dbal_semantic_search"
	"bitbucket.org/digi-sense/gg-core-x/gg_dbal/gg_dbal_showcase_search"
)

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func NewDatabase(driverName, connectionString string) (driver gg_dbal_drivers.IDatabase, err error) {
	return gg_dbal_drivers.NewDatabase(driverName, connectionString)
}

func NewDatabaseFromDsn(driverName string, dsn *gg_dbal_commons.Dsn) (gg_dbal_drivers.IDatabase, error) {
	return gg_dbal_drivers.NewDatabaseFromDsn(driverName, dsn)
}

func OpenDatabase(driver, connectionString string) (gg_dbal_drivers.IDatabase, error) {
	return gg_dbal_drivers.OpenDatabase(driver, connectionString)
}

func NewSemanticEngine(c interface{}) (*gg_dbal_semantic_search.SemanticEngine, error) {
	config, err := getConfig(c)
	if nil != err {
		return nil, err
	}
	return gg_dbal_semantic_search.NewSemanticEngine(config)
}

func NewShowcaseEngine(c interface{}) (*gg_dbal_showcase_search.ShowcaseEngine, error) {
	config, err := getConfigDB(c)
	if nil != err {
		return nil, err
	}
	return gg_dbal_showcase_search.NewShowcaseEngine(config)
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func getConfig(c interface{}) (*gg_dbal_commons.SemanticConfig, error) {
	var config *gg_dbal_commons.SemanticConfig
	if v, b := c.(*gg_dbal_commons.SemanticConfig); b {
		config = v
	} else if s, b := c.(string); b {
		if gg.Regex.IsValidJsonObject(s) {
			err := gg.JSON.Read(s, &config)
			if nil != err {
				return nil, err
			}
		} else {
			// file
			err := gg.JSON.ReadFromFile(s, &config)
			if nil != err {
				return nil, err
			}
		}
	} else if m, b := c.(map[string]interface{}); b {
		err := gg.JSON.Read(gg.JSON.Stringify(m), &config)
		if nil != err {
			return nil, err
		}
	} else if mp, b := c.(*map[string]interface{}); b {
		err := gg.JSON.Read(gg.JSON.Stringify(mp), &config)
		if nil != err {
			return nil, err
		}
	}
	if nil == config {
		return nil, gg_dbal_commons.ErrorMismatchConfiguration
	}
	return config, nil
}

func getConfigDB(c interface{}) (*gg_dbal_commons.SemanticConfigDb, error) {
	var config *gg_dbal_commons.SemanticConfigDb
	if v, b := c.(*gg_dbal_commons.SemanticConfigDb); b {
		config = v
	} else if s, b := c.(string); b {
		if gg.Regex.IsValidJsonObject(s) {
			err := gg.JSON.Read(s, &config)
			if nil != err {
				return nil, err
			}
		} else {
			// file
			err := gg.JSON.ReadFromFile(s, &config)
			if nil != err {
				return nil, err
			}
		}
	} else if m, b := c.(map[string]interface{}); b {
		err := gg.JSON.Read(gg.JSON.Stringify(m), &config)
		if nil != err {
			return nil, err
		}
	} else if mp, b := c.(*map[string]interface{}); b {
		err := gg.JSON.Read(gg.JSON.Stringify(mp), &config)
		if nil != err {
			return nil, err
		}
	}
	if nil == config {
		return nil, gg_dbal_commons.ErrorMismatchConfiguration
	}
	return config, nil
}
