package gg_dbal_drivers

import (
	"bitbucket.org/digi-sense/gg-core"
	"strings"
)

//----------------------------------------------------------------------------------------------------------------------
//	u t i l s
//----------------------------------------------------------------------------------------------------------------------

// QueryGetParamNames return unique param names
func QueryGetParamNames(query string) []string {
	response := make([]string, 0)
	params := queryParamNames(query)
	for _, param := range params {
		if gg.Arrays.IndexOf(param, response) == -1 {
			response = append(response, param)
		}
	}
	return response
}

func QuerySelectParams(query string, allParams map[string]interface{}) map[string]interface{} {
	names := QueryGetParamNames(query)
	params := map[string]interface{}{}
	for _, v := range names {
		params[v] = allParams[v]
	}
	return params
}

func BindParams(query string, bindVars map[string]interface{}, earlyReplace bool) (command string, values []interface{}) {
	command, values = mergeParams(query, bindVars, earlyReplace)
	return
}

func IsRecordNotFoundError(err error) bool {
	return nil != err && err.Error() == "record not found"
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func mergeParams(query string, bindVars map[string]interface{}, earlyReplace bool) (command string, values []interface{}) {
	values = make([]interface{}, 0)
	command = query
	if nil == bindVars || len(bindVars) == 0 {
		return
	} else {
		paramNames := queryParamNames(query)
		if len(paramNames) > 0 {
			for _, name := range paramNames {
				if earlyReplace {
					command = strings.ReplaceAll(command, "@"+name, gg.Reflect.GetString(bindVars, name))
				} else {
					if value, ok := bindVars[name]; ok {
						values = append(values, value)
					} else {
						values = append(values, "")
					}
				}
			}
		}
	}
	return
}

func queryParamNames(query string) (response []string) {
	response = make([]string, 0)
	idx := strings.Index(query, "@")
	if idx > 1 {
		// cut to first match
		_, query, _ = strings.Cut(query, "@")
		query = "@" + query

		// add spaces
		query = strings.ReplaceAll(query, ";", " ;")
		query = strings.ReplaceAll(query, "(", "( ")
		query = strings.ReplaceAll(query, ")", " )")
		query += " "

		// add placeholder
		query = strings.ReplaceAll(query, "\"@", "<%")
		query = strings.ReplaceAll(query, "'@", "<%")
		query = strings.ReplaceAll(query, "@", "<%")
		query = strings.ReplaceAll(query, "\",", "%>")
		query = strings.ReplaceAll(query, "',", "%>")
		query = strings.ReplaceAll(query, ",", "%>")
		query = strings.ReplaceAll(query, "\" ", "%>")
		query = strings.ReplaceAll(query, "' ", "%>")
		query = strings.ReplaceAll(query, " )", "%>")
		query = strings.ReplaceAll(query, "\"\n", "%>")
		query = strings.ReplaceAll(query, "'\n", "%>")

		// parse
		parsed := gg.Regex.TextBetweenStrings(query, "<%", "%>")

		// trim values
		for _, name := range parsed {
			response = append(response, strings.Trim(name, " ,"))
		}
	}
	return
}
