package gg_dbal_drivers

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_dbal/gg_dbal_commons"
	"sync"
)

// ---------------------------------------------------------------------------------------------------------------------
//	t y p e
// ---------------------------------------------------------------------------------------------------------------------

type CacheManager struct {
	cache map[string]IDatabase

	mux sync.Mutex
}

// ---------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
// ---------------------------------------------------------------------------------------------------------------------

func NewCache() *CacheManager {
	instance := new(CacheManager)
	instance.cache = make(map[string]IDatabase)
	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *CacheManager) Open() {

}

func (instance *CacheManager) Close() {
	instance.Clear()
}

func (instance *CacheManager) Clear() {
	instance.mux.Lock()
	defer instance.mux.Unlock()
	for _, db := range instance.cache {
		_ = db.Close()
	}
	instance.cache = make(map[string]IDatabase)
}

func (instance *CacheManager) Create(driver, dsn string) (IDatabase, error) {
	instance.mux.Lock()
	defer instance.mux.Unlock()

	d := gg_dbal_commons.NewDsn(dsn)
	key := keyFrom(driver, d.String())
	key += "_" + gg.Rnd.RndId()

	return instance.get(key, driver, dsn)
}

// Get
// "driver": "arango",
// "dsn": "root:1234567890@tcp(localhost:8529)/my-database)"
func (instance *CacheManager) Get(driver, dsn string) (IDatabase, error) {
	instance.mux.Lock()
	defer instance.mux.Unlock()

	d := gg_dbal_commons.NewDsn(dsn)
	key := keyFrom(driver, d.String())

	return instance.get(key, driver, dsn)
}

func (instance *CacheManager) GetOrCreate(key, driver, dsn string) (IDatabase, error) {
	instance.mux.Lock()
	defer instance.mux.Unlock()

	return instance.get(key, driver, dsn)
}

func (instance *CacheManager) Remove(uid string) {
	instance.mux.Lock()
	defer instance.mux.Unlock()

	if db, b := instance.cache[uid]; !b {
		_ = db.Close()
		delete(instance.cache, uid)
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *CacheManager) get(key, driver, dsn string) (IDatabase, error) {
	if _, b := instance.cache[key]; !b {
		// get and open database
		db, err := NewDatabase(driver, dsn)
		if nil == err {
			instance.cache[key] = db
		} else {
			return nil, err
		}
	}
	return instance.cache[key], nil
}
