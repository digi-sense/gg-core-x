package gg_dbal_drivers

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_dbal/gg_dbal_commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_dbal/gg_dbal_drivers/dbschema"
	"bitbucket.org/digi-sense/gg-core-x/gg_dbal/gg_dbal_drivers/dbsql"
	"bitbucket.org/digi-sense/gg-core-x/ggx_commons"
	"database/sql"
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/driver/sqlserver"
	"gorm.io/gorm"
	"strings"
)

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
//	NewDriverSQL
//----------------------------------------------------------------------------------------------------------------------

type DriverGorm struct {
	uid    string
	driver string
	dsn    string
	db     *gorm.DB
	err    error

	// gorm attributes
	mode string
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
//----------------------------------------------------------------------------------------------------------------------

// NewDriverGorm "admin:admin@tcp(localhost:3306)/test"
func NewDriverGorm(driver string, dsn ...interface{}) *DriverGorm {
	instance := new(DriverGorm).
		SetDriver(driver)
	instance.mode = gg.ModeProduction

	if len(dsn) == 1 {
		if s, b := dsn[0].(string); b {
			instance.dsn = s
		} else if d, b := dsn[0].(gg_dbal_commons.Dsn); b {
			instance.dsn = (&d).String()
		} else if d, b := dsn[0].(*gg_dbal_commons.Dsn); b {
			instance.dsn = d.String()
		} else {
			instance.err = gg_dbal_commons.ErrorInvalidDsn
		}
	}
	if len(instance.dsn) == 0 && nil == instance.err {
		instance.err = gg_dbal_commons.ErrorInvalidDsn
	}
	if len(instance.dsn) > 0 {
		instance.uid = keyFrom(driver, instance.dsn)
	}
	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *DriverGorm) GetDB() (response *gorm.DB, err error) {
	if nil != instance {
		if nil == instance.db {
			err = instance.Open()
		}
		if nil == err {
			response = instance.db
		}
	}
	return
}

func (instance *DriverGorm) SetDriver(value string) *DriverGorm {
	if nil != instance {
		tokens := gg.Strings.SplitTrimSpace(value, ":") // gorm:mysql, mysql, ""
		switch len(tokens) {
		case 1:
			instance.driver = tokens[0]
		case 2:
			instance.driver = tokens[1]
		}
		// correct with auto-value
		if len(instance.driver) == 0 || instance.driver == "gorm" {
			instance.driver = "mysql"
		}
	}
	return instance
}

func (instance *DriverGorm) GetDriver() (response string) {
	if nil != instance {
		response = instance.driver
	}
	return
}

func (instance *DriverGorm) SetMode(value string) *DriverGorm {
	instance.mode = value
	return instance
}

func (instance *DriverGorm) GetMode() string {
	return instance.mode
}

func (instance *DriverGorm) AutomigrateSchemaFromfile(filename string) (err error) {
	if nil != instance {
		var schema *dbschema.DbSchema
		schema, err = dbschema.NewDbSchemaFromFile(filename)
		if nil != err {
			return
		}
		err = instance.AutomigrateSchema(schema)
	}
	return
}

func (instance *DriverGorm) AutomigrateSchema(schema *dbschema.DbSchema) (err error) {
	if nil != instance {
		db := instance.db
		err = dbschema.GormMigrateSchemaToDb(db, schema)
	}
	return
}

func (instance *DriverGorm) AutomigrateModelSchema(models ...interface{}) (err error) {
	if nil != instance {
		db := instance.db
		err = dbschema.GormMigrateModelSchemaToDb(db, models...)
	}
	return
}

func (instance *DriverGorm) SaveSchemaToFile(filename string) (err error) {
	if nil != instance {
		db := instance.db
		var schema *dbschema.DbSchema
		schema, err = dbschema.GormGetSchemaFromDb(db)
		if nil != err {
			return
		}

		// ensure dir exists
		filename = gg.Paths.Absolute(filename)
		_ = gg.Paths.Mkdir(filename)

		// get text and save
		text := gg.JSON.Stringify(schema)
		_, err = gg.IO.WriteTextToFile(text, filename)
	}
	return
}

func (instance *DriverGorm) GetSchema() (schema *dbschema.DbSchema, err error) {
	if nil != instance {
		db := instance.db
		schema, err = dbschema.GormGetSchemaFromDb(db)
		if nil != err {
			return
		}
	}
	return
}

func (instance *DriverGorm) Uid() string {
	return instance.uid
}

func (instance *DriverGorm) DriverName() string {
	return instance.driver
}

func (instance *DriverGorm) Driver() interface{} {
	return instance
}

func (instance *DriverGorm) Enabled() bool {
	return nil != instance && len(instance.dsn) > 0
}

func (instance *DriverGorm) Open() error {
	if nil != instance {
		if nil == instance.err {
			instance.err = instance.init()
		}
		return instance.err
	}
	return nil
}

func (instance *DriverGorm) Close() (err error) {
	if nil != instance.db {
		var db *sql.DB
		db, err = instance.db.DB()
		if nil == err && nil != db {
			err = db.Close()
		}
		instance.db = nil
	}
	return
}

func (instance *DriverGorm) QueryGetParamNames(query string) []string {
	return QueryGetParamNames(query)
}

func (instance *DriverGorm) QuerySelectParams(query string, allParams map[string]interface{}) map[string]interface{} {
	return QuerySelectParams(query, allParams)
}

func (instance *DriverGorm) Remove(collection, key string) (err error) {
	if nil != instance && nil != instance.db {
		query := dbsql.BuildDeleteCommand(collection, "id="+key)
		tx := instance.db.Exec(query)
		if nil != tx.Error && !IsRecordNotFoundError(tx.Error) {
			err = tx.Error
		}
	} else {
		err = gg_dbal_commons.ErrorDatabaseDoesNotExists
	}
	return
}

func (instance *DriverGorm) Get(collection string, key string) (response map[string]interface{}, err error) {
	if nil != instance && nil != instance.db && len(key) > 0 && len(collection) > 0 {
		query := dbsql.BuildSelect(collection) +
			fmt.Sprintf(" WHERE t.id=%v", key)

		tx := instance.db.Raw(query).First(&response)
		if nil != tx.Error && !IsRecordNotFoundError(tx.Error) {
			err = tx.Error
		}
	} else {
		err = gg_dbal_commons.ErrorDatabaseDoesNotExists
	}
	return
}

func (instance *DriverGorm) Upsert(collection string, item map[string]interface{}) (response map[string]interface{}, err error) {
	if nil != instance && nil != instance.db && len(collection) > 0 {
		id := gg.Convert.ToString(item["id"])
		var tx *gorm.DB
		if ok, _ := instance.Exists(collection, id); ok {
			query := dbsql.BuildUpdateCommand(collection, "id", id, item)
			tx = instance.db.Exec(query)
			if nil != tx.Error && !IsRecordNotFoundError(tx.Error) {
				err = tx.Error
			} else {
				response = item
			}
		} else {
			query := dbsql.BuildInsertCommand(collection, item, "*")
			tx = instance.db.Raw(query)
			if nil != tx.Error {
				err = tx.Error
			} else {
				err = tx.Scan(&response).Error
			}
		}
	} else {
		err = gg_dbal_commons.ErrorDatabaseDoesNotExists
	}
	return
}

func (instance *DriverGorm) Find(collection string, fieldName string, fieldValue interface{}) (response interface{}, err error) {
	if nil != instance && nil != instance.db {
		var command string
		resp := make([]map[string]interface{}, 0)
		if len(fieldName) > 0 && nil != fieldValue {
			if s, ok := fieldValue.(string); ok {
				if strings.Index(s, "'") == -1 && strings.Index(s, "'") == -1 {
					fieldValue = fmt.Sprintf("'%s'", s)
				}
			}
			command = fmt.Sprintf("SELECT t.* FROM %s t WHERE t.%s=%v", collection, fieldName, fieldValue)
		} else {
			command = collection
		}
		tx := instance.db.Raw(command)
		if nil != tx.Error {
			err = tx.Error
		} else {
			err = tx.Scan(&resp).Error
			if nil == err {
				response = resp
			}
		}
		return
	}
	return nil, gg_dbal_commons.ErrorDatabaseDoesNotExists
}

func (instance *DriverGorm) ForEach(collection string, callback ForEachCallback) error {
	if nil != instance && nil != instance.db {
		if nil != callback {
			var response []map[string]interface{}
			query := dbsql.BuildSelect(collection)
			tx := instance.db.Raw(query)
			tx.Scan(&response)
			if nil != tx.Error && !IsRecordNotFoundError(tx.Error) {
				return tx.Error
			}
			if len(response) > 0 {
				for _, item := range response {
					if callback(item) {
						break
					}
				}
			}
		}
		return nil // do nothing
	}
	return gg_dbal_commons.ErrorDatabaseDoesNotExists
}

func (instance *DriverGorm) ExecNative(command string, bindVars map[string]interface{}) (result interface{}, err error) {
	if nil != instance && nil != instance.db {
		execResp, execErr := instance.exec(command, bindVars, false)
		if nil != execErr {
			err = execErr
		} else {
			result = execResp
			// some commands like "SELECT COUNT(id) FROM table1"
			// return a single value that is wrapped into a map array.
			// We want to extract this value and return it as single unique value
			if len(execResp) == 1 {
				m := execResp[0]
				if len(m) == 1 {
					var val interface{}
					for _, v := range m {
						val = v
					}
					// convert if is a pointer
					if gg.Compare.IsPtrValue(val) {
						result = gg.Reflect.InterfaceOf(val)
					}
				}
			}
		}
	} else {
		err = gg_dbal_commons.ErrorDatabaseDoesNotExists
	}
	return
}

func (instance *DriverGorm) ExecMultiple(commands []string, bindVars []map[string]interface{}, options interface{}) (response []interface{}, err error) {
	if nil != instance && nil != instance.db {
		if len(commands) != len(bindVars) {
			return nil, gg_dbal_commons.ErrorCommandAndParamsDoNotMatch
		}
		for i := 0; i < len(commands); i++ {
			command := commands[i]
			bindVar := bindVars[i]
			data, execErr := instance.exec(command, bindVar, false)
			if nil != execErr {
				err = execErr
				return
			}
			for _, item := range data {
				response = append(response, item)
			}
		}
		return
	}
	return nil, gg_dbal_commons.ErrorDatabaseDoesNotExists
}

func (instance *DriverGorm) EnsureIndex(collection string, typeName string, fields []string, unique bool) (bool, error) {
	if nil != instance && nil != instance.db {

		return true, nil
	}
	return false, gg_dbal_commons.ErrorDatabaseDoesNotExists
}

func (instance *DriverGorm) EnsureCollection(collection string) (bool, error) {
	if nil != instance && nil != instance.db {

		return true, nil
	}
	return false, gg_dbal_commons.ErrorDatabaseDoesNotExists
}

//----------------------------------------------------------------------------------------------------------------------
//	e x t e n d
//----------------------------------------------------------------------------------------------------------------------

func (instance *DriverGorm) Exists(collection, key string) (bool, interface{}) {
	if len(key) > 0 {
		response, err := instance.Get(collection, key)
		return nil == err && nil != response, response
	}
	return false, nil
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *DriverGorm) init() (err error) {
	if len(instance.dsn) > 0 {
		switch instance.driver {
		case "sqlite":
			filename := gg.Paths.WorkspacePath(instance.dsn)
			instance.db, err = gorm.Open(sqlite.Open(filename), ggx_commons.GormConfig(instance.mode))
		case "mysql":
			// "user:pass@tcp(127.0.0.1:3306)/dbname?charset=utf8mb4&parseTime=True&loc=Local"
			instance.db, err = gorm.Open(mysql.Open(instance.dsn), &gorm.Config{})
		case "postgres":
			// "host=localhost user=gorm password=gorm dbname=gorm port=9920 sslmode=disable TimeZone=Asia/Shanghai"
			instance.db, err = gorm.Open(postgres.Open(instance.dsn), &gorm.Config{})
		case "sqlserver":
			// "sqlserver://gorm:LoremIpsum86@localhost:9930?database=gorm"
			instance.db, err = gorm.Open(sqlserver.Open(instance.dsn), &gorm.Config{})
		default:
			instance.db = nil
			err = gg.Errors.Prefix(gg_dbal_commons.ErrorDriverNotImplemented,
				fmt.Sprintf("'%s': ", instance.driver))
		}

	}
	return
}

func (instance *DriverGorm) exec(command string, bindVars map[string]interface{}, replaceBindVars bool) (response []map[string]interface{}, err error) {
	queryResponse := make([]map[string]interface{}, 0)
	if nil != instance {

		namedParams := QuerySelectParams(command, bindVars)
		var tx *gorm.DB
		if len(namedParams) > 0 {
			tx = instance.db.Raw(command, namedParams) // named params
		} else {
			tx = instance.db.Raw(command)
		}

		tx.Scan(&queryResponse)

		if nil != tx.Error {
			if IsRecordNotFoundError(tx.Error) {
				return
			}
			err = tx.Error
		} else {
			response = queryResponse
		}
	} else {
		err = gg.PanicSystemError
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------
