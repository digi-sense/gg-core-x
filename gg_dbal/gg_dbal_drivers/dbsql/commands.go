package dbsql

import (
	"bitbucket.org/digi-sense/gg-core"
	"fmt"
	"strings"
	"time"
)

const mySQLDateTimePattern = "yyyy-MM-dd HH:mm:ss" // MySQL format expected YYYY-MM-DD HH:MM:SS
const dialectMySQL = "mysql"

func BuildSelect(tableOrQuery string) string {
	if strings.Index(strings.ToLower(tableOrQuery), "select") > -1 {
		return tableOrQuery
	} else {
		return fmt.Sprintf("SELECT t.* FROM %v t", tableOrQuery)
	}
}

func BuildInsertCommand(tableName string, raw interface{}, returning string) (response string) {
	// INSERT INTO test.table1 (age, first_name)
	// VALUES (12, 'Giorgio');
	if item, b := raw.(map[string]interface{}); b {
		if nil == item {
			return
		}

		names := make([]string, 0)
		values := make([]string, 0)
		for k, v := range item {
			if nil != v {
				names = append(names, fmt.Sprintf("%s", k)) // append(names, fmt.Sprintf("%s.%s", tableName, k))
				values = append(values, toSQLString(v, dialectMySQL))
			}
		}

		var buf strings.Builder
		buf.WriteString("INSERT INTO ")
		buf.WriteString(tableName)

		// names
		buf.WriteString(" (")
		for i, v := range names {
			if i > 0 {
				buf.WriteString(",")
			}
			buf.WriteString(v)
		}
		buf.WriteString(") ")
		// values
		buf.WriteString("VALUES (")
		for i, v := range values {
			if i > 0 {
				buf.WriteString(",")
			}
			buf.WriteString(v)
		}
		// close command
		if len(returning) > 0 {
			buf.WriteString(fmt.Sprintf(") RETURNING %s;", returning))
		} else {
			buf.WriteString(");")
		}

		response = buf.String()

	}

	return
}

func BuildInsertCommands(tableName string, data []interface{}) []string {
	// INSERT INTO test.table1 (age, first_name)
	// VALUES (12, 'Giorgio');
	response := make([]string, 0)
	for _, raw := range data {
		response = append(response, BuildInsertCommand(tableName, raw, ""))
	}

	return response
}

func BuildInsertCommandsReturning(tableName string, data []interface{}) []string {
	// INSERT INTO test.table1 (age, first_name)
	// VALUES (12, 'Giorgio');
	response := make([]string, 0)
	for _, raw := range data {
		response = append(response, BuildInsertCommand(tableName, raw, "id"))
	}
	return response
}

func BuildUpdateCommand(tableName string, keyName string, keyValue interface{}, data map[string]interface{}) string {
	// UPDATE test.table1 t
	// SET t.age = 23,
	//     t.first_name = 'Marcolino d'
	// WHERE t.id = 8;
	var buf strings.Builder
	buf.WriteString("UPDATE ")
	buf.WriteString(tableName)
	buf.WriteString(" \n")
	buf.WriteString("SET ")

	// data
	count := 0
	for k, v := range data {
		if nil != v {
			if count > 0 {
				buf.WriteString(", ")
			}
			buf.WriteString(fmt.Sprintf("%s.%s", tableName, k) + " = " + toSQLString(v, dialectMySQL))
			count++
		}
	}

	// filter
	buf.WriteString(" WHERE ")
	buf.WriteString(keyName + " = " + toSQLString(keyValue, ""))
	buf.WriteString(";")

	// response
	return buf.String()
}

func BuildUpdateCommandWithTableName(tableName string, keyName string, keyValue interface{}, data map[string]interface{}) string {
	// UPDATE test.table1 t
	// SET t.age = 23,
	//     t.first_name = 'Marcolino d'
	// WHERE t.id = 8;
	var buf strings.Builder
	buf.WriteString("UPDATE ")
	buf.WriteString(tableName)
	buf.WriteString(" t\n")
	buf.WriteString("SET ")

	// data
	count := 0
	for k, v := range data {
		if count > 0 {
			buf.WriteString(", ")
		}
		buf.WriteString("t." + k + " = " + toSQLString(v, dialectMySQL))
		count++
	}

	// filter
	buf.WriteString(" WHERE ")
	buf.WriteString("t." + keyName + " = " + toSQLString(keyValue, ""))
	buf.WriteString(";")

	// response
	return buf.String()
}

func BuildDeleteCommand(tableName string, filter string) string {
	// DELETE
	// FROM test.table1
	// WHERE id = 18;

	var buf strings.Builder
	buf.WriteString("DELETE FROM ")
	buf.WriteString(tableName)

	// filter
	if len(filter) > 0 {
		buf.WriteString(" WHERE ")
		buf.WriteString(filter)
		buf.WriteString(";")
	}

	// response
	return buf.String()
}

func BuildCountCommand(tableName string, filter string) string {
	// SELECT COUNT(*) FROM tableName
	// WHERE id > 18;
	var buf strings.Builder
	buf.WriteString("SELECT COUNT(*) FROM ")
	buf.WriteString(tableName)

	// filter
	if len(filter) > 0 {
		buf.WriteString(" WHERE ")
		buf.WriteString(filter)
		buf.WriteString(";")
	}

	// response
	return buf.String()
}

func BuildCountDistinctCommand(tableName, fieldName, filter string) string {
	// SELECT COUNT(DISTINCT fieldName ) FROM tableName
	// WHERE id > 18;
	var buf strings.Builder
	buf.WriteString("SELECT COUNT(DISTINCT " + fieldName + ") FROM ")
	buf.WriteString(tableName)

	// filter
	if len(filter) > 0 {
		buf.WriteString(" WHERE ")
		buf.WriteString(filter)
		buf.WriteString(";")
	}

	// response
	return buf.String()
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func toSQLString(val interface{}, sqlDialect string) (result string) {
	if v, b := val.(string); b {
		// escape string
		if strings.Index(v, "'") > -1 {
			v = strings.ReplaceAll(v, "'", "''")
		}
		result = "'" + v + "'"
	} else if dt, b := val.(time.Time); b {
		switch sqlDialect {
		case dialectMySQL:
			result = "'" + gg.Dates.FormatDate(dt, mySQLDateTimePattern) + "'"
		default:
			result = "'" + gg.Dates.FormatDate(dt, mySQLDateTimePattern) + "'"
		}
	} else {
		result = gg.Convert.ToString(val)
	}
	return
}

func toFilter(keyName, operator string, keyValue interface{}) string {
	var buf strings.Builder
	// filter
	if nil != keyValue || strings.TrimSpace(strings.ToUpper(operator)) == "IS NOT NULL" {
		buf.WriteString("WHERE ")
		buf.WriteString(keyName + " " + operator)
		if nil != keyValue {
			buf.WriteString(" " + toSQLString(keyValue, dialectMySQL))
		}
		buf.WriteString(";")
	}

	// response
	return buf.String()
}
