package dbschema

import (
	"bitbucket.org/digi-sense/gg-core"
	"fmt"
	"gorm.io/gorm"
	"strings"
)

// ---------------------------------------------------------------------------------------------------------------------
//	DbSchema
// ---------------------------------------------------------------------------------------------------------------------

type DbSchema struct {
	Tables []*DbSchemaTable `json:"tables"`
}

func (instance *DbSchema) Parse(i interface{}) (err error) {
	if nil != instance {
		err = gg.JSON.Read(gg.JSON.Stringify(i), &instance)
	}
	return
}

func (instance *DbSchema) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *DbSchema) Map() map[string]interface{} {
	return gg.Convert.ToMap(gg.JSON.Stringify(instance))
}

func (instance *DbSchema) Has(name string) bool {
	if nil != instance {
		for _, table := range instance.Tables {
			if table.Name == name {
				return true
			}
		}
	}
	return false
}

func (instance *DbSchema) Get(name string) *DbSchemaTable {
	if nil != instance {
		for _, table := range instance.Tables {
			if table.Name == name {
				return table
			}
		}
	}
	return nil
}

// Put add or replace a table
func (instance *DbSchema) Put(table *DbSchemaTable) {
	if nil != instance {
		// remove if exists
		index := -1
		for i := 0; i < len(instance.Tables); i++ {
			t := instance.Tables[i]
			if t.Name == table.Name {
				// remove table
				index = i
			}
		}
		if index > -1 {
			instance.Tables = gg.Arrays.RemoveIndex(index, instance.Tables).([]*DbSchemaTable)
		}
		// put new table
		instance.Tables = append(instance.Tables, table)
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	DbSchemaTable
// ---------------------------------------------------------------------------------------------------------------------

type DbSchemaTable struct {
	Name    string            `json:"name"`
	Columns []*DbSchemaColumn `json:"columns"`
}

func (instance *DbSchemaTable) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *DbSchemaTable) Map() map[string]interface{} {
	return gg.Convert.ToMap(gg.JSON.Stringify(instance))
}

func (instance *DbSchemaTable) Struct() interface{} {
	builder := gg.Structs.NewBuilder()
	for _, column := range instance.Columns {
		tag := column.Tag
		if len(tag) > 0 && strings.Index(tag, ":") == -1 {
			tag = fmt.Sprintf(`gorm:"%s"`, tag)
		}
		name := column.Name
		t := column.Type
		builder.AddFieldByStringType(name, tag, t)
	}
	return builder.Interface()
}

func (instance *DbSchemaTable) Parse(i interface{}) (err error) {
	if nil != instance {
		err = gg.JSON.Read(gg.JSON.Stringify(i), &instance)
	}
	return
}

func (instance *DbSchemaTable) Has(name string) bool {
	if nil != instance {
		for _, column := range instance.Columns {
			if column.Name == name {
				return true
			}
		}
	}
	return false
}

func (instance *DbSchemaTable) Get(name string) *DbSchemaColumn {
	if nil != instance {
		for _, column := range instance.Columns {
			if column.Name == name {
				return column
			}
		}
	}
	return nil
}

func (instance *DbSchemaTable) Put(column *DbSchemaColumn) {
	if nil != instance {
		// remove if exists
		index := -1
		for i := 0; i < len(instance.Columns); i++ {
			t := instance.Columns[i]
			if t.Name == column.Name {
				// remove table
				index = i
			}
		}
		if index > -1 {
			instance.Columns = gg.Arrays.RemoveIndex(index, instance.Columns).([]*DbSchemaColumn)
		}
		// put new table
		instance.Columns = append(instance.Columns, column)
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	DbSchemaColumn
// ---------------------------------------------------------------------------------------------------------------------

type DbSchemaColumn struct {
	Name     string `json:"name"`
	Nullable bool   `json:"nullable"`
	Type     string `json:"type"`
	Tag      string `json:"tag"`
}

func (instance *DbSchemaColumn) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *DbSchemaColumn) Map() map[string]interface{} {
	return gg.Convert.ToMap(gg.JSON.Stringify(instance))
}

func (instance *DbSchemaColumn) Parse(i interface{}) (err error) {
	if nil != instance {
		err = gg.JSON.Read(gg.JSON.Stringify(i), &instance)
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	Utils
// ---------------------------------------------------------------------------------------------------------------------

func NewDbSchema() *DbSchema {
	instance := new(DbSchema)
	instance.Tables = make([]*DbSchemaTable, 0)
	return instance
}

func NewDbSchemaFrom(i interface{}) (schema *DbSchema, err error) {
	if s, ok := i.(string); ok {
		if gg.Regex.IsValidJsonObject(s) {
			schema, err = NewDbSchemaFromJSON(s)
		} else {
			schema, err = NewDbSchemaFromFile(s)
		}
		return
	}

	if m, ok := i.(map[string]interface{}); ok {
		schema, err = NewDbSchemaFromMap(m)
		return
	}

	err = gg.Errors.Prefix(gg.PanicSystemError, fmt.Sprintf("Unsupported item: '%v'", i))

	return
}

func NewDbSchemaFromFile(filename string) (schema *DbSchema, err error) {
	err = gg.JSON.ReadFromFile(filename, &schema)
	return
}

func NewDbSchemaFromJSON(json string) (schema *DbSchema, err error) {
	err = gg.JSON.Read(json, &schema)
	return
}

func NewDbSchemaFromMap(m map[string]interface{}) (schema *DbSchema, err error) {
	err = gg.JSON.Read(gg.JSON.Stringify(m), &schema)
	return
}

func NewDbSchemaTable() *DbSchemaTable {
	instance := new(DbSchemaTable)
	instance.Columns = make([]*DbSchemaColumn, 0)
	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	GORM
// ---------------------------------------------------------------------------------------------------------------------

func GormGetSchemaFromDb(db *gorm.DB) (schema *DbSchema, err error) {
	if nil != db {
		// GENERATE SCHEMA
		var tbls []string
		tbls, err = db.Migrator().GetTables()
		if nil != err {
			return
		}

		schema = NewDbSchema()
		for _, tbl := range tbls {
			tx := db.Table(tbl)
			s := &struct{}{}
			var cols []gorm.ColumnType
			cols, err = tx.Migrator().ColumnTypes(&s)
			if nil != err {
				err = gg.Errors.Prefix(err, fmt.Sprintf("Error getting schema from '%s': ", tbl))
				return
			}
			// add table to schema
			table := NewDbSchemaTable()
			table.Name = tbl
			for _, col := range cols {
				column := &DbSchemaColumn{
					Name: col.Name(),
				}
				column.Nullable, _ = col.Nullable()
				column.Type = col.DatabaseTypeName()
				if isPrimaryKey, _ := col.PrimaryKey(); isPrimaryKey {
					column.Tag = "primarykey"
				}
				table.Columns = append(table.Columns, column)
			}
			schema.Tables = append(schema.Tables, table)
			// fmt.Println(table)
		}
	} else {
		err = gg.Errors.Prefix(gg.PanicSystemError, "Missing DB: ")
	}
	return
}

func GormMigrateSchemaToDb(db *gorm.DB, schema *DbSchema) (err error) {
	if nil != db {
		if nil != schema {
			// AUTOMIGRATE SCHEMA
			tables := schema.Tables
			for _, table := range tables {
				if !db.Migrator().HasTable(table.Name) {
					tx := db.Raw("CREATE TABLE ?", table.Name)
					if nil != tx.Error {
						return tx.Error
					}
				}
				s := table.Struct()
				e := db.Table(table.Name).Migrator().AutoMigrate(s)
				if nil != e {
					return e
				}
			}
		} else {
			err = gg.Errors.Prefix(gg.PanicSystemError, "Missing SCHEMA: ")
		}
	} else {
		err = gg.Errors.Prefix(gg.PanicSystemError, "Missing DB: ")
	}
	return
}

func GormMigrateModelSchemaToDb(db *gorm.DB, models ...interface{}) (err error) {
	if nil != db {
		if len(models) > 0 {
			err = db.AutoMigrate(models...)
		}
	}
	return
}
