package gg_dbal_semantic_search_test

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_dbal/gg_dbal_semantic_search"
	"fmt"
	"testing"
)

func TestToKeywords(t *testing.T) {
	keywords := gg_dbal_semantic_search.ToKeywords("hello this is a text to tokenize in keywords!!")
	fmt.Println(keywords)
}