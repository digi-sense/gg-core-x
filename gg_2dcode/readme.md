# 2dcode #

![icon](./icon.png)

## Quick Start ##

Code below generates a QRCODE using circle shapes with a centered logo image.

Note: Circle shapes may be not readable by standard readers.

```go
package main


func main() {
	generator := gg_2dcode.NewGenerator(commons.BarcodeFormat_QR_CODE)
	bytes, err := generator.Encode("http://www.gianangelogeminiani.me",
		&commons.OptionSize{Width: 300},
		&commons.OptionCircleShapes{},
		&commons.OptionImageLogo{Filename: "./icon.png"})
	if nil != err {
		panic(err)
	}
	_, err = gg.IO.WriteBytesToFile(bytes, "./qrcode.png")
	if nil != err {
		panic(err)
	}
}
```
