package gg_2dcode

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_2dcode/commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_2dcode/generators"
	"bitbucket.org/digi-sense/gg-core-x/gg_2dcode/scanners"
	"errors"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type IBarCodeGenerator interface {
	Encode(contents string, options ...interface{}) ([]byte, error)
}

type IBarCodeScanner interface {
	Decode(filename string, options ...interface{}) (string, error)
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func NewGenerator(format commons.BarcodeFormat) IBarCodeGenerator {
	switch format {
	case commons.BarcodeFormat_QR_CODE:
		return generators.NewGeneratorQrCode()
	case commons.BarcodeFormat_CODE_39:
		return generators.NewGeneratorCode39()
	case commons.BarcodeFormat_CODE_93:
		return generators.NewGeneratorCode93()
	case commons.BarcodeFormat_EAN_8:
		return generators.NewGeneratorEan8()
	case commons.BarcodeFormat_EAN_13:
		return generators.NewGeneratorEan13()
	}
	return nil
}

func NewScanner(format commons.BarcodeFormat) IBarCodeScanner {
	switch format {
	case commons.BarcodeFormat_QR_CODE:
		return scanners.NewScannerQrCode()
	case commons.BarcodeFormat_CODE_39:
		return scanners.NewScannerCode39()
	case commons.BarcodeFormat_CODE_93:
		return scanners.NewScannerCode93()
	case commons.BarcodeFormat_EAN_8:
		return scanners.NewScannerEan8()
	case commons.BarcodeFormat_EAN_13:
		return scanners.NewScannerEan13()
	}
	return nil
}

func Generate(format string, data string, options ...interface{}) ([]byte, error) {
	generator, err := getGenerator(format)
	if nil != err {
		return nil, err
	}
	return generator.Encode(data, options...)
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func getFormat(format string) (commons.BarcodeFormat, error) {
	var f commons.BarcodeFormat
	switch format {
	case "QR_CODE":
		f = commons.BarcodeFormat_QR_CODE
	case "CODE_39":
		f = commons.BarcodeFormat_CODE_39
	case "CODE_93":
		f = commons.BarcodeFormat_CODE_93
	case "EAN_8":
		f = commons.BarcodeFormat_EAN_8
	case "EAN_13":
		f = commons.BarcodeFormat_EAN_13
	default:
		return commons.BarcodeFormat_AZTEC, errors.New("unsupported format: " + format)
	}
	return f, nil
}

func getGenerator(format string) (IBarCodeGenerator, error) {
	f, err := getFormat(format)
	if nil != err {
		return nil, err
	}
	return NewGenerator(f), nil
}
