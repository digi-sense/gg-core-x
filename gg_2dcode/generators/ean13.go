package generators

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_2dcode/commons"
	"github.com/makiuchi-d/gozxing"
	"github.com/makiuchi-d/gozxing/oned"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type GeneratorEan13 struct {
	format commons.BarcodeFormat
}

func NewGeneratorEan13() *GeneratorEan13 {
	instance := new(GeneratorEan13)
	instance.format = commons.BarcodeFormat_EAN_13

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *GeneratorEan13) Encode(contents string, options ...interface{}) ([]byte, error) {
	writer := oned.NewEAN13Writer()
	return commons.Encode(writer, gozxing.BarcodeFormat_EAN_13, contents, options...)
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------
