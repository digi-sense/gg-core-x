package generators

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_2dcode/commons"
	"github.com/makiuchi-d/gozxing"
	"github.com/makiuchi-d/gozxing/oned"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type GeneratorEan8 struct {
	format commons.BarcodeFormat
}

func NewGeneratorEan8() *GeneratorEan8 {
	instance := new(GeneratorEan8)
	instance.format = commons.BarcodeFormat_EAN_8

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *GeneratorEan8) Encode(contents string, options ...interface{}) ([]byte, error) {
	writer := oned.NewEAN8Writer()
	return commons.Encode(writer, gozxing.BarcodeFormat_EAN_8, contents, options...)
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------
