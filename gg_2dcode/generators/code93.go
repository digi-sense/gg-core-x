package generators

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_2dcode/commons"
	"github.com/makiuchi-d/gozxing"
	"github.com/makiuchi-d/gozxing/oned"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type GeneratorCode93 struct {
	format commons.BarcodeFormat
}

func NewGeneratorCode93() *GeneratorCode93 {
	instance := new(GeneratorCode93)
	instance.format = commons.BarcodeFormat_CODE_93

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *GeneratorCode93) Encode(contents string, options ...interface{}) ([]byte, error) {
	writer := oned.NewCode93Writer()
	return commons.Encode(writer, gozxing.BarcodeFormat_CODE_93, contents, options...)
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------
