package commons

import (
	"bitbucket.org/digi-sense/gg-core"
	"bytes"
	"github.com/makiuchi-d/gozxing"
	"image"
	"image/color"
	"image/png"
	"os"
)

func ToImage(bm *gozxing.BitMatrix) image.Image {
	height := bm.GetHeight()
	width := bm.GetWidth()
	upLeft := image.Point{0, 0}
	lowRight := image.Point{width, height}

	img := image.NewRGBA(image.Rectangle{upLeft, lowRight})

	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {
			bit := bm.Get(x, y)
			if bit {
				img.Set(x, y, color.Black)
			} else {
				img.Set(x, y, color.White)
			}
		}
	}
	return img
}

func GetSize(options ...interface{}) (width, height int) {
	width = 64
	height = 64

	for _, option := range options {
		option = gg.Reflect.ValueOf(option).Interface()
		if o, b := option.(OptionSize); b {
			if o.Width > 0 {
				width = o.Width
			}
			if o.Height > 0 {
				height = o.Height
			}
		}
	}

	return
}

func Decode(reader gozxing.Reader, filename string, options ...interface{}) (string, error) {
	if b, err := gg.Paths.Exists(filename); b {
		// open and decode image file
		file, err := os.Open(filename)
		if nil != err {
			return "", err
		}
		img, _, err := image.Decode(file)
		if nil != err {
			return "", err
		}

		// prepare BinaryBitmap
		bmp, _ := gozxing.NewBinaryBitmapFromImage(img)
		result, err := reader.Decode(bmp, nil)
		if nil != err {
			return "", err
		}
		return result.String(), err
	} else {
		return "", err
	}
}

func Encode(writer gozxing.Writer, format gozxing.BarcodeFormat, contents string, options ...interface{}) ([]byte, error) {
	width, height := GetSize(options...)
	bmp, err := writer.Encode(contents, format, width, height, nil)
	if nil != err {
		return nil, err
	}

	img := ToImage(bmp)
	var buf bytes.Buffer
	err = png.Encode(&buf, img)
	if nil != err {
		return nil, err
	}

	return buf.Bytes(), nil
}
