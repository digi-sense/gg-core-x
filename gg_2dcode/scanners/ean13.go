package scanners

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_2dcode/commons"
	"github.com/makiuchi-d/gozxing/oned"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type ScannerEan13 struct {
	format commons.BarcodeFormat
}

func NewScannerEan13() *ScannerEan13 {
	instance := new(ScannerEan13)
	instance.format = commons.BarcodeFormat_EAN_13

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *ScannerEan13) Decode(filename string, options ...interface{}) (string, error) {
	reader := oned.NewEAN13Reader()
	return commons.Decode(reader, filename, options...)
}
