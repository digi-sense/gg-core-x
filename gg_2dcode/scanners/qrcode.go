package scanners

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_2dcode/commons"
	"github.com/makiuchi-d/gozxing"
	"github.com/makiuchi-d/gozxing/qrcode"
	"image"
	"os"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type ScannerQrCode struct {
	format commons.BarcodeFormat
}

func NewScannerQrCode() *ScannerQrCode {
	instance := new(ScannerQrCode)
	instance.format = commons.BarcodeFormat_QR_CODE

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *ScannerQrCode) Decode(filename string, options ...interface{}) (string, error) {
	if b, err := gg.Paths.Exists(filename); b {
		// open and decode image file
		file, err := os.Open(filename)
		if nil != err {
			return "", err
		}
		img, _, err := image.Decode(file)
		if nil != err {
			return "", err
		}

		// prepare BinaryBitmap
		bmp, _ := gozxing.NewBinaryBitmapFromImage(img)

		qrReader := qrcode.NewQRCodeReader()
		result, err := qrReader.Decode(bmp, nil)
		if nil != err {
			return "", err
		}

		return result.String(), nil
	} else {
		return "", err
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------
