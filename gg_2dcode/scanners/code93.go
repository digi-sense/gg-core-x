package scanners

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_2dcode/commons"
	"github.com/makiuchi-d/gozxing/oned"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type ScannerCode93 struct {
	format commons.BarcodeFormat
}

func NewScannerCode93() *ScannerCode93 {
	instance := new(ScannerCode93)
	instance.format = commons.BarcodeFormat_CODE_93

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *ScannerCode93) Decode(filename string, options ...interface{}) (string, error) {
	reader := oned.NewCode93Reader()
	return commons.Decode(reader, filename, options...)
}
