package scanners

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_2dcode/commons"
	"github.com/makiuchi-d/gozxing/oned"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type ScannerEan8 struct {
	format commons.BarcodeFormat
}

func NewScannerEan8() *ScannerEan8 {
	instance := new(ScannerEan8)
	instance.format = commons.BarcodeFormat_EAN_8

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *ScannerEan8) Decode(filename string, options ...interface{}) (string, error) {
	reader := oned.NewEAN8Reader()
	return commons.Decode(reader, filename, options...)
}
