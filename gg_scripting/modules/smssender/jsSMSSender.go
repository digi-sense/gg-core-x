package smssender

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting/commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_sms"
	"errors"
	"github.com/dop251/goja"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type JsSMSSender struct {
	runtime  *goja.Runtime
	object   *goja.Object
	provider string
	engine   *gg_sms.SMSEngine
}

//----------------------------------------------------------------------------------------------------------------------
//	JsElasticEngine
//----------------------------------------------------------------------------------------------------------------------

func WrapSMSSender(runtime *goja.Runtime, provider string, engine *gg_sms.SMSEngine) goja.Value {
	instance := new(JsSMSSender)
	instance.runtime = runtime
	instance.provider = provider
	instance.engine = engine

	instance.object = instance.runtime.NewObject()
	instance.export()

	// add closable: all closable objects must be exposed to avoid
	commons.AddClosableObject(instance.runtime, instance.object)

	return instance.value()
}

func (instance *JsSMSSender) value() goja.Value {
	if nil != instance.object {
		return instance.object
	}
	return goja.Undefined()
}

//----------------------------------------------------------------------------------------------------------------------
//	e x p o s e d
//----------------------------------------------------------------------------------------------------------------------

func (instance *JsSMSSender) open(_ goja.FunctionCall) goja.Value {
	if nil != instance {
		err := instance.init()
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}
	}
	return goja.Undefined()
}

func (instance *JsSMSSender) close(_ goja.FunctionCall) goja.Value {
	if nil != instance {

	}
	return goja.Undefined()
}

func (instance *JsSMSSender) send(call goja.FunctionCall) goja.Value {
	if nil != instance {
		err := instance.init()
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}

		var message, to, from string
		switch len(call.Arguments) {
		case 2:
			message = commons.GetString(call, 0)
			to = commons.GetString(call, 1)
		case 3:
			message = commons.GetString(call, 0)
			to = commons.GetString(call, 1)
			from = commons.GetString(call, 2)
		default:
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}

		var response string
		response, err = instance.engine.SendMessage(instance.provider, message, to, from)
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}
		return instance.runtime.ToValue(response)
	}
	return goja.Undefined()
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *JsSMSSender) init() (err error) {
	if nil != instance.engine && len(instance.engine.ProviderNames()) > 0 {
		if len(instance.provider) == 0 {
			instance.provider = instance.engine.ProviderNames()[0]
		}
	} else {
		return errors.New("missing_configuration")
	}
	return err
}

func (instance *JsSMSSender) export() {
	o := instance.object

	_ = o.Set("open", instance.open)
	_ = o.Set("close", instance.close)
	_ = o.Set("send", instance.send)
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------
