package smssender

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting/commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting/modules/defaults/require"
	"bitbucket.org/digi-sense/gg-core-x/gg_sms"
	"bitbucket.org/digi-sense/gg-core-x/gg_sms/gg_sms_engine"
	"github.com/dop251/goja"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

const NAME = "sms-sender"

type ModuleElasticSearch struct {
	runtime *goja.Runtime
}

//----------------------------------------------------------------------------------------------------------------------
//	e x p o s e d
//----------------------------------------------------------------------------------------------------------------------

// sms.createTransport([provider], settings)
func (instance *ModuleElasticSearch) createTransport(call goja.FunctionCall) goja.Value {
	if nil != instance {
		var settings interface{}
		var provider string
		switch len(call.Arguments) {
		case 1:
			settings = commons.GetExport(call, 0)
		case 2:
			provider = commons.GetString(call, 0)
			settings = commons.GetExport(call, 1)
		default:
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}

		var config *gg_sms_engine.SMSConfiguration
		if m, b := settings.(map[string]interface{}); b {
			config, _ = gg_sms_engine.NewSMSConfigurationFromMap(m)
		} else if s, b := settings.(string); b {
			if m, b := gg.JSON.StringToMap(s); b {
				config, _ = gg_sms_engine.NewSMSConfigurationFromMap(m)
			} else {
				config, _ = gg_sms_engine.NewSMSConfigurationFromFile(s)
			}
		}
		if nil != config {
			return WrapSMSSender(instance.runtime, provider, gg_sms.SMS.NewEngine(config))
		} else {
			// invalid options
			panic(instance.runtime.NewTypeError("Invalid Options."))
		}
	}
	return goja.Undefined()
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func load(runtime *goja.Runtime, module *goja.Object, _ ...interface{}) {
	instance := &ModuleElasticSearch{
		runtime: runtime,
	}

	o := module.Get("exports").(*goja.Object)
	_ = o.Set("createTransport", instance.createTransport)

}

func Enable(ctx *commons.RuntimeContext) {
	// register
	require.RegisterNativeModule(NAME, &commons.ModuleInfo{
		Context: ctx,
		Loader:  load,
	})
}
