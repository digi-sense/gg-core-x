package fileutils

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting/commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting/modules/defaults/require"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"fmt"
	"github.com/dop251/goja"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

const NAME = "file-utils"

type FileUtils struct {
	runtime *goja.Runtime
}

//----------------------------------------------------------------------------------------------------------------------
//	e x p o s e d
//----------------------------------------------------------------------------------------------------------------------

// utils.fileReadText(path)
func (instance *FileUtils) fileReadText(call goja.FunctionCall) goja.Value {
	path := call.Argument(0).String()
	text, err := gg.IO.ReadTextFromFile(path)
	if nil != err {
		// throw back error to javascript
		panic(instance.runtime.NewTypeError(err.Error()))
	}
	return instance.runtime.ToValue(text)
}

// utils.fileReadBytes(path)
func (instance *FileUtils) fileReadBytes(call goja.FunctionCall) goja.Value {
	path := call.Argument(0).String()
	bytes, err := gg.IO.ReadBytesFromFile(path)
	if nil != err {
		// throw back error to javascript
		panic(instance.runtime.NewTypeError(err.Error()))
	}
	return instance.runtime.ToValue(bytes)
}

// utils.fileWrite(path, data)
// utils.fileWrite(path, data, rotateDir, rotateBytes, rotateFiles)
func (instance *FileUtils) fileWrite(call goja.FunctionCall) goja.Value {
	if nil != instance {
		if len(call.Arguments) > 1 {
			path := commons.GetString(call, 0) // call.Argument(0).String()
			data := commons.GetExport(call, 1) // call.Argument(1).Export()
			rotateDir := commons.GetString(call, 2)
			rotateBytes := commons.GetInt(call, 3)
			rotateFiles := commons.GetInt(call, 4)

			err := write(path, data, false, rotateDir, rotateBytes, rotateFiles)
			if nil != err {
				panic(instance.runtime.NewTypeError(err.Error()))
			}
		} else {
			panic(instance.runtime.NewTypeError("missing required arguments"))
		}
	}
	return goja.Undefined()
}

// utils.fileAppend(path, data)
// utils.fileAppend(path, data, rotateDir, rotateBytes, rotateFiles)
func (instance *FileUtils) fileAppend(call goja.FunctionCall) goja.Value {
	if nil != instance {
		if len(call.Arguments) > 1 {
			path := commons.GetString(call, 0) // call.Argument(0).String()
			data := commons.GetExport(call, 1) // call.Argument(1).Export()
			rotateDir := commons.GetString(call, 2)
			rotateBytes := commons.GetInt(call, 3)
			rotateFiles := commons.GetInt(call, 4)

			err := write(path, data, true, rotateDir, rotateBytes, rotateFiles)
			if nil != err {
				panic(instance.runtime.NewTypeError(err.Error()))
			}
		} else {
			panic(instance.runtime.NewTypeError("missing required arguments"))
		}
	}
	return goja.Undefined()
}

func (instance *FileUtils) fileExists(call goja.FunctionCall) goja.Value {
	if nil != instance {
		if len(call.Arguments) > 1 {
			path := commons.GetString(call, 0) // call.Argument(0).String()
			response, _ := gg.Paths.Exists(path)
			return instance.runtime.ToValue(response)
		} else {
			panic(instance.runtime.NewTypeError("missing required arguments"))
		}
	}
	return goja.Undefined()
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func WriteDataToFile(path string, data interface{}) error {
	return write(path, data, false, "", 0, 0)
}

func AppendDataToFile(path string, data interface{}) error {
	return write(path, data, true, "", 0, 0)
}

func write(path string, data interface{}, append bool, rotateDir string, rotateBytes int64, rotateFiles int64) (err error) {
	if len(path) > 0 {
		var bytes []byte
		if bb, b := data.([]byte); b {
			bytes = bb
		} else if tt, b := data.(string); b {
			bytes = []byte(tt)
		}
		if len(rotateDir) > 0 {
			rotateDir = gg.Paths.Absolutize(rotateDir, gg.Paths.Dir(path))
		}
		// creates writer
		var writer *gg_utils.FileWriter
		writer, err = gg.IO.NewFileWriter()
		if nil != err {
			return
		}
		writer.SetAppend(append)
		writer.SetFilename(path)
		// file rotation
		if rotateBytes > 0 {
			writer.SetRotateDir(rotateDir)
			if rotateFiles > 0 {
				writer.SetRotateDirLimit(int(rotateFiles))
			} else {
				writer.SetRotateDirLimit(-1)
			}
			writer.SetRotateMode(fmt.Sprintf("size:%v", rotateBytes))
		}

		// write
		_, err = writer.Write(bytes)
	} else {
		err = gg.Errors.Prefix(gg.PanicSystemError, "Missing path: ")
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	I N I T
//----------------------------------------------------------------------------------------------------------------------

func load(runtime *goja.Runtime, module *goja.Object, _ ...interface{}) {
	instance := &FileUtils{
		runtime: runtime,
	}

	o := module.Get("exports").(*goja.Object)
	// file utility
	_ = o.Set("fileReadText", instance.fileReadText)
	_ = o.Set("fileReadBytes", instance.fileReadBytes)
	_ = o.Set("fileWrite", instance.fileWrite)
	_ = o.Set("fileAppend", instance.fileAppend)
	_ = o.Set("fileExists", instance.fileExists)

}

func Enable(ctx *commons.RuntimeContext) {
	// register
	require.RegisterNativeModule(NAME, &commons.ModuleInfo{
		Context: ctx,
		Loader:  load,
	})
}
