package barcodeutils

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_2dcode"
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting/commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting/modules/defaults/require"
	"github.com/dop251/goja"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

const NAME = "barcode-utils"

type BarcodeUtils struct {
	runtime *goja.Runtime
}

//----------------------------------------------------------------------------------------------------------------------
//	e x p o s e d
//----------------------------------------------------------------------------------------------------------------------

// barcode.writeBytes(format, data)
// ex: barcode.writeBytes("QR_CODE", "123456789")
func (instance *BarcodeUtils) writeBytes(call goja.FunctionCall) goja.Value {
	var format, data string
	switch len(call.Arguments) {
	case 2:
		format = commons.GetString(call, 0)
		data = commons.GetString(call, 1)
	default:
		panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
	}
	response, err := gg_2dcode.Generate(format, data)
	if nil != err {
		panic(instance.runtime.NewTypeError(err.Error()))
	}
	return instance.runtime.ToValue(response)
}

// barcode.writeBase64(format, data)
// ex: barcode.writeBase64("QR_CODE", "123456789")
func (instance *BarcodeUtils) writeBase64(call goja.FunctionCall) goja.Value {
	var format, data string
	switch len(call.Arguments) {
	case 2:
		format = commons.GetString(call, 0)
		data = commons.GetString(call, 1)
	default:
		panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
	}
	response, err := gg_2dcode.Generate(format, data)
	if nil != err {
		panic(instance.runtime.NewTypeError(err.Error()))
	}
	return instance.runtime.ToValue(gg.Coding.EncodeBase64(response))
}

// barcode.writeFile(format, data, filename)
// ex: barcode.writeFile("QR_CODE", "123456789", "/users/...../qrcode.png")
func (instance *BarcodeUtils) writeFile(call goja.FunctionCall) goja.Value {
	var format, data, filename string
	switch len(call.Arguments) {
	case 3:
		format = commons.GetString(call, 0)
		data = commons.GetString(call, 1)
		filename = commons.GetString(call, 2)
	default:
		panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
	}
	response, err := gg_2dcode.Generate(format, data)
	if nil != err {
		panic(instance.runtime.NewTypeError(err.Error()))
	}
	_, err = gg.IO.WriteBytesToFile(response, filename)
	if nil != err {
		panic(instance.runtime.NewTypeError(err.Error()))
	}
	return instance.runtime.ToValue(filename)
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func load(runtime *goja.Runtime, module *goja.Object, _ ...interface{}) {
	instance := &BarcodeUtils{
		runtime: runtime,
	}

	o := module.Get("exports").(*goja.Object)

	// writer
	_ = o.Set("writeBytes", instance.writeBytes)
	_ = o.Set("writeBase64", instance.writeBase64)
	_ = o.Set("writeFile", instance.writeFile)
}

func Enable(ctx *commons.RuntimeContext) {
	// register
	require.RegisterNativeModule(NAME, &commons.ModuleInfo{
		Context: ctx,
		Loader:  load,
	})
}
