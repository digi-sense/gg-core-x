package templateutils

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting/commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting/modules/defaults/require"
	"github.com/cbroglie/mustache"
	"github.com/dop251/goja"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

const NAME = "template-utils"

type TemplateUtils struct {
	runtime *goja.Runtime
}

//----------------------------------------------------------------------------------------------------------------------
//	e x p o s e d
//----------------------------------------------------------------------------------------------------------------------

// template.mergeFile(path, data)
func (instance *TemplateUtils) mergeFile(call goja.FunctionCall) goja.Value {
	if nil != instance {
		var path string
		var context map[string]interface{}
		switch len(call.Arguments) {
		case 2:
			path = commons.GetString(call, 0)
			context = commons.GetMap(call, 1)
		default:
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}
		text, err := gg.IO.ReadTextFromFile(path)
		if nil != err {
			panic(instance.runtime.NewTypeError(err))
		}
		content, err := mustache.Render(text, context)
		if nil != err {
			panic(instance.runtime.NewTypeError(err))
		}
		return instance.runtime.ToValue(content)
	}
	return goja.Undefined()
}

// template.mergeText(text, data)
func (instance *TemplateUtils) mergeText(call goja.FunctionCall) goja.Value {
	if nil != instance {
		var text string
		var context map[string]interface{}
		switch len(call.Arguments) {
		case 2:
			text = commons.GetString(call, 0)
			context = commons.GetMap(call, 1)
		default:
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}
		content, err := mustache.Render(text, context)
		if nil != err {
			panic(instance.runtime.NewTypeError(err))
		}
		return instance.runtime.ToValue(content)
	}
	return goja.Undefined()
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func load(runtime *goja.Runtime, module *goja.Object, _ ...interface{}) {
	instance := &TemplateUtils{
		runtime: runtime,
	}

	o := module.Get("exports").(*goja.Object)
	// format
	_ = o.Set("mergeFile", instance.mergeFile)
	_ = o.Set("mergeText", instance.mergeText)

}

func Enable(ctx *commons.RuntimeContext) {
	// register
	require.RegisterNativeModule(NAME, &commons.ModuleInfo{
		Context: ctx,
		Loader:  load,
	})
}
