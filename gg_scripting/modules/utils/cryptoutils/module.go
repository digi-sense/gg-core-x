package cryptoutils

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting/commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting/modules/defaults/require"
	"crypto/hmac"
	"crypto/sha256"
	"crypto/sha512"
	"errors"
	"github.com/dop251/goja"
	"hash"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

const NAME = "crypto-utils"

type CryptoUtils struct {
	runtime *goja.Runtime
}

//----------------------------------------------------------------------------------------------------------------------
//	e x p o s e d
//----------------------------------------------------------------------------------------------------------------------

// crypto.encodeBase64(data)
func (instance *CryptoUtils) encodeBase64(call goja.FunctionCall) goja.Value {
	var text string
	idata := call.Argument(0).Export()
	if data, b := idata.([]uint8); b {
		text = gg.Coding.EncodeBase64(data)
	} else if data, b := idata.([]byte); b {
		text = gg.Coding.EncodeBase64(data)
	} else if data, b := idata.(string); b {
		text = gg.Coding.EncodeBase64([]byte(data))
	} else {
		panic(instance.runtime.NewTypeError("invalid_data_type"))
	}
	return instance.runtime.ToValue(text)
}

// crypto.decodeBase64(data)
func (instance *CryptoUtils) decodeBase64(call goja.FunctionCall) goja.Value {
	text := commons.GetString(call, 0)
	data, err := gg.Coding.DecodeBase64(text)
	if nil != err {
		panic(instance.runtime.NewTypeError(err.Error()))
	}
	return instance.runtime.ToValue(data)
}

// crypto.decodeBase64ToText(data)
func (instance *CryptoUtils) decodeBase64ToText(call goja.FunctionCall) goja.Value {
	text := call.Argument(0).String()
	data, err := gg.Coding.DecodeBase64(text)
	if nil != err {
		panic(instance.runtime.NewTypeError(err.Error()))
	}
	return instance.runtime.ToValue(string(data))
}

//----------------------------------------------------------------------------------------------------------------------
//	g z i p
//----------------------------------------------------------------------------------------------------------------------

func (instance *CryptoUtils) gzip(call goja.FunctionCall) goja.Value {
	iData := call.Argument(0).Export()

	response, err := toByteArray(iData)
	if nil != err {
		response, err = gg.Zip.GZip(response) // zip
	}
	if nil != err {
		panic(instance.runtime.NewTypeError(err))
	}
	return instance.runtime.ToValue(response)
}

func (instance *CryptoUtils) gunzip(call goja.FunctionCall) goja.Value {
	iData := call.Argument(0).Export()

	response, err := toByteArray(iData)
	if nil != err {
		response, err = gg.Zip.GUnzip(response) // zip
	}
	if nil != err {
		panic(instance.runtime.NewTypeError(err))
	}
	return instance.runtime.ToValue(response)
}

//----------------------------------------------------------------------------------------------------------------------
//	w r a p p e r s
//----------------------------------------------------------------------------------------------------------------------

func (instance *CryptoUtils) wrapGZip(call goja.FunctionCall) goja.Value {
	iData := call.Argument(0).Export()
	bytes, err := wrapGzip(iData)
	if nil != err {
		panic(instance.runtime.NewTypeError(err))
	}
	return instance.runtime.ToValue(bytes)
}

func (instance *CryptoUtils) wrapGZipToText(call goja.FunctionCall) goja.Value {
	iData := call.Argument(0).Export()
	bytes, err := wrapGzip(iData)
	if nil != err {
		panic(instance.runtime.NewTypeError(err))
	}
	return instance.runtime.ToValue(string(bytes))
}

func (instance *CryptoUtils) unwrapGZip(call goja.FunctionCall) goja.Value {
	iData := call.Argument(0).Export()
	bytes, err := unwrapGzip(iData)
	if nil != err {
		panic(instance.runtime.NewTypeError(err))
	}
	return instance.runtime.ToValue(bytes)
}

func (instance *CryptoUtils) unwrapGZipToText(call goja.FunctionCall) goja.Value {
	iData := call.Argument(0).Export()
	bytes, err := unwrapGzip(iData)
	if nil != err {
		panic(instance.runtime.NewTypeError(err))
	}
	return instance.runtime.ToValue(string(bytes))
}

//----------------------------------------------------------------------------------------------------------------------
//	M D 5
//----------------------------------------------------------------------------------------------------------------------

func (instance *CryptoUtils) md5(call goja.FunctionCall) goja.Value {
	text := commons.GetString(call, 0)
	data := gg.Coding.MD5(text)
	return instance.runtime.ToValue(data)
}

//----------------------------------------------------------------------------------------------------------------------
//	H M A C
//----------------------------------------------------------------------------------------------------------------------

func (instance *CryptoUtils) encodeSha256(call goja.FunctionCall) goja.Value {
	if nil != instance {
		var secret, message string
		switch len(call.Arguments) {
		case 2:
			secret = commons.GetString(call, 0)
			message = commons.GetString(call, 1)
		default:
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}
		if len(secret) > 0 && len(message) > 0 {
			// encode
			bytes := instance.encode(sha256.New, []byte(secret), []byte(message))
			return instance.runtime.ToValue(bytes)
		} else {
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}
	}
	return goja.Undefined()
}

func (instance *CryptoUtils) encodeSha512(call goja.FunctionCall) goja.Value {
	if nil != instance {
		var secret, message string
		switch len(call.Arguments) {
		case 2:
			secret = commons.GetString(call, 0)
			message = commons.GetString(call, 1)
		default:
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}
		if len(secret) > 0 && len(message) > 0 {
			// encode
			bytes := instance.encode(sha512.New, []byte(secret), []byte(message))
			return instance.runtime.ToValue(bytes)
		} else {
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}
	}
	return goja.Undefined()
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *CryptoUtils) encode(h func() hash.Hash, secret []byte, message []byte) []byte {
	hasher := hmac.New(h, secret)
	hasher.Write(message)
	return hasher.Sum(nil)
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func wrapGzip(iData interface{}) (response []byte, err error) {
	response, err = toByteArray(iData)
	if nil == err {
		response, err = gg.Zip.GZip(response) // zip
		if nil == err {
			response = []byte(gg.Coding.EncodeBase64(response)) // encode
		}
	}
	return
}

func unwrapGzip(iData interface{}) (response []byte, err error) {
	response, err = toByteArray(iData)
	if nil == err {
		response, err = gg.Coding.DecodeBase64(string(response)) // decode
		if nil == err {
			response, err = gg.Zip.GUnzip(response) // unzip
		}
	}
	return
}

func toByteArray(iData interface{}) (response []byte, err error) {
	if data, b := iData.([]uint8); b {
		response = data
	} else if data, b := iData.([]interface{}); b {
		response = gg.Convert.ToArrayOfByte(data)
	} else if data, b := iData.([]byte); b {
		response = data
	} else if data, b := iData.(string); b {
		response = []byte(data)
	} else {
		err = errors.New("invalid_data_type")
	}
	return
}

func load(runtime *goja.Runtime, module *goja.Object, _ ...interface{}) {
	instance := &CryptoUtils{
		runtime: runtime,
	}

	o := module.Get("exports").(*goja.Object)

	// base64
	_ = o.Set("encodeBase64", instance.encodeBase64)
	_ = o.Set("decodeBase64", instance.decodeBase64)
	_ = o.Set("decodeBase64ToText", instance.decodeBase64ToText)
	// gzip
	_ = o.Set("gzip", instance.gzip)
	_ = o.Set("gunzip", instance.gunzip)
	// base64 + gzip
	_ = o.Set("wrapGZip", instance.wrapGZip)
	_ = o.Set("wrapGZipToText", instance.wrapGZipToText)
	_ = o.Set("unwrapGZip", instance.unwrapGZip)
	_ = o.Set("unwrapGZipToText", instance.unwrapGZipToText)
	// hmac
	_ = o.Set("encodeSha256", instance.encodeSha256)
	_ = o.Set("encodeSha512", instance.encodeSha512)
	// md5
	_ = o.Set("md5", instance.md5)

}

func Enable(ctx *commons.RuntimeContext) {
	// register
	require.RegisterNativeModule(NAME, &commons.ModuleInfo{
		Context: ctx,
		Loader:  load,
	})
}
