package sql

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting/commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting/modules/defaults/require"
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting/modules/sql/helpers"
	"github.com/dop251/goja"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

const NAME = "sql"

type ModuleSql struct {
	runtime *goja.Runtime
}

//----------------------------------------------------------------------------------------------------------------------
//	e x p o s e d
//----------------------------------------------------------------------------------------------------------------------

// sql.open(driverName, dataSourceName)
func (instance *ModuleSql) open(call goja.FunctionCall) goja.Value {
	driverName := call.Argument(0).String()
	dataSourceName := call.Argument(1).String()
	if len(driverName) > 0 && len(dataSourceName) > 0 {
		db, err := helpers.NewDatabase(driverName, dataSourceName)
		if nil != err {
			// throw back error to javascript
			panic(instance.runtime.NewTypeError(err.Error()))
		}
		if nil != db {
			return instance.runtime.ToValue(Wrap(instance.runtime, db))
		}
	}
	return goja.Undefined()
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func load(runtime *goja.Runtime, module *goja.Object, _ ...interface{}) {
	instance := &ModuleSql{
		runtime: runtime,
	}

	o := module.Get("exports").(*goja.Object)
	_ = o.Set("open", instance.open)

}

func Enable(ctx *commons.RuntimeContext) {
	// register
	require.RegisterNativeModule(NAME, &commons.ModuleInfo{
		Context: ctx,
		Loader:  load,
	})
}
