package message_queue

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_mq"
	"bitbucket.org/digi-sense/gg-core-x/gg_mq/mq_commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting/commons"
	"errors"
	"github.com/dop251/goja"
	"sync"
	"time"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type JsMqConnection struct {
	runtime    *goja.Runtime
	object     *goja.Object
	config     interface{}
	connection mq_commons.IDriver
	mux        sync.Mutex
}

//----------------------------------------------------------------------------------------------------------------------
//	JsShowcaseEngine
//----------------------------------------------------------------------------------------------------------------------

func WrapConnection(runtime *goja.Runtime, config interface{}) goja.Value {
	instance := new(JsMqConnection)
	instance.runtime = runtime
	instance.config = config

	instance.object = instance.export(instance.runtime.NewObject())

	// add closable: all closable objects must be exposed to avoid
	commons.AddClosableObject(instance.runtime, instance.object)

	return instance.value()
}

func (instance *JsMqConnection) value() goja.Value {
	if nil != instance.object {
		return instance.object
	}
	return goja.Undefined()
}

//----------------------------------------------------------------------------------------------------------------------
//	e x p o s e d
//----------------------------------------------------------------------------------------------------------------------

func (instance *JsMqConnection) close(_ goja.FunctionCall) goja.Value {
	if nil != instance && nil != instance.connection {

	}
	return goja.Undefined()
}

func (instance *JsMqConnection) ping(_ goja.FunctionCall) goja.Value {
	if nil != instance {
		err := instance.init()
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}

		_, err = instance.connection.Ping()
		response := nil == err
		return instance.runtime.ToValue(response)
	}
	return goja.Undefined()
}

func (instance *JsMqConnection) queueDeclare(call goja.FunctionCall) goja.Value {
	if nil != instance {
		err := instance.init()
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}

		var settings interface{}
		switch len(call.Arguments) {
		case 1:
			settings = commons.GetExport(call, 0)
		default:
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}
		response, err := instance.connection.QueueDeclare(settings)
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}
		return instance.runtime.ToValue(response)
	}
	return goja.Undefined()
}

func (instance *JsMqConnection) queueDelete(call goja.FunctionCall) goja.Value {
	if nil != instance {
		err := instance.init()
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}

		var name string
		var ifUnused, ifEmpty, noWait bool
		switch len(call.Arguments) {
		case 1:
			name = commons.GetString(call, 0)
		case 2:
			name = commons.GetString(call, 0)
			ifUnused = commons.GetBool(call, 1)
		case 3:
			name = commons.GetString(call, 0)
			ifUnused = commons.GetBool(call, 1)
			ifEmpty = commons.GetBool(call, 2)
		case 4:
			name = commons.GetString(call, 0)
			ifUnused = commons.GetBool(call, 1)
			ifEmpty = commons.GetBool(call, 2)
			noWait = commons.GetBool(call, 3)
		default:
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}
		response, err := instance.connection.QueueDelete(name, ifUnused, ifEmpty, noWait)
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}
		return instance.runtime.ToValue(response)
	}
	return goja.Undefined()
}

func (instance *JsMqConnection) queuePurge(call goja.FunctionCall) goja.Value {
	if nil != instance {
		err := instance.init()
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}

		var name string
		var noWait bool
		switch len(call.Arguments) {
		case 1:
			name = commons.GetString(call, 0)
		case 2:
			name = commons.GetString(call, 0)
			noWait = commons.GetBool(call, 1)
		default:
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}
		response, err := instance.connection.QueuePurge(name, noWait)
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}
		return instance.runtime.ToValue(response)
	}
	return goja.Undefined()
}

func (instance *JsMqConnection) queueInspect(call goja.FunctionCall) goja.Value {
	if nil != instance {
		err := instance.init()
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}

		var name string
		switch len(call.Arguments) {
		case 1:
			name = commons.GetString(call, 0)
		default:
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}
		response, err := instance.connection.QueueInspect(name)
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}
		return instance.runtime.ToValue(response)
	}
	return goja.Undefined()
}

func (instance *JsMqConnection) queueBind(call goja.FunctionCall) goja.Value {
	if nil != instance {
		err := instance.init()
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}

		var settings interface{}
		switch len(call.Arguments) {
		case 1:
			settings = commons.GetExport(call, 0)
		default:
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}
		err = instance.connection.QueueBind(settings)
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}
	}
	return goja.Undefined()
}

func (instance *JsMqConnection) queueUnbind(call goja.FunctionCall) goja.Value {
	if nil != instance {
		err := instance.init()
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}

		var settings interface{}
		switch len(call.Arguments) {
		case 1:
			settings = commons.GetExport(call, 0)
		default:
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}
		err = instance.connection.QueueUnbind(settings)
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}
	}
	return goja.Undefined()
}

func (instance *JsMqConnection) exchangeDeclare(call goja.FunctionCall) goja.Value {
	if nil != instance {
		err := instance.init()
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}

		var settings interface{}
		switch len(call.Arguments) {
		case 1:
			settings = commons.GetExport(call, 0)
		default:
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}
		err = instance.connection.ExchangeDeclare(settings)
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}
	}
	return goja.Undefined()
}

func (instance *JsMqConnection) exchangeDelete(call goja.FunctionCall) goja.Value {
	if nil != instance {
		err := instance.init()
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}

		var name string
		var ifUnused, noWait bool
		switch len(call.Arguments) {
		case 1:
			name = commons.GetString(call, 0)
		case 2:
			name = commons.GetString(call, 0)
			ifUnused = commons.GetBool(call, 1)
		case 3:
			name = commons.GetString(call, 0)
			ifUnused = commons.GetBool(call, 1)
			noWait = commons.GetBool(call, 2)
		default:
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}
		err = instance.connection.ExchangeDelete(name, ifUnused, noWait)
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}
	}
	return goja.Undefined()
}

func (instance *JsMqConnection) exchangeBind(call goja.FunctionCall) goja.Value {
	if nil != instance {
		err := instance.init()
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}

		var settings interface{}
		switch len(call.Arguments) {
		case 1:
			settings = commons.GetExport(call, 0)
		default:
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}
		err = instance.connection.ExchangeBind(settings)
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}
	}
	return goja.Undefined()
}

func (instance *JsMqConnection) exchangeUnbind(call goja.FunctionCall) goja.Value {
	if nil != instance {
		err := instance.init()
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}

		var settings interface{}
		switch len(call.Arguments) {
		case 1:
			settings = commons.GetExport(call, 0)
		default:
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}
		err = instance.connection.ExchangeUnbind(settings)
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}
	}
	return goja.Undefined()
}

func (instance *JsMqConnection) newEmitter(call goja.FunctionCall) goja.Value {
	if nil != instance {
		err := instance.init()
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}

		var settings interface{}
		switch len(call.Arguments) {
		case 1:
			settings = commons.GetExport(call, 0)
		default:
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}
		emitter, err := instance.connection.NewEmitter(settings)
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}
		return WrapEmitter(instance.runtime, emitter)
	}
	return goja.Undefined()
}

func (instance *JsMqConnection) newListener(call goja.FunctionCall) goja.Value {
	if nil != instance {
		err := instance.init()
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}

		var settings interface{}
		switch len(call.Arguments) {
		case 1:
			settings = commons.GetExport(call, 0)
		default:
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}
		listener, err := instance.connection.NewListener(settings)
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}
		return WrapListener(instance.runtime, listener, &instance.mux)
	}
	return goja.Undefined()
}

// rpc Send and RPC call to server
// usage: connection.rpc("rpc_channel", function (message){})
// usage: connection.rpc("rpc_channel", function (message){}, 5)
// usage: connection.rpc("rpc_channel", {"auto-ack": true}, {"key": "rpc_channel"}, function (message){}, 5)
func (instance *JsMqConnection) rpc(call goja.FunctionCall) goja.Value {
	// PANIC RECOVERY
	defer func() {
		if r := recover(); r != nil {
			// recovered from panic
		}
	}()
	if nil != instance {
		err := instance.init()
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}

		var emitterSettings, listenerSettings, rawMessage interface{}
		var callback goja.Callable
		var rpcChannel string
		timeout := 3 * time.Second
		switch len(call.Arguments) {
		case 2:
			rpcChannel = commons.GetString(call, 0)
			emitterSettings = nil
			listenerSettings = nil
			callback = commons.GetCallbackIfAny(call)
		case 3:
			rpcChannel = commons.GetString(call, 0)
			emitterSettings = nil
			listenerSettings = nil
			callback = commons.GetCallbackIfAny(call)
			t := commons.GetInt(call, 2)
			timeout = time.Duration(t) * time.Second
		case 5:
			rpcChannel = commons.GetString(call, 0)
			emitterSettings = commons.GetExport(call, 1)
			listenerSettings = commons.GetExport(call, 2)
			callback = commons.GetCallbackIfAny(call)
			t := commons.GetInt(call, 4)
			timeout = time.Duration(t) * time.Second
		default:
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}
		if nil == callback {
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}

		err = instance.connection.RpcCommand(rpcChannel, emitterSettings, listenerSettings, rawMessage, func(message map[string]interface{}) {
			if nil != callback {
				_, _ = callback(call.This, instance.runtime.ToValue(message))
			}
		}, timeout)
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}

	}
	return goja.Undefined()
}

func (instance *JsMqConnection) notifyClose(call goja.FunctionCall) goja.Value {
	if nil != instance {
		err := instance.init()
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}

		var callback goja.Callable
		switch len(call.Arguments) {
		case 1:
			callback = commons.GetCallbackIfAny(call)
		default:
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}
		if nil == callback {
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}

		instance.connection.NotifyDisconnection(func() {
			if nil != callback {
				_, _ = callback(call.This)
			}
		})
	}
	return goja.Undefined()
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *JsMqConnection) init() (err error) {
	if nil == instance.connection {
		if nil != instance.config {
			instance.connection, err = gg_mq.MQ.Build(instance.config)
		} else {
			return errors.New("missing_configuration")
		}
	}
	return err
}

func (instance *JsMqConnection) export(o *goja.Object) *goja.Object {
	_ = o.Set("close", instance.close)
	_ = o.Set("ping", instance.ping)
	_ = o.Set("rpc", instance.rpc)
	_ = o.Set("notifyClose", instance.notifyClose)
	// queue
	_ = o.Set("queueDeclare", instance.queueDeclare)
	_ = o.Set("queueDelete", instance.queueDelete)
	_ = o.Set("queuePurge", instance.queuePurge)
	_ = o.Set("queueInspect", instance.queueInspect)
	_ = o.Set("queueBind", instance.queueBind)
	_ = o.Set("queueUnbind", instance.queueUnbind)
	// exchange
	_ = o.Set("exchangeDeclare", instance.exchangeDeclare)
	_ = o.Set("exchangeDelete", instance.exchangeDelete)
	_ = o.Set("exchangeBind", instance.exchangeBind)
	_ = o.Set("exchangeUnbind", instance.exchangeUnbind)
	// listener and emitter
	_ = o.Set("newListener", instance.newListener)
	_ = o.Set("newEmitter", instance.newEmitter)

	return o
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------
