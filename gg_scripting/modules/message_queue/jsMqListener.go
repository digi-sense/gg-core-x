package message_queue

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_mq/mq_commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting/commons"
	"github.com/dop251/goja"
	"sync"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type JsMqListener struct {
	runtime  *goja.Runtime
	object   *goja.Object
	config   interface{}
	listener mq_commons.IListener
	mux      *sync.Mutex
}

//----------------------------------------------------------------------------------------------------------------------
//	JsShowcaseEngine
//----------------------------------------------------------------------------------------------------------------------

func WrapListener(runtime *goja.Runtime, listener mq_commons.IListener, mux *sync.Mutex) goja.Value {
	instance := new(JsMqListener)
	instance.runtime = runtime
	instance.listener = listener
	instance.mux = mux

	instance.object = instance.runtime.NewObject()
	instance.export()

	// add closable: all closable objects must be exposed to avoid
	commons.AddClosableObject(instance.runtime, instance.object)

	return instance.value()
}

//----------------------------------------------------------------------------------------------------------------------
//	e x p o s e d
//----------------------------------------------------------------------------------------------------------------------

func (instance *JsMqListener) close(_ goja.FunctionCall) goja.Value {
	if nil != instance && nil != instance.listener {
		_ = instance.listener.Close()
	}
	return goja.Undefined()
}

func (instance *JsMqListener) listen(call goja.FunctionCall) goja.Value {
	// PANIC RECOVERY
	defer func() {
		if r := recover(); r != nil {
			// recovered from panic
		}
	}()
	if nil != instance && nil != instance.listener {
		var callback goja.Callable
		switch len(call.Arguments) {
		case 1:
			callback = commons.GetCallbackIfAny(call)
		default:
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}

		err := instance.listener.Listen(func(message map[string]interface{}) {
			if nil != instance && nil != instance.listener && nil != instance.mux && nil != instance.runtime && nil != message && nil != callback {
				instance.mux.Lock()
				_, _ = callback(call.This, instance.runtime.ToValue(message))
				instance.mux.Unlock()
			}
		})
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}
	}
	return goja.Undefined()
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *JsMqListener) value() goja.Value {
	if nil != instance.object {
		return instance.object
	}
	return goja.Undefined()
}

func (instance *JsMqListener) export() {
	o := instance.object

	_ = o.Set("close", instance.close)
	_ = o.Set("listen", instance.listen)

}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------
