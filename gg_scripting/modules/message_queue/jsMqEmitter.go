package message_queue

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_mq/mq_commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting/commons"
	"github.com/dop251/goja"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type JsMqEmitter struct {
	runtime *goja.Runtime
	object  *goja.Object
	emitter mq_commons.IEmitter
}

//----------------------------------------------------------------------------------------------------------------------
//	JsShowcaseEngine
//----------------------------------------------------------------------------------------------------------------------

func WrapEmitter(runtime *goja.Runtime, emitter mq_commons.IEmitter) goja.Value {
	instance := new(JsMqEmitter)
	instance.runtime = runtime
	instance.emitter = emitter

	instance.object = instance.runtime.NewObject()
	instance.export()

	// add closable: all closable objects must be exposed to avoid
	commons.AddClosableObject(instance.runtime, instance.object)

	return instance.value()
}

//----------------------------------------------------------------------------------------------------------------------
//	e x p o s e d
//----------------------------------------------------------------------------------------------------------------------

func (instance *JsMqEmitter) close(_ goja.FunctionCall) goja.Value {
	if nil != instance && nil != instance.emitter {
		_ = instance.emitter.Close()
	}
	return goja.Undefined()
}

func (instance *JsMqEmitter) emit(call goja.FunctionCall) goja.Value {
	if nil != instance && nil != instance.emitter {
		var message interface{}
		switch len(call.Arguments) {
		case 1:
			message = commons.GetExport(call, 0)
		default:
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}
		// convert body
		if m, b := message.(map[string]interface{}); b {
			if v, b := m["body"]; b {
				m["body"] = gg.Convert.ToString(v)
			}
		}
		err := instance.emitter.Emit(message)
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}
	}
	return goja.Undefined()
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *JsMqEmitter) value() goja.Value {
	if nil != instance.object {
		return instance.object
	}
	return goja.Undefined()
}

func (instance *JsMqEmitter) export() {
	o := instance.object

	_ = o.Set("close", instance.close)
	_ = o.Set("emit", instance.emit)
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------
