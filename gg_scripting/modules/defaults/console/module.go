package console

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting/commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting/modules/defaults/require"
	"bitbucket.org/digi-sense/gg-core/gg_log"
	"errors"
	"fmt"
	"github.com/dop251/goja"
	"sync"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

const NAME = "console"

type Console struct {
	runtime   *goja.Runtime
	util      *goja.Object
	name      string
	filename  string
	logger    gg_log.ILogger
	isVerbose bool // enable/disable console output
	mux       sync.Mutex
}

//----------------------------------------------------------------------------------------------------------------------
//	e x p o s e d
//----------------------------------------------------------------------------------------------------------------------

func (instance *Console) close(_ goja.FunctionCall) goja.Value {
	if nil != instance {
		instance.logger.Debug("-> javascript console closed.")
	}
	return goja.Undefined()
}

func (instance *Console) reset(_ goja.FunctionCall) goja.Value {
	if nil != instance {
		_ = gg.IO.Remove(instance.filename)
		instance.logger.Debug("-> javascript console reset.")
	}
	return goja.Undefined()
}

func (instance *Console) verbose(call goja.FunctionCall) goja.Value {
	if nil != instance {
		instance.isVerbose = commons.GetBool(call, 0)
		return instance.runtime.ToValue(instance.isVerbose)
	}
	return goja.Undefined()
}

func (instance *Console) log(call goja.FunctionCall) goja.Value {
	if message, err := instance.format(call); nil == err {
		instance.write(gg_log.InfoLevel, message)
	} else {
		panic(instance.runtime.NewTypeError(err.Error()))
	}
	return nil
}

func (instance *Console) error(call goja.FunctionCall) goja.Value {
	if message, err := instance.format(call); nil == err {
		instance.write(gg_log.ErrorLevel, message)
	} else {
		panic(instance.runtime.NewTypeError(err.Error()))
	}
	return nil
}

func (instance *Console) warn(call goja.FunctionCall) goja.Value {
	if message, err := instance.format(call); nil == err {
		instance.write(gg_log.WarnLevel, message)
	} else {
		panic(instance.runtime.NewTypeError(err.Error()))
	}
	return nil
}

func (instance *Console) info(call goja.FunctionCall) goja.Value {
	if message, err := instance.format(call); nil == err {
		instance.write(gg_log.InfoLevel, message)
	} else {
		panic(instance.runtime.NewTypeError(err.Error()))
	}
	return nil
}

func (instance *Console) debug(call goja.FunctionCall) goja.Value {
	if message, err := instance.format(call); nil == err {
		instance.write(gg_log.DebugLevel, message)
	} else {
		panic(instance.runtime.NewTypeError(err.Error()))
	}
	return nil
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *Console) format(call goja.FunctionCall) (string, error) {
	if format, ok := goja.AssertFunction(instance.util.Get("format")); ok {
		ret, err := format(instance.util, call.Arguments...)
		if err != nil {
			return "", err
		}
		message := ret.String()

		return message, nil
	} else {
		return "", errors.New("util.format is not a function")
	}
}

func (instance *Console) write(level gg_log.Level, message string) {
	instance.mux.Lock()
	defer instance.mux.Unlock()

	// PANIC RECOVERY
	defer func() {
		if r := recover(); r != nil {
			// recovered from panic
			message := gg.Strings.Format("[panic] MODULE %s ERROR: %s", NAME, r)
			fmt.Println(message)
		}
	}()

	if nil != instance.logger {
		switch level {
		case gg_log.WarnLevel:
			instance.logger.Warn(message)
		case gg_log.InfoLevel:
			instance.logger.Info(message)
		case gg_log.ErrorLevel:
			instance.logger.Error(message)
		case gg_log.DebugLevel:
			instance.logger.Debug(message)
		case gg_log.TraceLevel:
			instance.logger.Trace(message)
		case gg_log.PanicLevel:
			instance.logger.Panic(message)
		}
	} else {
		if len(instance.filename) > 0 {
			if b, _ := gg.Paths.Exists(instance.filename); !b {
				_ = gg.Paths.Mkdir(instance.filename)
			}
			_, _ = gg.IO.AppendTextToFile(message+"\n", instance.filename)
		}
	}

	if instance.isVerbose {
		fmt.Println("["+instance.name+"] ", message)
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func load(runtime *goja.Runtime, module *goja.Object, args ...interface{}) {
	instance := &Console{
		runtime: runtime,
	}
	instance.util = require.Require(runtime, "util").(*goja.Object)
	instance.isVerbose = false // disable console output

	if len(args) > 3 {
		root := gg.Reflect.ValueOf(args[0]).String()
		name := gg.Reflect.ValueOf(args[1]).String()
		verbose := gg.Reflect.ValueOf(args[2]).Bool()
		level := gg.Reflect.ValueOf(args[3]).Interface()
		resetLog := gg.Reflect.ValueOf(args[4]).Bool()
		getEngine := args[5]
		filename := gg.Reflect.ValueOf(args[6]).String()

		if len(name) > 0 && len(root) > 0 {
			instance.name = name
			instance.isVerbose = verbose
			if f, b := getEngine.(func() gg_log.ILogger); b {
				instance.logger = f()
				if l, b := instance.logger.(*gg_log.Logger); b {
					instance.filename = l.GetFilename()
				}
			}
			if nil == instance.logger {
				if len(filename) > 0 {
					instance.filename = filename
				} else {
					instance.filename = gg.Paths.Concat(gg.Paths.Absolute(root), "logging", name+".log")
				}
				// reset log
				if resetLog {
					_ = gg.IO.Remove(instance.filename)
				}
				logger := gg_log.NewLogger()
				logger.SetFilename(instance.filename)
				logger.SetLevel(level)
				instance.logger = logger
			}
		}
	}

	o := module.Get("exports").(*goja.Object)

	//_ = o.Set("open", instance.open)
	_ = o.Set("close", instance.close) // close console log file
	_ = o.Set("reset", instance.reset) // remove console file
	_ = o.Set("verbose", instance.verbose)

	_ = o.Set("log", instance.log)
	_ = o.Set("error", instance.error)
	_ = o.Set("warn", instance.warn)
	_ = o.Set("debug", instance.debug)
	_ = o.Set("info", instance.info)

	commons.AddClosableObject(instance.runtime, o)
}

func Enable(ctx *commons.RuntimeContext) {
	// register
	require.RegisterNativeModule(NAME, &commons.ModuleInfo{
		Context: ctx,
		Loader:  load,
	})

	// add module to javascript context
	_ = ctx.Runtime.Set(NAME, require.Require(ctx.Runtime, NAME))
}
