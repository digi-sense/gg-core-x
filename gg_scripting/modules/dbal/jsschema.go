package dbal

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_dbal/gg_dbal_drivers"
	"bitbucket.org/digi-sense/gg-core-x/gg_dbal/gg_dbal_drivers/dbschema"
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting/commons"
	"fmt"
	"github.com/dop251/goja"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type JsDbalSchema struct {
	database gg_dbal_drivers.IDatabase
	runtime  *goja.Runtime
}

//----------------------------------------------------------------------------------------------------------------------
//	JsDbalSchema
//----------------------------------------------------------------------------------------------------------------------

func WrapDbalSchema(runtime *goja.Runtime, database gg_dbal_drivers.IDatabase) goja.Value {
	instance := new(JsDbalSchema)
	instance.runtime = runtime
	instance.database = database
	return instance.export(instance.runtime.NewObject())
}

//----------------------------------------------------------------------------------------------------------------------
//	e x p o r t s
//----------------------------------------------------------------------------------------------------------------------

// enabled return true if current SQL database support schema "auto-migration"
func (instance *JsDbalSchema) enabled(_ goja.FunctionCall) goja.Value {
	if nil != instance && nil != instance.runtime {
		return instance.runtime.ToValue(instance.isEnabled())
	}
	return goja.Undefined()
}

func (instance *JsDbalSchema) get(_ goja.FunctionCall) goja.Value {
	if nil != instance {
		schema, err := instance.schema()
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}
		return instance.runtime.ToValue(schema.Map())
	}
	return goja.Undefined()
}

func (instance *JsDbalSchema) save(call goja.FunctionCall) goja.Value {
	if nil != instance {
		driverName := instance.database.DriverName()
		if instance.isEnabled() {
			// get filename parameter
			filename := commons.GetString(call, 0)
			if len(filename) == 0 {
				panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
			}

			// get the driver
			gormDriver := instance.getGormDriver()
			if nil != gormDriver {
				err := gormDriver.SaveSchemaToFile(filename)
				if nil != err {
					panic(instance.runtime.NewTypeError(err.Error()))
				}
				return instance.runtime.ToValue(filename)
			}

		}
		// error
		panic(instance.runtime.NewTypeError(
			fmt.Sprintf("'schema.save()' function is not supported from this driver: '%s'. Only 'gorm' driver ('gorm:mysql', 'gorm:sqlite', ...) supports schema.save() action", driverName),
		))
	}
	return goja.Undefined()
}

func (instance *JsDbalSchema) update(call goja.FunctionCall) goja.Value {
	if nil != instance {
		var err error
		if instance.isEnabled() {
			// get json data or filename to load
			data := commons.GetString(call, 0)
			if len(data) == 0 {
				panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
			}

			// load data from file
			if !gg.Regex.IsValidJsonObject(data) {
				filename := gg.Paths.Absolute(data)
				data, err = gg.IO.ReadTextFromFile(filename)
				if nil != err {
					panic(instance.runtime.NewTypeError(err.Error()))
				}
			}

			var schema *dbschema.DbSchema
			err = gg.JSON.Read(data, &schema)
			if nil != err {
				panic(instance.runtime.NewTypeError(err.Error()))
			}

			// get the driver
			gormDriver := instance.getGormDriver()
			if nil != gormDriver {
				err = gormDriver.AutomigrateSchema(schema)
				if nil != err {
					panic(instance.runtime.NewTypeError(err.Error()))
				}
				return instance.runtime.ToValue(true) // success
			}

		}
		// error
		panic(instance.runtime.NewTypeError("'schema.save()' function is not supported from this driver!"))
	}
	return goja.Undefined()
}

func (instance *JsDbalSchema) hasTable(call goja.FunctionCall) goja.Value {
	if nil != instance {
		tableName := commons.GetString(call, 0)
		if len(tableName) == 0 {
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}

		table, err := instance.getSchemaTable(tableName)
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}
		return instance.runtime.ToValue(nil != table) // success
	}
	return goja.Undefined()
}

func (instance *JsDbalSchema) getTable(call goja.FunctionCall) goja.Value {
	if nil != instance {
		tableName := commons.GetString(call, 0)
		if len(tableName) == 0 {
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}

		table, err := instance.getSchemaTable(tableName)
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}
		return instance.runtime.ToValue(table.Map()) // success
	}
	return goja.Undefined()
}

func (instance *JsDbalSchema) getColumn(call goja.FunctionCall) goja.Value {
	if nil != instance {
		tableName := commons.GetString(call, 0)
		if len(tableName) == 0 {
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}
		columnName := commons.GetString(call, 1)
		if len(columnName) == 0 {
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}

		column, err := instance.getSchemaTableColumn(tableName, columnName)
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}
		return instance.runtime.ToValue(column.Map()) // success
	}
	return goja.Undefined()
}

func (instance *JsDbalSchema) addColumn(call goja.FunctionCall) goja.Value {
	if nil != instance {
		tableName := commons.GetString(call, 0)
		if len(tableName) == 0 {
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}
		columnMap := commons.GetMap(call, 1)
		if len(columnMap) == 0 {
			panic(instance.runtime.NewTypeError(commons.ErrorMissingParam))
		}

		success, err := instance.addSchemaTableColumn(tableName, columnMap)
		if nil != err {
			panic(instance.runtime.NewTypeError(err.Error()))
		}
		return instance.runtime.ToValue(success) // success
	}
	return goja.Undefined()
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *JsDbalSchema) isEnabled() bool {
	if nil != instance && nil != instance.database && nil != instance.runtime {
		driver := instance.database.Driver()
		if _, ok := driver.(*gg_dbal_drivers.DriverGorm); ok {
			return ok // GORM support schema migration
		}
	}
	return false
}

func (instance *JsDbalSchema) getGormDriver() *gg_dbal_drivers.DriverGorm {
	if nil != instance && nil != instance.database && nil != instance.runtime {
		if driver, ok := instance.database.Driver().(*gg_dbal_drivers.DriverGorm); ok {
			return driver
		}
	}
	return nil
}

func (instance *JsDbalSchema) schema() (schema *dbschema.DbSchema, err error) {
	if instance.isEnabled() {
		// get the driver
		gormDriver := instance.getGormDriver()
		if nil != gormDriver {
			schema, err = gormDriver.GetSchema()
		}
	} else {
		err = gg.Errors.Prefix(gg.PanicSystemError, "Schema is not supported by this driver: ")
	}
	return
}

func (instance *JsDbalSchema) getSchemaTable(name string) (*dbschema.DbSchemaTable, error) {
	if nil != instance {
		schema, err := instance.schema()
		if nil != err {
			return nil, err
		}
		return schema.Get(name), nil
	}
	return nil, nil
}

func (instance *JsDbalSchema) getSchemaTableColumn(tableName, columnName string) (column *dbschema.DbSchemaColumn, err error) {
	if nil != instance {
		var schema *dbschema.DbSchema
		schema, err = instance.schema()
		if nil != err {
			return
		}

		// get table
		var table *dbschema.DbSchemaTable
		table = schema.Get(tableName)
		if nil != table {
			column = table.Get(columnName)
		} else {
			// missing table
			err = gg.Errors.Prefix(gg.PanicSystemError, fmt.Sprintf("Missing table '%s': ", tableName))
		}
	}
	return
}

func (instance *JsDbalSchema) addSchemaTableColumn(tableName string, columnMap map[string]interface{}) (response bool, err error) {
	if nil != instance {
		var schema *dbschema.DbSchema
		schema, err = instance.schema()
		if nil != err {
			return
		}

		// get table
		var table *dbschema.DbSchemaTable
		table = schema.Get(tableName)
		if nil != table {
			column := new(dbschema.DbSchemaColumn)
			err = column.Parse(columnMap)
			if nil != err {
				err = gg.Errors.Prefix(err, fmt.Sprintf("Error adding column '%v': ", columnMap))
				return
			}
			// add the column
			table.Put(column)
			gormDriver := instance.getGormDriver()
			if nil != gormDriver {
				err = gormDriver.AutomigrateSchema(schema)
				response = nil == err
			}
		} else {
			// missing table
			err = gg.Errors.Prefix(gg.PanicSystemError, fmt.Sprintf("Missing table '%s': ", tableName))
		}
	}
	return
}

func (instance *JsDbalSchema) export(o *goja.Object) *goja.Object {
	_ = o.Set("enabled", instance.enabled)
	_ = o.Set("save", instance.save)
	_ = o.Set("update", instance.update)
	_ = o.Set("get", instance.get)
	_ = o.Set("hasTable", instance.hasTable)
	_ = o.Set("getTable", instance.getTable)
	_ = o.Set("getColumn", instance.getColumn)
	_ = o.Set("addColumn", instance.addColumn)

	return o
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------
