package gg_scripting

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"fmt"
	"github.com/dop251/goja"
	"strings"
)

//----------------------------------------------------------------------------------------------------------------------
//	ProgramMethod
//----------------------------------------------------------------------------------------------------------------------

type ProgramMethod struct {
	Name      string
	Arguments []interface{}
}

func NewProgramMethod(name string, arguments ...interface{}) *ProgramMethod {
	method := new(ProgramMethod)
	method.Name = name
	method.Arguments = make([]interface{}, 0)
	method.Arguments = append(method.Arguments, arguments...)

	return method
}

func (instance *ProgramMethod) Add(arguments ...interface{}) *ProgramMethod {
	if nil != instance && len(arguments) > 0 {
		instance.Arguments = append(instance.Arguments, arguments...)
	}
	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	Program
//----------------------------------------------------------------------------------------------------------------------

type Program struct {
	env *EnvSettings

	_vm *ScriptEngine // late initialized
}

func NewProgram(env *EnvSettings) (instance *Program, err error) {
	err = validateEnv(env, nil)
	if nil != err {
		return
	}

	instance = new(Program)
	instance.env = env

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *Program) Close() {
	if nil != instance && nil != instance._vm {
		instance._vm.Close()
		instance._vm = nil
	}
}

func (instance *Program) HasLogger() bool {
	if nil != instance && nil != instance.env {
		return instance.env.Logger != nil
	}
	return false
}

func (instance *Program) Logger(logger ...gg_.ILogger) gg_.ILogger {
	if nil != instance && nil != instance.env {
		if len(logger) == 1 {
			instance.env.Logger = logger[0]
		}
		return instance.env.Logger
	}
	return nil
}

// Execute run the program and return response as a string
func (instance *Program) Execute() (response string, err error) {
	if nil != instance && nil != instance.env {
		resp, e := instance.Run()
		if nil != e {
			err = e
			return
		}
		if nil != resp {
			response = gg_utils.Convert.ToString(resp)
		}
	}
	return
}

// Run run the program and invoke a specific method passing params
func (instance *Program) Run(methods ...*ProgramMethod) (response interface{}, err error) {
	if nil != instance && nil != instance.env {
		response, err = instance.run(false, methods...)
	}
	return
}

func (instance *Program) RunSequence(methods ...*ProgramMethod) (response interface{}, err error) {
	if nil != instance && nil != instance.env {
		response, err = instance.run(true, methods...)
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *Program) vm() *ScriptEngine {
	if nil != instance && nil != instance.env {
		if nil == instance._vm {
			// create engine
			instance._vm = newEngine(instance.env.EngineName)
			instance._vm.Name = instance.env.ProgramName
			// logger
			if nil != instance.env.Logger {
				instance._vm.SetLogger(instance.env.Logger)
				instance._vm.AllowMultipleLoggers = false
			} else {
				if len(instance.env.LoggerFileName) > 0 {
					instance._vm.LogFile = instance.env.LoggerFileName
				} else {
					instance._vm.LogFile = gg.Paths.Concat(instance.env.Root, instance.env.ProgramName+".log")
				}
			}
			instance._vm.ResetLogOnEachRun = instance.env.LogReset
			// context
			if nil != instance.env.Context {
				for k, v := range instance.env.Context {
					// add prefix
					if strings.Index(k, "$") == -1 {
						k = "$" + k
					}
					instance._vm.Set(k, v)
				}
			}

			// registry
			registry := NewModuleRegistry(Loader)
			rtContext := registry.Start(instance._vm)
			if nil != rtContext && nil != instance.env.OnReady {
				// here we can add modules to runtime context
				instance.env.OnReady(rtContext) // usage: "console.Enable(rtContext)"
			}
		}
	}
	return instance._vm
}

func (instance *Program) run(chainArgs bool, methods ...*ProgramMethod) (response interface{}, err error) {
	if nil != instance && nil != instance.env {
		defer instance.Close()
		vm := instance.vm()
		if nil != vm {
			v, e := vm.RunProgram(instance.env.ProgramName, instance.env.ProgramScript)
			if nil != e {
				err = e
				return
			}

			if len(methods) > 0 {
				for _, m := range methods {
					callable := vm.GetMemberAsCallable(v, m.Name)
					if nil == callable {
						err = gg.Errors.Prefix(gg.PanicSystemError, fmt.Sprintf("Method '%s' do not exists: ", m.Name))
					} else {
						arguments := make([]goja.Value, 0)
						if nil != response && chainArgs {
							// previous response appended to
							arguments = append(arguments, vm.Runtime().ToValue(response))
						}
						arguments = append(arguments, toValues(vm.Runtime(), m.Arguments...)...)
						cr, ce := callable(nil, arguments...)
						if nil != ce {
							err = ce
							return
						}
						response = cr.Export() // set last response
					}
				}
			} else {
				response = v.Export() // set the response
			}
		}
	}
	return
}
