package gg_scripting

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting/commons"
	"bitbucket.org/digi-sense/gg-core/gg_log"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"github.com/dop251/goja"
)

var _js *goja.Runtime

type OnReadyCallback func(rtContext *commons.RuntimeContext)
type EnvSettings struct {
	Root           string
	EngineName     string
	FileName       string
	ProgramName    string
	ProgramScript  string
	Context        map[string]interface{}
	Logger         gg_log.ILogger
	LoggerFileName string
	LogReset       bool
	OnReady        OnReadyCallback
}

type ScriptingHelper struct {
	logger gg_log.ILogger
}

var Scripting *ScriptingHelper

func init() {
	Scripting = new(ScriptingHelper)
}

func (instance *ScriptingHelper) SetLogger(logger gg_log.ILogger) {
	instance.logger = logger
}

func (instance *ScriptingHelper) EvalJs(expression string, useGlobalRuntime, overwriteContext bool, context ...map[string]interface{}) (interface{}, error) {
	if len(expression) > 0 {
		var runtime *goja.Runtime
		if useGlobalRuntime {
			runtime = js()

		} else {
			runtime = goja.New()
		}
		for _, ctx := range context {
			for k, v := range ctx {
				if overwriteContext || nil == runtime.Get(k) {
					_ = runtime.Set(k, v)
				}
			}
		}
		value, err := runtime.RunString(expression)
		if nil != err {
			return nil, err
		}
		return value.Export(), err
	}
	return nil, nil
}

func (instance *ScriptingHelper) NewEngine(name ...interface{}) *ScriptEngine {
	return newEngine(name...)
}

func (instance *ScriptingHelper) ValidateEnv(env *EnvSettings) error {
	// check env validity
	return validateEnv(env, instance.logger)
}

func (instance *ScriptingHelper) Build(env *EnvSettings) (response *Program, err error) {
	err = instance.ValidateEnv(env)
	if nil != err {
		return
	}
	response, err = NewProgram(env)
	return
}

func (instance *ScriptingHelper) RunFile(filename string, context map[string]interface{}, args ...interface{}) (response string, err error) {
	env := new(EnvSettings)
	env.FileName = filename
	env.Context = context

	parseArgs(env, args...)

	return instance.RunWithStringResponse(env)
}

func (instance *ScriptingHelper) RunProgram(programName string, script string, context map[string]interface{}, args ...interface{}) (response string, err error) {
	env := new(EnvSettings)
	env.EngineName = "js"
	env.ProgramName = programName
	env.ProgramScript = script
	env.Context = context

	parseArgs(env, args...)

	return instance.RunWithStringResponse(env)
}

func (instance *ScriptingHelper) RunWithStringResponse(env *EnvSettings, methods ...*ProgramMethod) (response string, err error) {
	var resp interface{}
	resp, err = instance.Run(env, methods...)
	if nil == err {
		response = gg_utils.Convert.ToString(resp)
	}
	return
}

func (instance *ScriptingHelper) RunMethod(env *EnvSettings, method string, args ...interface{}) (response interface{}, err error) {
	if nil != instance {
		response, err = instance.Run(env, NewProgramMethod(method, args...))
	}
	return
}

func (instance *ScriptingHelper) Run(env *EnvSettings, methods ...*ProgramMethod) (response interface{}, err error) {
	var program *Program
	program, err = instance.Build(env)
	if nil != err {
		return
	}
	response, err = program.Run(methods...)
	return
}

// RunSequence run a sequence of methods passing response of previous to next method
func (instance *ScriptingHelper) RunSequence(env *EnvSettings, methods ...*ProgramMethod) (response interface{}, err error) {
	var program *Program
	program, err = instance.Build(env)
	if nil != err {
		return
	}
	response, err = program.RunSequence(methods...)
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	u t i l s
//----------------------------------------------------------------------------------------------------------------------

func (instance *ScriptingHelper) NewProgramMethod(name string, arguments ...interface{}) *ProgramMethod {
	return NewProgramMethod(name, arguments...)
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func newEngine(name ...interface{}) *ScriptEngine {
	if len(name) == 1 {
		n := gg.Convert.ToString(name[0])
		switch n {
		case "javascript", "js":
			return NewJsEngine()
		default:
			return NewJsEngine()
		}
	}
	return NewJsEngine()
}

// Loader default javascript Module registry loader
func Loader(path string) ([]byte, error) {
	path = gg.Paths.Concat(gg.Paths.WorkspacePath("modules"), path)
	return gg.IO.ReadBytesFromFile(path)
}

func validateEnv(env *EnvSettings, fallbackLogger gg_log.ILogger) error {
	// check env validity
	if len(env.EngineName) == 0 {
		env.EngineName = "js"
	}
	if nil == env.Logger {
		env.Logger = fallbackLogger
	}

	if len(env.FileName) > 0 {
		env.FileName = gg.Paths.Absolute(env.FileName)
		text, ioErr := gg.IO.ReadTextFromFile(env.FileName)
		if nil != ioErr {
			return ioErr
		}
		env.ProgramScript = text
		env.ProgramName = gg.Paths.FileName(env.FileName, false)
		env.EngineName = gg.Paths.ExtensionName(env.FileName)
		env.Root = gg.Paths.Dir(env.FileName)
	}

	if len(env.Root) == 0 {
		env.Root = gg.Paths.WorkspacePath("./")
	}
	if len(env.ProgramName) == 0 {
		env.ProgramName = gg.Rnd.RndId()
	}
	return nil
}

func parseArgs(env *EnvSettings, args ...interface{}) {
	for _, arg := range args {
		if engine, ok := arg.(string); ok {
			env.EngineName = engine
		} else if callback, ok := arg.(OnReadyCallback); ok {
			env.OnReady = callback
		}
	}
}

func js() *goja.Runtime {
	if nil == _js {
		_js = goja.New()
	}
	return _js
}

func toValues(rt *goja.Runtime, args ...interface{}) []goja.Value {
	response := make([]goja.Value, 0)
	if nil == rt {
		rt = goja.New()
	}
	for _, arg := range args {
		response = append(response, rt.ToValue(arg))
	}
	return response
}
