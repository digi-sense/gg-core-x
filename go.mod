module bitbucket.org/digi-sense/gg-core-x

go 1.23.6

require (
	bitbucket.org/digi-sense/gg-core v0.3.41
	github.com/arangodb/go-driver v1.6.5
	github.com/cbroglie/mustache v1.4.0
	github.com/chromedp/cdproto v0.0.0-20250216233945-bd41ad9b04ce
	github.com/chromedp/chromedp v0.12.1
	github.com/creack/pty v1.1.23
	github.com/dop251/goja v0.0.0-20250125213203-5ef83b82af17
	github.com/eclipse/paho.mqtt.golang v1.5.0
	github.com/go-sql-driver/mysql v1.9.0
	github.com/gofiber/fiber/v2 v2.52.6
	github.com/gofiber/websocket/v2 v2.2.1
	github.com/jlaffaye/ftp v0.2.0
	github.com/makiuchi-d/gozxing v0.1.1
	github.com/ollama/ollama v0.5.12
	github.com/pborman/ansi v1.0.0
	github.com/philippgille/chromem-go v0.7.0
	github.com/pkg/sftp v1.13.7
	github.com/rabbitmq/amqp091-go v1.10.0
	github.com/sashabaranov/go-openai v1.38.0
	github.com/valyala/fasthttp v1.59.0
	github.com/yeqown/go-qrcode v1.5.10
	go.etcd.io/bbolt v1.4.0
	golang.org/x/crypto v0.36.0
	golang.org/x/net v0.37.0
	golang.org/x/sys v0.31.0
	golang.org/x/text v0.23.0
	gorm.io/driver/mysql v1.5.7
	gorm.io/driver/postgres v1.5.11
	gorm.io/driver/sqlite v1.5.7
	gorm.io/driver/sqlserver v1.5.4
	gorm.io/gorm v1.25.12
)

require (
	filippo.io/edwards25519 v1.1.0 // indirect
	github.com/andybalholm/brotli v1.1.1 // indirect
	github.com/arangodb/go-velocypack v0.0.0-20200318135517-5af53c29c67e // indirect
	github.com/chromedp/sysutil v1.1.0 // indirect
	github.com/dlclark/regexp2 v1.11.5 // indirect
	github.com/fasthttp/websocket v1.5.12 // indirect
	github.com/fogleman/gg v1.3.0 // indirect
	github.com/go-sourcemap/sourcemap v2.1.4+incompatible // indirect
	github.com/gobwas/httphead v0.1.0 // indirect
	github.com/gobwas/pool v0.2.1 // indirect
	github.com/gobwas/ws v1.4.0 // indirect
	github.com/golang-sql/civil v0.0.0-20220223132316-b832511892a9 // indirect
	github.com/golang-sql/sqlexp v0.1.0 // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/google/pprof v0.0.0-20250208200701-d0013a598941 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/gorilla/websocket v1.5.3 // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20240606120523-5a60cdf6a761 // indirect
	github.com/jackc/pgx/v5 v5.7.2 // indirect
	github.com/jackc/puddle/v2 v2.2.2 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/klauspost/compress v1.18.0 // indirect
	github.com/kr/fs v0.1.0 // indirect
	github.com/mailru/easyjson v0.9.0 // indirect
	github.com/mattn/go-colorable v0.1.14 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/mattn/go-runewidth v0.0.16 // indirect
	github.com/mattn/go-sqlite3 v1.14.24 // indirect
	github.com/microsoft/go-mssqldb v1.8.0 // indirect
	github.com/philhofer/fwd v1.1.3-0.20240916144458-20a13a1f6b7c // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rivo/uniseg v0.4.7 // indirect
	github.com/savsgio/gotils v0.0.0-20240704082632-aef3928b8a38 // indirect
	github.com/tinylib/msgp v1.2.5 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/yeqown/reedsolomon v1.0.0 // indirect
	golang.org/x/image v0.24.0 // indirect
	golang.org/x/sync v0.12.0 // indirect
	golang.org/x/xerrors v0.0.0-20240903120638-7835f813f4da // indirect
)
