package storage

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_dbal/gg_bolt"
	"time"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

var (
	BoltConst = struct {
		FieldKey    string
		FieldExpire string
		FieldOTP    string
	}{
		FieldKey:    "_key",
		FieldExpire: "_expire",
		FieldOTP:    "otp",
	}
)

type DriverBolt struct {
	dsn         *Dsn
	enableCache bool
	db          *gg_bolt.BoltDatabase
	err         error
}

func NewDriverBolt(dsn ...interface{}) *DriverBolt {
	instance := new(DriverBolt)
	if len(dsn) == 1 {
		if s, b := dsn[0].(string); b {
			instance.dsn = NewDsn(s)
		} else if d, b := dsn[0].(Dsn); b {
			instance.dsn = &d
		} else if d, b := dsn[0].(*Dsn); b {
			instance.dsn = d
		} else {
			instance.err = ErrorInvalidDsn
		}
	}
	if nil == instance.dsn && nil == instance.err {
		instance.err = ErrorInvalidDsn
	}
	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *DriverBolt) EnableCache(value bool) {
	instance.enableCache = value
}

func (instance *DriverBolt) Enabled() bool {
	return nil != instance && nil != instance.dsn && nil == instance.err && instance.dsn.IsValid()
}

func (instance *DriverBolt) Open() error {
	if nil != instance {
		if nil == instance.err {
			filename := gg.Paths.Absolute(instance.dsn.Database)
			err := gg.Paths.Mkdir(filename)
			if nil != err {
				instance.err = err
			} else {
				config := gg_bolt.NewBoltConfig()
				config.Name = filename
				instance.db = gg_bolt.NewBoltDatabase(config)
				instance.err = instance.db.Open()
			}
		}
		return instance.err
	}
	return nil
}

func (instance *DriverBolt) Close() error {
	if nil != instance && nil != instance.db {
		return instance.db.Close()
	}
	return nil
}

//----------------------------------------------------------------------------------------------------------------------
//	a u t h
//----------------------------------------------------------------------------------------------------------------------

func (instance *DriverBolt) AuthRegister(key, payload string) error {
	if nil != instance && nil != instance.db {
		if instance.enableCache {
			return ErrorDatabaseCacheCannotAuthenticate
		}
		coll, err := instance.db.Collection(CollectionAuth, true)
		if nil != err {
			return err
		}
		i, err := coll.Get(key)
		if nil != err {
			return err
		}
		if nil != i {
			return ErrorEntityAlreadyRegistered
		}
		item := map[string]interface{}{
			"_key":    key,
			"payload": payload,
		}
		return coll.Upsert(item)
	}
	return nil
}

func (instance *DriverBolt) AuthOverwrite(key, payload string) error {
	if nil != instance && nil != instance.db {
		if instance.enableCache {
			return ErrorDatabaseCacheCannotAuthenticate
		}
		coll, err := instance.db.Collection(CollectionAuth, true)
		if nil != err {
			return err
		}

		item := map[string]interface{}{
			"_key":    key,
			"payload": payload,
		}
		return coll.Upsert(item)
	}
	return nil
}

func (instance *DriverBolt) AuthGet(key string) (payload string, err error) {
	if nil != instance && nil != instance.db {
		if instance.enableCache {
			return payload, ErrorDatabaseCacheCannotAuthenticate
		}
		var coll *gg_bolt.BoltCollection
		coll, err = instance.db.Collection(CollectionAuth, true)
		if nil == err {
			var i interface{}
			i, err = coll.Get(key)
			if nil == err {
				if nil != i {
					payload = gg.Reflect.GetString(i, "payload")
				} else {
					err = ErrorEntityDoesNotExists
				}
			}
		}
	}
	return payload, err
}

func (instance *DriverBolt) AuthRemove(key string) (err error) {
	if nil != instance && nil != instance.db {
		if instance.enableCache {
			return ErrorDatabaseCacheCannotAuthenticate
		}
		err = instance.remove(CollectionAuth, key)
	}
	return err
}

//----------------------------------------------------------------------------------------------------------------------
//	c a c h e
//----------------------------------------------------------------------------------------------------------------------

func (instance *DriverBolt) CacheGet(key string) (string, error) {
	if nil != instance && nil != instance.db {
		if !instance.enableCache {
			return "", ErrorDatabaseCacheNotEnabled
		}

		coll, err := instance.db.Collection(CollectionCache, true)
		if nil != err {
			return "", err
		}
		item, err := coll.Get(key)
		if nil != err {
			return "", err
		}
		if nil != item {
			if m, b := item.(map[string]interface{}); b {
				var token string
				var err error
				now := int(time.Now().Unix())
				expire := gg.Convert.ToInt(m[gg_bolt.FieldExpire])
				token = m["token"].(string)
				if now-expire > 0 {
					// expired
					err = ErrorTokenExpired
				}
				return token, err
			}
			// not found or expired
			return "", ErrorTokenDoesNotExists
		} else {
			// not found or expired
			return "", ErrorTokenDoesNotExists
		}
	}
	return "", ErrorTokenDoesNotExists
}

func (instance *DriverBolt) CacheAdd(key, token string, duration time.Duration) error {
	if nil != instance && nil != instance.db {
		if !instance.enableCache {
			return ErrorDatabaseCacheNotEnabled
		}
		coll, err := instance.db.CollectionAutoCreate(CollectionCache)
		if nil != err {
			return err
		}
		item := map[string]interface{}{
			"_key":  key,
			"token": token,
		}
		item[gg_bolt.FieldExpire] = time.Now().Add(duration).Unix()
		return coll.Upsert(item)
	}
	return nil
}

func (instance *DriverBolt) CacheRemove(key string) error {
	if nil != instance && nil != instance.db {
		if !instance.enableCache {
			return ErrorDatabaseCacheNotEnabled
		}
		return instance.remove(CollectionCache, key)
	}
	return nil
}

func (instance *DriverBolt) OTPVerify(key, otp string) (response bool, err error) {
	if nil != instance && nil != instance.db {
		var coll *gg_bolt.BoltCollection
		coll, err = instance.db.Collection(CollectionOTP, true)
		if nil != err {
			return
		}
		var item interface{}
		item, err = coll.Get(key)
		if nil != err {
			return
		}
		if m, b := item.(map[string]interface{}); b {
			itemExpire := gg.Maps.GetInt(m, BoltConst.FieldExpire)
			itemOTP := gg.Maps.GetString(m, BoltConst.FieldOTP)

			// check expire time
			now := time.Now().Unix()
			expired := int(now)-itemExpire > 0
			if expired {
				response = false
				err = gg.Errors.Prefix(gg.PanicSystemError, "OTP is expired:  ")
				return
			}

			response = len(m) > 0 && otp == itemOTP
			if !response {
				err = gg.Errors.Prefix(gg.PanicSystemError, "OTP do not match:  ")
			} else {
				// remove
				err = instance.remove(CollectionOTP, key)
				if nil != err {
					response = false
				}
			}
		} else {
			// invalid otp
			response = false
			err = gg.Errors.Prefix(gg.PanicSystemError, "OTP do not match:  ")
		}
	}
	return
}

func (instance *DriverBolt) OTPNew(key string, len int, onlyDigits bool, duration time.Duration) (response string, err error) {
	if nil != instance && nil != instance.db {
		var coll *gg_bolt.BoltCollection
		coll, err = instance.db.Collection(CollectionOTP, true)
		if nil != err {
			return
		}

		// create or update new otp
		otp := GenerateOTP(onlyDigits, len)
		item := map[string]interface{}{}
		item[ArangoConst.FieldKey] = key
		item[ArangoConst.FieldExpire] = time.Now().Add(duration).Unix()
		item[ArangoConst.FieldOTP] = otp
		err = coll.Upsert(item)
		if nil != err {
			return
		}

		response = gg.Maps.GetString(item, ArangoConst.FieldOTP)
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *DriverBolt) remove(collectionName string, key string) (err error) {
	if nil != instance && nil != instance.db {
		var coll *gg_bolt.BoltCollection
		coll, err = instance.db.Collection(collectionName, true)
		if nil == err {
			err = coll.Remove(key)
		}
	}
	return err
}
