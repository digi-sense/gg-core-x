package test

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_auth0/jwt"
	"bitbucket.org/digi-sense/gg-core-x/gg_auth0/jwt/elements"
	"bitbucket.org/digi-sense/gg-core-x/gg_auth0/jwt/signing"
	"crypto/rsa"
	"io/ioutil"
)

func LoadRSAPrivateKeyFromDisk(location string) *rsa.PrivateKey {
	keyData, e := ioutil.ReadFile(location)
	if e != nil {
		panic(e.Error())
	}
	key, e := signing.ParseRSAPrivateKeyFromPEM(keyData)
	if e != nil {
		panic(e.Error())
	}
	return key
}

func LoadRSAPublicKeyFromDisk(location string) *rsa.PublicKey {
	keyData, e := ioutil.ReadFile(location)
	if e != nil {
		panic(e.Error())
	}
	key, e := signing.ParseRSAPublicKeyFromPEM(keyData)
	if e != nil {
		panic(e.Error())
	}
	return key
}

func MakeSampleToken(c elements.Claims, key interface{}) string {
	token := jwt.NewWithClaims(signing.SigningMethodRS256, c)
	s, e := token.SignedString(key)

	if e != nil {
		panic(e.Error())
	}

	return s
}
