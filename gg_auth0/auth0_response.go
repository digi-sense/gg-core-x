package gg_auth0

import "bitbucket.org/digi-sense/gg-core"

//----------------------------------------------------------------------------------------------------------------------
//	Auth0Response
//----------------------------------------------------------------------------------------------------------------------

type Auth0Response struct {
	Error        string                 `json:"error"`
	ItemId       string                 `json:"item_id"`
	ItemPayload  map[string]interface{} `json:"item_payload"`
	AccessToken  string                 `json:"access_token"`
	RefreshToken string                 `json:"refresh_token"`
	ConfirmToken string                 `json:"confirm_token"`
	OTP          string                 `json:"otp"`
}

func (instance *Auth0Response) GoString() string {
	return gg.JSON.Stringify(instance)
}

func (instance *Auth0Response) String() string {
	return gg.JSON.Stringify(instance)
}
