# G&G Core X #

![](icon.png)

Extended Utility Library.

## LICENSE

[3-Clause BSD License](./LICENSE.txt)

## How to Use

To use just call:

```
go get bitbucket.org/digi-sense/gg-core-x@v0.3.41
```

### Versioning ###

Sources are versioned using git tags:

```
git tag v0.3.41
git push origin v0.3.41
```