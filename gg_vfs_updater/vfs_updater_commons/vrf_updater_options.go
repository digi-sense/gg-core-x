package vfs_updater_commons

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs/vfsoptions"
	"bitbucket.org/digi-sense/gg-core/gg_scheduler"
)

type VfsUpdaterOptions struct {
	Backend             *vfsoptions.VfsOptions `json:"backend"`
	VersionFileRequired bool                   `json:"version_file_required"` // if true, first start will update all if version file does not exists
	VersionFile         string                 `json:"version_file"`
	PackageMode         string                 `json:"package_mode"` // literal, local, remote, auto
	PackageFiles        []*PackageFile         `json:"package_files"`
	Scheduled           *gg_scheduler.Schedule `json:"scheduled"`
}

type PackageFile struct {
	File   string `json:"file"`
	Target string `json:"target"`
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
//----------------------------------------------------------------------------------------------------------------------

func NewVfsUpdaterOptions() (instance *VfsUpdaterOptions) {
	instance = new(VfsUpdaterOptions)
	instance.PackageFiles = make([]*PackageFile, 0)
	instance.Backend = vfsoptions.Create()
	instance.VersionFileRequired = true
	instance.PackageMode = PackageModeAuto
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *VfsUpdaterOptions) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *VfsUpdaterOptions) GetPackageMode() string {
	if nil != instance {
		if len(instance.PackageMode) == 0 {
			instance.PackageMode = PackageModeAuto
		}
		return instance.PackageMode
	}
	return PackageModeAuto
}
