package vfs_updater_commons

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"path/filepath"
	"strings"
)

const (
	PackageModeLiteral = "literal" // use only declared parameters
	PackageModeLocal   = "local"   // when need to search, search only in a local path
	PackageModeRemote  = "remote"  // when need to search, search only in a remote path
	PackageModeAuto    = "auto"    //  when need to search, search both in a remote and local path
)

var blacklist = []string{"/", "../", "./", ".", ".."}

func Trim(value string) string {
	return strings.Trim(value, " \t\n")
}

// IsValidSubPath return true is passed path is a path that can be listed as sub-path
func IsValidSubPath(value string) bool {
	isBlackListed := gg.Arrays.IndexOf(Trim(value), blacklist) > -1
	if !isBlackListed {
		name := filepath.Base(value)
		return !strings.HasPrefix(name, ".")
	}
	return false
}

func IsInHistory(value string, history []string) bool {
	for _, h := range history {
		if h == value {
			return true
		}
		if strings.HasPrefix(h, value) {
			return true
		}
	}
	return false
}

func NeedUpdate(currentVersion, remoteVersion string, versionISRequired bool) bool {
	if versionISRequired && len(currentVersion) == 0 && len(remoteVersion) > 0 {
		// missing a current version, but have a remote
		return true
	}
	if len(currentVersion) > 0 && len(remoteVersion) > 0 && currentVersion != remoteVersion {
		v1 := strings.Split(strings.Trim(currentVersion, " \n"), ".")
		v2 := strings.Split(strings.Trim(remoteVersion, " \n"), ".")
		for i := 0; i < 3; i++ {
			a := gg_utils.Convert.ToInt(gg_utils.Arrays.GetAt(v1, i, 0))
			b := gg_utils.Convert.ToInt(gg_utils.Arrays.GetAt(v2, i, 0))
			if b == a {
				continue
			} else if b < a {
				return false
			} else if b > a {
				return true
			}
		}
	}
	return false
}

func ToPackageFiles(dirTarget string, files, blacklist []string) (response []*PackageFile) {
	for _, f := range files {
		if !IsInBlacklist(f, blacklist) {
			response = append(response, ToPackageFile(dirTarget, f))
		}
	}
	return
}

func ToPackageFile(dirTarget, file string) *PackageFile {
	return &PackageFile{
		File:   file,
		Target: gg.Paths.Absolutize(gg.Paths.Dir(file), dirTarget),
	}
}

func IsInBlacklist(filename string, blacklist []string) bool {
	if len(filename) > 0 && len(blacklist) > 0 {
		for _, b := range blacklist {
			if gg.Paths.PatternMatchBase(filename, b) {
				return true
			}
		}
	}
	return false
}
