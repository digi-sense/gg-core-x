package gg_vfs_updater

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs"
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs/vfscommons"
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs_updater/vfs_updater_commons"
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_scheduler"
	"bitbucket.org/digi-sense/gg-core/gg_stoppable"
	ggvfscore "bitbucket.org/digi-sense/gg-core/gg_vfs"
	"errors"
	"fmt"
	"path/filepath"
	"strings"
	"sync"
)

type OnBeforeUpdateHandler func(from, to, filename string) bool // return true if can continue to update
type OnUpdatingHandler func(from, to, filename string, err error)
type OnAfterUpdateHandler func(from, to string, files []string)
type OnErrorHandler func(err error)
type OnReadyHandler func()

type VFSUpdater struct {
	uid       string
	dirTarget string // local path root to update
	options   *vfs_updater_commons.VfsUpdaterOptions
	muxUpdate *sync.Mutex
	muxVfs    *sync.Mutex

	initialized       bool
	keepConnected     bool // Resiliency. If true, the client continues ping the server until connection is established
	currVersion       string
	onBeforeUpdate    []OnBeforeUpdateHandler
	onUpdating        []OnUpdatingHandler
	onAfterUpdate     []OnAfterUpdateHandler
	onError           []OnErrorHandler
	onReady           []OnReadyHandler
	latestUpdateError error
	remoteDir         string // set when remote vfs is created
	remoteVersion     string // set when remote vfs is created

	_logger    gg_.ILogger
	_stoppable *gg_stoppable.Stoppable
	_scheduler *gg_scheduler.Scheduler
	_vfs       vfscommons.IVfs
}

func NewVfsUpdater(args ...interface{}) (instance *VFSUpdater, err error) {
	instance = new(VFSUpdater)
	instance.uid = gg.Rnd.Uuid()
	instance.dirTarget = gg.Paths.Absolute(".")
	instance.onUpdating = make([]OnUpdatingHandler, 0)
	instance.onBeforeUpdate = make([]OnBeforeUpdateHandler, 0)
	instance.onAfterUpdate = make([]OnAfterUpdateHandler, 0)
	instance.onError = make([]OnErrorHandler, 0)
	instance.onReady = make([]OnReadyHandler, 0)
	instance.muxUpdate = new(sync.Mutex)
	instance.muxVfs = new(sync.Mutex)
	instance.keepConnected = true // resilient to network errors
	instance.remoteVersion = "Undefined"

	err = instance.init(args...)

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *VFSUpdater) Map() map[string]interface{} {
	if nil != instance {
		m := make(map[string]interface{})
		m["options"] = gg.Convert.ToMap(instance.options)
		m["dir_target"] = instance.dirTarget
		m["curr_version"] = instance.currVersion
		return m
	}
	return nil
}

func (instance *VFSUpdater) String() string {
	if nil != instance {
		return gg.JSON.Stringify(instance.Map())
	}
	return ""
}

func (instance *VFSUpdater) Text() string {
	if nil != instance {
		currVersion := instance.CurrVersion()
		remoVersion := instance.RemoteVersion()
		isRemoteVersionRequired := instance.options.VersionFileRequired
		var s strings.Builder
		s.WriteString("\n===============\n")
		s.WriteString(fmt.Sprintf("\tUID: %s\n", instance.uid))
		s.WriteString(fmt.Sprintf("\tCurr. Version: %s\n", currVersion))
		s.WriteString(fmt.Sprintf("\tRemote Version: %s\n", remoVersion))
		s.WriteString(fmt.Sprintf("\tSchedule: %s\n", instance.options.Scheduled))
		s.WriteString(fmt.Sprintf("\tBackend: %s\n", instance.options.Backend.Location))
		s.WriteString(fmt.Sprintf("\tBackend User: %s\n", instance.options.Backend.UserName()))
		s.WriteString(fmt.Sprintf("\tLocal Root: %s\n", instance.dirTarget))
		s.WriteString(fmt.Sprintf("\tRemote Root: %s\n", instance.remoteDir))
		s.WriteString(fmt.Sprintf("\tRemote Version File: %s\n", instance.options.VersionFile))
		s.WriteString(fmt.Sprintf("\tFiles to Update: %v\n", len(instance.options.PackageFiles)))
		s.WriteString(fmt.Sprintf("\tFile Mode: %v\n", instance.options.GetPackageMode()))
		if isRemoteVersionRequired {
			if len(remoVersion) == 0 || remoVersion == "Undefined" {
				s.WriteString(fmt.Sprintf("\t* ERROR: %s\n", "Did not find a required remote version!"))
			}
		} else {
			if len(remoVersion) == 0 || remoVersion == "Undefined" {
				s.WriteString(fmt.Sprintf("\t* INFO: %s\n", "Resilient mode enabled. The remote version is empty or do not exists, but updater will continue waiting for a remote version.txt file."))
			}
		}
		s.WriteString("===============")
		return s.String()
	}
	return ""
}

func (instance *VFSUpdater) SetUid(value string) *VFSUpdater {
	if nil != instance {
		instance.uid = value
	}
	return instance
}

func (instance *VFSUpdater) GetUid() string {
	if nil != instance {
		return instance.uid
	}
	return ""
}

func (instance *VFSUpdater) SetRoot(value string) *VFSUpdater {
	if nil != instance {
		instance.dirTarget = gg.Paths.Absolute(value)

	}
	return instance
}

func (instance *VFSUpdater) GetRoot() string {
	if nil != instance {
		return instance.dirTarget
	}
	return ""
}

func (instance *VFSUpdater) SetKeepConnected(value bool) *VFSUpdater {
	if nil != instance {
		instance.keepConnected = value
	}
	return instance
}

func (instance *VFSUpdater) GetKeepConnected() bool {
	if nil != instance {
		return instance.keepConnected
	}
	return false
}

func (instance *VFSUpdater) SetLogger(value gg_.ILogger) *VFSUpdater {
	if nil != instance {
		instance._logger = value
	}
	return instance
}

func (instance *VFSUpdater) PackageFiles() []*vfs_updater_commons.PackageFile {
	if nil != instance && nil != instance.options {
		return instance.options.PackageFiles
	}
	return nil
}

func (instance *VFSUpdater) VersionFile() string {
	if nil != instance && nil != instance.options {
		return instance.options.VersionFile
	}
	return ""
}

func (instance *VFSUpdater) HasUpdates() bool {
	return instance.hasUpdates()
}

func (instance *VFSUpdater) IsUpgradable(currentVersion, remoteVersion string) bool {
	return instance.isUpgradable(currentVersion, remoteVersion)
}

func (instance *VFSUpdater) CurrVersion() string {
	if nil != instance {
		return instance.getCurrentVersion()
	}
	return ""
}

func (instance *VFSUpdater) RemoteVersion() string {
	if nil != instance && nil != instance._vfs {
		return instance.remoteVersion
	}
	return "Undefined"
}

func (instance *VFSUpdater) ReloadPackagesFromRemote() (err error) {
	if nil != instance && nil != instance._vfs {
		instance.options.PackageFiles, err = instance.remotePackageFiles()
	} else {
		err = gg.Errors.Prefix(gg.PanicSystemError, "Environment not ready: ")
	}
	return
}

func (instance *VFSUpdater) ReloadPackages() (err error) {
	if nil != instance && nil != instance._vfs {
		switch instance.options.GetPackageMode() {
		case vfs_updater_commons.PackageModeAuto:
			// auto
			instance.options.PackageFiles, err = instance.remotePackageFiles()
			if nil != err {
				return
			}
			if len(instance.options.PackageFiles) == 0 {
				instance.options.PackageFiles, err = instance.localPackageFiles()
				if nil != err {
					return
				}
			}
		case vfs_updater_commons.PackageModeRemote:
			// only remote
			instance.options.PackageFiles, err = instance.remotePackageFiles()
		case vfs_updater_commons.PackageModeLocal:
			// only local
			instance.options.PackageFiles, err = instance.localPackageFiles()
		case vfs_updater_commons.PackageModeLiteral:
			// only local if empty
			if len(instance.options.PackageFiles) == 0 {
				instance.options.PackageFiles, err = instance.localPackageFiles()
			}
		}
	} else {
		err = gg.Errors.Prefix(gg.PanicSystemError, "Environment not ready: ")
	}
	return
}

func (instance *VFSUpdater) HasRemoteVersion() bool {
	if nil != instance {
		s := instance.getRemoteVersion()
		return len(s) > 0 && s != "Undefined"
	}
	return false
}

//----------------------------------------------------------------------------------------------------------------------
//	e v e n t s
//----------------------------------------------------------------------------------------------------------------------

func (instance *VFSUpdater) OnReady(handler OnReadyHandler) {
	if nil != instance {
		instance.onReady = append(instance.onReady, handler)
	}
}

func (instance *VFSUpdater) OnBeforeUpdate(handler OnBeforeUpdateHandler) {
	if nil != instance {
		instance.onBeforeUpdate = append(instance.onBeforeUpdate, handler)
	}
}

func (instance *VFSUpdater) OnUpdate(handler OnUpdatingHandler) {
	if nil != instance {
		instance.onUpdating = append(instance.onUpdating, handler)
	}
}

func (instance *VFSUpdater) OnAfterUpdate(handler OnAfterUpdateHandler) {
	if nil != instance {
		instance.onAfterUpdate = append(instance.onAfterUpdate, handler)
	}
}

func (instance *VFSUpdater) OnError(handler OnErrorHandler) {
	if nil != instance {
		instance.onError = append(instance.onError, handler)
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	m e t h o d s
//----------------------------------------------------------------------------------------------------------------------

func (instance *VFSUpdater) CheckForUpdates() (updated bool, fromVersion string, toVersion string, files []string, err error) {
	if nil != instance {
		err = instance.initialize()
		if nil != err {
			return
		}

		return instance.checkForUpdates()
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	s t o p p a b l e
//----------------------------------------------------------------------------------------------------------------------

func (instance *VFSUpdater) Start() {
	if nil != instance {
		instance.stoppable().Start()
	}
	return
}

func (instance *VFSUpdater) Stop() {
	if nil != instance && nil != instance._stoppable {
		instance._stoppable.Stop()
	}
	return
}

func (instance *VFSUpdater) Join() {
	if nil != instance && nil != instance._stoppable {
		instance._stoppable.Join()
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *VFSUpdater) init(args ...interface{}) (err error) {
	if nil != instance {
		// auto-detect arguments
		for _, arg := range args {
			if o, ok := arg.(*vfs_updater_commons.VfsUpdaterOptions); ok {
				instance.options = o
				break
			}
			if o, ok := arg.(vfs_updater_commons.VfsUpdaterOptions); ok {
				instance.options = &o
				break
			}
			if logger, ok := arg.(gg_.ILogger); ok {
				instance._logger = logger
				break
			}
			if s, ok := arg.(string); ok {
				if gg.Regex.IsValidJsonObject(s) {
					err = gg.JSON.Read(s, &instance.options)
					if err != nil {
						return
					}
					break
				} else {
					instance.SetRoot(gg.Paths.Dir(s))
					err = gg.JSON.ReadFromFile(s, &instance.options)
					if err != nil {
						return
					}
					break
				}
			}
		}

		if nil == instance.options {
			instance.options = vfs_updater_commons.NewVfsUpdaterOptions()
		}
	}
	return
}

func (instance *VFSUpdater) startScheduler() (err error) {
	if nil != instance {
		// start scheduler
		var scheduler *gg_scheduler.Scheduler
		scheduler, err = instance.scheduler()
		if nil != err {
			return
		}
		scheduler.Start()
	}
	return
}

func (instance *VFSUpdater) initialize() (err error) {
	if nil != instance {
		// ensure initialized vfs
		var vfs vfscommons.IVfs
		vfs, err = instance.vfs()
		if nil != err {
			return
		}

		if !instance.initialized {
			// check target dir
			if len(instance.dirTarget) > 0 {
				// _ = gg.Paths.Mkdir(instance.dirTarget)
				// check version file
				_ = instance.getCurrentVersion()
			} else {
				err = gg.Errors.Prefix(gg.PanicSystemError, "Target dir cannot be empty: ")
				return
			}
			// check options
			if nil != instance.options {
				if nil == instance.options.Scheduled {
					err = gg.Errors.Prefix(gg.PanicSystemError, "Missing scheduled options: ")
					return
				}
			} else {
				err = gg.Errors.Prefix(gg.PanicSystemError, "Missing options: ")
				return
			}

			// check packages if empty
			if len(instance.options.PackageFiles) == 0 {
				err = instance.ReloadPackages()
				if nil != err {
					return
				}
			}
			instance.initialized = true

			instance.notifyReady()
		} else {
			// already initialized
			// TEST REMOTE CONNECTION IS ALREADY AVAILABLE
			if !vfs.IsConnected() {
				err = gg.Errors.Prefix(gg.PanicSystemError, "Vfs is not connected: ")
			}

			if nil != err {
				instance.initialized = false
				instance.notifyErrorIfAny(err)
			}
		}
	}
	return
}

func (instance *VFSUpdater) finalize() (err error) {
	if nil != instance && instance.initialized {
		instance.initialized = false
		if nil != instance._scheduler {
			instance._scheduler.Stop()
			instance._scheduler = nil
		}
		if nil != instance._vfs {
			instance._vfs.Close()
			instance._vfs = nil
		}
		if nil != instance._stoppable {
			instance._stoppable = nil
		}
	}
	return
}

func (instance *VFSUpdater) stoppable() *gg_stoppable.Stoppable {
	if nil != instance {
		if nil == instance._stoppable {
			// stoppable object
			instance._stoppable = gg_stoppable.NewStoppable()
			instance._stoppable.SetName(fmt.Sprintf("Updater '%s'", instance.uid))
			instance._stoppable.SetLogger(instance.getLogger)
			instance._stoppable.OnStart(instance.onStartService)
			instance._stoppable.OnStop(instance.onStopService)
		}
		return instance._stoppable
	}
	return nil
}

func (instance *VFSUpdater) onStartService() {
	if nil != instance {
		err := instance.startScheduler()
		if nil != err {
			// blocking error during initialization
			instance.notifyLatestUpdateError(err)
			return
		}
	}
}

func (instance *VFSUpdater) onStopService() {
	if nil != instance {
		err := instance.finalize()
		if nil != err {
			return
		}
	}
}

func (instance *VFSUpdater) getLogger() gg_.ILogger {
	if nil != instance {
		if nil == instance._logger {
			// creates new internal logger
			instance._logger = gg.Log.NewNoRotate("info", gg.Paths.Absolutize("./logging.log", instance.dirTarget))
		}
		return instance._logger
	}
	return nil
}

func (instance *VFSUpdater) logError(context string, err error) {
	if nil != instance && nil != err {
		instance.getLogger().Error(context, err)
	}
}

func (instance *VFSUpdater) logInfo(context string) {
	if nil != instance {
		instance.getLogger().Info(context)
	}
}

func (instance *VFSUpdater) hasUpdates() bool {
	currentVersion, remoteVersion := instance.getVersions()
	return instance.isUpgradable(currentVersion, remoteVersion)
}

func (instance *VFSUpdater) isUpgradable(currentVersion, remoteVersion string) bool {
	return vfs_updater_commons.NeedUpdate(currentVersion, remoteVersion, instance.options.VersionFileRequired)
}

func (instance *VFSUpdater) isVersionFile(filename string) bool {
	if nil != instance && nil != instance.options {
		return strings.ToLower(gg.Paths.FileName(filename, true)) == strings.ToLower(gg.Paths.FileName(instance.options.VersionFile, true))
	}
	return false
}

func (instance *VFSUpdater) defaultBlackList() []string {
	return []string{gg.Paths.FileName(instance.VersionFile(), true)}
}

func (instance *VFSUpdater) scheduler() (response *gg_scheduler.Scheduler, err error) {
	if nil != instance {
		if nil == instance._scheduler {
			if nil == instance.options {
				err = gg.Errors.Prefix(gg.PanicSystemError, "Missing options: ")
				return
			}
			if nil != instance.options.Scheduled {
				instance._scheduler = gg_scheduler.NewScheduler()
				instance._scheduler.AddSchedule(instance.options.Scheduled)
				instance._scheduler.OnError(instance.handleScheduleError)
				instance._scheduler.OnSchedule(instance.handleSchedule)
			} else {
				err = gg.Errors.Prefix(gg.PanicSystemError, "Missing scheduled options: ")
			}
		}
		response = instance._scheduler
	}
	return
}

func (instance *VFSUpdater) vfs() (response vfscommons.IVfs, err error) {
	if nil != instance && nil != instance.muxVfs {
		instance.muxVfs.Lock()
		defer instance.muxVfs.Unlock()

		if nil == instance._vfs {
			if nil != instance.options.Backend {
				instance._vfs, err = gg_vfs.VFS.New(instance.options.Backend)
				if nil != err {
					instance._vfs = nil
					return
				}

				// set remote root
				root := instance._vfs.Path()
				dir := gg.Paths.Dir(instance.options.VersionFile)
				instance.remoteDir = gg.Paths.Concat(root, dir)
				_, err = instance._vfs.Cd(instance.remoteDir)
				if nil == err {
					name := filepath.Base(instance.options.VersionFile)
					instance.options.VersionFile = vfscommons.EnsureRelativePath(name)
					var data []byte
					data, err = instance.readVfs(instance._vfs, instance.options.VersionFile)
					if nil == err {
						instance.remoteVersion = vfs_updater_commons.Trim(string(data))
					} else {
						// remote file do not exists.
						// is this an error????
						if !instance.options.VersionFileRequired {
							err = nil
						}
					}
				} else {
					instance._vfs.Close()
					instance._vfs = nil
					err = gg.Errors.Prefix(err, fmt.Sprintf("VFSUpdater.vfs() -> Unable to change directory to '%s': ", instance.remoteDir))
				}
			} else {
				err = gg.Errors.Prefix(gg.PanicSystemError, "Missing backend options: ")
			}
		}
		// assign the response
		response = instance._vfs
	}
	return
}

func (instance *VFSUpdater) getVersions() (currentVersion string, remoteVersion string) {
	if nil != instance.options && len(instance.options.VersionFile) > 0 {
		currentVersion = instance.getCurrentVersion()
		remoteVersion = instance.getRemoteVersion()
	}
	return
}

func (instance *VFSUpdater) setCurrentVersion(value string) string {
	if nil != instance {
		value = vfs_updater_commons.Trim(value)
		filename := gg.Paths.FileName(instance.VersionFile(), true)
		_, err := instance.writeLocal([]byte(value), filename)
		if nil == err {
			instance.currVersion = value
		}
		return instance.currVersion
	}
	return ""
}

func (instance *VFSUpdater) getCurrentVersion() string {
	if nil != instance {
		instance.currVersion = ""
		data, err := instance.readLocal(gg.Paths.Concat(instance.dirTarget, gg.Paths.FileName(instance.options.VersionFile, true)))
		if nil == err {
			instance.currVersion = vfs_updater_commons.Trim(string(data))
		}
		return instance.currVersion
	}
	return ""
}

func (instance *VFSUpdater) getRemoteVersion() (response string) {
	if nil != instance {
		data, err := instance.readRemote(instance.options.VersionFile)
		if nil == err {
			response = strings.Trim(string(data), " \n")
		}
	}
	return
}

func (instance *VFSUpdater) listLocal(dir string) (response []string, err error) {
	if nil != instance {
		response, err = gg.Paths.ListFiles(dir, "*.*")
		for i, _ := range response {
			response[i] = strings.Replace(response[i], dir, ".", 1)
		}
	}
	return
}

func (instance *VFSUpdater) listRemote() (response []string, err error) {
	if nil != instance {
		var vfs vfscommons.IVfs
		vfs, err = instance.vfs()
		if nil != err {
			return
		}

		absPath := vfs.Path()
		err = instance.listDeep(vfs, absPath, &[]string{}, &response)
		// relativize all files
		if len(response) > 0 {
			for i, _ := range response {
				response[i] = vfscommons.Relativize(absPath, response[i])
			}
		}
	}
	return
}

func (instance *VFSUpdater) listDeep(vfs vfscommons.IVfs, dir string, history, output *[]string) (err error) {
	if nil != instance {
		// check if already navigated
		if vfs_updater_commons.IsInHistory(dir, *history) {
			return
		} else {
			*history = append(*history, dir)
		}

		var files []*ggvfscore.VfsFile
		files, err = vfs.ListAll(dir)
		if nil != err {
			return
		}
		for _, file := range files {
			if !file.IsDir {
				*output = append(*output, file.AbsolutePath)
			} else {
				// exclude some paths
				if !vfs_updater_commons.IsValidSubPath(file.AbsolutePath) || file.AbsolutePath == dir {
					continue
				}
				subErr := instance.listDeep(vfs, file.AbsolutePath, history, output)
				if nil != subErr {
					err = subErr
					break
				}
			}
		}
	}
	return
}

func (instance *VFSUpdater) writeLocal(data []byte, filename string) (response int, err error) {
	if nil != instance {
		filename = gg.Paths.Absolutize(filename, instance.dirTarget)
		_ = gg.Paths.Mkdir(filename)
		response, err = gg.IO.WriteBytesToFile(data, filename)
	}
	return
}

func (instance *VFSUpdater) readLocal(filename string) (response []byte, err error) {
	if nil != instance {
		response, err = gg.IO.ReadBytesFromFile(filename)
	}
	return
}

func (instance *VFSUpdater) readRemote(filename string) (response []byte, err error) {
	if nil != instance {
		var vfs vfscommons.IVfs
		vfs, err = instance.vfs()
		if nil != err {
			return
		}
		response, err = instance.readVfs(vfs, filename)
	}
	return
}

func (instance *VFSUpdater) readVfs(vfs vfscommons.IVfs, filename string) (response []byte, err error) {
	if nil != instance && nil != vfs {
		response, err = vfs.Read(filename)
	}
	return
}

func (instance *VFSUpdater) download(history *[]string, remoteFilename, targetDir string) (err error) {
	var vfs vfscommons.IVfs
	vfs, err = instance.vfs()
	if nil != err {
		return
	}

	var data []byte
	data, err = vfs.Read(remoteFilename)
	if nil != err {
		return
	}

	targetFilename := gg.Paths.Concat(targetDir, gg.Paths.FileName(remoteFilename, true))
	_, err = instance.writeLocal(data, targetFilename)
	if nil == err {
		*history = append(*history, remoteFilename)
	}
	return
}

func (instance *VFSUpdater) localPackageFiles() (response []*vfs_updater_commons.PackageFile, err error) {
	if nil != instance {
		response = make([]*vfs_updater_commons.PackageFile, 0)
		var files []string
		files, err = instance.listLocal(instance.dirTarget)
		if err != nil {
			return
		}
		response = vfs_updater_commons.ToPackageFiles(instance.dirTarget, files, instance.defaultBlackList())
	}
	return
}

func (instance *VFSUpdater) remotePackageFiles() (response []*vfs_updater_commons.PackageFile, err error) {
	if nil != instance {
		response = make([]*vfs_updater_commons.PackageFile, 0)
		var files []string
		files, err = instance.listRemote()
		if err != nil {
			return
		}
		response = vfs_updater_commons.ToPackageFiles(instance.dirTarget, files, instance.defaultBlackList())
	}
	return
}

func (instance *VFSUpdater) handleScheduleError(err string) {
	if nil != instance {
		instance.notifyErrorIfAny(errors.New(err))
	}
}

func (instance *VFSUpdater) handleSchedule(_ *gg_scheduler.SchedulerTask) {
	if nil != instance {
		if nil != instance._scheduler {
			instance._scheduler.Pause()
			defer instance._scheduler.Resume()

			err := instance.initialize()
			if nil != err {
				// blocking error during initialization
				instance.notifyLatestUpdateError(err)
				if !instance.keepConnected {
					go instance.Stop() // exit process and finalize the client
				}
				return
			}

			_, _, _, _, err = instance.checkForUpdates()
			if err != nil {
				instance.notifyLatestUpdateError(err)
				return
			} else {
				instance.latestUpdateError = nil
			}
		}
	}
}

func (instance *VFSUpdater) checkForUpdates() (updated bool, fromVersion string, toVersion string, files []string, err error) {
	if nil != instance {
		instance.muxUpdate.Lock()
		defer instance.muxUpdate.Unlock()

		updated = false
		fromVersion = instance.getCurrentVersion()
		toVersion = instance.getRemoteVersion()

		if len(toVersion) == 0 {
			if !instance.options.VersionFileRequired {
				// silent exit, it's not a problem if the remote file does not exist
				return
			} else {
				// update not available
				err = gg.Errors.Prefix(gg.PanicSystemError, "Remote Version file not found.")
			}
			return
		}

		if instance.hasUpdates() {

			// autoload package files
			err = instance.ReloadPackages()
			if nil != err {
				return
			}

			// download all updates
			for _, res := range instance.options.PackageFiles {
				if len(instance.onBeforeUpdate) > 0 {
					// BEFORE
					for _, handler := range instance.onBeforeUpdate {
						if nil != handler {
							if handler(fromVersion, toVersion, res.File) {
								err = instance.download(&files, res.File, res.Target)
								instance.notifyUpdating(fromVersion, toVersion, res.File, err)
								if nil != err {
									return
								}
							}
						}
					}
				} else {
					err = instance.download(&files, res.File, res.Target)
					instance.notifyUpdating(fromVersion, toVersion, res.File, err)
					if nil != err {
						return
					}
				}
			}

			// AFTER
			if len(files) > 0 {

				// write the new version
				updated = true
				instance.setCurrentVersion(toVersion)
				instance.notifyAfter(fromVersion, toVersion, files)
				instance.logInfo(fmt.Sprintf("Updated from '%s' to '%s': %v", fromVersion, toVersion, files))
			}
		}
	}
	return
}

func (instance *VFSUpdater) notifyReady() {
	if nil != instance {
		// log initialized
		instance.logInfo(instance.Text())

		if len(instance.onReady) > 0 {
			for _, handler := range instance.onReady {
				if nil != handler {
					go handler()
				}
			}
		}
	}
}

func (instance *VFSUpdater) notifyUpdating(fromVersion, toVersion, filename string, err error) {
	if len(instance.onUpdating) > 0 {
		instance.notifyErrorIfAny(err)
		for _, handler := range instance.onUpdating {
			if nil != handler {
				go handler(fromVersion, toVersion, filename, err)
			}
		}
	}
}

func (instance *VFSUpdater) notifyAfter(fromVersion, toVersion string, files []string) {
	if len(instance.onAfterUpdate) > 0 {
		for _, handler := range instance.onAfterUpdate {
			if nil != handler {
				go handler(fromVersion, toVersion, files)
			}
		}
	}
}

func (instance *VFSUpdater) notifyLatestUpdateError(err error) {
	if nil != instance && nil != err {
		if nil == instance.latestUpdateError {
			instance.latestUpdateError = err
			instance.notifyErrorIfAny(err)
		}
	}
}

func (instance *VFSUpdater) notifyErrorIfAny(err error) {
	if nil != err {
		if len(instance.onError) > 0 {
			for _, handler := range instance.onError {
				if nil != handler {
					go handler(err)
				}
			}
		}

		// write a log
		logger := instance.getLogger()
		if nil != logger {
			logger.Error(err)
		}
	}
}
