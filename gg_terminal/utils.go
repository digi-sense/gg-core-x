package gg_terminal

import (
	"bytes"
	"github.com/pborman/ansi"
	"regexp"
	"strings"
)

const ansiChars = "[\u001B\u009B][[\\]()#;?]*(?:(?:(?:[a-zA-Z\\d]*(?:;[a-zA-Z\\d]*)*)?\u0007)|(?:(?:\\d{1,4}(?:;\\d{0,4})*)?[\\dA-PRZcf-ntqry=><~]))"

var re = regexp.MustCompile(ansiChars)

func StripANSIs(str string) string {
	return re.ReplaceAllString(str, "")
}

func StripANSI2(data []byte) []byte {
	return re.ReplaceAll(data, []byte(""))
}

func StripANSI(data []byte) []byte {
	response, err := ansi.Strip(data)
	if err != nil {
		return data
	}
	return response
}

func StripANSIString(str string) string {
	response, err := ansi.Strip([]byte(str))
	if err != nil {
		return str
	}
	return string(response)
}

func StripAllString(str string, cutSet []string) string {
	if len(str) > 0 && len(cutSet) > 0 {
		for _, c := range cutSet {
			str = strings.ReplaceAll(str, c, "")
		}
	}
	return str
}

func StripAll(data []byte, cutSet []string) []byte {
	if len(data) > 0 && len(cutSet) > 0 {
		for _, c := range cutSet {
			bb := []byte(c)
			data = bytes.ReplaceAll(data, bb, []byte(""))
		}
	}
	return data
}

func EnsureLineBreakToString(data string) (response string) {
	response = data
	if !strings.HasSuffix(data, "\n") {
		response = data + "\n"
	}
	return
}

func EnsureLineBreakToBytes(data []byte) (response []byte) {
	s := string(data)
	if !strings.HasSuffix(s, "\n") {
		s = s + "\n"
	}
	response = []byte(s)
	return
}

func HasLineBreak(data []byte) bool {
	return strings.HasSuffix(string(data), "\n")
}
