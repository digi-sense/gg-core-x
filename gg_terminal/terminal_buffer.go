package gg_terminal

import (
	"bitbucket.org/digi-sense/gg-core"
	"bytes"
	"strings"
	"sync"
)

type TerminalXBufferOutput struct {
	Cursor   string
	Response []byte
}

type TerminalXBuffer struct {
	cursor    string   // end of line.
	cutSet    []string // to remove from string
	lastInput string   // last string written in terminal from input

	buff               *bytes.Buffer
	mux                *sync.Mutex
	chanOutput         chan *TerminalXBufferOutput
	onProgressHandlers []OnProgressHandler
}

func NewTerminalXBuffer(cursor string, cutSet []string) (instance *TerminalXBuffer) {
	instance = new(TerminalXBuffer)
	instance.cursor = cursor
	instance.cutSet = cutSet
	instance.buff = new(bytes.Buffer)
	instance.mux = new(sync.Mutex)
	instance.chanOutput = make(chan *TerminalXBufferOutput)
	if len(instance.cursor) == 0 {
		instance.cursor = "\n"
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *TerminalXBuffer) String() string {
	if nil != instance && nil != instance.buff {
		return instance.buff.String()
	}
	return ""
}

func (instance *TerminalXBuffer) Reset() {
	if nil != instance && nil != instance.buff {
		instance.buff.Reset()
		instance.onProgressHandlers = make([]OnProgressHandler, 0)
	}
}

func (instance *TerminalXBuffer) Bytes() []byte {
	if nil != instance && nil != instance.buff {
		return instance.buff.Bytes()
	}
	return nil
}

func (instance *TerminalXBuffer) ChanOutput() chan *TerminalXBufferOutput {
	return instance.chanOutput
}

func (instance *TerminalXBuffer) OnProgress(h ...OnProgressHandler) {
	if nil != instance {
		instance.onProgressHandlers = append(instance.onProgressHandlers, h...)
	}
}

func (instance *TerminalXBuffer) SetInput(p []byte) {
	if nil != instance {
		instance.lastInput = string(p)
	}
}

func (instance *TerminalXBuffer) SetInputString(s string) {
	if nil != instance {
		instance.lastInput = s
	}
}

func (instance *TerminalXBuffer) Write(p []byte) {
	if nil != instance && nil != instance.buff {
		instance.buff.Write(p)
		instance.process()
	}
}

func (instance *TerminalXBuffer) WriteString(s string) {
	if nil != instance && nil != instance.buff {
		instance.buff.WriteString(s)
		instance.process()
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *TerminalXBuffer) process() {
	if nil != instance && nil != instance.buff {
		s := StripAllString(instance.buff.String(), instance.cutSet)
		if len(s) > 0 {
			// ping progress listeners
			go instance.progress()

			idx := strings.Index(s, instance.cursor)
			if idx != -1 {
				before := s[:idx]
				after := s[idx+len(instance.cursor):]
				instance.buff.Reset()
				instance.buff.WriteString(after)

				go instance.found(before, instance.cursor)
			}
		}
	}
}

func (instance *TerminalXBuffer) progress() {
	if nil != instance {
		gg.Recover("TerminalXBuffer.progress")
		for _, h := range instance.onProgressHandlers {
			h()
		}
	}
}

func (instance *TerminalXBuffer) found(before, cursor string) {
	if nil != instance {
		gg.Recover("TerminalXBuffer.progress")
		instance.mux.Lock()
		defer instance.mux.Unlock()

		text := strings.ReplaceAll(strings.TrimSpace(before), strings.TrimSpace(instance.lastInput), "")
		text = strings.TrimSpace(text)

		if nil != instance.chanOutput {
			instance.chanOutput <- &TerminalXBufferOutput{
				Cursor:   cursor,
				Response: []byte(text),
			}
		}
	}
}
