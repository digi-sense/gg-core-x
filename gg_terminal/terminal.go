package gg_terminal

type TerminalHelper struct {
}

var Terminal *TerminalHelper

func init() {
	Terminal = new(TerminalHelper)
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *TerminalHelper) Create(args ...interface{}) (response ITerminal, err error) {
	if nil != instance {
		response, err = CreateTerminal(args...)
	}
	return
}
