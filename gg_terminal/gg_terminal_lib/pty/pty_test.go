package pty_test

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_terminal/gg_terminal_lib/pty"
	"io"
	"os"
	"testing"
	"time"
)

func TestPtyGrep(t *testing.T) {
	term, err := pty.New()
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	defer term.Close()
	c := term.Command("grep", "--color=auto", "bar")
	if err = c.Start(); err != nil {
		t.Error(err)
		t.FailNow()
	}

	go func() {
		_, _ = term.Write([]byte("foo\n"))
		_, _ = term.Write([]byte("bar\n"))
		_, _ = term.Write([]byte("baz\n"))
		_, _ = term.Write([]byte{4}) // EOT
	}()
	go io.Copy(os.Stdout, term)

	if err = c.Wait(); err != nil {
		t.Error(err)
		t.FailNow()
	}
}

func TestPtyPython(t *testing.T) {
	term, err := pty.New()
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	defer term.Close()

	c := term.Command("python")
	if err = c.Start(); err != nil {
		t.Error(err)
		t.FailNow()
	}
	go func() {
		_, _ = term.Write([]byte("credits\n"))
		// _, _ = term.Write([]byte{4}) // EOT
	}()

	go func() {
		time.Sleep(3 * time.Second)
		_, _ = term.Write([]byte("quit()\n"))
	}()

	time.Sleep(500 * time.Millisecond)

	go io.Copy(os.Stdout, term)

	if err = c.Wait(); err != nil {
		t.Error(err)
		t.FailNow()
	}
}
