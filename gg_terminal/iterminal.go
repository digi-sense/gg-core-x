package gg_terminal

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_terminal/gg_terminal_lib/pty"
	"bitbucket.org/digi-sense/gg-core/gg_stoppable"
	"io"
	"os"
	"sync"
	"time"
)

type OnProgressHandler func()
type OnReadHandler func(cursor, text string)

type ITerminal interface {
	Open(args ...string) (err error)
	Close() (err error)
	WaitFinish() (err error)
	SetCommand(args ...string) (err error)
	Options(value ...*TerminalOptions) *TerminalOptions
	OnProgress(h OnProgressHandler)
	OnRead(h OnReadHandler)
	AddOutput(writers ...io.Writer)
	Pid() int
	Write(data []byte) (int, error)
	WriteLine(data []byte) (int, error)
	WriteLineString(data string) (int, error)
	Prompt(prompt string) (response int, err error)
	WaitCursor() (response string)
	WaitResponse() (response []byte)
}

type TerminalOptions struct {
	Cursor string   `json:"cursor"`
	CutSet []string `json:"cutset"`
}

//----------------------------------------------------------------------------------------------------------------------
//	f a c t o r y
//----------------------------------------------------------------------------------------------------------------------

func CreateTerminal(args ...interface{}) (response ITerminal, err error) {
	// parse arguments
	var output []io.Writer
	cmdArgs := make([]string, 0)
	var options *TerminalOptions
	for _, arg := range args {
		if s, ok := arg.(string); ok {
			cmdArgs = append(cmdArgs, s)
			continue
		}
		if w, ok := arg.(io.Writer); ok {
			output = append(output, w)
			continue
		}
		if o, ok := arg.(*TerminalOptions); ok {
			options = o
			continue
		}
		if o, ok := arg.(TerminalOptions); ok {
			options = &o
			continue
		}
	}

	response, err = NewTerminalX(cmdArgs...)
	if nil != err {
		return
	}
	response.AddOutput(output...)
	response.Options(options)
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	t e r m i n a l
//----------------------------------------------------------------------------------------------------------------------

// TerminalX Implementation of generic cross-platform terminal
type TerminalX struct {
	commandStr  string
	commandArgs []string

	closed             bool
	term               pty.IPty
	cmd                *pty.Cmd
	extWriters         []io.Writer
	extWriterMutex     *sync.Mutex
	chanOut            chan []byte
	chanErr            chan error
	_chanCursor        chan string // lazy loading
	_chanResponse      chan []byte // lazy loading
	stoppable          *gg_stoppable.Stoppable
	errors             []error
	tbuff              *TerminalXBuffer
	onProgressHandlers []OnProgressHandler
	onReadHandlers     []OnReadHandler

	_options *TerminalOptions // characters to remove
}

func NewTerminalX(args ...string) (instance *TerminalX, err error) {
	instance = new(TerminalX)
	instance.extWriterMutex = new(sync.Mutex)
	instance.stoppable = gg_stoppable.NewStoppable()
	instance.stoppable.SetName("XTerminal")
	instance.stoppable.OnStart(instance.doOpen)
	instance.stoppable.OnStop(instance.doClose)
	instance.tbuff = NewTerminalXBuffer("\n", nil)
	err = instance.SetCommand(args...)
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *TerminalX) String() string {
	if nil != instance {
		m := make(map[string]interface{})
		m["command"] = instance.commandStr
		m["arguments"] = instance.commandArgs
		m["pid"] = instance.Pid()
		return gg.JSON.Stringify(m)
	}
	return ""
}

func (instance *TerminalX) SetCommand(args ...string) (err error) {
	if nil != instance && len(args) > 0 {
		err = instance.setCommand(true, args...)
	}
	return
}

func (instance *TerminalX) GetName() string {
	if nil != instance && nil != instance.stoppable {
		return instance.stoppable.GetName()
	}
	return ""
}

func (instance *TerminalX) SetName(value string) *TerminalX {
	if nil != instance && nil != instance.stoppable {
		instance.stoppable.SetName("XTerminal(" + value + ")")
	}
	return instance
}

func (instance *TerminalX) Options(value ...*TerminalOptions) *TerminalOptions {
	if nil != instance {
		if len(value) > 0 {
			instance._options = value[0]
		}
		if nil == instance._options {
			instance._options = new(TerminalOptions)
		}
		if nil != instance.tbuff {
			instance.tbuff.cursor = instance._options.Cursor
			instance.tbuff.cutSet = instance._options.CutSet
		}
		return instance._options
	}
	return nil
}

func (instance *TerminalX) AddOutput(writers ...io.Writer) {
	if nil != instance && len(writers) > 0 {
		instance.extWriters = append(instance.extWriters, writers...)
	}
}

func (instance *TerminalX) OnProgress(h OnProgressHandler) {
	if nil != instance {
		instance.onProgressHandlers = append(instance.onProgressHandlers, h)
	}
}

func (instance *TerminalX) OnRead(h OnReadHandler) {
	if nil != instance {
		instance.onReadHandlers = append(instance.onReadHandlers, h)
	}
}

func (instance *TerminalX) Open(args ...string) (err error) {
	if nil != instance && nil == instance.term && nil != instance.stoppable {
		err = instance.setCommand(false, args...)
		if nil != err {
			return
		}

		instance.stoppable.Start()

	}
	return
}

func (instance *TerminalX) Close() (err error) {
	if nil != instance {
		instance.stoppable.Stop()
	}
	return
}

func (instance *TerminalX) Pid() int {
	if nil != instance && nil != instance.cmd && nil != instance.cmd.Process {
		return instance.cmd.Process.Pid
	}
	return 0
}

func (instance *TerminalX) WriteLine(data []byte) (response int, err error) {
	if nil != instance && nil != instance.term {
		data = EnsureLineBreakToBytes(data)
		response, err = instance.Write(data)
	}
	return
}

func (instance *TerminalX) WriteLineString(data string) (response int, err error) {
	if nil != instance {
		data = EnsureLineBreakToString(data)
		response, err = instance.Write([]byte(data))
	}
	return
}

func (instance *TerminalX) Prompt(prompt string) (response int, err error) {
	if nil != instance {
		data := EnsureLineBreakToString(prompt)
		response, err = instance.Write([]byte(data))
	}
	return
}

func (instance *TerminalX) Write(data []byte) (response int, err error) {
	if nil != instance && nil != instance.term {
		if HasLineBreak(data) && nil != instance.tbuff {
			// new input
			instance.tbuff.SetInput(data)
		}
		response, err = instance.term.Write(data)
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	w a i t
//----------------------------------------------------------------------------------------------------------------------

func (instance *TerminalX) WaitFinish() (err error) {
	if nil != instance && nil != instance.cmd {
		err = instance.cmd.Wait()
		if nil != err {
			if err.Error() == "signal: killed" {
				err = nil
			}
		}
	}
	return
}

func (instance *TerminalX) WaitCursor() (response string) {
	if nil != instance {
		if nil == instance._chanCursor {
			instance._chanCursor = make(chan string, 1)
		}
		response = <-instance._chanCursor
		instance._chanCursor = nil
	}
	return
}

func (instance *TerminalX) WaitResponse() (response []byte) {
	if nil != instance {
		if nil == instance._chanResponse {
			instance._chanResponse = make(chan []byte, 1)
		}
		response = <-instance._chanResponse
		instance._chanResponse = nil
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *TerminalX) doOpen() {
	if nil != instance && nil == instance.term {
		var err error

		instance.chanOut = make(chan []byte)
		instance.chanErr = make(chan error)
		instance.errors = make([]error, 0)

		instance.term, err = pty.New()
		if nil != err {
			instance.term = nil
			instance.addError(err)
			return
		}

		if nil != instance.tbuff {
			instance.tbuff.Reset()
			if nil != instance.onProgressHandlers {
				instance.tbuff.OnProgress(instance.onProgressHandlers...)
			}
		}

		instance.cmd = instance.term.Command(instance.commandStr, instance.commandArgs...)
		if nil != instance.cmd {
			err = instance.cmd.Start()
			if nil != err {
				instance.addError(err)
				return
			}
		}

		instance.closed = false
		// start
		go instance.startMainLoop()
	}
	return
}

func (instance *TerminalX) doClose() {
	if nil != instance {
		instance.closed = true
		instance.chanOut = nil
		instance.chanErr = nil

		if nil != instance.term {
			_ = instance.term.Close()
			instance.term = nil
		}
		if nil != instance.cmd {
			if nil != instance.cmd.Process {
				_ = instance.cmd.Process.Kill()
			}
			instance.cmd = nil
		}
		if nil != instance.tbuff {
			instance.tbuff.Reset()
		}
	}
	return
}

func (instance *TerminalX) addError(err error) {
	if nil != instance {
		instance.errors = append(instance.errors, err)
	}
}

func (instance *TerminalX) setCommand(overwrite bool, args ...string) (err error) {
	if nil != instance && len(args) > 0 {
		arguments := make([]string, 0)
		for _, arg := range args {
			a, e := gg.ShellQuote.Split(arg)
			if nil == e {
				arguments = append(arguments, a...)
			}
		}
		// get the full command
		s := gg.ShellQuote.Join(arguments...)
		if len(s) == 0 {
			err = gg.Errors.Prefix(gg.PanicSystemError, "Missing command: ")
			return
		}
		args, err = gg.ShellQuote.Split(s)
		if nil != err {
			return
		}

		if len(instance.commandStr) != 0 {
			if overwrite {
				instance.commandStr = args[0]
				instance.commandArgs = args[1:]
			} else {
				instance.commandArgs = append(instance.commandArgs, args...)
			}
		} else {
			instance.commandStr = args[0]
			instance.commandArgs = args[1:]
		}

		// We have command and arguments here.
		// Now, we should test the command
		err = instance.test(instance.commandStr, instance.commandArgs...)
	}
	return
}

func (instance *TerminalX) test(cmd string, args ...string) (err error) {
	if nil != instance && len(cmd) > 0 {
		// test the command before wrapping it in a virtual terminal
		_, err = gg.Exec.Run(cmd, args...)
	}
	return
}

// ASYNC: read the terminal buffer for outputs
func (instance *TerminalX) startMainLoop() {
	if nil != instance && nil != instance.term && nil != instance.cmd && nil != instance.cmd.Process {
		// avoid the app crash with a recovery function
		gg.Recover("TerminalX.startMainLoop()")

		// init listening output channel
		go instance.listenOut()
		go instance.listenErr()
		go instance.listenBuffer() // buffer filtrated content

		// start internal loop
		for {
			if nil != instance &&
				nil != instance.chanOut &&
				nil != instance.term &&
				nil != instance.cmd &&
				nil != instance.cmd.Process &&
				!instance.closed {
				// read buffer
				buff := make([]byte, 1024)
				n, err := read(instance.term, buff)
				if nil != err {
					if err.Error() != "EOF" {
						// log.Println("ERROR:", err)
						instance.chanErr <- err
					}
				} else {
					data := buff[:n]
					if len(data) > 0 {
						instance.chanOut <- data
					}
				}
			} else {
				// exit loop
				return
			}
		}
	}
}

func (instance *TerminalX) listenOut() {
	for {
		if nil != instance && nil != instance.chanOut {
			select {
			case data := <-instance.chanOut:
				if len(data) > 0 {
					instance.writeToBuffer(data)
					instance.propagateOutputs(data)
				}
			}
		} else {
			return // exit
		}
	}
}

func (instance *TerminalX) listenErr() {
	for {
		if nil != instance && nil != instance.chanOut {
			select {
			case err := <-instance.chanErr:
				if nil != err {
					instance.propagateOutputs(err)
				}
			}
		} else {
			return // exit
		}
	}
}

func (instance *TerminalX) listenBuffer() {
	if nil != instance && nil != instance.tbuff {

		// loop reading the buffer outputs
		for {
			if nil != instance && nil != instance.tbuff {
				select {
				case output := <-instance.tbuff.ChanOutput():
					if nil != output {

						if len(output.Response) > 0 {
							// log.Println("---------> ", "RESPONSE")
							if nil != instance._chanResponse {
								instance._chanResponse <- output.Response
							}
							if len(instance.onReadHandlers) > 0 {
								for _, handler := range instance.onReadHandlers {
									if nil != handler {
										handler(instance.Options().Cursor, string(output.Response))
									}
								}
							}
						}

						// process next request in process queue
						time.Sleep(100 * time.Millisecond)

						if len(output.Cursor) > 0 && nil != instance._chanCursor {
							instance._chanCursor <- output.Cursor
						}
					}
				}
			} else {
				return // exit
			}
		}
	}
}

func (instance *TerminalX) propagateOutputs(i interface{}) {
	if nil != instance && nil != instance.term && len(instance.extWriters) > 0 {
		instance.extWriterMutex.Lock()
		defer instance.extWriterMutex.Unlock()

		for _, out := range instance.extWriters {
			if f, wok := out.(*os.File); wok && f.Name() == "/dev/stderr" {
				if err, ok := i.(error); ok {
					_, _ = f.WriteString(err.Error())
				}
				continue
			}
			if w, wok := out.(io.Writer); wok {
				if b, ok := i.([]byte); ok {
					_, _ = w.Write(b)
				}
				continue
			}
		}
	}
}

func (instance *TerminalX) writeToBuffer(data []byte) {
	if nil != instance {
		instance.extWriterMutex.Lock()
		defer instance.extWriterMutex.Unlock()

		if nil != instance.tbuff {
			clean := StripANSI2(data)
			instance.tbuff.Write(clean)
		}
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func read(reader io.Reader, buff []byte) (n int, err error) {
	if nil != reader && nil != buff {
		n, err = reader.Read(buff)
	}
	return
}
