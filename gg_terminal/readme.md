# Terminal

Wrap a terminal and allow to send commands to it and wait for the cursor.

Tested on Mac and Linux with:
- OLLAMA
- MYSQL
- PYTHON
- 
Should work also with Microsoft Windows.

Usage:

```go
package terminal_test

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_terminal"
	"log"
	"os"
	"testing"
)

func TestTerminalPython(t *testing.T) {
	options := &gg_terminal.TerminalOptions{
		CutSet: nil,
		Cursor: ">>>",
	}
	term, err := gg_terminal.Terminal.Create("python", os.Stderr, options)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	log.Println("Terminal created:", term)
	defer term.Close()

	err = term.Open()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	log.Println("Terminal opened with PID:", term.Pid())

	// WAIT THE CURSOR
	log.Println(term.WaitCursor())

	prompt := "credits"
	log.Println("PROMPT:", prompt)
	_, err = term.Prompt(prompt)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// WAIT RESPONSE TO PROMPT
	log.Println(string(term.WaitResponse()))

	// WAIT THE CURSOR
	log.Println(term.WaitCursor())

	prompt = "copyright"
	log.Println("PROMPT:", prompt)
	_, err = term.WriteLine([]byte(prompt))
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// WAIT RESPONSE TO PROMPT
	log.Println(string(term.WaitResponse()))

	// WAIT THE CURSOR
	log.Println(term.WaitCursor())

	go func() {
		_, err = term.WriteLineString("quit()")
	}()

	err = term.WaitFinish()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	log.Println("Quitting...")
}
```