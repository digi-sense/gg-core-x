package test

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_dbal/gg_arango"
	"fmt"
	"github.com/arangodb/go-driver"
	"testing"
	"time"
)

func TestSimple(t *testing.T) {

	ctext, _ := gg.IO.ReadTextFromFile("config.json")
	psw, _ := gg.IO.ReadTextFromFile("psw.txt")
	config := gg_arango.NewArangoConfig()
	config.Parse(ctext)
	config.Authentication.Password = psw

	conn := gg_arango.NewArangoConnection(config)
	err := conn.Open()
	if nil != err {
		// fmt.Println(err)
		t.Error(err, ctext)
		t.Fail()
		return
	}
	// print version
	fmt.Println("ARANGO SERVER", conn.Server)
	fmt.Println("ARANGO VERSION", conn.Version)
	fmt.Println("ARANGO LICENSE", conn.License)

	// remove
	conn.DropDatabase("test_sample")

	// create a db
	db, err := conn.Database("test_sample", true)
	if nil != err {
		// fmt.Println(err)
		t.Error(err)
	}

	if nil != db {
		fmt.Println(db.Name())
	}

	coll, err := db.Collection("not_exists", true)
	if nil != err {
		t.Error(err)
	}
	if nil == coll {
		t.Fail()
	}

	// entity
	entity := map[string]interface{}{
		"_key":    "258647",
		"name":    "Angelo",
		"surname": "Geminiani",
	}

	Key := gg.Reflect.GetString(entity, "Key")
	fmt.Println("KEY", Key)

	gg.Maps.Set(entity, "Name", "Gian Angelo")

	doc, meta, err := coll.Upsert(entity)
	if nil != err {
		t.Error(err)
	}
	fmt.Println("META", meta)
	fmt.Println("DOC", doc)

	entity = map[string]interface{}{
		"_key":    gg.Rnd.Uuid(),
		"name":    "Marco",
		"surname": gg.Strings.Format("%s", time.Now()),
	}
	doc, meta, err = coll.Upsert(entity)
	if nil != err {
		t.Error(err)
	}
	fmt.Println("META", meta)
	fmt.Println("DOC", doc)

	// bew entity that test upsert used for insert
	newEntity := map[string]interface{}{
		"_key":    gg.Rnd.Uuid(),
		"name":    "I'm new",
		"surname": gg.Strings.Format("%s", time.Now()),
	}
	doc, meta, err = coll.Upsert(newEntity)
	if nil != err {
		t.Error(err)
		t.Fail()
	}
	fmt.Println("META", meta)
	fmt.Println("DOC", doc)

	doc, meta, err = coll.Read(gg.Convert.ToString(doc["_key"]))
	if nil != err {
		t.Error(err)
		t.Fail()
	}
	fmt.Println("META", meta)
	fmt.Println("DOC", doc)

	// remove
	removed, err := conn.DropDatabase("test_sample")
	if nil != err {
		// fmt.Println(err)
		t.Error(err)
	}
	if removed {
		fmt.Println("REMOVED", "test_sample")
	}
}

func TestInsert(t *testing.T) {
	ctext, _ := gg.IO.ReadTextFromFile("config.json")
	config := gg_arango.NewArangoConfig()
	config.Parse(ctext)

	conn := gg_arango.NewArangoConnection(config)
	err := conn.Open()
	if nil != err {
		// fmt.Println(err)
		t.Error(err)
		t.Fail()
		return
	}

	// remove
	conn.DropDatabase("test_sample")

	db, err := conn.Database("test_sample", true)
	if nil != err {
		// fmt.Println(err)
		t.Error(err)
	}
	coll, err := db.Collection("coll_insert", true)
	if nil != err {
		t.Error(err)
	}

	for i := 0; i < 10; i++ {
		// entity
		entity := map[string]interface{}{
			"_key":    gg.Strings.Format("key_%s", i),
			"name":    gg.Strings.Format("Name:%s", i),
			"surname": gg.Strings.Format("Surname:%s", i),
			"address": gg.Strings.Format("Address:%s", i),
		}

		doc, meta, err := coll.Insert(entity)
		if nil != err {
			t.Error(err)
		}
		fmt.Println("META", meta)
		fmt.Println("DOC", doc)
	}

	updEntity := map[string]interface{}{
		"_key": "key_1",
		"name": "Gian Angelo",
	}
	_, _, err = coll.Update(updEntity)
	if nil != err {
		t.Error(err)
	}

	noKeyEntity := map[string]interface{}{
		"name":    "NO KEY",
		"surname": "ZERO ZERO",
		"address": "",
	}
	doc, meta, err := coll.Insert(noKeyEntity)
	if nil != err {
		t.Error(err)
	}
	fmt.Println("---------------------")
	fmt.Println("META", meta)
	fmt.Println("DOC KEY", doc["_key"])
	fmt.Println("DOC", gg.Convert.ToString(doc))
	fmt.Println("---------------------")

	query := "FOR d IN coll_insert RETURN d"
	db.Query(query, nil, gotDocument)

	fmt.Println("---------------------")

}

func TestImport(t *testing.T) {
	ctext, _ := gg.IO.ReadTextFromFile("config.json")
	config := gg_arango.NewArangoConfig()
	config.Parse(ctext)

	conn := gg_arango.NewArangoConnection(config)
	err := conn.Open()
	if nil != err {
		// fmt.Println(err)
		t.Error(err)
		t.Fail()
		return
	}

	// remove
	conn.DropDatabase("test_sample")

	db, err := conn.Database("test_sample", true)
	if nil != err {
		// fmt.Println(err)
		t.Error(err)
	}

	err = db.ImportFile("./toImport.json")
	if nil != err {
		// fmt.Println(err)
		t.Error(err)
	}

	err = db.ImportFile("./toImport.csv")
	if nil != err {
		// fmt.Println(err)
		t.Error(err)
	}

	coll, err := db.Collection("toImport", false)
	if nil != err {
		// fmt.Println(err)
		t.Error(err)
	}
	_, err = coll.EnsureIndex([]string{"name"}, false)
	if nil != err {
		// fmt.Println(err)
		t.Error(err)
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func gotDocument(meta driver.DocumentMeta, doc interface{}, err error) bool {
	fmt.Print("META: ", meta, " ENTITY: ", gg.Convert.ToString(doc), " ERR: ", err)
	m := gg.Convert.ToMap(doc)
	if b, _ := gg.Compare.IsMap(m); b {
		fmt.Printf(" IS MAP: %v\n", b)
	}
	return false // continue
}
