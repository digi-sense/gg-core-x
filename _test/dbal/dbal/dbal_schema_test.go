package _test

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_dbal/gg_dbal_drivers"
	"bitbucket.org/digi-sense/gg-core-x/gg_dbal/gg_dbal_drivers/dbschema"
	"fmt"
	"testing"
)

func TestDbalSchema(t *testing.T) {
	var m map[string]string
	err := gg.JSON.ReadFromFile("./sql_sqlite_testdb.json", &m)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	conn, err := gg_dbal_drivers.NewDatabase(m["driver"], m["dsn"])
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	if nil == conn {
		t.Error("Driver not found")
		t.FailNow()
	}

	driver := conn.(*gg_dbal_drivers.DriverGorm)
	if nil != driver {
		var schema *dbschema.DbSchema
		schema, err = driver.GetSchema()
		if nil != err {
			t.Error(err)
			t.FailNow()
		}

		fmt.Println("SCHEMA: ", schema.String())

		tb1 := schema.Get("table1")
		coll := &dbschema.DbSchemaColumn{
			Name:     "col_custom",
			Nullable: false,
			Type:     "TEXT",
			Tag:      "",
		}
		tb1.Put(coll)

		fmt.Println("SCHEMA: ", schema.String())

		err = driver.AutomigrateSchema(schema)
		if nil != err {
			t.Error(err)
			t.FailNow()
		}

		schema, err = driver.GetSchema()
		if nil != err {
			t.Error(err)
			t.FailNow()
		}

		fmt.Println("SCHEMA AFTER MIGRATION: ", schema.String())
	}
}

func TestDbalModelSchema(t *testing.T) {
	conn, err := getConn()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	driver := conn.(*gg_dbal_drivers.DriverGorm)
	if nil != driver {

	}
}

func getConn() (conn gg_dbal_drivers.IDatabase, err error) {
	var m map[string]string
	err = gg.JSON.ReadFromFile("./sql_sqlite_testdb.json", &m)
	if nil != err {
		return
	}
	conn, err = gg_dbal_drivers.NewDatabase(m["driver"], m["dsn"])
	if nil != err {
		return
	}
	if nil == conn {
		err = gg.Errors.Prefix(gg.PanicSystemError, "Driver not found")
		return
	}
	return
}
