package _test

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_dbal/gg_dbal_drivers"
	"fmt"
	"testing"
)

func TestDriverSqlite(t *testing.T) {
	var m map[string]string
	err := gg.JSON.ReadFromFile("./sql_sqlite.json", &m)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	driver, err := gg_dbal_drivers.NewDatabase(m["driver"], m["dsn"])
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	if nil == driver {
		t.Error("Driver not found")
		t.FailNow()
	}

	items, err := driver.ExecNative("INSERT INTO test (name, surname) VALUES (@name, @surname)",
		map[string]interface{}{
			"name":    "Mario",
			"surname": "Rossi",
		})
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("SELECT * FROM test: ", items)

	items, err = driver.ExecNative("SELECT * FROM test", nil)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("SELECT * FROM test: ", items)

	items, err = driver.ExecNative("SELECT COUNT(id)\nFROM test", nil)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("COUNT: ", items)

	item, err := driver.Upsert("test", map[string]interface{}{"name": "Mariolino"})
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("UPSERT: ", item)

	list, err := driver.Find("test", "name", "Mariolino")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("FIND RESPONSE: ", list)

	items, err = driver.ExecNative("SELECT COUNT(id)\nFROM test", nil)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("COUNT: ", items)

	err = driver.ForEach("test", func(doc map[string]interface{}) bool {
		fmt.Println("FOREACH ITEM: ", gg.JSON.Stringify(doc))
		id := gg.Convert.ToInt(doc["id"])
		if id > 4 {
			// remove
			fmt.Println("\tREMOVING: ", gg.JSON.Stringify(doc))
			err = driver.Remove("test", gg.Convert.ToString(id))
			if nil != err {
				t.Error(err)
				t.FailNow()
			}
		} else {
			doc["surname"] = "Geminiani"
			item, err = driver.Upsert("test", doc)
			if nil != err {
				t.Error(err)
				t.FailNow()
			}
			fmt.Println("\tUPDATED: ", gg.JSON.Stringify(item))
		}
		return false // continue loop
	})
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
}
