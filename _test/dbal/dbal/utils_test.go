package _test

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_dbal/gg_dbal_drivers"
	"fmt"
	"testing"
)

func TestParamNames(t *testing.T) {
	query := "INSERT INTO contacts (name, phone)\nVALUES (@name, @phone);"
	names := gg_dbal_drivers.QueryGetParamNames(query)
	for _, name := range names {
		fmt.Println(name)
	}
}

func TestParamValues(t *testing.T) {
	query := "INSERT INTO contacts (name, phone) VALUES (\"@name\", \"@phone\")"
	bindVars := map[string]interface{}{
		"name":  "Mario Rossi",
		"phone": "+3974746352",
	}
	command, values := gg_dbal_drivers.BindParams(query, bindVars, true)
	fmt.Println("REPLACED: ", command)
	for _, value := range values {
		fmt.Println(value)
	}
	command, values = gg_dbal_drivers.BindParams(query, bindVars, false)
	fmt.Println("NOT REPLACED: ", command)
	for _, value := range values {
		fmt.Println(value)
	}
}
