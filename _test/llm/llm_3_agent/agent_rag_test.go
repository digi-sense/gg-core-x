package llm_3_agent_test

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_prompts_impl/skill_base"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_zagent"
	"fmt"
	"testing"
)

func TestAgentWithFileLong(t *testing.T) {
	// creates an agent
	agent, err := llm_zagent.NewAIAgent("./")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	err = agent.LoadFromFile("./agent_test.json")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	// add some skills
	agent.AddSkill(skill_base.Skill())

	// open agent
	err = agent.Open()
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	// validate the agent
	if len(agent.GetName()) == 0 {
		t.Error("missing agent name")
		t.FailNow()
	}
	if len(agent.GetDrivers()) != 2 {
		t.Error("wrong number of drivers")
		t.FailNow()
	}

	agent.SessionClear(sessionWithDocument)

	// prepare a request
	request := llm_commons.NewLLMRequest()
	request.SkillName = skill_base.UID
	request.PromptName = "basic"       // very basic prompt that uses the user-query as is.
	request.Driver = "simple-llama3.2" // driver nick name
	request.Model = ""
	request.Payload.UserQuery = "Descrivimi le linee guida ESC"
	request.SessionId = sessionWithDocument
	request.VectorId = "" // decide the  internal controller for RAG or not
	request.Payload.SetLang("it")
	// ADD A FILE THAT WILL BE TRANSFORMED IN CONTEXT
	// --------------
	request.ContextualizerAddData("./session_file_long.txt")

	response, err := agent.Submit(request)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	fmt.Println("RESPONSE: ", response.RespText)
	fmt.Println("\t*", "elapsed:", response.Elapsed)
	fmt.Println("\t*", "context:", len(response.RespContext))
	fmt.Println("\t*", "knowledge:", len(response.RespKnowledge))
	fmt.Println("\t*", "knowledge-elements:", response.RespKnowledgeElements)
	fmt.Println("\t*", "session-id:", response.SessionId)
	fmt.Println("\t*", "vector-id:", response.VectorId)

	// exit agent
	agent.Close()
}
