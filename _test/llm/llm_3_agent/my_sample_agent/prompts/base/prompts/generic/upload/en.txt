Consider the next document as part of your knowledge and use it in your answers.
Please, write in {{lang-name}}.

**START DOCUMENT**
{{document}}
**END DOCUMENT**

Please, analyze the document and do this tasks:
1) Create a summary of the document
2) Add a bullet point with the most relevant concepts.
3) Add a bullet point with all entities like name and surnames, companies, addresses, phone numbers, credit card numbers, etc...
4) Add a bullet point with persons cited in the document.
5) Add a bullet point with companies cited in the document.
6) Add bullet point with products, if any, or any other relevant item cited in the document.

Format your answer in Markdown.