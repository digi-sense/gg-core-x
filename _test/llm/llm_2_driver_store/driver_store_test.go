package llm_2_driver_store_test

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_ctrl_drivers"
	"fmt"
	"testing"
)

func TestDriverStore(t *testing.T) {

	drivers, err := llm_ctrl_drivers.Factory.Store().LoadFromFile("options_test.json")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	for _, driver := range drivers {

		driverUID := driver.Uid()

		promptSystem, err := gg.IO.ReadTextFromFile("system.txt")
		if err != nil {
			t.Error(err)
			t.FailNow()
		}
		prompt, err := gg.IO.ReadTextFromFile("prompt.txt")
		if err != nil {
			t.Error(err)
			t.FailNow()
		}

		// build a request
		request := llm_commons.NewLLMRequest(prompt)
		request.Action = llm_commons.ActionGenerate
		// request.Format = "json"
		request.SetOptionMaxTokens(2048)
		request.SetPromptSystem(promptSystem)
		request.Model = driver.GetOptions().Model
		request.Driver = driver.GetOptions().DriverName
		request.Payload.SetLang("it")

		// add some context data
		request.Payload.KBContextSetItems(
			map[string]interface{}{
				"content": "Address of CICCO is: 5th Avenue, New York.",
			},
		)

		fmt.Println("------------------")
		fmt.Println("\t", driverUID)
		fmt.Println("------------------")

		response, err := driver.Submit(request)
		if err != nil {
			t.Error(fmt.Sprintf("Driver '%s' gor error %s", driverUID, err))
		} else {
			fmt.Println(response.RespText)
			fmt.Println("\t*", "driver:", response.Driver)
			fmt.Println("\t*", "model:", response.Model)
			fmt.Println("\t*", "JSON objects:", response.JSONCount())
			fmt.Println("\t*", "context len:", len(response.RespContext))
			fmt.Println("\t*", "elapsed:", response.Elapsed)
		}
	}
}
