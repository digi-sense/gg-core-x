package llm_1_prompts_test

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_prompts/skills"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_prompts_impl/skill_base"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_prompts_impl/skill_copywriter"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_prompts_impl/skill_interpreter"
	"testing"
)

const dirRoot = "./skills"

func TestPrompts(t *testing.T) {
	gg.Paths.SetWorkspacePath(".")

	ctrlSkills, err := skills.NewSkills(dirRoot, "")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// add skills to main controller
	ctrlSkills.Add(skill_base.Skill())
	ctrlSkills.Add(skill_copywriter.Skill())
	ctrlSkills.Add(skill_interpreter.Skill())

	err = ctrlSkills.Deploy()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
}
