package llm_2_driver_test

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_commons"
	"bitbucket.org/digi-sense/gg-core-x/gg_llm/llm_ctrl_drivers"
	"fmt"
	"testing"
)

var drivers = []string{
	llm_ctrl_drivers.DriverOllama,
	// llm_drivers.DriverChatGPT,
}

func TestDrivers(t *testing.T) {

	for _, driverName := range drivers {

		var m map[string]interface{}
		err := gg.JSON.ReadFromFile("options_test.json", &m)
		if err != nil {
			t.Error(err)
			t.FailNow()
		}
		options := gg.Maps.GetMap(m, driverName)
		if len(options) == 0 {
			t.Error("options is empty")
			t.FailNow()
		}

		driver, err := llm_ctrl_drivers.Factory.New(driverName)
		if err != nil {
			t.Error(err)
			t.FailNow()
		}
		driver.GetOptions().Parse(options)

		promptSystem, err := gg.IO.ReadTextFromFile("system.txt")
		if err != nil {
			t.Error(err)
			t.FailNow()
		}
		prompt, err := gg.IO.ReadTextFromFile("prompt.txt")
		if err != nil {
			t.Error(err)
			t.FailNow()
		}

		// build a request
		request := llm_commons.NewLLMRequest(prompt)
		request.Action = llm_commons.ActionGenerate
		// request.Format = "json"
		request.SetOptionMaxTokens(2048)
		request.SetPromptSystem(promptSystem)
		request.Model = driver.GetOptions().Model
		request.Payload.SetLang("it")

		// add some context data
		request.Payload.KBContextSetItems(
			map[string]interface{}{
				"content": "Address of CICCO is: 5th Avenue, New York.",
			},
		)

		fmt.Println("------------------")
		fmt.Println("\t", driverName)
		fmt.Println("------------------")

		response, err := driver.Submit(request)
		if err != nil {
			t.Error(fmt.Sprintf("Driver '%s' gor error %s", driverName, err))
		} else {
			fmt.Println(response.RespText)
			fmt.Println("\t*", "driver:", response.Driver)
			fmt.Println("\t*", "model:", response.Model)
			fmt.Println("\t*", "JSON objects:", response.JSONCount())
			fmt.Println("\t*", "context len:", len(response.RespContext))
			fmt.Println("\t*", "elapsed:", response.Elapsed)
		}
	}
}
