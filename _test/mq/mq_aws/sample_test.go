package test

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_mq/mq_drivers/driver_mqtt_aws"
	MQTT "github.com/eclipse/paho.mqtt.golang"
	"log"
	"testing"
	"time"
)

func TestSubscriber(t *testing.T) {
	filename := gg.Paths.Absolute("./config.json")
	config, err := driver_mqtt_aws.NewAWSIoTConnectionConfig(filename)
	if err != nil {
		t.Error(err)
	}
	connection, err := driver_mqtt_aws.NewAWSIoTConnection(config)
	if err != nil {
		t.Error(err)
	}
	err = connection.Open()
	if err != nil {
		t.Error(err)
	}
	defer connection.Close()

	go func() {
		err = connection.SubscribeWithHandler("acy/project/19/+/ecg", 0, func(client MQTT.Client, message MQTT.Message) {
			log.Println("TOPIC: ", message.Topic(), "PAYLOAD:", string(message.Payload()))
		})
		if err != nil {
			t.Error(err)
		}
	}()

	// wait a while for subscription
	time.Sleep(100 * time.Millisecond)

	if err != nil {
		t.Error(err)
	}

	message := map[string]interface{}{
		"message": "Hello",
	}
	err = connection.Publish("acy/project/19/hola/ecg", gg.JSON.Stringify(message), 0)
	if err != nil {
		t.Error(err)
	}

	time.Sleep(1 * time.Second)
}
