package chrome

import (
	"bitbucket.org/digi-sense/gg-core"
	ggx "bitbucket.org/digi-sense/gg-core-x"
	"log"
	"strings"
	"testing"
)

func TestChromeBasic(t *testing.T) {
	err := ggx.Chrome.Test()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
}

func TestChromeFullScreenshot(t *testing.T) {
	bytes, err := ggx.Chrome.FullScreenshot("https://ggtechnologies.sm")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	_, err = gg.IO.WriteBytesToFile(bytes, "./full-screenshot.jpg")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
}

func TestChromeScreenshot(t *testing.T) {
	bytes, err := ggx.Chrome.Screenshot("https://ggtechnologies.sm")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	_, err = gg.IO.WriteBytesToFile(bytes, "./screenshot.jpg")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
}

func TestChromeElementScreenshot(t *testing.T) {
	bytes, err := ggx.Chrome.ElementScreenshot("https://pkg.go.dev/", "img.Homepage-logo")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	_, err = gg.IO.WriteBytesToFile(bytes, "./element-screenshot.jpg")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
}

func TestChromePDF(t *testing.T) {
	bytes, err := ggx.Chrome.ToPDF("https://ggtechnologies.sm")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	_, err = gg.IO.WriteBytesToFile(bytes, "./site.pdf")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
}

func TestChromeEval(t *testing.T) {
	url := `https://ggtechnologies.sm/tests/test.html`
	script := `Object.keys(window);`

	response, err := ggx.Chrome.Eval(url, script)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	res := gg.Convert.ToArrayOfString(response)
	for _, item := range res {
		if strings.Index(item, "_") == 0 {
			log.Println("\t", item)
		}
	}
}

func TestChromeGetWindowVariable(t *testing.T) {
	url := `https://ggtechnologies.sm/tests/test.html`
	variable := `_wf`

	res, err := ggx.Chrome.GetWindowVariable(url, variable)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	log.Println(nil != res)
}
