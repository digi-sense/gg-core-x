package _test

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_i18n_bundle_mustache"
	"fmt"
	"testing"
)

func TestTemplate(t *testing.T) {
	templateName := "mustache_template.html"
	modelName := "mustache_model.json"

	html, err := gg.IO.ReadTextFromFile("./template/" + templateName)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	if len(html) == 0 {
		t.Error("HTML file should not be empty")
		t.FailNow()
	}

	model, err := gg.JSON.ReadMapFromFile("./template/" + modelName)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(gg.JSON.Stringify(model))

	localizer, err := gg.I18N.NewLocalizer("en", "./template")
	localizer.SetRenderer(new(gg_i18n_bundle_mustache.RendererMustache))
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("LOCALIZER", localizer)
	localized, err := localizer.Localize("it", html, model)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(localized)
}
