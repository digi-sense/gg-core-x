package scripting

import (
	"bitbucket.org/digi-sense/gg-core"
	ggx "bitbucket.org/digi-sense/gg-core-x"
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting"
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting/commons"
	"fmt"
	"log"
	"testing"
	"time"
)

func Test_Simple(t *testing.T) {
	gg.Paths.SetWorkspacePath(gg.Paths.Absolute("./")) // look for modules in this folder

	response, err := ggx.Scripting.Run(&gg_scripting.EnvSettings{
		FileName: "./js/simple.js",
		Logger:   nil, //logger,
		LogReset: true,
		OnReady: func(rtContext *commons.RuntimeContext) {
			EnableSimpleModule(rtContext)
		},
	})
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("Script returned: ", response)
	time.Sleep(3 * time.Second)
}

func Test_DBAL(t *testing.T) {
	gg.Paths.SetWorkspacePath(gg.Paths.Absolute("./")) // look for modules in this folder

	response, err := ggx.Scripting.Run(&gg_scripting.EnvSettings{
		FileName: "./js/dbal.js",
		Logger:   nil, //logger,
		LogReset: true,
		OnReady: func(rtContext *commons.RuntimeContext) {
			// EnableSimpleModule(rtContext)
		},
	})
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("Script returned: ", response)
	time.Sleep(3 * time.Second)
}

func Test_Dataware(t *testing.T) {
	gg.Paths.SetWorkspacePath(gg.Paths.Absolute("./")) // look for modules in this folder

	response, err := ggx.Scripting.Run(&gg_scripting.EnvSettings{
		FileName: "./js/dataware.js",
		Logger:   nil, //logger,
		LogReset: true,
		OnReady: func(rtContext *commons.RuntimeContext) {
			// EnableSimpleModule(rtContext)
		},
	})
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("Script returned: ", response)
	time.Sleep(3 * time.Second)
}

func Test_Method(t *testing.T) {
	gg.Paths.SetWorkspacePath(gg.Paths.Absolute("./")) // look for modules in this folder

	program, err := ggx.Scripting.Build(&gg_scripting.EnvSettings{
		FileName: "./js/main.js",
		Logger:   nil, //logger,
		LogReset: true,
		OnReady: func(rtContext *commons.RuntimeContext) {

		},
	})
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	var response interface{}
	response, err = program.Run(gg_scripting.NewProgramMethod("main", "param1", 123))
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	log.Println("Script returned: ", response)

	response, err = program.Run(gg_scripting.NewProgramMethod("main", "HELLO", "INVOCATION"))
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	log.Println("Script returned: ", response)

	response, err = program.Run(
		gg_scripting.NewProgramMethod("main", "A", 1),
		gg_scripting.NewProgramMethod("main", "B", 2),
	)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	log.Println("Script returned: ", response)

	response, err = program.RunSequence(
		gg_scripting.NewProgramMethod("main", "A", 1),
		gg_scripting.NewProgramMethod("main", "B", 2),
	)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	log.Println("Script returned: ", response)

	time.Sleep(1 * time.Second)
}
