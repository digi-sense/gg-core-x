(function () {
    const crypto = require("crypto-utils");

    const response = main();
    if (!!response["error"]) {
        console.error(response["error"]);
    }
    return response;


    function unwrapSource() {
        const source = "H4sIAAAAAAAAA3WU260kMQhEU9kAdiTe4FhWm38aFxfzd81XiaPGYKj2v0/6qb9/PiknrjD5lSdkIsC0ljjKG4wOrxQjirPDMkfeQQUR2mDyHH1MEaHeAlkAZUR1g12IJj0GnhWm3bwkKXxydhhyu+4BXjGaa74gO3WCiPVYYs74jYqzSXff/TKm+5t8zPmW7lXkHTEb+dkxu8jEoveKLDa6cTLM4FDdsL6nPGkmLppliHz28oT2lZNY8ljsBSMJg7WAaPEKFbbtaUMydYWmWLIo7CCcG/QT11SeWJ07zPiGMy4nuNCCa4MmeaFmJERWqIZelPDTSKDBJxTKK3xovIVLP2HvF1DREqv6Cn0MkK445ezwTBOMetKr3qAS3hSVxB28aoWJ3eqBe03YN9i9Y4JRt5AV5woj8Wec2QrDbk/oUrNUPBjerl1hMWAU8pxkhZo+pzAKjYne0L4JiGh89oJWGKslfi7Ls0PHs9O/AwZCGPIb9gauWAAaNv2GYoA0hYRihe19wHmxuGSHicjQmSXpDmFaY/8+q/WF/38A4xxO+tgGAAA=";
        // const source = "H4sIAAAAAAAAA3WU260kMQhEU9kAdiTe4FhWm38aFxfzd81XiaPGYKj2v0/6qb9/PiknrjD5lSdkIsC0ljjKG4wOrxQjirPDMkfeQQUR2mDyHH1MEaHeAlkAZUR1g12IJj0GnhWm3bwkKXxydhhyu+4BXjGaa74gO3WCiPVYYs74jYqzSXff/TKm+5t8zPmW7lXkHTEb+dkxu8jEoveKLDa6cTLM4FDdsL6nPGkmLppliHz28oT2lZNY8ljsBSMJg7WAaPEKFbbtaUMydYWmWLIo7CCcG/QT11SeWJ07zPiGMy4nuNCCa4MmeaFmJERWqIZelPDTSKDBJxTKK3xovIVLP2HvF1DREqv6Cn0MkK445ezwTBOMetKr3qAS3hSVxB28aoWJ3eqBe03YN9i9Y4JRt5AV5woj8Wec2QrDbk/oUrNUPBjerl1hMWAU8pxkhZo+pzAKjYne0L4JiGh89oJWGKslfi7Ls0PHs9O/AwZCGPIb9gauWAAaNv2GYoA0hYRihe19wHmxuGSHicjQmSXpDmFaY/8+q/WF/38A4xxO+tgGAAA=";
        console.info("--------------------------------", source);

        const response = {};
        try {
            const unwrapped = crypto.unwrapGZipToText(source); // array of numbers
            console.info("unwrapped", unwrapped);
            response["source"] = source;
            response["unwrapped"] = unwrapped;
        } catch (err) {
            response["error"] = err.message;
        }
        return response;
    }

    function wrapUnwrapTest() {
        const source = "[1,2,3,4,5,6,7,8,9]";
        console.info(source, "--------------------------------");

        const wrapped = crypto.wrapGZipToText(JSON.stringify(source));
        console.info("wrapGZipToText", wrapped);

        const unwrapped = crypto.unwrapGZipToText(wrapped); // array of numbers
        console.info("unwrapGZipToText", unwrapped);

        return {
            "source": source,
            "wrapped": wrapped,
            "unwrapped": unwrapped
        };
    }


    function main() {
        const response = {};
        try {

            response["unwrap-source"] = unwrapSource();
            response["wrap-unwrap"] = wrapUnwrapTest();

            return response;
        } catch (err) {
            response["error"] = err.message;
        }
        return response;
    }
})();