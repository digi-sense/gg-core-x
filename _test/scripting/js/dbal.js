(function () {
    try {
        //-- REQUIRE --//
        const dbal = require("dbal");

        //-- MAIN --//
        try {
            const conn = dbal.create("sqlite", "./data/test.db");
            console.info("Connection to DB opened at: " + new Date());

            let size = count(conn);
            console.info("TOTAL RECORDS: ", size);

            // save schema to json file
            schemaSave(conn, "./data/scheme.test.json");
            // update schema to json file
            schemaUpdate(conn, "./data/scheme.test_new.json");
            if (conn.schema.enabled) {
                console.info("SCHEMA: " + JSON.stringify(conn.schema.get()));
            }

            // get all data, write log and then remove
            data = getAll(conn);
            if (data.length > 0) {
                console.info("Found existing records: ", data.length);
                // loop on data
                for (item of data) {
                    console.info("\t ", JSON.stringify(item));
                }
            }

            // REMOVE
            // remove all data
            if (data.length > 0) {
                for (item of data) {
                    removed = remove(conn, item);
                    console.info("\t Removed: ", JSON.stringify(removed));
                }
            }

            // insert some data
            for (let i = 0; i < 30; i++) {
                const item = {
                    "name": "Rnd_" + i,
                    "description": "Created at " + new Date(),
                }
                upsert(conn, item);
                console.info("\t Inserted: ", JSON.stringify(item));
            }

            // update data
            data = getAll(conn);
            if (data.length > 0) {
                console.info("Updating records: ", data.length);
                // loop on data
                for (item of data) {
                    item["note"] = "ID=" + item["id"] + " updated at: " + new Date();
                    upsert(conn, item);
                    console.info("\t ", JSON.stringify(item));
                }
            }

            // count final result
            size = count(conn);
            console.info("TOTAL RECORDS: ", size);
        } catch (err) {
            console.error("Error in main function", err);
        }

        //-- FUNCTIONS --//

        /**
         *Save database schema to file
         * @param conn Valid database connection
         * @param filename Output file name
         */
        function schemaSave(conn, filename) {
            if (!!conn) {
                if (conn.schema.enabled) {
                    path = conn.schema.save(filename);
                    console.info("Saving schema into: " + path);
                    return true;
                } else {
                    console.error("schemaSave() -> Schema is not enabled on this driver")
                }
            } else {
                console.error("schemaSave() -> Invalid database connection!")
            }
            return false;
        }

        function schemaUpdate(conn, filename) {
            if (!!conn) {
                if (conn.schema.enabled) {
                    // update from json file
                    const path = conn.schema.update(filename);
                    console.info("Update schema from: " + path);

                    // update from schema object
                    const coll = {"name": "manual_insert_col", "nullable": true, "type": "TEXT", "tag": ""}
                    conn.schema.addColumn("table1",coll);

                    return true;
                } else {
                    console.error("schemaUpdate() -> Schema is not enabled on this driver")
                }
            } else {
                console.error("schemaUpdate() -> Invalid database connection!")
                return [];
            }
        }

        function getAll(conn) {
            if (!!conn) {
                let response = [];
                try {
                    query = "SELECT * FROM table1";
                    params = undefined;
                    response = conn.exec(query, params)
                } catch (err) {
                    console.error("getAll() -> Error:", err)
                }
                return response;
            } else {
                console.error("getAll() -> Invalid database connection!")
                return [];
            }
        }

        function remove(conn, item) {
            if (!!conn) {
                let response = {};
                try {
                    query = "DELETE FROM table1 WHERE id=@id";
                    params = {id: item["id"]};
                    conn.exec(query, params)
                    response = item;
                } catch (err) {
                    console.error("remove() -> Error:", err)
                }
                return response;
            } else {
                console.error("remove() -> Invalid database connection!")
                return {};
            }
        }

        function upsert(conn, item) {
            if (!!conn) {
                let response = {};
                try {
                    response = conn.upsert("table1", item)
                } catch (err) {
                    console.error("upsert() -> Error:", err)
                }
                return response;
            } else {
                console.error("upsert() -> Invalid database connection!")
                return [];
            }
        }

        function count(conn) {
            if (!!conn) {
                let response = 0;
                try {
                    const query = "SELECT COUNT(id)\nFROM table1";
                    response = conn.exec(query);
                } catch (err) {
                    console.error("count() -> Error:", err)
                }
                return response;
            } else {
                console.error("count() -> Invalid database connection!")
                return -1;
            }
        }


    } catch (err) {
        console.error("Error in launch function", err);
    }
})();