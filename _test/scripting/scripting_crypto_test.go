package _test

import (
	"bitbucket.org/digi-sense/gg-core"
	ggx "bitbucket.org/digi-sense/gg-core-x"
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting"
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting/commons"
	"log"
	"testing"
	"time"
)

func TestCryptoEncoders(t *testing.T) {

	gg.Paths.SetWorkspacePath(gg.Paths.Absolute("./")) // look for modules in this folder

	program, err := ggx.Scripting.Build(&gg_scripting.EnvSettings{
		FileName: "./js/crypto.js",
		Logger:   nil, //logger,
		LogReset: true,
		OnReady: func(rtContext *commons.RuntimeContext) {

		},
	})
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	var response interface{}
	response, err = program.Run()
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	log.Println("response", response)

	time.Sleep(1 * time.Second)
}
