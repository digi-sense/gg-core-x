package _test

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x"
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting"
	"bitbucket.org/digi-sense/gg-core/gg_log"
	"fmt"
	"testing"
	"time"
)

func Test_fs(t *testing.T) {
	const SCRIPT = `
	(function(){
		var test = require("./modules/test_fs.js");
		return test.run();
	})();
	
	`

	expected := map[string]string{
		"exists": "true",
	}

	registry := gg_scripting.NewModuleRegistry(func(path string) ([]byte, error) {
		return gg.IO.ReadBytesFromFile(path)
	})

	vm1 := ggx.Scripting.NewEngine("javascript")
	vm1.Name = "fs" // creates log file vm1.log
	registry.Start(vm1)

	v, err := vm1.RunScript(SCRIPT)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	response := v.String()
	if gg.Regex.IsValidJsonObject(response) {
		m := make(map[string]interface{})
		err = gg.JSON.Read(response, &m)
		if err != nil {
			t.Error(err)
		}

		for k, v := range expected {
			value := gg.Reflect.GetString(m, k)
			if value != v {
				t.Error("Expected: " + v + " but got " + value)
			}
		}
	} else {
		t.Error(response)
	}

	time.Sleep(5 * time.Second)
}

func Test_fs2(t *testing.T) {
	gg.Paths.SetWorkspacePath(gg.Paths.Absolute("./")) // look for modules in this folder

	logger := gg_log.NewLogger()
	logger.SetFilename(gg.Paths.WorkspacePath("custom_fs.log"))

	response, err := ggx.Scripting.Run(&gg_scripting.EnvSettings{
		FileName: "./fs.js",
		Logger:   nil, //logger,
		LogReset: true,
	})
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(response)
}

func Test_err(t *testing.T) {
	gg.Paths.SetWorkspacePath(gg.Paths.Absolute("./")) // look for modules in this folder
	response, err := ggx.Scripting.RunFile("./fs2.js", nil)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(response)
}
