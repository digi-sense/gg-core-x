package vfs_updater

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs_updater"
	"log"
	"testing"
)

func TestVfsUpdater(t *testing.T) {
	gg.Paths.SetWorkspacePath(".")
	updater, err := gg_vfs_updater.NewVfsUpdater("vfs_updater_test_locale.json")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	updater.SetRoot("./updater/update_root")
	updater.OnUpdate(func(from, to, filename string, err error) {
		if err != nil {
			t.Error(err)
			t.FailNow()
		}
		log.Println("\t", from, to, filename)
	})
	updater.OnError(func(err error) {
		t.Error(err)
		t.FailNow()
	})

	updater.OnReady(func() {
		log.Println(updater)
		log.Println("Package Files: ", len(updater.PackageFiles()))
		log.Println("Curr Version: ", updater.CurrVersion())
		log.Println("Remote Version: ", updater.RemoteVersion())

	})
	updater.OnAfterUpdate(func(from, to string, files []string) {
		log.Println("UPDATED END!", from, to, files)
	})
	// start
	updater.Start()

	updater.Join()
}

func TestVfsUpdaterFTP(t *testing.T) {
	gg.Paths.SetWorkspacePath(".")
	updater, err := gg_vfs_updater.NewVfsUpdater("vfs_updater_test_ftp.json")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	updater.SetRoot("./updater/update_root")
	updater.SetKeepConnected(true) // resilient to network errors
	updater.OnUpdate(func(from, to, filename string, err error) {
		if err != nil {
			t.Error(err)
			t.FailNow()
		}
		log.Println(from, to, filename)
	})
	updater.OnError(func(err error) {
		t.Error(err)
		t.FailNow()
	})
	updater.OnReady(func() {
		log.Println("Ready!!!!!!!")
		log.Println(updater)
		log.Println("Package Files: ", len(updater.PackageFiles()))
		log.Println("Curr Version: ", updater.CurrVersion())
		log.Println("Remote Version: ", updater.RemoteVersion())
	})
	// start
	updater.Start()

	updater.Join()
}
