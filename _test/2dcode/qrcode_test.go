package _test

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_2dcode"
	"bitbucket.org/digi-sense/gg-core-x/gg_2dcode/commons"
	"fmt"
	"testing"
)

func TestGenerator(t *testing.T) {
	generator := gg_2dcode.NewGenerator(commons.BarcodeFormat_QR_CODE)
	bytes, err := generator.Encode("http://www.gianangelogeminiani.me",
		&commons.OptionSize{Width: 300},
		// &commons.OptionCircleShapes{},
		&commons.OptionImageLogo{Filename: "./icon.png"})
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	_, err = gg.IO.WriteBytesToFile(bytes, "./qrcode.png")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
}

func TestScanner(t *testing.T) {
	scanner := gg_2dcode.NewScanner(commons.BarcodeFormat_QR_CODE)
	text, err := scanner.Decode("./qrcode.png")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(text)
}
