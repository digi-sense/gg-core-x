package _test

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_2dcode"
	"bitbucket.org/digi-sense/gg-core-x/gg_2dcode/commons"
	"fmt"
	"testing"
)

func TestEan13(t *testing.T) {
	DATA := "1234567890128"
	FORMAT := commons.BarcodeFormat_EAN_13
	FILE := "./ean13.png"


	// GENERATOR
	generator := gg_2dcode.NewGenerator(FORMAT)
	bytes, err := generator.Encode(DATA,
		&commons.OptionSize{Height: 50})
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	_, err = gg.IO.WriteBytesToFile(bytes, FILE)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// SCANNER
	scanner := gg_2dcode.NewScanner(FORMAT)
	text, err := scanner.Decode(FILE)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	if DATA!=text{
		t.Error(fmt.Sprintf("Expected: '%v' got '%v'" , DATA, text))
		t.FailNow()
	}
	fmt.Println(text)
}

