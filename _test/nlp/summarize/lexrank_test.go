package summarize

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_nlp/nlp_summarize"
	"fmt"
	"os"
	"testing"
)

func TestLexRankSummarize(t *testing.T) {
	intoSentences := 2
	textB, err := os.ReadFile("./textlong.txt")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	text := string(textB)
	//intoSentences = nlp_summarize.HighSummarySentences(text)
	summarizer := nlp_summarize.NewSummarizer()
	//summarizer.Algorithm = lygo_nlpsumlexrank.AlgCentrality
	result, err := summarizer.Summarize(text, intoSentences)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	for _, row := range result {
		fmt.Println("\n", row)
	}

}
