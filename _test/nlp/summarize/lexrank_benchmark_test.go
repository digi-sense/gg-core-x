package summarize

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_nlp/nlp_summarize"
	"fmt"
	"os"
	"testing"
)

const (
	SAMPLE_FILE_PATH = "./text.txt"
	NUM_SENTENCES    = 3
)

var (
	benchSummarizer *nlp_summarize.Summarizer
	benchText       string
	resultText      []string
)

func init() {
	raw, err := os.ReadFile(SAMPLE_FILE_PATH)
	if err != nil {
		panic(err)
	}
	benchText = string(raw)
}

func BenchmarkSummarizeCentralityHamming(b *testing.B) {
	var rtxt []string

	for n := 0; n < b.N; n++ {
		benchSummarizer = nlp_summarize.NewSummarizer()
		benchSummarizer.Algorithm = "centrality"
		benchSummarizer.Weighing = "hamming"
		rtxt, _ = benchSummarizer.Summarize(benchText, NUM_SENTENCES)
	}

	resultText = rtxt

	fmt.Println(rtxt)
}

func BenchmarkSummarizeCentralityJaccard(b *testing.B) {
	var rtxt []string

	for n := 0; n < b.N; n++ {
		benchSummarizer = nlp_summarize.NewSummarizer()
		benchSummarizer.Algorithm = "centrality"
		benchSummarizer.Weighing = "jaccard"
		rtxt, _ = benchSummarizer.Summarize(benchText, NUM_SENTENCES)
	}

	resultText = rtxt
}

func BenchmarkSummarizePagerankHamming(b *testing.B) {
	var rtxt []string

	for n := 0; n < b.N; n++ {
		benchSummarizer = nlp_summarize.NewSummarizer()
		benchSummarizer.Algorithm = "pagerank"
		benchSummarizer.Weighing = "hamming"
		rtxt, _ = benchSummarizer.Summarize(benchText, NUM_SENTENCES)
	}

	resultText = rtxt
}

func BenchmarkSummarizePagerankJaccard(b *testing.B) {
	var rtxt []string

	for n := 0; n < b.N; n++ {
		benchSummarizer = nlp_summarize.NewSummarizer()
		benchSummarizer.Algorithm = "pagerank"
		benchSummarizer.Weighing = "jaccard"
		rtxt, _ = benchSummarizer.Summarize(benchText, NUM_SENTENCES)
	}

	resultText = rtxt
}
