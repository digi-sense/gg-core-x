package classifier

import (
	"bitbucket.org/digi-sense/gg-core"
	ggx "bitbucket.org/digi-sense/gg-core-x"
	"bitbucket.org/digi-sense/gg-core-x/gg_nlp/nlp_classifier"
	"fmt"
	"strings"
	"testing"
)

func TestCategorize(t *testing.T) {
	text, err := gg.IO.ReadTextFromFile("./text.txt")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	soptions, err := gg.IO.ReadTextFromFile("./options.json")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	options, err := nlp_classifier.ParseOptionsFromJson(soptions)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	fmt.Println("START CATEGORIZING TEXT: ")
	fmt.Println("\t", "Match Level:", options.MatchLevel)
	fmt.Println("\t", "Categories:", len(options.Categories))
	for _, cat := range options.Categories {
		fmt.Println("\t\t", cat.Name, strings.Join(cat.Tags, ", "))
	}

	classifier := ggx.NLP.Classifier()
	out, err := classifier.Classify(text, options)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	fmt.Println("CATEGORIZING RESPONSE: ")
	fmt.Println(out)
}
