package nlp

import (
	"bitbucket.org/digi-sense/gg-core"
	ggx "bitbucket.org/digi-sense/gg-core-x"
	"fmt"
	"testing"
)

func TestNlpTokenizer(t *testing.T) {
	text := "word longggggggggggg longggggggggggg and short, o q w e rrr "
	fmt.Println("TEXT:", text)

	keywords := ggx.NLP.GetKeywordsSorted(text, 3)
	fmt.Println("\tGetKeywordsSorted (min len 3):", keywords)

	km := ggx.NLP.MapKeywords(text, 3)
	fmt.Println("\tMapKeywords:", km)

	kk := ggx.NLP.ListKeywords(text, 3, true, true)
	fmt.Println("\tList Keywords:", kk)
	kk = ggx.NLP.ListKeywordsWithMinWeight(text, 3, 2, true, true)
	fmt.Println("\tList Keywords:", kk)

	expressions := []string{"longggggggg*", "cat*"}
	score := ggx.NLP.ScoreBest(text, expressions)
	fmt.Println("\tScoreBest:", score, expressions)
	expressions = []string{"w??*", "longggggggg*"}
	score = ggx.NLP.ScoreBest(text, expressions)
	fmt.Println("\tScoreBest:", score, expressions)

	expressions = []string{"longggggggg*", "cat*"}
	score = ggx.NLP.ScoreAnd(text, []string{"longggggggg*", "cat*"})
	fmt.Println("\tScoreAll:", score, expressions)
	expressions = []string{"w??*", "longggggggg*"}
	score = ggx.NLP.ScoreAnd(text, expressions)
	fmt.Println("\tScoreAll:", score)

	expressions = []string{"longggggggg*", "cat*"}
	score = ggx.NLP.ScoreOr(text, expressions)
	fmt.Println("\tScoreAny:", score)
	expressions = []string{"w??*", "longggggggg*"}
	score = ggx.NLP.ScoreOr(text, expressions)
	fmt.Println("\tScoreAny:", score, expressions)
}

func TestNlpKeywords(t *testing.T) {
	text := "word longggggggggggg longggggggggggg and short, o q w e rrr verrà é òççççççççccccc"
	fmt.Println("TEXT:", text)

	kk := ggx.NLP.ListKeywords(text, 3, true, true)
	fmt.Println("\tList Keywords:", kk)
}

func TestLanguageDetection(t *testing.T) {
	text := "Ciao, questa è una frase in italiano. Non confonderla col catalano"
	lang := ggx.NLP.GetLanguageCode(text)
	if lang != "ita" {
		t.Error("Expected 'ita', got '" + lang + "'")
		t.FailNow()
	}
	fmt.Println(lang)

	code1 := ggx.NLP.LanguageDetector().GetLanguageISO(lang).Code1
	code2 := ggx.NLP.LanguageDetector().GetLanguageISO(lang).Code2
	name := ggx.NLP.LanguageDetector().GetLanguageISO(lang).Name
	fmt.Println(code1, code2, name)
}

func TestEntitiesDetection(t *testing.T) {
	text, err := gg.IO.ReadTextFromFile("./text.txt")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	response := ggx.NLP.GetEntitiesAll(text)
	fmt.Println("entities", response)
	lang := ggx.NLP.GetLanguageCode(text)
	fmt.Println("Language", lang)
}
