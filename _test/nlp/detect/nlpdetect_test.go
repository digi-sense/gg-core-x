package detect

import (
	ggx "bitbucket.org/digi-sense/gg-core-x"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"testing"
)

func TestDetectLang(t *testing.T) {

	fisxtureFile, e := os.ReadFile("test.json")
	if e != nil {
		log.Printf("File error: %v\n", e)
		os.Exit(1)
	}

	var fisxtureData map[string]interface{}
	err := json.Unmarshal(fisxtureFile, &fisxtureData)
	if e != nil {
		log.Printf("Error during languages decoding: %v\n", err)
		os.Exit(1)
	}
	detector := ggx.NLP.LanguageDetector()
	for code, values := range fisxtureData {
		for _, v := range values.([]interface{}) {
			res := detector.DetectOne(v.(string))
			if code != res.Code {
				t.Errorf("FixtureCode:%s  DetectedCode:%s", code, res.Code)
			}
		}
	}
}

func TestGetLanguageISO(t *testing.T) {
	detector := ggx.NLP.LanguageDetector()
	v := detector.GetLanguageISO("sco")
	fmt.Println(v)

	fmt.Println(detector.GetLanguageISOName("ita"))
	fmt.Println(detector.GetLanguageISO("ita"))
	fmt.Println(detector.GetLanguageISOName("ru"))
}
