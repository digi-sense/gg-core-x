package terminal_test

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_terminal"
	"fmt"
	"log"
	"os"
	"sync"
	"testing"
)

func TestTerminalOllamaList(t *testing.T) {
	term, err := gg_terminal.Terminal.Create("ollama list", os.Stdout, os.Stderr)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	log.Println("Terminal created:", term)
	defer term.Close()

	err = term.Open()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	log.Println("Terminal opened with PID:", term.Pid())

	err = term.WaitFinish()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	log.Println("Quitting...")
}

var IDLE = []string{"\r\n... ", "⠙", "⠹", "⠸", "⠼", "⠴", "⠦", "⠧", "⠇", "⠏", "⠋", "⠙", "⠹", "⠸", "⠼", "⠴"}

func TestTerminalOllamaRun(t *testing.T) {
	err := ollamaRunInstance(0, nil)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
}

func TestTerminalOllamaRunMultiple(t *testing.T) {
	wg := new(sync.WaitGroup)
	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func(id int, w *sync.WaitGroup) {
			err := ollamaRunInstance(id, w)
			if nil != err {
				t.Error(err)
			}
		}(i, wg)
	}
	wg.Wait()
}

func ollamaRunInstance(id int, wg *sync.WaitGroup) (err error) {
	if nil != wg {
		defer wg.Done()
	}
	uid := fmt.Sprintf("[-%v-]", id)
	options := &gg_terminal.TerminalOptions{
		CutSet: IDLE,
		Cursor: ">>> Send a message (/? for help)",
	}
	term, err := gg_terminal.Terminal.Create("ollama run llama3", os.Stderr, options)
	if nil != err {
		return
	}
	log.Println(uid, "Terminal created:", term)
	defer term.Close()

	// open the terminal
	err = term.Open()
	if nil != err {
		return
	}
	log.Println(uid, "Terminal opened with PID:", term.Pid())

	// WAIT THE CURSOR
	log.Println(uid, term.WaitCursor())

	// SEND A PROMPT
	prompt := "Scrivimi una pagina HTML che contiene il testo 'ciao mondo!!!'"
	log.Println(uid, "PROMPT:", prompt)
	_, err = term.WriteLineString(prompt)
	if nil != err {
		return
	}

	// WAIT RESPONSE TO PROMPT
	log.Println(uid, string(term.WaitResponse()))

	// WAIT THE CURSOR
	log.Println(uid, term.WaitCursor())

	go term.Close()

	// ----------------------------------
	err = term.WaitFinish()
	if nil != err {
		return
	}
	log.Println(uid, "Quitting...")

	return
}
