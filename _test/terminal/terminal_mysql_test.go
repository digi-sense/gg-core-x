package terminal_test

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_terminal"
	"log"
	"os"
	"testing"
)

func TestTerminalMySQL(t *testing.T) {
	options := &gg_terminal.TerminalOptions{
		CutSet: nil,
		Cursor: "MariaDB [(none)]>",
	}
	term, err := gg_terminal.Terminal.Create("mysql", os.Stderr, options)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	log.Println("Terminal created:", term)
	defer term.Close()

	err = term.Open()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	log.Println("Terminal opened with PID:", term.Pid())

	// WAIT THE CURSOR
	log.Println(term.WaitCursor())

	prompt := "help"
	log.Println("PROMPT:", prompt)
	_, err = term.Prompt(prompt)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// WAIT RESPONSE TO PROMPT
	log.Println(string(term.WaitResponse()))

	// WAIT THE CURSOR
	log.Println(term.WaitCursor())

	go func() {
		_, err = term.WriteLineString("quit")
	}()

	err = term.WaitFinish()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	log.Println("Quitting...")
}
