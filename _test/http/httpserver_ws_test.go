package _test

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_http/httpserver"
	"github.com/gofiber/fiber/v2"
	"log"
	"testing"
)

func TestWsServer(t *testing.T) {
	server := httpserver.NewHttpServer("./server", nil, nil)
	server.Configure(80, 443,
		"./cert/ssl.cert", "./cert/ssl.key",
		"./www4", false).
		All("/api/v1/*", handleServerAPI)

	// ws://localhost/api/w1/command1
	server.Websocket("/api/w1/:method", func(conn *httpserver.HttpWebsocketConn) {
		addr := conn.Conn().RemoteAddr().String()
		log.Println("NEW CONNECTION", conn.ClientsUUIDs(), addr)

		conn.OnDisconnect(func(payload *httpserver.HttpWebsocketEventPayload) {
			log.Println("DISCONNECT: ", payload.Websocket.UUID)
		})
		conn.OnMessage(func(payload *httpserver.HttpWebsocketEventPayload) {
			log.Println("MESSAGE: ", payload.Message)
		})
	})

	errs := server.Start()
	if len(errs) > 0 {
		t.Error(errs)
		t.FailNow()
	}

	log.Println("USE CHROME EXT 'Websocket Test Client' to test connection")

	// lock
	err := server.Join()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
}

func handleServerAPI(ctx *fiber.Ctx) error {
	// http://localhost:9090/api/v1/sys/version
	_, _ = ctx.WriteString("{ \"response\":\"v0.1\"}")
	return nil
}
