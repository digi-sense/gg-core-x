package _test

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_http/httpserver"
	"bitbucket.org/digi-sense/gg-core-x/gg_http/httpserver/httprewrite"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"mime"
	"testing"
	"time"
)

func TestServer(t *testing.T) {

	ct := mime.TypeByExtension(".js")
	fmt.Printf("ct: %s\n", ct)
	restart := false

	server := httpserver.NewHttpServer("./server", nil, nil)
	/** rewrite */
	server.Use(httprewrite.New(map[string]interface{}{
		"#IGNORE:/api/*": "",
		"#ROUTES:.":      "/index.html",
	}))

	errs := server.Configure(80, 443,
		"./cert/ssl.cert", "./cert/ssl.key",
		"./www4", false).
		All("/api/v1/*", handleAPI).
		Middleware("/test/h1", h1).Use(h2).
		Start()
	if len(errs) > 0 {
		t.Error(errs)
		t.FailNow()
	}
	_ = gg.Exec.Open("http://127.0.0.1")

	if restart {
		errs = server.Restart()
		if len(errs) > 0 {
			t.Error(errs)
			t.FailNow()
		}
	}

	time.Sleep(20 * time.Minute)
}

func h1(c *fiber.Ctx) error {
	_, _ = c.WriteString("<p>H1 RESPONSE</p>")
	_, _ = c.WriteString("<p>H1 RESPONSE double insert</p>")
	return nil
}

func h2(c *fiber.Ctx) error {
	_, _ = c.WriteString("<p>H2 RESPONSE</p>")
	return nil
}

func handleAPI(ctx *fiber.Ctx) error {
	// http://localhost:9090/api/v1/sys/version
	_, _ = ctx.WriteString("{ \"response\":\"v0.1\"}")
	return nil
}
