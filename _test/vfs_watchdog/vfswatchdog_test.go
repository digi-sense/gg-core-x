package vfs_watchdog_test

import (
	"bitbucket.org/digi-sense/gg-core"
	ggx "bitbucket.org/digi-sense/gg-core-x"
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs_watchdog/vfswatchdog"
	"bitbucket.org/digi-sense/gg-core/gg_events"
	"log"
	"testing"
	"time"
)

func TestWatchdog(t *testing.T) {
	gg.Paths.SetWorkspacePath(".")
	dog, err := ggx.VFSWatchdog.New(".", "options.json")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	log.Println(dog)

	dog.Reset() // remove history

	dog.SetHistoryHook(verifyFileExists)

	err = dog.Start()
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	dog.OnNewFile(func(event *gg_events.Event) {
		eventData := event.Argument(0).(*vfswatchdog.WatchdogEventData)
		log.Println("NEW FILE: ", eventData.Key)
		log.Println("\t", eventData.Path())
		log.Println("\t", eventData.Dir())
		log.Println("\t", eventData.Name(), eventData.Ext())
		log.Println("\t", eventData.Tags())
		log.Println("\t", eventData.Content())
		log.Println("\t", eventData.Base64())
		log.Println("\t", eventData.Text())
	})

	go func() {
		time.Sleep(5 * time.Second)
		dog.Reset()
	}()

	log.Println("WAITING FOR EXTERNAL STOP....")
	dog.Join()
}

func verifyFileExists(key string) bool {
	log.Println("VERIFY FILE EXISTS: ", key)
	return true
}
