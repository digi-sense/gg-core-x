package _test

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs/vfsoptions"
	"fmt"
	"testing"
)

func TestSettings(t *testing.T) {

	settings := vfsoptions.Create("./settings_os.json")
	if nil == settings {
		t.Error("Settings are nil")
		t.FailNow()
	}
	fmt.Println(settings)
}
