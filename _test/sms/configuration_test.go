package _test

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_sms/gg_sms_engine"
	"fmt"
	"testing"
)

func TestConfiguration(t *testing.T) {
	config, err := gg_sms_engine.NewSMSConfigurationFromFile("./settings.prod.json")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(gg.JSON.Stringify(config))

	prov1 := config.Provider()
	fmt.Println("Provider", prov1)
	fmt.Println("FROM", prov1.Param("from"))
}
