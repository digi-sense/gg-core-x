package _test

import (
	"bitbucket.org/digi-sense/gg-core-x"
	"bitbucket.org/digi-sense/gg-core-x/gg_sms/gg_sms_engine"
	"fmt"
	"testing"
)

func TestSender(t *testing.T) {
	target := "+393477857785"
	config, err := gg_sms_engine.NewSMSConfigurationFromFile("./settings.prod.json")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	sender := ggx.SMS.NewEngine(config)
	resp, err := sender.SendMessage("smshosting",
		"Check this: https://gianangelogeminiani.me", target, "Hospitex")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(resp)
}

func TestSkebby(t *testing.T) {
	config, err := gg_sms_engine.NewSMSConfigurationFromFile("./settings.skebby.json")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	sender := ggx.SMS.NewEngine(config)
	resp, err := sender.SendMessage("skebby",
		"Check this: gianangelogeminiani . me", "393477857785", "ANGELO")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(resp)
}
