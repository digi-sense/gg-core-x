package templating

import (
	"bitbucket.org/digi-sense/gg-core-x"
	"fmt"
	"testing"
)

type Model struct {
	Greetings string
	Subject   string
}

func TestRender(t *testing.T) {
	text := "\"{{ .Greetings }}\" - \"{{ .Subject }}\"  \n \"{{\" some text \"}}\""
	response := ggx.Templating.Render(text, false, map[string]interface{}{
		"Greetings": "HELLO--*°",
		"Subject":   "world",
	})
	fmt.Println(response)

	response = ggx.Templating.Render(text, false, &Model{
		Greetings: "HELLO--*°",
		Subject:   "world",
	})
	fmt.Println(response)
}
