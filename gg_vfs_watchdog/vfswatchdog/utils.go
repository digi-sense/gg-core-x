package vfswatchdog

import "fmt"

func BuildFileKey(hash, schema, path string) string {
	return fmt.Sprintf("%s|%s|%s", hash, schema, path)
}
