package vfswatchdog

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs"
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs/vfscommons"
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs_watchdog/vfswatchdog_options"
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_scheduler"
	gg_vfs2 "bitbucket.org/digi-sense/gg-core/gg_vfs"
	"fmt"
)

//----------------------------------------------------------------------------------------------------------------------
//	WatchdogTask
//----------------------------------------------------------------------------------------------------------------------

type WatchdogTask struct {
	uid      string
	options  *vfswatchdog_options.VfsWatchdogOptionsTarget
	logger   gg_.ILogger
	listener listener

	scheduler *gg_scheduler.Scheduler
	vfs       vfscommons.IVfs
	vfsRoot   *gg_vfs2.VfsFile
}

func NewWatchdogTask(args ...any) (instance *WatchdogTask, err error) {
	instance = new(WatchdogTask)
	err = instance.init(args...)
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *WatchdogTask) String() (response string) {
	if nil != instance {
		response = instance.uid
	}
	return
}

func (instance *WatchdogTask) Uid() (response string) {
	if nil != instance {
		response = instance.uid
	}
	return
}

func (instance *WatchdogTask) Payload() (response map[string]interface{}) {
	if nil != instance && nil != instance.options {
		response = instance.options.GetPayload()
	}
	return
}

func (instance *WatchdogTask) Start() {
	if nil != instance && nil != instance.scheduler {
		instance.scheduler.Start()
	}
	return
}

func (instance *WatchdogTask) Stop() {
	if nil != instance && nil != instance.scheduler {
		instance.scheduler.Stop()
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *WatchdogTask) init(args ...any) (err error) {
	if nil != instance {
		for _, arg := range args {
			if o, ok := arg.(vfswatchdog_options.VfsWatchdogOptionsTarget); ok {
				instance.options = &o
				continue
			}
			if o, ok := arg.(*vfswatchdog_options.VfsWatchdogOptionsTarget); ok {
				instance.options = o
				continue
			}
			if l, ok := arg.(gg_.ILogger); ok {
				instance.logger = l
				continue
			}
			if l, ok := arg.(listener); ok {
				instance.listener = l
				continue
			}
		}

		if nil == instance.options {
			err = gg.Errors.Prefix(gg.PanicSystemError, fmt.Sprintf("Missing options: "))
			return
		}
		if nil == instance.listener {
			err = gg.Errors.Prefix(gg.PanicSystemError, fmt.Sprintf("Missing parent listener: "))
			return
		}

		instance.uid = instance.options.Uid()

		instance.scheduler = gg_scheduler.NewScheduler()
		instance.scheduler.AddSchedule(&gg_scheduler.Schedule{
			Uid:      instance.options.Uid(),
			StartAt:  "",
			Timeline: instance.options.Schedule,
			Payload:  map[string]interface{}{},
		})
		instance.scheduler.OnError(instance.onSchedulerError)
		instance.scheduler.OnSchedule(instance.onSchedule)

		// VFS driver
		// open VFS driver and test the connection
		instance.vfs, err = gg_vfs.VFS.New(instance.options.Vfs)
		if nil != err {
			return
		}
		instance.vfsRoot, err = instance.vfs.Stat(".")
		if nil != err {
			return
		}
	}
	return
}

func (instance *WatchdogTask) onSchedulerError(err string) {
	if nil != instance && nil != instance.logger {
		instance.logger.Error(fmt.Sprintf("WatchdogTask.onSchedulerError '%s' -> ", instance.Uid()), err)
	}
}

func (instance *WatchdogTask) onSchedule(_ *gg_scheduler.SchedulerTask) {
	if nil != instance && nil != instance.listener {
		instance.scheduler.Pause()
		defer instance.scheduler.Resume()

		// list root
		if nil != instance.vfsRoot && nil != instance.vfs {
			instance.walk(instance.vfsRoot)
		}
	}
}

func (instance *WatchdogTask) walk(elem *gg_vfs2.VfsFile) {
	if nil != instance && nil != instance.listener && nil != elem {
		if elem.IsDir {
			elements, err := instance.vfs.ListAll(elem.AbsolutePath)
			if nil == err {
				if len(elements) > 0 {
					for _, element := range elements {
						if nil != element {
							instance.walk(element)
						}
					}
				}
			} else {
				// error
				instance.logger.Error(fmt.Sprintf("WatchdogTask.walk '%s' -> ", instance.Uid()), err)
			}
		} else {
			if nil != instance.listener && nil != instance.listener.onFsElement {
				instance.listener.onFsElement(instance.vfs, elem, instance.Payload())
			}
		}
	}
}
