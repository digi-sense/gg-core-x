package vfswatchdog

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs/vfscommons"
	"bitbucket.org/digi-sense/gg-core/gg_vfs"
)

//----------------------------------------------------------------------------------------------------------------------
//	WatchdogEventData
//----------------------------------------------------------------------------------------------------------------------

type WatchdogEventData struct {
	Key     string
	VFS     vfscommons.IVfs
	FsElem  *gg_vfs.VfsFile
	Payload map[string]interface{}
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *WatchdogEventData) Map() (response map[string]interface{}) {
	if nil != instance {
		response = make(map[string]interface{})
		response["key"] = instance.Key
		response["dir"] = instance.Dir()
		response["path"] = instance.Path()
		response["name"] = instance.Name()
		response["ext"] = instance.Ext()
		response["tags"] = instance.Tags()
	}
	return
}

func (instance *WatchdogEventData) Schema() (response string) {
	if nil != instance {
		if nil != instance.VFS {
			response = instance.VFS.Schema()
		}
	}
	return
}

func (instance *WatchdogEventData) Dir() (response string) {
	if nil != instance {
		response = gg.Paths.Dir(instance.FsElem.AbsolutePath)
	}
	return
}

func (instance *WatchdogEventData) Path() (response string) {
	if nil != instance {
		response = instance.FsElem.AbsolutePath
	}
	return
}

func (instance *WatchdogEventData) Filename() (response string) {
	if nil != instance {
		response = instance.FsElem.Name
	}
	return
}

func (instance *WatchdogEventData) Name() (response string) {
	if nil != instance {
		response = gg.Paths.FileName(instance.FsElem.Name, false)
	}
	return
}

func (instance *WatchdogEventData) Ext() (response string) {
	if nil != instance {
		response = gg.Paths.ExtensionName(instance.FsElem.Name)
	}
	return
}

func (instance *WatchdogEventData) Tags() (response []string) {
	if nil != instance {
		response = make([]string, 0)

		// get tag from dir names
		tokens := gg.Strings.Split(instance.Dir(), "/")
		for _, token := range tokens {
			if len(token) > 0 {
				subTokens := gg.Strings.Split(token, "_  -.")
				for _, subToken := range subTokens {
					if len(subToken) > 0 {
						response = append(response, subToken)
					}
				}
			}
		}

		// get tags from name
		tokens = gg.Strings.Split(instance.Name(), "_  -.")
		for _, token := range tokens {
			if len(token) > 0 {
				response = append(response, token)
			}
		}
	}
	return
}

func (instance *WatchdogEventData) Size() (response int64) {
	if nil != instance {
		response = instance.FsElem.Size
	}
	return
}

func (instance *WatchdogEventData) Content() (response []byte) {
	if nil != instance {
		response, _ = instance.VFS.Read(instance.FsElem.AbsolutePath)
	}
	return
}

func (instance *WatchdogEventData) Base64() (response string) {
	if nil != instance {
		response = gg.Coding.EncodeBase64(instance.Content())
	}
	return
}

func (instance *WatchdogEventData) Text() (response string) {
	if nil != instance {
		response = string(instance.Content())
	}
	return
}
