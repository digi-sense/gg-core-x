package vfswatchdog

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs/vfscommons"
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs_watchdog/vfswatchdog_options"
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_errors"
	"bitbucket.org/digi-sense/gg-core/gg_events"
	"bitbucket.org/digi-sense/gg-core/gg_stoppable"
	"bitbucket.org/digi-sense/gg-core/gg_vfs"
	"fmt"
	"sync"
)

const EventOnNewFile = "on_new_file"
const EventOnFile = "on_file"

type listener interface {
	onFsElement(vfs vfscommons.IVfs, fsElem *gg_vfs.VfsFile, payload map[string]interface{})
}

type HistoryExistsHook func(key string) bool

//----------------------------------------------------------------------------------------------------------------------
//	Watchdog
//----------------------------------------------------------------------------------------------------------------------

// The Watchdog represents a monitoring utility designed to observe and respond to specific system or application conditions.
// EVENTS:
// The Watchdog emits 2 events:
// - on_new_file: each time a NEW file is read from internal daemon
// - on_file: each time a file is read from internal daemon
type Watchdog struct {
	workspaceDir string
	options      *vfswatchdog_options.VfsWatchdogOptions
	logger       gg_.ILogger
	events       *gg_events.Emitter

	stoppable *gg_stoppable.Stoppable
	open      bool
	tasks     []*WatchdogTask
	mux       *sync.Mutex

	history           *WatchdogHistory      // internal history
	historyExistsFunc func(key string) bool // hook to validate externally the file unique key
}

func NewWatchdog(root string, args ...any) (instance *Watchdog, err error) {
	instance = new(Watchdog)
	instance.tasks = make([]*WatchdogTask, 0)
	instance.mux = new(sync.Mutex)
	instance.workspaceDir = gg.Paths.Absolutize(root, gg.Paths.WorkspacePath("."))

	err = instance.init(args...)

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	s e r i a l i z a b l e
//----------------------------------------------------------------------------------------------------------------------

func (instance *Watchdog) Map() (response map[string]interface{}) {
	if nil != instance && nil != instance.options {
		response = map[string]interface{}{}
		for _, target := range instance.options.Targets {
			if nil != target {
				response[target.Location()] = target.Schedule
			}
		}
	}
	return
}

func (instance *Watchdog) String() (response string) {
	if nil != instance && nil != instance.options {
		return instance.options.String()
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

// SetHistoryFileName sets the filename for the internal history management of the Watchdog instance.
func (instance *Watchdog) SetHistoryFileName(value string) *Watchdog {
	if nil != instance && nil != instance.history {
		instance.history.SetFileName(value)
	}
	return instance
}

// SetHistoryHook sets a custom function as a hook to validate file unique keys and disables internal history management.
func (instance *Watchdog) SetHistoryHook(f HistoryExistsHook) *Watchdog {
	if nil != instance {
		instance.historyExistsFunc = f
		if nil != instance.history {
			instance.history.Clear()
			instance.history = nil
			instance.options.EnableHistory = false
		}
	}
	return instance
}

func (instance *Watchdog) Reset() *Watchdog {
	if nil != instance && nil != instance.history {
		instance.history.Clear()
	}
	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	s t o p p a b l e
//----------------------------------------------------------------------------------------------------------------------

func (instance *Watchdog) Start() (err error) {
	if nil != instance && nil != instance.stoppable {
		instance.stoppable.Start()
	}
	return
}

func (instance *Watchdog) Stop() {
	if nil != instance && nil != instance.stoppable {
		instance.stoppable.Stop()
	}
	return
}

func (instance *Watchdog) Join() {
	if nil != instance && nil != instance.stoppable {
		instance.stoppable.Join()
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	H I S T O R Y
//----------------------------------------------------------------------------------------------------------------------

func (instance *Watchdog) History() *WatchdogHistory {
	return instance.history
}

//----------------------------------------------------------------------------------------------------------------------
//	E V E N T S
//----------------------------------------------------------------------------------------------------------------------

func (instance *Watchdog) OnFile(callback func(event *gg_events.Event)) *Watchdog {
	if nil != instance && nil != instance.events {
		instance.events.On(EventOnFile, callback)
	}
	return instance
}

func (instance *Watchdog) OnNewFile(callback func(event *gg_events.Event)) *Watchdog {
	if nil != instance && nil != instance.events {
		instance.events.On(EventOnNewFile, callback)
	}
	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *Watchdog) init(args ...any) (err error) {
	if nil != instance {
		instance.options = vfswatchdog_options.Create(args...)

		for _, arg := range args {
			if l, ok := arg.(gg_.ILogger); ok {
				instance.logger = l
				continue
			}
			if e, ok := arg.(*gg_events.Emitter); ok {
				instance.events = e
				continue
			}
		}

		if instance.logger == nil {
			instance.logger = gg.Log.NewNoRotate("info",
				gg.Paths.Concat(instance.workspaceDir, "./logging/vfs-watchdog.log"))
		}

		if nil == instance.events {
			instance.events = gg.Events.NewEmitter()
		}

		instance.stoppable = gg_stoppable.NewStoppable()
		instance.stoppable.SetName("VFS Watchdog: file monitor " + gg.JSON.Stringify(instance.Map()))
		instance.stoppable.OnStart(instance.start)
		instance.stoppable.OnStop(instance.stop)
		instance.stoppable.SetLogger(instance.log)

		// options
		if nil == instance.options {
			err = gg_errors.Errors.Prefix(gg_.PanicSystemError, "Missing options: ")
			return
		}

		// history
		if instance.options.EnableHistory {
			instance.history, err = NewWatchdogHistory(instance.workspaceDir)
			if nil != err {
				return
			}
		}

	}
	return
}

func (instance *Watchdog) log() gg_.ILogger {
	return instance.logger
}

func (instance *Watchdog) start() {
	if nil != instance && !instance.open {
		instance.open = true

		info := instance.Map()
		instance.logger.Info("STARTING WATCHDOG:")
		for k, v := range info {
			instance.logger.Info("\t", k, ":", v)
		}

		var err error
		if nil != instance.options {
			if len(instance.options.Targets) > 0 {

				// creates services
				for _, target := range instance.options.Targets {
					if nil != target {
						if len(target.Schedule) == 0 {
							target.Schedule = instance.options.Schedule
						}
						var task *WatchdogTask
						task, err = NewWatchdogTask(target, instance.logger, instance)
						if nil != err {
							break
						}
						instance.tasks = append(instance.tasks, task)
					}
				}

				// now start all
				if nil == err {
					for _, task := range instance.tasks {
						task.Start()
					}
					instance.logger.Info(fmt.Sprintf("STARTED '%d' TASKS.", len(instance.tasks)))
				}
			} else {
				err = gg.Errors.Prefix(gg.PanicSystemError, "Missing targets to monitor: ")
			}
		} else {
			err = gg.Errors.Prefix(gg.PanicSystemError, "Invalid status: ")
		}
		if nil != err && nil != instance.logger {
			instance.logger.Error("Watchdog.start() -> ", err)
		}
	}
	return
}

func (instance *Watchdog) stop() {
	if nil != instance && instance.open {
		instance.open = false

		for _, task := range instance.tasks {
			if nil != task {
				task.Stop()
			}
		}
		instance.tasks = []*WatchdogTask{}
	}
}

func (instance *Watchdog) onFsElement(vfs vfscommons.IVfs, fsElem *gg_vfs.VfsFile, payload map[string]interface{}) {
	if nil != instance && nil != instance.events && nil != instance.options && instance.open && nil != vfs && nil != fsElem && vfs.IsConnected() {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		if !fsElem.IsDir {

			// validate filename and size
			if !instance.options.IsFileAllowed(fsElem.Name, fsElem.Size) {
				return
			}

			// try to add new item
			// the key of history map is a compound string of path and hash
			// only new files should trigger events
			if key, ok := instance.addElement(vfs, fsElem); ok {
				eventData := &WatchdogEventData{
					Key:     key,
					VFS:     vfs,
					FsElem:  fsElem,
					Payload: payload,
				}

				if nil != instance.events {
					// generic event
					instance.events.Emit(EventOnFile, eventData)

					// new file event
					instance.events.Emit(EventOnNewFile, eventData)
				}

				if nil != instance.logger {
					// debug logging
					instance.logger.Debug(fmt.Sprintf("New file (size %v): '%s'", gg.Formatter.FormatBytes(fsElem.Size), key))
				}
			} else {
				if len(key) > 0 {
					eventData := &WatchdogEventData{
						Key:     key,
						VFS:     vfs,
						FsElem:  fsElem,
						Payload: payload,
					}

					if nil != instance.events {
						// generic event
						instance.events.Emit(EventOnFile, eventData)
					}

					if nil != instance.logger {
						// debug logging
						instance.logger.Debug(fmt.Sprintf("On file (size %v): '%s'", gg.Formatter.FormatBytes(fsElem.Size), key))
					}
				}
			}
		}
	}
}

func (instance *Watchdog) addElement(vfs vfscommons.IVfs, fsElem *gg_vfs.VfsFile) (key string, success bool) {
	if nil != instance && nil != vfs && nil != fsElem {

		// read the file
		bytes, err := vfs.Read(fsElem.RelativePath)
		if nil != err {
			return
		}

		hash, _ := gg.IO.ReadHashFromBytes(bytes)
		schema := vfs.Schema()
		path := fsElem.AbsolutePath
		key = BuildFileKey(hash, schema, path)

		enableHistory := nil != instance.options && instance.options.EnableHistory
		if enableHistory && nil != instance.history {
			// uses file enableHistory
			// check if the history file already exists
			_ = instance.history.Assert()
			// try to add key
			success = instance.history.Add(key)
		} else {
			if nil != instance.historyExistsFunc {
				success = !instance.historyExistsFunc(key)
			}
		}
	}
	return
}
