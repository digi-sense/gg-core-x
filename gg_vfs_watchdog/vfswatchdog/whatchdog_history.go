package vfswatchdog

import (
	"bitbucket.org/digi-sense/gg-core"
	"sync"
	"time"
)

//----------------------------------------------------------------------------------------------------------------------
//	WatchdogHistory
//----------------------------------------------------------------------------------------------------------------------

type WatchdogHistory struct {
	enabled      bool
	history      map[string]interface{}
	mux          *sync.Mutex
	workspaceDir string
	filename     string
}

func NewWatchdogHistory(dir string) (instance *WatchdogHistory, err error) {
	dir = gg.Paths.Absolutize(dir, gg.Paths.WorkspacePath("."))
	instance = &WatchdogHistory{
		history:      make(map[string]interface{}),
		mux:          new(sync.Mutex),
		workspaceDir: dir,
		filename:     gg.Paths.Concat(dir, "./vfs-watchdog.history"),
	}
	err = instance.historyInit()

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *WatchdogHistory) SetFileName(value string) {
	if nil != instance {
		instance.filename = gg.Paths.Absolutize(value, instance.workspaceDir)
	}
}

func (instance *WatchdogHistory) Save() (err error) {
	if nil != instance {
		err = instance.historySave()
	}
	return
}

func (instance *WatchdogHistory) Reload() (err error) {
	if nil != instance {
		err = instance.historyInit()
	}
	return
}

func (instance *WatchdogHistory) Assert() (response bool) {
	if nil != instance && nil != instance.history {
		response, _ = gg.Paths.Exists(instance.filename)
		if !response {
			_ = instance.historyInit()
		}
	}
	return
}

func (instance *WatchdogHistory) Add(key string) (success bool) {
	if nil != instance && nil != instance.history {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		if !instance.exists(key) {
			success = true
			value := gg.Dates.FormatDate(time.Now(), "yyyyMMdd HH:mm:ss")
			instance.history[key] = value

			// autosave
			_ = instance.historySave()
		}
	}
	return
}

func (instance *WatchdogHistory) Get(key string) interface{} {
	instance.mux.Lock()
	defer instance.mux.Unlock()

	if val, ok := instance.history[key]; ok {
		return val
	}

	return nil
}

func (instance *WatchdogHistory) Clear() {
	instance.mux.Lock()
	defer instance.mux.Unlock()

	instance.history = make(map[string]interface{})
	_ = gg.IO.Remove(instance.filename)
}

func (instance *WatchdogHistory) Len() int {
	instance.mux.Lock()
	defer instance.mux.Unlock()

	return len(instance.history)
}

func (instance *WatchdogHistory) Keys() []string {
	instance.mux.Lock()
	defer instance.mux.Unlock()

	keys := make([]string, 0, len(instance.history))
	for k := range instance.history {
		keys = append(keys, k)
	}
	return keys
}

func (instance *WatchdogHistory) Values() []interface{} {
	instance.mux.Lock()
	defer instance.mux.Unlock()

	values := make([]interface{}, 0, len(instance.history))
	for _, v := range instance.history {
		values = append(values, v)
	}
	return values
}

func (instance *WatchdogHistory) Items() (response map[string]interface{}) {
	instance.mux.Lock()
	defer instance.mux.Unlock()

	response = gg.Maps.Clone(instance.history)

	return
}

func (instance *WatchdogHistory) Delete(key string) {
	instance.mux.Lock()
	defer instance.mux.Unlock()

	delete(instance.history, key)

	// autosave
	_ = instance.historySave()
}

func (instance *WatchdogHistory) Exists(key string) bool {
	instance.mux.Lock()
	defer instance.mux.Unlock()

	_, ok := instance.history[key]
	return ok
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *WatchdogHistory) historyInit() (err error) {
	if nil != instance {
		instance.history = make(map[string]interface{})

		filename := gg.Paths.Concat(instance.workspaceDir, "./vfs-watchdog.history")
		_ = gg.JSON.ReadFromFile(filename, &instance.history)
	}
	return
}

func (instance *WatchdogHistory) historySave() (err error) {
	if nil != instance {
		filename := gg.Paths.Concat(instance.workspaceDir, "./vfs-watchdog.history")
		_ = gg.Paths.Mkdir(filename)
		_, err = gg.IO.WriteTextToFile(gg.JSON.Stringify(instance.history), filename)
	}
	return
}

func (instance *WatchdogHistory) exists(key string) bool {
	_, ok := instance.history[key]
	return ok
}
