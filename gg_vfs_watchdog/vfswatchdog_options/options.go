package vfswatchdog_options

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs/vfsoptions"
	"fmt"
	"path/filepath"
	"strings"
)

//----------------------------------------------------------------------------------------------------------------------
//	utility
//----------------------------------------------------------------------------------------------------------------------

func Create(args ...any) (response *VfsWatchdogOptions) {
	for _, arg := range args {
		if nil != arg {
			if c, b := arg.(VfsWatchdogOptions); b {
				response = &c
				continue
			}
			if p, b := arg.(*VfsWatchdogOptions); b {
				response = p
				continue
			}
			if s, ok := arg.(string); ok {
				_ = gg.JSON.Read(s, &response)
				continue
			}
			if s, ok := arg.(map[string]interface{}); ok {
				_ = gg.JSON.Read(s, &response)
				continue
			}
		}
	}

	if nil == response {
		response = new(VfsWatchdogOptions)
	}
	if nil == response.Targets {
		response.Targets = make([]*VfsWatchdogOptionsTarget, 0)
	}

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	VfsWatchdogOptions
//----------------------------------------------------------------------------------------------------------------------

type VfsWatchdogOptions struct {
	Schedule            string                      `json:"schedule"`             // default schedule, used if target has no schedule declared
	EnableHistory       bool                        `json:"enable-history"`       // enable history
	Targets             []*VfsWatchdogOptionsTarget `json:"targets"`              // target list
	AllowedExtensions   []string                    `json:"allowed-extensions"`   // array of "whitelisted" files
	ForbiddenExtensions []string                    `json:"forbidden-extensions"` // array of "blacklisted" files
	MaxFileSize         int                         `json:"max-file-size"`        // max byte size of a file
}

func (instance *VfsWatchdogOptions) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *VfsWatchdogOptions) Map() map[string]interface{} {
	return gg.Convert.ToMap(gg.JSON.Stringify(instance))
}

func (instance *VfsWatchdogOptions) IsFileAllowed(name string, size int64) (response bool) {
	if nil != instance && len(name) > 0 && size > 0 {
		// validate extension
		ext := filepath.Ext(name)
		if len(ext) > 0 {
			response = true // optimistic
			if len(instance.ForbiddenExtensions) > 0 || len(instance.AllowedExtensions) > 0 {
				if len(instance.ForbiddenExtensions) > 0 {
					for _, x := range instance.ForbiddenExtensions {
						if strings.ToLower(x) == strings.ToLower(ext) {
							response = false
							return
						}
					}
				}
				if len(instance.AllowedExtensions) > 0 {
					response = false // pessimistic
					for _, x := range instance.AllowedExtensions {
						if strings.ToLower(x) == strings.ToLower(ext) {
							response = true
							break
						}
					}
				}
			}
		}
		if !response {
			return
		}

		// validate size
		if instance.MaxFileSize > 0 {
			response = size <= int64(instance.MaxFileSize)
		}
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	VfsWatchdogOptionsTarget
//----------------------------------------------------------------------------------------------------------------------

type VfsWatchdogOptionsTarget struct {
	Schedule string                 `json:"schedule"` // custom schedule
	Payload  map[string]interface{} `json:"payload"`  // custom data
	Vfs      *vfsoptions.VfsOptions `json:"vfs"`      // vfs settings
}

func (instance *VfsWatchdogOptionsTarget) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *VfsWatchdogOptionsTarget) Map() map[string]interface{} {
	return gg.Convert.ToMap(gg.JSON.Stringify(instance))
}

func (instance *VfsWatchdogOptionsTarget) Location() string {
	if nil != instance && nil != instance.Vfs {
		return instance.Vfs.Location
	}
	return ""
}

func (instance *VfsWatchdogOptionsTarget) GetPayload() (response map[string]interface{}) {
	if nil != instance {
		if nil == instance.Payload {
			instance.Payload = make(map[string]interface{})
		}
		response = instance.Payload
	}
	return
}

func (instance *VfsWatchdogOptionsTarget) Uid() string {
	if nil != instance {
		return fmt.Sprintf("location(%s)-schedule(%s)", instance.Location(), instance.Schedule)
	}
	return ""
}
