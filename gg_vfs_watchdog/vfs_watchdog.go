package gg_vfs_watchdog

import "bitbucket.org/digi-sense/gg-core-x/gg_vfs_watchdog/vfswatchdog"

type VFSWatchdogHelper struct {
}

var VFSWatchdog *VFSWatchdogHelper

func init() {
	VFSWatchdog = new(VFSWatchdogHelper)
}

func (instance *VFSWatchdogHelper) New(root string, args ...any) (response *vfswatchdog.Watchdog, err error) {
	if nil != instance {
		response, err = vfswatchdog.NewWatchdog(root, args...)
	}
	return
}
