package vfsoptions

import (
	"bitbucket.org/digi-sense/gg-core"
	"strings"
)

//----------------------------------------------------------------------------------------------------------------------
//	utility
//----------------------------------------------------------------------------------------------------------------------

func Create(args ...any) (response *VfsOptions) {
	for _, arg := range args {
		if nil != arg {
			if c, b := arg.(VfsOptions); b {
				response = &c
				continue
			}
			if p, b := arg.(*VfsOptions); b {
				response = p
				continue
			}
			if s, ok := arg.(string); ok {
				_ = gg.JSON.Read(s, &response)
				continue
			}
			if s, ok := arg.(map[string]interface{}); ok {
				_ = gg.JSON.Read(s, &response)
				continue
			}
		}
	}

	if nil == response {
		response = newOptions()
	}
	if nil == response.Auth {
		response.Auth = new(VfsOptionsAuth)
	}

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type VfsOptions struct {
	Location string          `json:"location"`
	Auth     *VfsOptionsAuth `json:"auth"`
}

type VfsOptionsAuth struct {
	User     string `json:"user"`
	Password string `json:"pass"`
	Key      string `json:"key"`
}

func newOptions() *VfsOptions {
	instance := new(VfsOptions)
	instance.Location = ""
	instance.Auth = new(VfsOptionsAuth)

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	VfsOptions
//----------------------------------------------------------------------------------------------------------------------

func (instance *VfsOptions) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *VfsOptions) Map() map[string]interface{} {
	return gg.Convert.ToMap(gg.JSON.Stringify(instance))
}

func (instance *VfsOptions) Schema() string {
	schema, _ := instance.SplitLocation()
	return schema
}

func (instance *VfsOptions) SplitLocation() (scheme, host string) {
	tokens := strings.Split(instance.Location, "://")
	if len(tokens) == 2 {
		scheme = tokens[0]
		host = tokens[1]
	}
	return
}

func (instance *VfsOptions) UserName() (response string) {
	if nil != instance && nil != instance.Auth {
		return instance.Auth.User
	}
	return
}
