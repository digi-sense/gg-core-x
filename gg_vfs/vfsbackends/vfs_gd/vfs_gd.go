package vfs_gd

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs/vfscommons"
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs/vfsoptions"
)

// GOOGLE DRIVE SUPPORT
// https://developers.google.com/drive/api/v3/quickstart/go

//----------------------------------------------------------------------------------------------------------------------
//	IVfsBuilder
//----------------------------------------------------------------------------------------------------------------------

func Build(opts any) (response vfscommons.IVfs, err error) {
	options := vfsoptions.Create(opts)
	if nil != options {
		response, err = NewVfsGD(options)
	} else {
		err = gg.Errors.Prefix(gg.PanicSystemError, "Missing options: ")
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	VfsGD
//----------------------------------------------------------------------------------------------------------------------

type VfsGD struct {
	settings *vfsoptions.VfsOptions

	user string

	startDir string
	curDir   string
}

func NewVfsGD(opts ...any) (response vfscommons.IVfs, err error) {
	options := vfsoptions.Create(opts)
	if nil != options {
		err = gg.Errors.Prefix(gg.PanicSystemError, "NOT YET IMPLEMENTED: ")
	} else {
		err = gg.Errors.Prefix(gg.PanicSystemError, "Missing options: ")
	}
	return
}

func (instance *VfsGD) IsConnected() bool {
	if nil != instance {

	}
	return false
}

func (instance *VfsGD) Schema() string {
	if nil != instance && nil != instance.settings {
		return instance.settings.Schema()
	}
	return ""
}

func (instance *VfsGD) Options() (response map[string]interface{}) {
	if nil!=instance {

	}
	return
}