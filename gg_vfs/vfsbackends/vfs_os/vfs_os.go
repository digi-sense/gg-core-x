package vfs_os

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs/vfscommons"
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs/vfsoptions"
	"bitbucket.org/digi-sense/gg-core/gg_vfs"
	"os"
	"strings"
)

//----------------------------------------------------------------------------------------------------------------------
//	IVfsBuilder
//----------------------------------------------------------------------------------------------------------------------

func Build(opts any) (response vfscommons.IVfs, err error) {
	options := vfsoptions.Create(opts)
	if nil != options {
		response, err = NewVfsOS(options)
	} else {
		err = gg.Errors.Prefix(gg.PanicSystemError, "Missing options: ")
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	VfsOS
//----------------------------------------------------------------------------------------------------------------------

type VfsOS struct {
	options *vfsoptions.VfsOptions

	user string

	startDir string
	curDir   string
}

func NewVfsOS(options *vfsoptions.VfsOptions) (instance *VfsOS, err error) {
	instance = new(VfsOS)
	instance.options = options

	err = instance.init()

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *VfsOS) String() string {
	return gg.JSON.Stringify(instance.options)
}

func (instance *VfsOS) IsConnected() bool {
	if nil != instance {
		if ok, _ := gg.Paths.Exists(instance.startDir); ok {
			return true
		}
	}
	return false
}

func (instance *VfsOS) Close() {

}

func (instance *VfsOS) Schema() string {
	if nil != instance && nil != instance.options {
		return instance.options.Schema()
	}
	return ""
}

func (instance *VfsOS) Path() string {
	return instance.curDir
}

func (instance *VfsOS) Options() (response map[string]interface{}) {
	if nil!=instance {
		return instance.options.Map()
	}
	return
}

func (instance *VfsOS) Cd(path string) (bool, error) {
	instance.curDir = vfscommons.Absolutize(instance.curDir, path)
	return gg.Paths.Exists(instance.curDir)
}

func (instance *VfsOS) Stat(path string) (*gg_vfs.VfsFile, error) {
	fullPath := vfscommons.Absolutize(instance.curDir, path)
	info, err := os.Stat(fullPath)
	if nil != err {
		return nil, err
	}
	return vfscommons.NewVfsFile(fullPath, instance.curDir, info), nil

}

func (instance *VfsOS) Exists(path string) (bool, error) {
	return gg.Paths.Exists(vfscommons.Absolutize(instance.curDir, path))
}

func (instance *VfsOS) ListAll(dir string) ([]*gg_vfs.VfsFile, error) {
	response := make([]*gg_vfs.VfsFile, 0)
	dir = vfscommons.Absolutize(instance.curDir, dir)
	list, err := os.ReadDir(dir)
	if nil == err {
		for _, entry := range list {
			fullPath := gg.Paths.Concat(dir, entry.Name())
			info, err := instance.Stat(fullPath)
			if nil == err {
				response = append(response, info)
			}
		}
	}
	return response, err
}

func (instance *VfsOS) Read(source string) ([]byte, error) {
	path := vfscommons.Absolutize(instance.curDir, source)
	return gg.IO.ReadBytesFromFile(path)
}

func (instance *VfsOS) Write(data []byte, target string) (int, error) {
	target = vfscommons.Absolutize(instance.curDir, target)
	err := gg.Paths.Mkdir(target)
	if nil != err {
		return 0, err
	}
	return gg.IO.WriteBytesToFile(data, target)
}

func (instance *VfsOS) Download(source, target string) ([]byte, error) {
	data, err := instance.Read(vfscommons.Absolutize(instance.curDir, source))
	if nil != err {
		return nil, err
	}
	_, err = gg.IO.WriteBytesToFile(data, vfscommons.Absolutize(instance.curDir, target))
	if nil != err {
		return nil, err
	}
	return data, nil
}

func (instance *VfsOS) Remove(source string) error {
	return gg.IO.Remove(vfscommons.Absolutize(instance.curDir, source))
}

func (instance *VfsOS) MkDir(path string) error {
	return gg.Paths.Mkdir(vfscommons.Absolutize(instance.curDir, path))
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *VfsOS) init() error {
	if nil != instance.options {
		_, root := instance.options.SplitLocation()

		if strings.HasPrefix(root, vfscommons.PrefixUserHome) {
			// PREFIX: ~
			userHome, err := gg.Paths.UserHomeDir()
			if nil != err {
				return err
			}
			instance.startDir = gg.Paths.Concat(userHome, root)
		} else if strings.HasPrefix(root, vfscommons.PrefixWorkspace) {
			// PREFIX: *
			instance.startDir = gg.Paths.Concat(gg.Paths.WorkspacePath("."), root)
		} else if strings.HasPrefix(root, ".") {
			instance.startDir = gg.Paths.Absolute(root)
		} else {
			// absolute path (user dir not used)
			instance.startDir = root
		}
		instance.curDir = instance.startDir
		if b, err := gg.Paths.Exists(instance.curDir); !b {
			return err
		}
		return nil
	}
	return vfscommons.MissingConfigurationError
}
