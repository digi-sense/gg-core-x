package gg_vfs

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs/vfsbackends/vfs_ftp"
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs/vfsbackends/vfs_os"
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs/vfsbackends/vfs_sftp"
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs/vfscommons"
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs/vfsoptions"
	"fmt"
)

type VFSHelper struct {
}

var VFS *VFSHelper

var backends = make(map[string]vfscommons.IVfsBuilder)

func init() {

	// add default backends
	backends[vfscommons.SchemaFTP] = vfs_ftp.Build
	backends[vfscommons.SchemaSFTP] = vfs_sftp.Build
	backends[vfscommons.SchemaOS] = vfs_os.Build
	// backends[vfscommons.SchemaGD] = vfs_gd.Build

	VFS = new(VFSHelper)
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *VFSHelper) New(optionsOrFile any) (response vfscommons.IVfs, err error) {
	options := vfsoptions.Create(optionsOrFile)
	if nil != options {
		if len(options.Location) > 0 {
			schema := options.Schema()
			if len(schema) > 0 {
				if builder, ok := backends[schema]; ok {
					response, err = builder(options)
				} else {
					err = gg.Errors.Prefix(gg.PanicSystemError, fmt.Sprintf("Unable to find a driver for '%s' schema: ", schema))
				}
			} else {
				err = gg.Errors.Prefix(gg.PanicSystemError, fmt.Sprintf("Unable to get schema form '%s': ", options.Location))
			}
		} else {
			err = gg.Errors.Prefix(gg.PanicSystemError, "Missing 'location' property: ")
		}
	} else {
		err = gg.Errors.Prefix(gg.PanicSystemError, "Missing options: ")
	}
	return
}

func (instance *VFSHelper) AddBuilder(schema string, builder vfscommons.IVfsBuilder) (err error) {
	if _, ok := backends[schema]; !ok {
		backends[schema] = builder
	} else {
		err = gg.Errors.Prefix(gg.PanicSystemError, fmt.Sprintf("Driver '%s' already exists", schema))
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------
