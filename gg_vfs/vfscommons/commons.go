package vfscommons

import (
	"bitbucket.org/digi-sense/gg-core/gg_vfs"
	"errors"
)

//----------------------------------------------------------------------------------------------------------------------
//	IVfsBuilder
//----------------------------------------------------------------------------------------------------------------------

type IVfsBuilder func(opts any) (response IVfs, err error)

//----------------------------------------------------------------------------------------------------------------------
//	IVfs driver
//----------------------------------------------------------------------------------------------------------------------

type IVfs interface {
	IsConnected() bool
	Schema() string
	Path() string
	Options() (response map[string]interface{})
	Close()
	Cd(path string) (bool, error)
	Download(source, target string) ([]byte, error)
	gg_vfs.IVFSController
}

//----------------------------------------------------------------------------------------------------------------------
//	e r r o r s
//----------------------------------------------------------------------------------------------------------------------

var (
	MissingConfigurationError  = errors.New("missing configuration")
	MismatchConfigurationError = errors.New("mismatch configuration")
	MissingConnectionError     = errors.New("missing connection")
	UnsupportedSchemaError     = errors.New("unsupported schema")
)

//----------------------------------------------------------------------------------------------------------------------
//	s c h e m a s
//----------------------------------------------------------------------------------------------------------------------

const (
	SchemaFTP  = "ftp"
	SchemaSFTP = "sftp"
	SchemaOS   = "file"
)

//----------------------------------------------------------------------------------------------------------------------
//	schema constants
//----------------------------------------------------------------------------------------------------------------------

const (
	PrefixUserHome  = "~"
	PrefixWorkspace = "*"
)
