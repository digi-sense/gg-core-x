package vfscommons

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs/vfsoptions"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"strings"
)

func ErrorContains(err error, text string) bool {
	lowErr := strings.ToLower(err.Error())
	lowTxt := strings.ToLower(text)
	return strings.Index(lowErr, lowTxt) > -1
}

func IsFile(path string) bool {
	return len(gg.Paths.Extension(path)) > 0
}

func Absolutize(root, path string) string {
	if strings.HasPrefix(path, ".") {
		return gg.Paths.Concat(root, path)
	}
	return path
}

func Relativize(root string, path string) string {
	if gg.Paths.IsAbs(path) && gg.Paths.IsAbs(root) {
		response := strings.Replace(path, root, "", 1)
		return EnsureRelativePath(response)
	}
	return path
}

func EnsureRelativePath(path string) string {
	if !strings.HasPrefix(path, "."+gg_utils.OS_PATH_SEPARATOR) {
		if strings.HasPrefix(path, gg_utils.OS_PATH_SEPARATOR) {
			path = "." + path
		} else {
			path = "." + gg_utils.OS_PATH_SEPARATOR + path
		}
	}
	return path
}

func ReadKey(pathOrKey string) ([]byte, error) {
	if len(pathOrKey) == 0 {
		return []byte{}, nil
	}
	if b, err := gg.Paths.IsFile(pathOrKey); b && nil == err {
		data, err := gg.IO.ReadBytesFromFile(pathOrKey)
		if nil != err {
			return []byte{}, err
		}
		return data, nil
	}
	return []byte(pathOrKey), nil
}

func SplitHost(settings *vfsoptions.VfsOptions) (host string, port int) {
	port = 22
	_, full := settings.SplitLocation()
	tokens := strings.Split(full, ":")
	switch len(tokens) {
	case 1:
		host = tokens[0]
	case 2:
		host = tokens[0]
		port = gg.Convert.ToInt(tokens[1])
	}
	return
}
