package vfscommons

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_vfs"
	"github.com/jlaffaye/ftp"
	"os"
)

func NewVfsFile(absolutePath, root string, item interface{}) *gg_vfs.VfsFile {
	f := new(gg_vfs.VfsFile)
	if file, b := item.(os.FileInfo); b {
		f.Name = file.Name()
		f.Root = root
		f.AbsolutePath = absolutePath
		f.RelativePath = Relativize(root, absolutePath)
		f.Size = file.Size()
		f.ModTime = file.ModTime()
		f.IsDir = file.IsDir()
		f.Mode = file.Mode().String()
	} else if entry, b := item.(*ftp.Entry); b {
		f.Name = entry.Name
		f.Root = root
		f.AbsolutePath = gg.Paths.Concat(root, entry.Name)
		f.RelativePath = Relativize(root, f.AbsolutePath)
		f.Size = int64(entry.Size)
		f.ModTime = entry.Time
		f.IsDir = entry.Type == ftp.EntryTypeFolder

	}

	return f
}
