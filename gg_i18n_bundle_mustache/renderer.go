package gg_i18n_bundle_mustache

import (
	"github.com/cbroglie/mustache"
)


type RendererMustache struct {
}

func (instance *RendererMustache) Render(text string, model ...interface{}) (string, error) {
	return renderMustache(text, model...)

}

func (instance *RendererMustache) TagsFrom(text string) ([]string, error) {
	return tagsFromMustache(text)
}

func renderMustache(text string, model ...interface{}) (string, error) {
	res, err := mustache.Render(text, model...) // gg_utils.Formatter.Merge(text, model) // mustache.Render(text, model...)
	if nil != err {
		return text, nil
	}
	return res, nil
}

func tagsFromMustache(text string) ([]string, error) {
	tpl, err := mustache.ParseString(text)
	if nil != err {
		return nil, err
	}
	return getTagNames(tpl), nil
}

func getTagNames(tpl *mustache.Template) []string {
	response := make([]string, 0)
	deepTags(tpl.Tags(), &response)
	return response
}

func deepTags(tags []mustache.Tag, response *[]string) {
	for _, tag := range tags {
		switch tag.Type() {
		case mustache.Section, mustache.InvertedSection:
			deepTags(tag.Tags(), response)
		default:
			*response = append(*response, tag.Name())
		}
	}
}
