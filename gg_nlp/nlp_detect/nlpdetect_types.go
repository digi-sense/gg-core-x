package nlp_detect

type Tuple struct {
	Code  string
	Count float64
}

type Tuples []Tuple

func (t Tuples) Len() int {
	return len(t)
}

func (t Tuples) Less(i, j int) bool {
	return t[i].Count < t[j].Count
}

func (t Tuples) Swap(i, j int) {
	t[i], t[j] = t[j], t[i]
}

type languages map[string]trigrams
type trigrams map[string]int

type LanguageISO struct {
	Code1   string `json:"code-1"`
	Code2   string `json:"code-2"`
	Name    string `json:"name"`
	WikiUrl string `json:"wiki-url"`
}
