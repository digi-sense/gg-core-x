package nlp_detect

import _ "embed"

//go:embed embed_scriptData.json
var SCRIPT_DATA string

//go:embed embed_expressionData.json
var EXPRESSION_DATA string

//go:embed embed_iso6392.json
var NAMES_DATA string
