package nlp_detect

import (
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"encoding/json"
	"log"
	"os"
	"regexp"
	"sort"
	"strings"
	"sync"
	"unicode/utf8"
)

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t
//----------------------------------------------------------------------------------------------------------------------

const MAX_DIFFERENCE = float64(300)
const MAX_LENGTH = 2048
const MIN_LENGTH = 3

//----------------------------------------------------------------------------------------------------------------------
//	f i e l d s
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
//	i n i t
//----------------------------------------------------------------------------------------------------------------------

type LanguageDetector struct {
	initialized bool

	mux         *sync.Mutex
	scripts     map[string]languages
	expressions map[string]regexp.Regexp
	names       map[string]*LanguageISO
}

func NewLanguageDetector() (instance *LanguageDetector) {
	instance = new(LanguageDetector)
	instance.mux = new(sync.Mutex)

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *LanguageDetector) GetLanguageISO(value string) *LanguageISO {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()
		instance.initialize()

		return instance.getLanguageISO(value)
	}
	return nil
}

func (instance *LanguageDetector) GetLanguageISOName(value string) string {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		instance.initialize()

		v := instance.getLanguageISO(value)
		if nil != v {
			return v.Name
		}
	}
	return ""
}

// DetectWithFilters Get a list of probable languages the given value is written in.
func (instance *LanguageDetector) DetectWithFilters(value string, whitelist, blacklist []string) Tuples {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		instance.initialize()

		return instance.detectWithFilters(value, whitelist, blacklist)
	}
	return Tuples{}
}

// Detect Get a list of probable languages the given value is written in.
func (instance *LanguageDetector) Detect(value string) Tuples {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		instance.initialize()

		return instance.detectWithFilters(value, make([]string, 0), make([]string, 0))
	}
	return Tuples{}
}

// DetectOne Get the most probable language for the given value.
func (instance *LanguageDetector) DetectOne(value string) Tuple {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		instance.initialize()

		return instance.detect(value)[0]
	}
	return Tuple{}
}

// DetectOneWithFilters Get the most probable language for the given value.
func (instance *LanguageDetector) DetectOneWithFilters(value string, whitelist []string, blacklist []string) Tuple {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		instance.initialize()

		return instance.detectWithFilters(value, whitelist, blacklist)[0]
	}
	return Tuple{}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *LanguageDetector) initialize() {
	if nil != instance && !instance.initialized {
		instance.initialized = true

		//Init `scripts` and `expressions` dictionaries
		var scriptData map[string]interface{}
		err := json.Unmarshal([]byte(SCRIPT_DATA), &scriptData)
		if err != nil {
			log.Printf("Error during languages decoding: %v\n", err)
			os.Exit(1)
		}

		instance.scripts = make(map[string]languages)
		for scriptCode, scriptValue := range scriptData {
			mLanguages := make(map[string]trigrams)

			lang := scriptValue.(map[string]interface{})
			for code, trigramsRaw := range lang {
				trigramsString := trigramsRaw.(string)
				trigramsStringArray := strings.Split(trigramsString, "|")
				mTrigrams := make(map[string]int)
				for i := len(trigramsStringArray) - 1; i >= 0; i-- {
					mTrigrams[trigramsStringArray[i]] = i + 1
				}
				mLanguages[code] = mTrigrams
			}

			instance.scripts[scriptCode] = mLanguages
		}

		var expressionsData map[string]interface{}
		err = json.Unmarshal([]byte(EXPRESSION_DATA), &expressionsData)
		if err != nil {
			log.Printf("Error during expressions decoding: %v\n", err)
			os.Exit(1)
		}

		instance.expressions = make(map[string]regexp.Regexp)
		for code, v := range expressionsData {
			instance.expressions[code] = *regexp.MustCompile(v.(string))
		}

		// NAMES_DATA
		var namesData map[string]interface{}
		err = json.Unmarshal([]byte(NAMES_DATA), &namesData)
		if err != nil {
			log.Printf("Error during names decoding: %v\n", err)
			os.Exit(1)
		}
		instance.names = make(map[string]*LanguageISO)
		for k, m := range namesData {
			l := &LanguageISO{}
			name := gg_utils.Convert.ToString(gg_utils.Arrays.GetAt(gg_utils.Reflect.GetArray(m, "en"), 0, ""))
			if len(name) > 0 {
				l.Name = name
				l.Code1 = gg_utils.Reflect.GetString(m, "639-1")
				l.Code2 = gg_utils.Reflect.GetString(m, "639-2")
				l.WikiUrl = gg_utils.Reflect.GetString(m, "wikiUrl")
				instance.names[k] = l
			}
		}

	}
}

func (instance *LanguageDetector) getLanguageISO(value string) *LanguageISO {
	if nil != instance {
		if v, b := instance.names[value]; b {
			return v
		}
		for _, v := range instance.names {
			if v.Code1 == value || v.Code2 == value || v.Name == value {
				return v
			}
		}
	}
	return nil
}

func (instance *LanguageDetector) detect(value string) Tuples {
	if nil != instance {
		return instance.detectWithFilters(value, make([]string, 0), make([]string, 0))
	}
	return Tuples{}
}

func (instance *LanguageDetector) detectWithFilters(value string, whitelist, blacklist []string) Tuples {
	if nil != instance {

		if len(value) < MIN_LENGTH {
			return instance.singleLanguageTuples("und")
		}

		if len(value) > MAX_LENGTH {
			value = value[0:MAX_LENGTH]
		}

		code := instance.getTopScript(value)

		if _, ok := instance.scripts[code]; !ok {
			return instance.singleLanguageTuples(code)
		}

		return instance.normalize(value, instance.getDistances(instance.getTrigramsAsTuples(value),
			instance.scripts[code], whitelist, blacklist))
	}
	return Tuples{}
}

// Filter `languages` by removing languages in `blacklist`, or including languages in `whitelist`.
func (instance *LanguageDetector) filterLanguages(langs languages, whitelist []string, blacklist []string) languages {
	filteredLanguages := make(languages)

	if len(whitelist) == 0 && len(blacklist) == 0 {
		return langs
	}

	if len(whitelist) == 0 {
		filteredLanguages = langs
	} else {
		for _, code := range whitelist {
			filteredLanguages[code] = langs[code]
		}
	}

	for _, code := range blacklist {
		if _, ok := filteredLanguages[code]; ok {
			delete(filteredLanguages, code)
		}
	}
	return filteredLanguages
}

// Get the distance between `trigrams`, and a trigram `model`.
func (instance *LanguageDetector) getDistance(trigrams Tuples, model trigrams) float64 {
	distance := float64(0)

	for _, t := range trigrams {
		difference := float64(0)
		if modelVal, ok := model[t.Code]; ok {
			difference = t.Count - float64(modelVal)

			if difference < 0 {
				difference = -difference
			}
		} else {
			difference = MAX_DIFFERENCE
		}

		distance += difference
	}

	return distance
}

// Get the distance between `trigrams`, and multiple trigram dictionaries `languages`.
func (instance *LanguageDetector) getDistances(trigrams Tuples, languages languages, whitelist []string, blacklist []string) Tuples {
	filteredLanguages := instance.filterLanguages(languages, whitelist, blacklist)
	tuples := make(Tuples, 0)

	for code, language := range filteredLanguages {
		dis := instance.getDistance(trigrams, language)
		t := Tuple{Code: code, Count: dis}
		tuples = append(tuples, t)
	}

	return tuples
}

// Get the occurrence ratio of `expression` for `value`.
func (instance *LanguageDetector) getOccurrence(value string, expression regexp.Regexp) float64 {
	count := len(expression.FindAllString(value, -1))

	if count < 1 {
		count = 0
	}

	return float64(count) / float64(len(value))
}

// From `scripts`, get the most occurring script for `value`
func (instance *LanguageDetector) getTopScript(value string) string {

	topCount := float64(-1)
	expressionCode := ""

	for code, e := range instance.expressions {
		count := instance.getOccurrence(value, e)

		if count > topCount {
			topCount = count
			expressionCode = code
		}
	}

	return expressionCode
}

// Create a single tuple as a list of tuples from a given language code.
func (instance *LanguageDetector) singleLanguageTuples(code string) Tuples {
	tuples := make(Tuples, 1)
	tuples[0] = Tuple{Code: code, Count: 1}
	return tuples
}

func (instance *LanguageDetector) normalize(value string, distances Tuples) Tuples {
	sort.Sort(distances)

	min := distances[0].Count
	max := (float64(len(value)) * MAX_DIFFERENCE) - min

	for i, d := range distances {
		distances[i].Count = float64(1) - ((d.Count - min) / max)
	}

	return distances
}

func (instance *LanguageDetector) clean(value string) string {
	re := regexp.MustCompile("[\u0021-\u0040]+")
	value = re.ReplaceAllString(value, " ")

	re = regexp.MustCompile(`\s+`)
	value = re.ReplaceAllString(value, " ")

	value = strings.ToLower(value)
	value = strings.Trim(value, " ")

	return value
}

func (instance *LanguageDetector) getTrigrams(value string) []string {
	value = " " + instance.clean(value) + " "
	res := make([]string, 0)
	val := NewString(value) // utf8string.NewString(value)
	i := 0
	for i+3 < utf8.RuneCountInString(value) {
		res = append(res, val.Slice(i, i+3))
		i++
	}

	return res
}

func (instance *LanguageDetector) getTrigramsAsMap(value string) map[string]int {
	trigrams := instance.getTrigrams(value)
	res := make(map[string]int)
	for _, t := range trigrams {
		if _, ok := res[t]; !ok {
			res[t] = 0
		}
		res[t]++
	}

	return res
}

func (instance *LanguageDetector) getTrigramsAsTuples(value string) []Tuple {
	trigrams := instance.getTrigramsAsMap(value)
	res := make([]Tuple, len(trigrams))
	i := 0
	for code, count := range trigrams {
		res[i] = Tuple{Code: code, Count: float64(count)}
		i++
	}

	return res
}
