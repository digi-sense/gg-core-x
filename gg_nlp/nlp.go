package gg_nlp

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_nlp/nlp_classifier"
	"bitbucket.org/digi-sense/gg-core-x/gg_nlp/nlp_detect"
	"bitbucket.org/digi-sense/gg-core-x/gg_nlp/nlp_entities"
	"bitbucket.org/digi-sense/gg-core-x/gg_nlp/nlp_scoring"
	"bitbucket.org/digi-sense/gg-core-x/gg_nlp/nlp_summarize"
	"bitbucket.org/digi-sense/gg-core-x/gg_nlp/nlp_utils"
	"bitbucket.org/digi-sense/gg-core/gg_num2word"
	"sort"
)

type NLPHelper struct {
	_languageDetector *nlp_detect.LanguageDetector
	// _summarizer *nlp_summarize.Summarizer
	_entities   *nlp_entities.EntityMatcher
	_scoring    *nlp_scoring.ScoreCalculator
	_classifier *nlp_classifier.NlpClassifier
}

var NLP *NLPHelper

func init() {
	NLP = new(NLPHelper)
}

type Keyword struct {
	Weight int
	Value  string
}

func (instance *Keyword) String() string {
	if nil != instance {
		return instance.Value
	}
	return ""
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *NLPHelper) LanguageDetector() *nlp_detect.LanguageDetector {
	if nil != instance {
		if nil == instance._languageDetector {
			instance._languageDetector = nlp_detect.NewLanguageDetector()
		}
		return instance._languageDetector
	}
	return nil
}

func (instance *NLPHelper) Summarizer() *nlp_summarize.Summarizer {
	if nil != instance {
		return nlp_summarize.NewSummarizer()
	}
	return nil
}

func (instance *NLPHelper) Entities() *nlp_entities.EntityMatcher {
	if nil != instance {
		if nil == instance._entities {
			instance._entities = nlp_entities.NewEntityMatcher()
		}
		return instance._entities
	}
	return nil
}

func (instance *NLPHelper) Score() *nlp_scoring.ScoreCalculator {
	if nil != instance {
		if nil == instance._scoring {
			instance._scoring = nlp_scoring.NewScoreCalculator()
		}
		return instance._scoring
	}
	return nil
}

func (instance *NLPHelper) Classifier() *nlp_classifier.NlpClassifier {
	if nil != instance {
		if nil == instance._classifier {
			instance._classifier = nlp_classifier.NewNlpClassifier()
		}
		return instance._classifier
	}
	return nil
}

//----------------------------------------------------------------------------------------------------------------------
//	d e t e c t    l a n g u a g e
//----------------------------------------------------------------------------------------------------------------------

func (instance *NLPHelper) GetLanguageCode(text string) string {
	if nil != instance {
		return instance.LanguageDetector().DetectOne(text).Code
	}
	return ""
}

//----------------------------------------------------------------------------------------------------------------------
//	n u m 2 w o r d
//----------------------------------------------------------------------------------------------------------------------

func (instance *NLPHelper) Num2Word(num int, lang ...string) string {
	if nil != instance {
		converter := gg_num2word.Num2Word2
		if len(lang) == 1 {
			return converter.Convert(num, lang[0])
		}
		return converter.ConvertDefault(num)
	}
	return ""
}

//----------------------------------------------------------------------------------------------------------------------
//	s u m m a r i z e
//----------------------------------------------------------------------------------------------------------------------

// SummarizeLexRankHigh very short text. Response is about 15% of original text
func (instance *NLPHelper) SummarizeLexRankHigh(text string) []string {
	return instance.SummarizeLexRank(text, nlp_summarize.HighSummarySentences(text))
}

// SummarizeLexRankMedium response is about 25% of original text
func (instance *NLPHelper) SummarizeLexRankMedium(text string) []string {
	return instance.SummarizeLexRank(text, nlp_summarize.MediumSummarySentences(text))
}

// SummarizeLexRankLow response is about 40% of original text
func (instance *NLPHelper) SummarizeLexRankLow(text string) []string {
	return instance.SummarizeLexRank(text, nlp_summarize.LowSummarySentences(text))
}

func (instance *NLPHelper) SummarizeLexRank(text string, intoSentences int) []string {
	summarizer := instance.Summarizer()
	result, _ := summarizer.Summarize(text, intoSentences)

	return result
}

//----------------------------------------------------------------------------------------------------------------------
//	e n t i t i e s
//----------------------------------------------------------------------------------------------------------------------

func (instance *NLPHelper) MatchEntitiesAll(text string) map[string][]string {
	return instance.Entities().MatchEntitiesAll(text)
}

func (instance *NLPHelper) MatchEntities(text string, list []string) map[string][]string {
	return instance.Entities().MatchEntities(text, list)
}

// GetEntitiesAll return only existing entities
func (instance *NLPHelper) GetEntitiesAll(text string) map[string][]string {
	return instance.Entities().GetEntitiesAll(text)
}

func (instance *NLPHelper) GetEntities(text string, list []string) map[string][]string {
	return instance.Entities().GetEntities(text, list)
}

//----------------------------------------------------------------------------------------------------------------------
//	s c o r e
//----------------------------------------------------------------------------------------------------------------------

// ScoreBest Calculate a matching score between a phrase and a check test using expressions.
// ALL expressions are evaluated.
// Failed expressions  do not add negative score to result.
// Return best score above all
// @param [string] phrase. "hello humanity!! I'm Mario rossi"
// @param [string] expressions. All expressions to match. ["hel??0 h*", "I* * ros*"]
func (instance *NLPHelper) ScoreBest(text string, expressions []string) float32 {
	return instance.Score().ScoreBest(text, expressions)
}

// ScoreOr Calculate a matching score between a phrase and a check test using expressions.
// ALL expressions are evaluated.
// Failed expressions  do not add negative score to result
// @param [string] phrase. "hello humanity!! I'm Mario rossi"
// @param [string] expressions. All expressions to match. ["hel??0 h*", "I* * ros*"]
func (instance *NLPHelper) ScoreOr(text string, expressions []string) float32 {
	return instance.Score().ScoreOr(text, expressions)
}

// ScoreAnd Calculate a matching score between a phrase and a check test using expressions.
// ALL expressions are evaluated.
// Failed expressions  add negative score to result
// @param [string] phrase. "hello humanity!! I'm Mario rossi"
// @param [string] expressions. All expressions to match. ["hel??0 h*", "I* * ros*"]
func (instance *NLPHelper) ScoreAnd(text string, expressions []string) float32 {
	return instance.Score().ScoreAnd(text, expressions)
}

//----------------------------------------------------------------------------------------------------------------------
//	k e y w o r d s
//----------------------------------------------------------------------------------------------------------------------

func (instance *NLPHelper) GetKeywords(text string, minLen int) []string {
	return nlp_utils.GetKeywords(text, minLen, false)
}

func (instance *NLPHelper) GetKeywordsWithSpecialChars(text string, minLen int) []string {
	return nlp_utils.GetKeywords(text, minLen, true)
}

func (instance *NLPHelper) GetKeywordsSorted(text string, minLen int) []string {
	return nlp_utils.GetKeywordsSorted(text, minLen, false)
}

func (instance *NLPHelper) GetKeywordsSortedWithSpecialChars(text string, minLen int) []string {
	return nlp_utils.GetKeywordsSorted(text, minLen, true)
}

// GetKeywordsProgression generate an array of keywords with also partial words like ["word", "wor", "wo"].
// This is useful searching for partial words
func (instance *NLPHelper) GetKeywordsProgression(text string, minLen int) []string {
	return nlp_utils.GetKeywordsProgression(text, minLen)
}

func (instance *NLPHelper) MapKeywords(text string, minLen int) map[string]int {
	return nlp_utils.MapKeywords(text, minLen, false)
}

func (instance *NLPHelper) MapKeywordsWithSpecialChars(text string, minLen int) map[string]int {
	return nlp_utils.MapKeywords(text, minLen, true)
}

func (instance *NLPHelper) ListKeywords(text string, minLen int, sortByWeight, includeSpecialChars bool) (response []*Keyword) {
	return instance.ListKeywordsWithMinWeight(text, minLen, 0, sortByWeight, includeSpecialChars)
}

func (instance *NLPHelper) ListKeywordsWith(text string, minLen int, sortByWeight, includeSpecialChars bool) (response []*Keyword) {
	return instance.ListKeywordsWithMinWeight(text, minLen, 0, sortByWeight, includeSpecialChars)
}

func (instance *NLPHelper) ListKeywordsWithMinWeight(text string, minLen, minWeight int, sortByWeight, includeSpecialChars bool) (response []*Keyword) {
	response = make([]*Keyword, 0)
	var m map[string]int
	if includeSpecialChars {
		m = instance.MapKeywordsWithSpecialChars(text, minLen)
	} else {
		m = instance.MapKeywords(text, minLen)
	}
	for value, weight := range m {
		if weight >= minWeight {
			response = append(response, &Keyword{weight, value})
		}
	}
	if sortByWeight {
		sort.Slice(response, func(i, j int) bool {
			return response[i].Weight > response[j].Weight
		})
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------
