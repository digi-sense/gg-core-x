package nlp_entities

import "bitbucket.org/digi-sense/gg-core"

type EntityMatcher struct {
}

func NewEntityMatcher() (instance *EntityMatcher) {
	instance = new(EntityMatcher)
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *EntityMatcher) MatchEntitiesAll(text string) map[string][]string {
	return instance.MatchEntities(text, EntitiesAll)
}

func (instance *EntityMatcher) MatchEntities(text string, list []string) map[string][]string {
	response := make(map[string][]string)
	for _, name := range list {
		data := entityByName(name, text)
		response[name] = data
	}
	return response
}

// GetEntitiesAll return only existing entities
func (instance *EntityMatcher) GetEntitiesAll(text string) map[string][]string {
	return instance.GetEntities(text, EntitiesAll)
}

func (instance *EntityMatcher) GetEntities(text string, list []string) map[string][]string {
	response := make(map[string][]string)
	entities := instance.MatchEntities(text, list)
	for k, v := range entities {
		if len(v) > 0 {
			response[k] = v
		}
	}
	return response
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func entityByName(name, text string) []string {
	switch name {
	case entityBtc:
		return gg.Regex.BtcAddresses(text)
	case entityStreet:
		return gg.Regex.StreetAddresses(text)
	case entityIP:
		return gg.Regex.IPs(text)
	case entityIPv4:
		return gg.Regex.IPv4s(text)
	case entityIPv6:
		return gg.Regex.IPv6s(text)
	case entityEmail:
		return gg.Regex.Emails(text)
	case entityNumber:
		return gg.Regex.Numbers(text)
	case entityDate:
		return gg.Regex.Date(text)
	case entityTime:
		return gg.Regex.Time(text)
	case entityPhone:
		response := gg.Regex.Phones(text)
		response = gg.Arrays.AppendUnique(response, gg.Regex.PhonesWithExts(text)).([]string)
		return response
	case entityLink:
		return gg.Regex.Urls(text)
	case entityPort:
		return gg.Regex.NotKnownPorts(text)
	case entityPrice:
		return gg.Regex.Prices(text)
	case entityHexColor:
		return gg.Regex.HexColors(text)
	case entityCreditCard:
		return gg.Regex.CreditCards(text)
	case entityVisaCreditCard:
		return gg.Regex.VISACreditCards(text)
	case entityMCCreditCard:
		return gg.Regex.MCCreditCards(text)
	case entityZipCode:
		return gg.Regex.ZipCodes(text)
	case entityMACAddress:
		return gg.Regex.MACAddresses(text)
	case entityIBAN:
		return gg.Regex.IBANs(text)
	}
	return make([]string, 0)
}
