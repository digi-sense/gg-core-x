package nlp_entities

const (
	entityBtc            = "btc"
	entityStreet         = "street"
	entityIP             = "ip"
	entityIPv4           = "ipv4"
	entityIPv6           = "ipv6"
	entityEmail          = "email"
	entityNumber         = "number"
	entityDate           = "date"
	entityTime           = "time"
	entityPhone          = "phone"
	entityLink           = "link"
	entityPort           = "port"
	entityPrice          = "price"
	entityHexColor       = "hex-color"
	entityCreditCard     = "credit-card"
	entityVisaCreditCard = "visa-credit-card"
	entityMCCreditCard   = "mastercard-credit-card"
	entityZipCode        = "zip-code"
	entityMACAddress     = "mac-address"
	entityIBAN           = "iban"
)

var (
	EntitiesAll = []string{
		entityBtc,
		entityStreet,
		entityIP,
		entityIPv4,
		entityIPv6,
		entityEmail,
		entityNumber,
		entityDate,
		entityTime,
		entityPhone,
		entityLink,
		entityPort,
		entityPrice,
		entityHexColor,
		entityCreditCard,
		entityVisaCreditCard,
		entityMCCreditCard,
		entityZipCode,
		entityMACAddress,
		entityIBAN,
	}

	EntitiesBasic = []string{
		entityEmail,
		entityNumber,
		entityNumber,
		entityDate,
		entityTime,
		entityPhone,
		entityLink,
		entityZipCode,
		entityStreet,
	}

	EntitiesBank = []string{
		entityCreditCard,
		entityVisaCreditCard,
		entityMCCreditCard,
		entityIBAN,
	}

	EntitiesNetwork = []string{
		entityMACAddress,
		entityIP,
		entityIPv6,
		entityIPv4,
		entityPort,
		entityBtc,
	}
)
