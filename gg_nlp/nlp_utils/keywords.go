package nlp_utils

import (
	"bitbucket.org/digi-sense/gg-core"
	"fmt"
	"regexp"
)

//----------------------------------------------------------------------------------------------------------------------
//	k e y w o r d s
//----------------------------------------------------------------------------------------------------------------------

func GetKeywords(text string, minLen int, includeSpecialChars bool) []string {
	var exp *regexp.Regexp
	if includeSpecialChars {
		exp = regexp.MustCompile(fmt.Sprintf("\\b[\\w]{%v,100}\\b", minLen))
	} else {
		exp = regexp.MustCompile(fmt.Sprintf("\\b\\w{%v,100}\\b", minLen))
	}
	return exp.FindAllString(text, -1)
}

func GetKeywordsSorted(text string, minLen int, includeSpecialChars bool) []string {
	keywords := GetKeywords(text, minLen, includeSpecialChars)
	gg.Arrays.Sort(keywords)
	return keywords
}

// GetKeywordsProgression generate an array of keywords with also partial words like ["word", "wor", "wo"].
// This is useful searching for partial words
func GetKeywordsProgression(text string, minLen int) []string {
	limit := minLen - 1
	response := make([]string, 0)
	if len(text) > 0 {
		tokens := gg.Strings.Split(text, " ,.!?")
		for _, token := range tokens {
			if len(token) > limit {
				if len(token) > limit+2 {
					for i := 0; i < len(token)-2; i++ {
						response = append(response, token[:len(token)-i])
					}
				} else {
					response = append(response, token)
				}
			}
		}
	}
	return response
}

func MapKeywords(text string, minLen int, includeSpecialChars bool) map[string]int {
	response := map[string]int{}
	keywords := GetKeywords(text, minLen, includeSpecialChars)
	for _, k := range keywords {
		if _, b := response[k]; !b {
			response[k] = 0
		}
		response[k] = response[k] + 1
	}
	return response
}
