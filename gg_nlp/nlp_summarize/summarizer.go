package nlp_summarize

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_nlp/nlp_utils"
	"encoding/json"
	"sort"
	"strings"
	"unicode"
)

const (
	AlgPageRank   = "pagerank"
	AlgCentrality = "centrality"
	AlgCustom     = "custom"

	EdgJaccard = "jaccard"
	EdgHamming = "hamming"
	EdgCustom  = "custom"

	DEFAULT_ALGORITHM                    = AlgPageRank
	DEFAULT_WEIGHING                     = EdgHamming
	DEFAULT_DAMPING                      = 0.85
	DEFAULT_TOLERANCE                    = 0.0001
	DEFAULT_THRESHOLD                    = 0.001
	DEFAULT_MAX_CHARACTERS               = 0
	DEFAULT_SENTENCES_DISTANCE_THRESHOLD = 0.95
)

type Rank struct {
	idx   int
	score float64
}

type Edge struct {
	src    int     // index of node
	dst    int     // index of node
	weight float64 // weight of the similarity between two sentences
}

type Node struct {
	sentenceIndex int   // index of sentence from the bag
	vector        []int // map of word count in respect with dict, should we use map instead of slice?
	// for example :
	/*
		dict = {
			i : 1
			am : 2
			the : 3
			shit : 4
		}
		str = "I am not shit, you effin shit"
		vector = [1, 1, 0, 2] => [1, 1, 0, 1] because should be binary
	*/
}

type Summarizer struct {
	BagOfWordsPerSentence [][]string
	OriginalSentences     []string
	Dict                  map[string]int
	Nodes                 []*Node
	Edges                 []*Edge
	Ranks                 []int

	MaxCharacters              int
	Algorithm                  string // "centrality" or "pagerank" or "custom"
	Weighing                   string // "hamming" or "jaccard" or "custom"
	Damping                    float64
	Tolerance                  float64
	Threshold                  float64
	SentencesDistanceThreshold float64

	customAlgorithm func(e []*Edge) []int
	customWeighing  func(src, dst []int) float64
	wordTokenizer   func(sentence string) []string

	vectorLength int
}

// NewSummarizer creates a new summarizer
func NewSummarizer() *Summarizer {
	return &Summarizer{
		MaxCharacters:              DEFAULT_MAX_CHARACTERS,
		Algorithm:                  DEFAULT_ALGORITHM,
		Weighing:                   DEFAULT_WEIGHING,
		Damping:                    DEFAULT_DAMPING,
		Tolerance:                  DEFAULT_TOLERANCE,
		Threshold:                  DEFAULT_THRESHOLD,
		SentencesDistanceThreshold: DEFAULT_SENTENCES_DISTANCE_THRESHOLD,
		wordTokenizer:              DefaultWordTokenizer,
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *Summarizer) String() string {
	r, _ := json.MarshalIndent(instance, "", "  ")
	return string(r)
}

// Set max characters, damping, tolerance, threshold, sentences distance threshold, algorithm, and weighing
func (instance *Summarizer) Set(m int, d, t, th, sth float64, alg, w string) {
	instance.MaxCharacters = m
	instance.Damping = d
	instance.Tolerance = t
	instance.Threshold = th
	instance.Algorithm = alg
	instance.Weighing = w
	instance.SentencesDistanceThreshold = sth
}

// SetDictionary Useful if you already have your own dictionary (example: from your database)
// Dictionary is a map[string]int where the key is the word and int is the position in vector, starting from 1
func (instance *Summarizer) SetDictionary(dict map[string]int) {
	instance.Dict = dict
}

func (instance *Summarizer) SetCustomAlgorithm(f func(e []*Edge) []int) {
	instance.customAlgorithm = f
}

func (instance *Summarizer) SetCustomWeighing(f func(src, dst []int) float64) {
	instance.customWeighing = f
}

func (instance *Summarizer) SetWordTokenizer(f func(string) []string) {
	instance.wordTokenizer = f
}

// Summarize the text to num sentences
func (instance *Summarizer) Summarize(text string, num int) ([]string, error) {
	text = strings.TrimSpace(text)
	if len(text) < 1 && len(instance.OriginalSentences) == 0 {
		return nil, nil
	}

	instance.createSentences(text) // only actually creates sentences if no OrignalSentences

	// If user already provide dictionary, pass creating dictionary
	if len(instance.Dict) < 1 {
		if text == "" {
			text = strings.TrimSpace(strings.Join(instance.OriginalSentences, " "))
		}
		instance.createDictionary(text)
	}

	instance.createNodes()
	instance.createEdges()

	switch instance.Algorithm {
	case AlgCentrality:
		instance.centrality()
	case AlgPageRank:
		instance.pageRank()
	case AlgCustom:
		instance.Ranks = instance.customAlgorithm(instance.Edges)
	default:
		instance.pageRank()
	}

	// if no ranks, return error
	lenRanks := len(instance.Ranks)
	if lenRanks == 0 {
		return nil, nil
	}

	// guard so it won't crash but return only the highest rank sentence
	// if num is invalid
	if num > lenRanks || num < 1 {
		num = 1
	}

	// get only top num of ranks
	idx := instance.Ranks[:num]
	// sort it ascending by how the sentences appeared on the original text
	sort.Ints(idx)

	return instance.concatResult(idx), nil
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

// concatenate sentences at idx to result string
func (instance *Summarizer) concatResult(idx []int) []string {
	var res []string
	if instance.MaxCharacters > 0 {
		lenRes := 0
		for i := range idx {
			lenOrig := len([]rune(instance.OriginalSentences[idx[i]]))
			if lenRes+lenOrig <= instance.MaxCharacters {
				res = append(res, instance.OriginalSentences[idx[i]])
			} else {
				n := instance.MaxCharacters - lenRes
				if n > lenOrig {
					n = lenOrig
				}
				res = append(res, string([]rune(instance.OriginalSentences[idx[i]])[:n]))
				break
			}
			lenRes += lenOrig
		}
		return res
	}

	for i := range idx {
		res = append(res, instance.OriginalSentences[idx[i]])
	}

	return res
}

func (instance *Summarizer) centrality() {
	// first remove edges under Threshold weight
	var newEdges []*Edge
	for _, edge := range instance.Edges {
		if edge.weight > instance.Threshold {
			newEdges = append(newEdges, edge)
		}
	}
	// sort them by weight descending
	sort.Sort(ByWeight(newEdges))
	ReverseEdge(newEdges)
	rankBySrc := make([]int, len(newEdges))
	for i, v := range newEdges {
		rankBySrc[i] = v.src
	}
	// uniq it without disturbing the order
	m := make(map[int]bool)
	var uniq []int
	for _, v := range rankBySrc {
		if m[v] {
			continue
		}
		uniq = append(uniq, v)
		m[v] = true
	}
	instance.Ranks = uniq
}

func (instance *Summarizer) pageRank() {
	// first remove edges under Threshold weight
	var newEdges []*Edge
	for _, edge := range instance.Edges {
		if edge.weight > instance.Threshold {
			newEdges = append(newEdges, edge)
		}
	}
	// then page rank them
	graph := nlp_utils.NewGraph()
	defer graph.Reset()
	for _, edge := range newEdges {
		graph.Link(uint32(edge.src), uint32(edge.dst), edge.weight)
	}
	var ranks []*Rank
	graph.Rank(instance.Damping, instance.Tolerance, func(sentenceIndex uint32, rank float64) {
		ranks = append(ranks, &Rank{int(sentenceIndex), rank})
	})
	// sort ranks into an array of sentence index, by score descending
	sort.Sort(ByScore(ranks))
	ReverseRank(ranks)
	idx := make([]int, len(ranks))
	for i, v := range ranks {
		idx[i] = v.idx
	}

	instance.Ranks = idx
}

func (instance *Summarizer) createEdges() {
	for i, src := range instance.Nodes {
		for j, dst := range instance.Nodes {
			// don't compare same node
			if i != j {
				var weight float64
				switch instance.Weighing {
				case EdgJaccard:
					commonElements := Intersection(src.vector, dst.vector)
					weight = 1.0 - float64(len(commonElements))/((float64(instance.vectorLength)*2)-float64(len(commonElements)))
				case EdgHamming:
					differentElements := SymmetricDifference(src.vector, dst.vector)
					weight = float64(len(differentElements))
				case EdgCustom:
					weight = instance.customWeighing(src.vector, dst.vector)
				default:
					differentElements := SymmetricDifference(src.vector, dst.vector)
					weight = float64(len(differentElements))
				}
				edge := &Edge{i, j, weight}
				instance.Edges = append(instance.Edges, edge)
			}
		}
	}
}

func (instance *Summarizer) createNodes() {
	instance.vectorLength = len(instance.Dict)
	for i, sentence := range instance.BagOfWordsPerSentence {
		// vector length is len(dict)
		vector := make([]int, instance.vectorLength)
		// word for word now
		for _, word := range sentence {
			// check word dict position, if doesn't exist, skip
			if instance.Dict[word] == 0 {
				continue
			}
			// minus 1, because array started from 0 and lowest dict is 1
			pos := instance.Dict[word] - 1
			// set 1 to the position
			vector[pos] = 1
		}
		// vector is now created, put it into the node
		node := &Node{i, vector}
		// node is now completed, put into the instance
		instance.Nodes = append(instance.Nodes, node)
	}
}

func (instance *Summarizer) createSentences(text string) {
	if len(instance.OriginalSentences) == 0 {
		// trim all spaces
		// done by calling func: text = strings.TrimSpace(text)
		// tokenize text as sentences
		// sentence is a group of words separated by whitespaces or punctuation other than !?.
		instance.OriginalSentences = TokenizeSentences(text)
	}

	// from original sentences, explode each sentences into instance of words
	instance.BagOfWordsPerSentence = [][]string{}
	for _, sentence := range instance.OriginalSentences {
		words := instance.wordTokenizer(sentence)
		instance.BagOfWordsPerSentence = append(instance.BagOfWordsPerSentence, words)
	}

	// then uniq it
	UniqSentences(instance.BagOfWordsPerSentence, instance.SentencesDistanceThreshold)
}

func (instance *Summarizer) createDictionary(text string) {
	// trim all spaces
	// this already done by calling func:	text = strings.TrimSpace(text)
	// lowercase the text
	text = strings.ToLower(text)
	// remove all non alphanumerics but spaces
	var prev rune
	text = strings.Map(func(r rune) rune {
		if r == '-' && (unicode.IsDigit(prev) || unicode.IsLetter(prev)) {
			return r
		}
		if !unicode.IsDigit(r) && !unicode.IsLetter(r) && !unicode.IsSpace(r) {
			return -1
		}
		prev = r
		return r
	}, text)
	// turn it into instance of words
	words := strings.Fields(text)
	// turn it into dictionary
	dict := make(map[string]int)
	i := 1
	for _, word := range words {
		if dict[word] == 0 {
			dict[word] = i
			i++
		}
	}
	instance.Dict = dict
}
