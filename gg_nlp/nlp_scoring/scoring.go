package nlp_scoring

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_nlp/nlp_utils"
	"strings"
)

type ScoreCalculator struct {
}

func NewScoreCalculator() (instance *ScoreCalculator) {
	instance = new(ScoreCalculator)
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	s c o r e
//----------------------------------------------------------------------------------------------------------------------

// ScoreBest Calculate a matching score between a phrase and a check test using expressions.
// ALL expressions are evaluated.
// Failed expressions  do not add negative score to result.
// Return best score above all
// @param [string] phrase. "hello humanity!! I'm Mario rossi"
// @param [string] expressions. All expressions to match. ["hel??0 h*", "I* * ros*"]
func (instance *ScoreCalculator) ScoreBest(text string, expressions []string) float32 {
	return gg.Regex.WildcardScoreBest(normalize(text), expressions)
}

// ScoreOr Calculate a matching score between a phrase and a check test using expressions.
// ALL expressions are evaluated.
// Failed expressions  do not add negative score to result
// @param [string] phrase. "hello humanity!! I'm Mario rossi"
// @param [string] expressions. All expressions to match. ["hel??0 h*", "I* * ros*"]
func (instance *ScoreCalculator) ScoreOr(text string, expressions []string) float32 {
	return gg.Regex.WildcardScoreAny(normalize(text), expressions)
}

// ScoreAnd Calculate a matching score between a phrase and a check test using expressions.
// ALL expressions are evaluated.
// Failed expressions  add negative score to result
// @param [string] phrase. "hello humanity!! I'm Mario rossi"
// @param [string] expressions. All expressions to match. ["hel??0 h*", "I* * ros*"]
func (instance *ScoreCalculator) ScoreAnd(text string, expressions []string) float32 {
	return gg.Regex.WildcardScoreAll(normalize(text), expressions)
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func normalize(text string) string {
	tokens := nlp_utils.GetKeywords(text, 3, false)
	return strings.Join(tokens, " ")
}
