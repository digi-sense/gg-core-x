package nlp_classifier

import (
	"bitbucket.org/digi-sense/gg-core"
)

type NlpClassificationCategory struct {
	Name string   `json:"name"`
	Tags []string `json:"tags"`
}

type NlpClassificationOptions struct {
	DetectLanguage      bool                         `json:"detect-lang"`
	DetectKeywordsMode  string                       `json:"detect-keywords-mode"` // all, unique, none
	DetectEntities      []string                     `json:"detect-entities"`
	SummarizerSentences int                          `json:"summarizer-sentences"`
	MatchLevel          string                       `json:"match-level"` // all, any, best
	Categories          []*NlpClassificationCategory `json:"categories"`
}

func ParseOptionsFromJson(text string) (options *NlpClassificationOptions, err error) {
	err = gg.JSON.Read(text, &options)
	return
}
