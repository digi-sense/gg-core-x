package nlp_classifier

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_nlp/nlp_detect"
	"bitbucket.org/digi-sense/gg-core-x/gg_nlp/nlp_entities"
	"bitbucket.org/digi-sense/gg-core-x/gg_nlp/nlp_scoring"
	"bitbucket.org/digi-sense/gg-core-x/gg_nlp/nlp_summarize"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"errors"
	"strings"
)

var (
	MissingTextError = errors.New("missing_text_error")
	MissingTagsError = errors.New("missing_tags_error")
)

const (
	MatchLevelAll  = "all"
	MatchLevelAny  = "any"
	MatchLevelBest = "best"
)

type NlpClassifier struct {
	calculator       *nlp_scoring.ScoreCalculator
	languageDetector *nlp_detect.LanguageDetector
	entityMatcher    *nlp_entities.EntityMatcher
	benchSummarizer  *nlp_summarize.Summarizer
}

func NewNlpClassifier() (instance *NlpClassifier) {
	instance = new(NlpClassifier)
	instance.calculator = nlp_scoring.NewScoreCalculator()
	instance.languageDetector = nlp_detect.NewLanguageDetector()
	instance.entityMatcher = nlp_entities.NewEntityMatcher()
	instance.benchSummarizer = nlp_summarize.NewSummarizer()

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	public
//----------------------------------------------------------------------------------------------------------------------

func (instance *NlpClassifier) Classify(text string, options *NlpClassificationOptions) (out *NlpClassificationOutput, err error) {
	out = new(NlpClassificationOutput)
	if len(text) > 0 {
		if nil != options {
			// get language
			if options.DetectLanguage {
				out.Language = instance.languageDetector.DetectOne(text).Code
			}
			// get entities
			if len(options.DetectEntities) > 0 {
				out.Entities = instance.entityMatcher.GetEntities(text, options.DetectEntities)
			}
			if options.SummarizerSentences > 0 {
				rows, _ := instance.benchSummarizer.Summarize(text, options.SummarizerSentences)
				if len(rows) > 0 {
					out.Summary = strings.Join(rows, "\n")
				}
			}
			level := options.MatchLevel
			for _, category := range options.Categories {
				// get score
				var score float32
				score, err = instance.Score(text, level, category.Tags)
				// get keywords
				keywords := make([]string, 0)
				if len(options.DetectKeywordsMode) > 0 {
					switch options.DetectKeywordsMode {
					case "all":
						keywords = instance.MatchKeywordsAll(text, category.Tags)
					case "unique":
						keywords = instance.MatchKeywordsUnique(text, category.Tags)
					}
				}

				out.Response = append(out.Response, &NlpClassificationResponse{
					Language: out.Language,
					Category: category.Name,
					Score:    score,
					Keywords: keywords,
				})
			}
			out.SortDesc()
		} else {
			err = MissingTagsError
		}
	} else {
		err = MissingTextError
	}
	if nil != err {
		out.Error = err.Error()
	}
	return
}

func (instance *NlpClassifier) Score(text, level string, tags []string) (score float32, err error) {
	if len(text) > 0 {
		if nil != tags {
			switch level {
			case MatchLevelAll:
				score = instance.calculator.ScoreAnd(text, tags)
			case MatchLevelAny:
				score = instance.calculator.ScoreOr(text, tags)
			case MatchLevelBest:
				score = instance.calculator.ScoreBest(text, tags)
			default:
				// best
				score = instance.calculator.ScoreBest(text, tags)
			}
		} else {
			err = MissingTagsError
		}
	} else {
		err = MissingTextError
	}
	return
}

func (instance *NlpClassifier) MatchKeywordsAll(text string, tags []string) (keywords []string) {
	keywords = make([]string, 0)
	for _, expression := range tags {
		tag := gg_utils.ParseExpression(expression)
		matching := gg.Regex.WildcardMatch(text, tag.Expression)
		if len(matching) > 0 {
			keywords = append(keywords, matching...)
		}
	}
	return
}

func (instance *NlpClassifier) MatchKeywordsUnique(text string, tags []string) (keywords []string) {
	keywords = make([]string, 0)
	for _, expression := range tags {
		tag := gg_utils.ParseExpression(expression)
		matching := gg.Regex.WildcardMatch(text, tag.Expression)
		if len(matching) > 0 {
			for _, item := range matching {
				keywords = gg_utils.Arrays.AppendUnique(keywords, item).([]string)
			}
		}
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	private
//----------------------------------------------------------------------------------------------------------------------
