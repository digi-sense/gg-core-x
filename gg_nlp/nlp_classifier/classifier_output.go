package nlp_classifier

import (
	"bitbucket.org/digi-sense/gg-core"
	"sort"
)

type NlpClassificationResponse struct {
	Language string   `json:"language"`
	Category string   `json:"category"`
	Score    float32  `json:"score"`
	Keywords []string `json:"keywords"`
}
type NlpClassificationOutput struct {
	Language string                       `json:"language"`
	Entities map[string][]string          `json:"entities"`
	Summary  string                       `json:"summary"`
	Response []*NlpClassificationResponse `json:"response"`
	Error    string                       `json:"error"`
}

func (instance *NlpClassificationOutput) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *NlpClassificationOutput) HasError() bool {
	if nil != instance {
		return len(instance.Error) > 0
	}
	return false
}

func (instance *NlpClassificationOutput) SortAsc() *NlpClassificationOutput {
	if nil != instance {
		sort.Slice(instance.Response, func(i, j int) bool {
			a := instance.Response[i]
			b := instance.Response[j]
			return gg.Compare.IsLower(a.Score, b.Score)
		})
	}
	return instance
}

func (instance *NlpClassificationOutput) SortDesc() *NlpClassificationOutput {
	if nil != instance {
		sort.Slice(instance.Response, func(i, j int) bool {
			a := instance.Response[i]
			b := instance.Response[j]
			return gg.Compare.IsGreater(a.Score, b.Score)
		})
	}
	return instance
}
