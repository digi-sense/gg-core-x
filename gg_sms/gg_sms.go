package gg_sms

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_sms/gg_sms_engine"
	"errors"
	"strings"
)

// ---------------------------------------------------------------------------------------------------------------------
// 	c o n s t
// ---------------------------------------------------------------------------------------------------------------------

var (
	MismatchConfigurationError = errors.New("mismatch_configuration")
	EngineNotEnabledError      = errors.New("engine_not_enabled")
	ProviderNotFoundError      = errors.New("provider_not_found")
)

type SMSHelper struct {
}

var SMS *SMSHelper

func init() {
	SMS = new(SMSHelper)
}

func (instance *SMSHelper) NewEngine(settings ...interface{}) *SMSEngine {
	response := new(SMSEngine)
	response.configuration = new(gg_sms_engine.SMSConfiguration)
	if len(settings) > 0 {
		p1 := settings[0]
		if conf, b := p1.(*gg_sms_engine.SMSConfiguration); b {
			response.configuration = conf
		} else if provider, b := p1.(*gg_sms_engine.SMSProvider); b {
			response.configuration.Providers[provider.Driver] = provider
		} else {
			response.tryLoadSettings(p1)
		}
	}

	return response
}

// ---------------------------------------------------------------------------------------------------------------------
// 	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

type SMSEngine struct {
	configuration *gg_sms_engine.SMSConfiguration
}

func (instance *SMSEngine) ProviderNames() []string {
	result := make([]string, 0)
	if nil != instance.configuration {
		for k, _ := range instance.configuration.Providers {
			result = append(result, k)
		}
	}
	return result
}

func (instance *SMSEngine) SendMessage(providerName, message, to, from string) (string, error) {
	if nil != instance && nil != instance.configuration {
		if instance.configuration.Enabled {
			provider, err := instance.getDriver(providerName)
			if nil != err {
				return "", err
			}
			return provider.Send(message, to, from)
		}
		return "", EngineNotEnabledError
	}
	return "", MismatchConfigurationError
}

// ---------------------------------------------------------------------------------------------------------------------
// 	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *SMSEngine) tryLoadSettings(item interface{}) {
	text := gg.JSON.Stringify(item)
	if len(text) > 0 {
		// test settings
		var config gg_sms_engine.SMSConfiguration
		err := gg.JSON.Read(text, &config)
		if nil == err && len(config.Providers) > 0 {
			instance.configuration = &config
			return
		}
		// test a provider
		var provider gg_sms_engine.SMSProvider
		err = gg.JSON.Read(text, &provider)
		if nil == err && len(provider.Driver) > 0 {
			instance.configuration.Providers[provider.Driver] = &provider
			return
		}
	}
}

func (instance *SMSEngine) getDriver(name string) (driver gg_sms_engine.IDriver, err error) {
	provider, err := instance.getProvider(name)
	if nil != err {
		return nil, err
	}

	switch strings.ToLower(provider.Driver) {
	case "skebby":
		driver = gg_sms_engine.NewDriverSkebby(instance.configuration.AutoShortUrl, provider)
	default:
		driver = gg_sms_engine.NewDriverGeneric(instance.configuration.AutoShortUrl, provider)
	}

	return driver, nil
}

func (instance *SMSEngine) getProvider(name string) (*gg_sms_engine.SMSProvider, error) {
	if len(name) > 0 {
		if _, b := instance.configuration.Providers[name]; !b {
			return nil, ProviderNotFoundError
		}
		provider := instance.configuration.Providers[name]
		return provider, nil
	} else {
		names := instance.ProviderNames()
		if len(names) > 0 {
			firstName := names[0]
			if len(firstName) > 0 {
				return instance.getProvider(firstName)
			}
		}
	}
	return nil, ProviderNotFoundError
}
