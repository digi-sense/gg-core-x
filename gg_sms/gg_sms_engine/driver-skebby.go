package gg_sms_engine

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_http/httpclient"
	"errors"
	"fmt"
	"strings"
)

// https://www.skebby.it/business/index/code-examples/

const (
	BASEURL                = "https://api.skebby.it/API/v1.0/REST/"
	TESTURL                = "https://httpbin.org/post"
	MESSAGE_HIGH_QUALITY   = "GP"
	MESSAGE_MEDIUM_QUALITY = "TI"
	MESSAGE_LOW_QUALITY    = "SI"
)

// ---------------------------------------------------------------------------------------------------------------------
// 	DriverSkebby
// ---------------------------------------------------------------------------------------------------------------------

type DriverSkebby struct {
	shortURL bool
	settings *SMSProvider
}

func NewDriverSkebby(shortURl bool, settings *SMSProvider) *DriverSkebby {
	instance := new(DriverSkebby)
	instance.shortURL = shortURl
	instance.settings = settings

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
// 	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DriverSkebby) Send(message, to, from string) (string, error) {
	if instance.shortURL {
		message = ShortUrlsInMessage(message)
	}

	username := gg.Reflect.GetString(instance.settings.Params, "username")
	password := gg.Reflect.GetString(instance.settings.Params, "password")
	auth, err := instance.login(username, password)
	if nil != err {
		return "", err
	}

	userKey := gg.Reflect.GetString(auth, "user_key")
	sessionKey := gg.Reflect.GetString(auth, "session_key")
	quality := gg.Reflect.GetString(instance.settings.Params, "quality")
	if len(quality) == 0 {
		quality = MESSAGE_LOW_QUALITY
	}

	var response string
	response, err = instance.send(userKey, sessionKey, message, to, from, quality)

	return response, err
}

// ---------------------------------------------------------------------------------------------------------------------
// 	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DriverSkebby) login(username, password string) (map[string]interface{}, error) {
	address := fmt.Sprintf("%vlogin?username=%v&password=%v", BASEURL, username, password)
	client := httpclient.NewHttpClient()
	response, err := client.Get(address)
	if nil != err {
		return nil, err
	}
	if response.StatusCode == 200 {
		text := string(response.Body)
		tokens := strings.Split(text, ";")
		if len(tokens) == 2 {
			m := map[string]interface{}{
				"user_key":    tokens[0],
				"session_key": tokens[1],
			}
			return m, nil
		} else {
			return nil, errors.New(fmt.Sprintf("unespected response: %v", text))
		}
	} else {
		return nil, errors.New(fmt.Sprintf("error: %v - %v", response.StatusCode, string(response.Body)))
	}
}

func (instance *DriverSkebby) send(userKey, sessionKey, message, to, from, quality string) (string, error) {
	recipient := []string{CheckPrefix(ClearPhone(to))}
	body := map[string]interface{}{}
	body["returnCredits"] = true
	body["returnCredits"] = true
	body["message"] = message //url.QueryEscape(message)
	body["message_type"] = quality
	body["recipient"] = recipient
	if len(from) > 0 && quality!=MESSAGE_LOW_QUALITY{
		body["sender"] = from //url.QueryEscape(from)
	}
	sBody := gg.JSON.Stringify(body)

	// address := TESTURL
	address := fmt.Sprintf("%vsms", BASEURL)
	client := httpclient.NewHttpClient()
	client.AddHeader("Content-Type", "application/json")
	client.AddHeader("user_key", userKey)
	client.AddHeader("Session_key", sessionKey)
	response, err := client.Post(address, sBody)
	if nil != err {
		return "", err
	}
	if response.StatusCode >201 {
		return "", errors.New(fmt.Sprintf("error: %v - %v", response.StatusCode, string(response.Body)))
	}
	return string(response.Body), nil
}
