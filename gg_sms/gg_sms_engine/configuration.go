package gg_sms_engine

import (
	"bitbucket.org/digi-sense/gg-core"
)

// ---------------------------------------------------------------------------------------------------------------------
// 	SMSProvider
// ---------------------------------------------------------------------------------------------------------------------

type SMSProvider struct {
	Driver   string            `json:"driver"`
	Method   string            `json:"method"`
	Endpoint string            `json:"endpoint"`
	Params   map[string]string `json:"params"`
	Headers  map[string]string `json:"headers"`
}

func (instance *SMSProvider) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *SMSProvider) Param(name string) string {
	if nil != instance.Params {
		if v, b := instance.Params[name]; b {
			return v
		}
	}
	return ""
}

func (instance *SMSProvider) Header(name string) string {
	if nil != instance.Headers {
		if v, b := instance.Headers[name]; b {
			return v
		}
	}
	return ""
}

// ---------------------------------------------------------------------------------------------------------------------
// 	SMSConfiguration
// ---------------------------------------------------------------------------------------------------------------------

type SMSConfiguration struct {
	Enabled      bool                    `json:"enabled"`
	AutoShortUrl bool                    `json:"auto-short-url"`
	Providers    map[string]*SMSProvider `json:"providers"`
}

func NewSMSConfigurationFromFile(filename string) (*SMSConfiguration, error) {
	text, err := gg.IO.ReadTextFromFile(filename)
	if nil != err {
		return nil, err
	}
	return NewSMSConfigurationFromString(text)
}

func NewSMSConfigurationFromMap(m map[string]interface{}) (*SMSConfiguration, error) {
	text := gg.JSON.Stringify(m)
	return NewSMSConfigurationFromString(text)
}

func NewSMSConfigurationFromString(text string) (*SMSConfiguration, error) {
	var instance SMSConfiguration
	err := gg.JSON.Read(text, &instance)
	if nil != err {
		return nil, err
	}
	return &instance, nil
}

func (instance *SMSConfiguration) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *SMSConfiguration) ProviderNames() []string {
	result := make([]string, 0)
	if nil != instance.Providers {
		for k, _ := range instance.Providers {
			result = append(result, k)
		}
	}
	return result
}

func (instance *SMSConfiguration) Provider(name ...string) *SMSProvider {
	if len(instance.Providers) > 0 {
		n := gg.Arrays.GetAt(name, 0, "").(string)
		if len(n) > 0 {
			if v, b := instance.Providers[n]; b {
				return v
			}
		} else {
			names := instance.ProviderNames()
			if len(names) > 0 {
				return instance.Provider(names[0])
			}
		}
	}
	return &SMSProvider{
		Driver:   "",
		Method:   "",
		Endpoint: "",
		Params:   nil,
		Headers:  nil,
	}
}

// ---------------------------------------------------------------------------------------------------------------------
// 	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------
