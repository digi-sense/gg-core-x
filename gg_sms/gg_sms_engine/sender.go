package gg_sms_engine

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_http/httpclient"
	"bitbucket.org/digi-sense/gg-core-x/gg_http/httputils"
	"errors"
	"net/url"
	"strings"
)

type IDriver interface {
	Send(message, to, from string) (string, error)
}

// ---------------------------------------------------------------------------------------------------------------------
// 	t y p e
// ---------------------------------------------------------------------------------------------------------------------

type SMSSender struct {
	autoShortUrl bool
	provider     *SMSProvider
}

func NewSMSSender(autoShortUrl bool, provider *SMSProvider) *SMSSender {
	instance := new(SMSSender)
	instance.autoShortUrl = autoShortUrl
	instance.provider = provider

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
// 	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *SMSSender) Send(message, to, from string) error {
	if instance.autoShortUrl {
		message = ShortUrlsInMessage(message)
	}
	client := new(httpclient.HttpClient)

	// headers
	if len(instance.provider.Headers) > 0 {
		for k, v := range instance.provider.Headers {
			client.AddHeader(k, v)
		}
	}

	body := instance.provider.Params
	body["message"] = url.QueryEscape(message)
	body["to"] = strings.ReplaceAll(ClearPhone(to), "+", "")
	if len(from) > 0 {
		body["from"] = url.QueryEscape(from)
	}

	uri := Render(instance.provider.Endpoint, body)

	var response *httputils.ResponseData
	var err error

	switch strings.ToLower(instance.provider.Method) {
	case "get":
		response, err = client.Get(uri)
	case "post":
		response, err = client.Post(uri, body)
	}

	if nil != err {
		return err
	}
	if nil != response && response.StatusCode >= 400 {
		return errors.New(string(response.Body))
	}

	return nil
}

// ---------------------------------------------------------------------------------------------------------------------
// 	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------
