package gg_sms_engine

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_http/httpclient"
	"bitbucket.org/digi-sense/gg-core-x/gg_http/httputils"
	"errors"
	"net/url"
	"strings"
)

// ---------------------------------------------------------------------------------------------------------------------
// 	DriverGeneric
// ---------------------------------------------------------------------------------------------------------------------

type DriverGeneric struct {
	shortURL bool
	settings *SMSProvider
}

func NewDriverGeneric(shortURl bool, settings *SMSProvider) *DriverGeneric {
	instance := new(DriverGeneric)
	instance.shortURL = shortURl
	instance.settings = settings

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
// 	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DriverGeneric) Send(message, to, from string) (string, error) {
	if instance.shortURL {
		message = ShortUrlsInMessage(message)
	}

	client := new(httpclient.HttpClient)

	// headers
	if len(instance.settings.Headers) > 0 {
		for k, v := range instance.settings.Headers {
			client.AddHeader(k, v)
		}
	}

	body := instance.settings.Params
	body["message"] = url.QueryEscape(message)
	body["to"] = strings.ReplaceAll(ClearPhone(to), "+", "")
	if len(from) > 0 {
		body["from"] = url.QueryEscape(from)
	}

	uri := Render(instance.settings.Endpoint, body)

	var response *httputils.ResponseData
	var err error

	switch strings.ToLower(instance.settings.Method) {
	case "get":
		response, err = client.Get(uri)
	case "post":
		response, err = client.Post(uri, body)
	}

	if nil != err {
		return "", err
	}
	if nil != response && response.StatusCode >= 400 {
		return "", errors.New(string(response.Body))
	}

	return string(response.Body), nil
}
