package gg_chrome

import (
	"context"
	"fmt"
	"github.com/chromedp/cdproto/page"
	"github.com/chromedp/chromedp"
)

var Chrome *ChromeHelper

func init() {
	Chrome = new(ChromeHelper)
}

type ChromeHelper struct {
}

func NewChromePuppet(args ...interface{}) (instance *ChromeHelper, err error) {
	instance = new(ChromeHelper)
	err = instance.init(args...)

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *ChromeHelper) Test() (err error) {
	if nil != instance {
		// create context
		ctx, cancel := chromedp.NewContext(context.Background())
		defer cancel()

		// run task list
		err = chromedp.Run(ctx,
			chromedp.Navigate("https://www.google.com"),
		)
	}
	return
}

func (instance *ChromeHelper) FullScreenshot(url string) (response []byte, err error) {
	// create context
	ctx, cancel := chromedp.NewContext(
		context.Background(),
		// chromedp.WithDebugf(log.Printf),
	)
	defer cancel()

	err = chromedp.Run(ctx, fullScreenshot(url, 90, &response))
	if nil != err {
		return
	}

	return
}

func (instance *ChromeHelper) Screenshot(url string) (response []byte, err error) {
	// create context
	ctx, cancel := chromedp.NewContext(
		context.Background(),
		// chromedp.WithDebugf(log.Printf),
	)
	defer cancel()

	err = chromedp.Run(ctx, screenshot(url, &response))
	if nil != err {
		return
	}

	return
}

func (instance *ChromeHelper) ElementScreenshot(url string, sel string) (response []byte, err error) {
	// create context
	ctx, cancel := chromedp.NewContext(
		context.Background(),
		// chromedp.WithDebugf(log.Printf),
	)
	defer cancel()

	err = chromedp.Run(ctx, elementScreenshot(url, sel, &response))
	if nil != err {
		return
	}

	return
}

func (instance *ChromeHelper) ToPDF(url string) (response []byte, err error) {
	// create context
	ctx, cancel := chromedp.NewContext(
		context.Background(),
		// chromedp.WithDebugf(log.Printf),
	)
	defer cancel()

	err = chromedp.Run(ctx, printToPDF(url, &response))
	if nil != err {
		return
	}

	return
}

func (instance *ChromeHelper) Eval(url string, script string) (response interface{}, err error) {
	// create context
	ctx, cancel := chromedp.NewContext(
		context.Background(),
		// chromedp.WithDebugf(log.Printf),
	)
	defer cancel()

	err = chromedp.Run(ctx,
		chromedp.Navigate(url),
		// chromedp.Sleep(time.Second*1),
		chromedp.Evaluate(script, &response),
	)

	return
}

func (instance *ChromeHelper) GetWindowVariable(url string, variable string) (response interface{}, err error) {
	// create context
	ctx, cancel := chromedp.NewContext(
		context.Background(),
		// chromedp.WithDebugf(log.Printf),
	)
	defer cancel()

	script := fmt.Sprintf("window.hasOwnProperty('%s')?window['%s']:null", variable, variable)
	err = chromedp.Run(ctx,
		chromedp.Navigate(url),
		// chromedp.Sleep(time.Second*1),
		chromedp.Evaluate(script, &response),
	)

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *ChromeHelper) init(args ...interface{}) (err error) {
	if nil != instance {
		err = instance.Test()
		if nil != err {
			return
		}

	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

// elementScreenshot takes a screenshot of a specific element.
func elementScreenshot(urlstr, sel string, res *[]byte) chromedp.Tasks {
	return chromedp.Tasks{
		chromedp.Navigate(urlstr),
		chromedp.Screenshot(sel, res, chromedp.NodeVisible),
	}
}

// fullScreenshot takes a screenshot of the entire browser viewport.
//
// Note: chromedp.FullScreenshot overrides the device's emulation settings. Use
// device.Reset to reset the emulation and viewport settings.
func fullScreenshot(urlstr string, quality int, res *[]byte) chromedp.Tasks {
	return chromedp.Tasks{
		chromedp.Navigate(urlstr),
		chromedp.FullScreenshot(res, quality),
	}
}

func screenshot(urlstr string, res *[]byte) chromedp.Tasks {
	return chromedp.Tasks{
		chromedp.Navigate(urlstr),
		chromedp.CaptureScreenshot(res),
	}
}

// print a specific pdf page.
func printToPDF(urlstr string, res *[]byte) chromedp.Tasks {
	return chromedp.Tasks{
		chromedp.Navigate(urlstr),
		chromedp.ActionFunc(func(ctx context.Context) error {
			buf, _, err := page.PrintToPDF().WithPrintBackground(false).Do(ctx)
			if err != nil {
				return err
			}
			*res = buf
			return nil
		}),
	}
}
