# Chrome

Utility based on [chromedp](https://github.com/chromedp/chromedp) package.

```
$ go get -u github.com/chromedp/chromedp
```

See here for examples: https://github.com/chromedp/examples

## Google Chrome Headless

On a Server without a GUI you have to use Google Chrome in headless mode.

https://askubuntu.com/questions/245041/how-do-i-install-chrome-on-a-server

To install Google Chrome:

**Step-1:** First, add (if not added already) the Google Chrome repository on your system using the
following command.
While using PPA to our system we also receive the latest updates whenever you check for system updates.

```
$ wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add - 
```

Now add the PPA url to the /etc/apt/sources.list.d/google.list file as per Ubuntu system architecture.

```
$ sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
```

**Step-2:**
Install or Upgrade Google Chrome

After adding Google Chrome repository in our system use following commands to install the latest
Google Chrome stable release. If you already have installed an older version,
It will upgrade currently installed version with recent stable version.

```
$ sudo apt-get update
$ sudo apt-get install google-chrome-stable
```

**Step-3:**
Verify version

```
$ google-chrome --version
```

## Optional "In Memory" renderer

Chrome should not need Xvfb

```
#install Xvfb
sudo apt-get install xvfb

#set display number to :99
Xvfb :99 -ac &
export DISPLAY=:99

#you are now having an X display by Xvfb
```

## Alternative package

https://github.com/go-rod/rod

Rod is similar to Chromedp and may be a good alternative