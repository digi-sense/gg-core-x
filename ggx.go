package ggx

import (
	_ "bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_chrome"
	"bitbucket.org/digi-sense/gg-core-x/gg_html"
	"bitbucket.org/digi-sense/gg-core-x/gg_http"
	"bitbucket.org/digi-sense/gg-core-x/gg_mq"
	"bitbucket.org/digi-sense/gg-core-x/gg_nlp"
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting"
	"bitbucket.org/digi-sense/gg-core-x/gg_sms"
	"bitbucket.org/digi-sense/gg-core-x/gg_templating"
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs"
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs_watchdog"
	_ "gorm.io/driver/sqlite"
	_ "gorm.io/gorm"
)

var VFS *gg_vfs.VFSHelper
var VFSWatchdog *gg_vfs_watchdog.VFSWatchdogHelper
var Http *gg_http.HttpHelper
var SMS *gg_sms.SMSHelper
var MQ *gg_mq.MQHelper
var HTML *gg_html.HTMLHelper
var Scripting *gg_scripting.ScriptingHelper
var NLP *gg_nlp.NLPHelper
var Chrome *gg_chrome.ChromeHelper
var Templating *gg_templating.TemplatingHelper

func init() {
	VFS = gg_vfs.VFS
	VFSWatchdog = gg_vfs_watchdog.VFSWatchdog
	Http = gg_http.Http
	SMS = gg_sms.SMS
	MQ = gg_mq.MQ
	HTML = gg_html.HTML
	Scripting = gg_scripting.Scripting
	NLP = gg_nlp.NLP
	Chrome = gg_chrome.Chrome
	Templating = gg_templating.Templating
}
